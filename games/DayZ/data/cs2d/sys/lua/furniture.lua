furniture = {}
furniture.category = {}
furniture.playerSelectedCategory = {}
furniture.selected = {}
furniture.ownerList = {}
furniture.entities = {}
furniture.baseImageDir = "gfx/furniture/"
furniture.influence = 5*32

misc.addServerTransfer("gfx/test_zone/marker96x96.png")

function furniture.new(name,category,cost,image,depth,canRotate)
	temp = {}
	temp.name = name
	temp.cost = cost
	misc.addServerTransfer(furniture.baseImageDir..image)
	temp.image = image
	temp.depth = depth
	temp.canRotate = canRotate
	categoryExists = false
	i = 0
	while (i < #furniture.category) do
		i = i + 1
		if furniture.category[i].name == category then
			furniture.category[i].item[#furniture.category[i].item+1] = temp
			categoryExists = true
			-- DEBUG -- print("added object: "..temp.name.." to "..category)
		end
	end
	if categoryExists == false then
		-- DEBUG -- print("added buy category: "..category)
		furniture.category[#furniture.category+1] = {}
		furniture.category[#furniture.category].name = category
		furniture.category[#furniture.category].item = {}
		furniture.category[#furniture.category].item[#furniture.category[#furniture.category].item+1] = temp
		-- DEBUG -- print("added object: "..temp.name.." to "..category)
	end
end

addhook("ms100","furniture.ms100")
function furniture.ms100()
	for v,k in pairs(furniture.selected) do
		reqcld2(v,2,furniture.moveMarker)
	end
end

function furniture.moveMarker(id,x,y)
	if not (furniture.selected[id] == nil) then
		x = misc.tile_to_pixel(misc.pixel_to_tile(x))
		y = misc.tile_to_pixel(misc.pixel_to_tile(y))
		furniture.selected[id].x = x
		furniture.selected[id].y = y
		tween_move(furniture.selected[id].marker_image,100,x,y)
		tween_move(furniture.selected[id].object_image,100,x,y)
	end
end

addhook("say","furniture.say")
function furniture.say(id,text)
	text = string.lower(text)
	if text == "!buy" then
		if not (furniture.selected[id] == nil) then
			freeimage(furniture.selected[id].marker_image)
			freeimage(furniture.selected[id].object_image)
			furniture.selected[id] = nil
		end
		sme.createMenu(id,furniture.categoryMenu,furniture.categoryProcessor,"Furniture",true,furniture.category)
		return 1
	end	
	if text == "!sell" then
		if not (furniture.selected[id] == nil) then
			freeimage(furniture.selected[id].marker_image)
			freeimage(furniture.selected[id].object_image)
			furniture.selected[id] = nil
		end
		furniture.ownerList[id] = {}
		i = 0
		while (i < #furniture.entities) do
			i = i + 1
			if furniture.entities[i].owner == id then
				furniture.ownerList[id][#furniture.ownerList[id]+1] = i
			end
		end
		sme.createMenu(id,furniture.sellMenu,furniture.sellProcess,"Sell Furniture",true,furniture.ownerList[id])
		return 1
	end
	if text == "!rotate" then
		furniture.attack(id)
		return 1
	end
	if text == "!place" then
		furniture.attack2(id)
		return 1
	end
	if text == "!cancel" then
		if not (furniture.selected[id] == nil) then
			freeimage(furniture.selected[id].marker_image)
			freeimage(furniture.selected[id].object_image)
			furniture.selected[id] = nil
			return 1
		end
	end
end

function furniture.sellProcess(id,data)
	return furniture.entities[data].name.."|sells for: "..furniture.entities[data].obj.cost
end

function furniture.sellMenu(id,item)
	local entity = furniture.entities[furniture.ownerList[id][item]]
	parse("setmoney "..id.." "..player(id,"money")+entity.obj.cost)
	freeimage(entity.image)
	furniture.entities[furniture.ownerList[id][item]] = furniture.entities[#furniture.entities]
	furniture.entities[#furniture.entities] = nil
end

addhook("attack","furniture.attack")
function furniture.attack(id)
	if not (furniture.selected[id] == nil) then
		if furniture.selected[id].obj.canRotate then
			furniture.selected[id].rot = furniture.selected[id].rot + 90
			tween_rotate(furniture.selected[id].marker_image,350,furniture.selected[id].rot)
			tween_rotate(furniture.selected[id].object_image,350,furniture.selected[id].rot)
		end
	end
end

addhook("attack2","furniture.attack2")
function furniture.attack2(id)
	if not (furniture.selected[id] == nil) then
		if furniture.furnitureNear(id,furniture.selected[id].x,furniture.selected[id].y) then
		obj = furniture.selected[id].obj
		local temp = {}
		temp.obj = obj
		temp.name = obj.name
		temp.owner = id
		temp.x = furniture.selected[id].x
		temp.y = furniture.selected[id].y
		temp.rot = furniture.selected[id].rot
		if not (misc.fileExists(furniture.baseImageDir..obj.image)) then
			msg(misc.rgb(255,0,0).."MISSING IMAGE: "..furniture.baseImageDir..obj.image)
		end
		temp.image = image(furniture.baseImageDir..obj.image,0,0,obj.depth)
		imagepos(temp.image,furniture.selected[id].x,furniture.selected[id].y,furniture.selected[id].rot)
		furniture.entities[#furniture.entities+1] = temp
		freeimage(furniture.selected[id].marker_image)
		freeimage(furniture.selected[id].object_image)
		furniture.selected[id] = nil
		end
	end
end

addhook("leave","furniture.leave")
function furniture.leave(id)
	if (furniture.selected[id] ~= nil) then
		freeimage(furniture.selected[id].marker_image)
		freeimage(furniture.selected[id].object_image)
		furniture.selected[id] = nil
	end
	i = 0
	while (i < #furniture.entities) do
		i = i + 1
		if furniture.entities[i].owner == id then
			freeimage(furniture.entities[i].image)
			furniture.entities[i] = furniture.entities[#furniture.entities]
			furniture.entities[#furniture.entities] = nil
		end
	end
end

function furniture.furnitureNear(id,x,y)
	i = 0
	while (i < #furniture.entities) do
		i = i + 1
		if not (furniture.entities[i].owner == id) then
			if (x < furniture.entities[i].x+furniture.influence) and (y < furniture.entities[i].y+furniture.influence) and (x > furniture.entities[i].x-furniture.influence) and (y > furniture.entities[i].y-furniture.influence) then
				msg2(id,"Cant building near furniture by "..player(furniture.entities[i].owner,"name"))
				return false
			end
		end
	end
	return true
end

function furniture.categoryProcessor(id,data)
	return data.name
end

function furniture.categoryMenu(id,category)
	furniture.playerSelectedCategory[id] = category
	sme.createMenu(id,furniture.itemMenu,furniture.itemProcessor,"Furniture",true,furniture.category[category].item)
end

function furniture.itemProcessor(id,data)
	if (player(id,"money") >= data.cost) then
		ret = data.name.."|cost: "..data.cost
	else
		ret = "("..data.name.."|cost: "..data.cost..")"
	end
	return ret
end

function furniture.itemMenu(id,item)
	obj = furniture.category[furniture.playerSelectedCategory[id]].item[item]
	parse("setmoney "..id.." "..player(id,"money")-obj.cost)
	furniture.selected[id] = {}
	furniture.selected[id].obj = obj
	furniture.selected[id].rot = 0
	furniture.selected[id].x = 0
	furniture.selected[id].y = 0
	furniture.selected[id].object_image = image(furniture.baseImageDir..obj.image,0,0,obj.depth,id)
	furniture.selected[id].marker_image = image("gfx/test_zone/marker96x96.png",0,0,3,id)
end

furniture.new("soffa","inside",2000,"soffa.png",0,true)
furniture.new("altar","inside",2000,"altar.png",0,true)
furniture.new("hev armor","Half life",16000,"armor.png",1,true)
furniture.new("bed","Bedroom",1000,"bed.png",0,true)
furniture.new("carpet","floor",2000,"carpet.png",0,true)
furniture.new("bench","outside",250,"bench.png",0,true)
furniture.new("bush","outside",500,"bush.png",1,true)
furniture.new("block","cs2d",3000,"block.png",1,true)
furniture.new("???","misc",5000,"boxen.png",1,true)
furniture.new("bear carpet","floor",16000,"brown_bear_fur.png",0,true)
furniture.new("banner","floor",8000,"carlin_banner.png",1,false)
furniture.new("chair","inside",1500,"chair.png",0,true)
furniture.new("chiney","misc",16000,"chimney.png",1,false)
furniture.new("crossed weapons","misc",10000,"crossed_weapons.png",1,false)
furniture.new("well","outside",16000,"draw_well.png",1,false)
furniture.new("vase","floor",500,"elven_vase.png",0,false)
furniture.new("food","misc",100,"food.png",1,true)
furniture.new("fake football","outside",250,"football.png",0,true)
furniture.new("fountain","outside",16000,"fountain.png",0,false)
furniture.new("golden goblet","misc",16000,"golden_goblet.png",1,false)
furniture.new("grave","outside",16000,"grave.png",0,true)
furniture.new("health","Half life",16000,"health.png",1,true)
furniture.new("hotel","signs",1000,"hotel.png",1,true)
furniture.new("knight statue","misc",12500,"knight_statue.png",1,false)
furniture.new("Mysterious Machine","misc",16000,"mysterious_machine.png",1,false)
furniture.new("Orcish Totem Pole","outside",16000,"orcish_totem_pole.png",1,false)
furniture.new("palm","outside",750,"palm.png",1,true)
furniture.new("piggy bank","misc",1000,"piggy_bank.png",1,false)
furniture.new("???","misc",5000,"pult.png",1,true)
furniture.new("red bed","Bedroom",16000,"red_bed.png",0,true)
furniture.new("Sacrificial Stone","misc",16000,"sacrificial_stone.png",0,true)
furniture.new("armory","signs",5000,"sign_(armory).png",0,true)
furniture.new("jewelry","signs",5000,"sign_(jewelry).png",0,true)
furniture.new("smith","signs",5000,"sign_(smith).png",0,true)
furniture.new("Fletcher","signs",5000,"sign_(fletcher).png",0,true)
furniture.new("soldier","cs2d",10000,"soldier.png",1,true)
furniture.new("Sorcerer Statue","misc",16000,"sorcerer_statue.png",1,false)
furniture.new("super armor","items",1500,"superarmor.png",0,true)
furniture.new("Treasure Chest","misc",16000,"treasure_chest.png",0,false)
furniture.new("trollface","misc",16000,"trollface.png",0,true)
furniture.new("wall lamp","outside",3000,"wall_lamp.png",1,false)
furniture.new("vortigaunt","cs2d",7500,"vortigaunt.png",1,true)
furniture.new("zombie","cs2d",5000,"zombie.png",1,true)
