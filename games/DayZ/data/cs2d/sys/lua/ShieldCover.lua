ShieldCover = {}
ShieldCover.gfx = "gfx/ShieldCover/"
ShieldCover.sound = "ShieldCover/beep.ogg"
misc.addServerTransfer("sfx/"..ShieldCover.sound)
ShieldCover.effectTimer = 250
ShieldCover.areaArray = const.dirs.eight

ShieldCover.emptyShield = ShieldCover.gfx.."shield_empty.png"
misc.addServerTransfer(ShieldCover.emptyShield)

-- config
parse("mp_building_price barricade 500")

-- players
ShieldCover.player = {}

-- ways of becoming protected
ShieldCover.protections = {} -- strength refers to index in ShieldCover.strength
function ShieldCover.addProtection(strength, check)
	ShieldCover.protections[#ShieldCover.protections+1] = {strength=strength,check=check}
end

ShieldCover.addProtection(3, function(x, y)
	local objId = objectat(x,y)
	if (objId > 0) then
		local objType = object(objId,"type")
		if (objType == 1) then
			return true
		end
	end
end)

ShieldCover.addProtection(2, function(x, y) return tile(x,y,"obstacle") end)

ShieldCover.addProtection(1, function(x, y)
	local typename = entity(x,y,"typename")
	if (typename ~= false) then
		if (typename == "Env_Object") then
			local t = entity(x,y,"int0")
			if (t == 0) or (t == 1) or (t == 3) or (t == 4) then -- palm/tree/sink/toilet
				return true
			end
		end
	end
end)

-- use natural ordering instead table.sort(ShieldCover.protections, function(a,b) return a.strength < b.strength end)

-- protection levels
ShieldCover.strength = {}
ShieldCover.strength[1] = {protection=25,gfx="shield_1_placeholder"}
ShieldCover.strength[2] = {protection=50,gfx="shield_2_placeholder"}
ShieldCover.strength[3] = {protection=75,gfx="shield_3_placeholder"}

-- gfx
for index,data in ipairs(ShieldCover.strength) do
	data.fullGfx = ShieldCover.gfx..data.gfx..".png"
	misc.addServerTransfer(data.fullGfx)
end

addhook("hit", "ShieldCover.hit")
function ShieldCover.hit(id, source, weapon, hpdmg, apdmg, rawdmg, objId)
	
	local sx = nil
	local sy = nil
	-- check player
	if (source > 0) and (source <= 32) then
		sx = player(source,"x")
		sy = player(source,"y")
	end
	-- any object
	--print("objId: "..tostring(objId))
	if (type(objId) ~= "table") and (objId > 0) then
		sx = object(objId,"x")
		sy = object(objId,"y")
	end
	-- perform
	local playerData = ShieldCover.player[id]
	if (#playerData > 0) and (sx ~= nil) and (sy ~= nil) then
		local x = player(id,"x")
		local y = player(id,"y")
		local tx = misc.pixel_to_tile(x)
		local ty = misc.pixel_to_tile(y)
		local dir = misc.point_direction(x, y, sx, sy)
		local px = misc.round(tx + misc.lengthdir_x(dir,1))
		local py = misc.round(ty + misc.lengthdir_y(dir,1))
		for _,protection in ipairs(playerData) do
			if (px == protection.x) and (py == protection.y) then
				local protectionLevel = ShieldCover.strength[protection.strength].protection
				if (math.random()*100 < protectionLevel) then
					parse("sv_sound2 "..id.." "..ShieldCover.sound)
					local img = image(ShieldCover.emptyShield, 0, 0, 200+id)
					imagealpha(img,0.75)
					tween_alpha(img,1000,0.1)
					timer2(1000,{img},function(img)
						freeimage(img)
					end)
					return 1
				end
			end
		end
	end
	return 0
end

--[[
addhook("attack","ShieldCover.attack")
function ShieldCover.attack(id)
	local weapon = player(id,"weapontype")
	local firerate = itemtype(weapon,"rate")
	local dispersion = itemtype(weapon,"dispersion")
	print("weapon: "..itemtype(weapon,"name").." firerate: "..firerate.." dispersion: "..dispersion)
end
]]--

addhook("build","ShieldCover.build")
function ShieldCover.build(id, type, x, y, mode)
	for i=0,5 do
		timer2(1500+(i*500),{x, y},function(x, y) -- makeshift since build hook doesent activate when built
			ShieldCover.checkPlayersNear(x, y)
		end)
	end
end

addhook("objectkill", "ShieldCover.objectkill")
function ShieldCover.objectkill(id, pid)
	local tilex = object(id,"tilex")
	local tiley = object(id,"tiley")
	timer2(1,{tilex, tiley},function(x, y)
		ShieldCover.checkPlayersNear(x, y)
	end)
end

addhook("endround","ShieldCover.endround")
function ShieldCover.endround(mode)
	for _,id in ipairs(player(0,"tableliving")) do
		ShieldCover.destroyPlayer(id)
	end
end

addhook("leave","ShieldCover.leave")
function ShieldCover.leave(id, reason)
	ShieldCover.destroyPlayer(id)
end

addhook("die","ShieldCover.die")
function ShieldCover.die(id, killer, weapon, x, y, killerObj)
	ShieldCover.destroyPlayer(id)
end

addhook("movetile","ShieldCover.movetile")
function ShieldCover.movetile(id, x, y)
	if (ShieldCover.player[id] == nil) then -- SPAWN HOOK bug
		ShieldCover.player[id] = {}
	end
	ShieldCover.check(id)
end

function ShieldCover.checkPlayersNear(checkx, checky)
	for _,id in ipairs(player(0,"tableliving")) do
		local tilex = player(id,"tilex")
		local tiley = player(id,"tiley")
		for _,dir in ipairs(ShieldCover.areaArray) do
			local x = tilex + dir.x
			local y = tiley + dir.y
			if (x == checkx) and (y == checky) then
				ShieldCover.check(id)
				break
			end
		end
	end
end

function ShieldCover.check(id)
	local playerData = ShieldCover.player[id]
	-- get tile x/y
	local tilex = player(id,"tilex")
	local tiley = player(id,"tiley")
	-- check if protections are near player
	local i = 0
	while (i < #playerData) do
		i = i + 1
		local protection = playerData[i]
		local valid = false
		for _,dir in ipairs(ShieldCover.areaArray) do
			local x = tilex + dir.x
			local y = tiley + dir.y
			if (x == protection.x) and (y == protection.y) then
				valid = true
				break
			end
		end
		if (valid == false) then
			ShieldCover.destroy(protection)
			table.remove(playerData, i)
			i = i - 1
		end
	end
	-- check current player protections if they are still protective
	local i = 0
	while (i < #playerData) do
		i = i + 1
		local protection = playerData[i]
		local shield = ShieldCover.checkTile(protection.x, protection.y)
		if (shield ~= protection.strength) then
			ShieldCover.destroy(protection)
			table.remove(playerData, i)
			i = i - 1
		end
	end
	-- check for new protections
	for _,dir in ipairs(ShieldCover.areaArray) do
		local tx = tilex + dir.x
		local ty = tiley + dir.y
		-- check for already protected
		local hasProtection = false
		for _,protection in ipairs(playerData) do
			if (protection.x == tx) and (protection.y == ty) then
				hasProtection = true
			end
		end
		if (hasProtection == false) then
			-- add protection
			local shieldStrength = ShieldCover.checkTile(tx, ty)
			if (shieldStrength > 0) then
				local strength = ShieldCover.strength[shieldStrength]
				local x = misc.tile_to_pixel(tx)
				local y = misc.tile_to_pixel(ty)
				local img = image(strength.fullGfx,x,y,3)
				imagescale(img,0,0)
				tween_scale(img,ShieldCover.effectTimer,0.75,0.75)
				imagealpha(img,0.75)
				playerData[#playerData+1] = {x=tx, y=ty, strength=shieldStrength, image=img}
			end
		end
	end
end

function ShieldCover.checkTile(tilex, tiley)
	for _, protection in ipairs(ShieldCover.protections) do
		if protection.check(tilex, tiley) == true then
			return protection.strength
		end
	end
	return 0
end

function ShieldCover.destroyPlayer(id)
	if (ShieldCover.player[id] ~= nil) then
		for _,protection in ipairs(ShieldCover.player[id]) do
			ShieldCover.destroy(protection)
		end
		ShieldCover.player[id] = {}
	end
end

function ShieldCover.destroy(protection)
	tween_scale(protection.image,ShieldCover.effectTimer,0,0)
	tween_alpha(protection.image,ShieldCover.effectTimer,0)
	timer2(ShieldCover.effectTimer,{protection.image},function(image)
		freeimage(image)
	end)
end

