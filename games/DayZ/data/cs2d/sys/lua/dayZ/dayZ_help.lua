dayZhelp = {}
dayZhelp.help_image = {help_1={},help_2={}}
dayZhelp.justJoined = {}

addhook("join","dayZhelp.join")
function dayZhelp.join(id)
	dayZhelp.justJoined[id] = true
end

addhook("spawn","dayZhelp.spawn")
function dayZhelp.spawn(id)
	if dayZhelp.justJoined[id] == true then
		dayZhelp.justJoined[id] = false
		dayZhelp.help_image.help_1[id] = image("gfx/dayZ/help_1.png",320,240-32,2,id)
		dayZhelp.help_image.help_2[id] = image("gfx/dayZ/help_2.png",320,240+32,2,id)
		timer(5000, "dayZhelp.startFade",dayZhelp.help_image.help_1[id])
		timer(5000, "dayZhelp.startFade",dayZhelp.help_image.help_2[id])
	end
end

function dayZhelp.startFade(id)
	tween_scale(id,5000,5,5)
	tween_alpha(id,5000,0)
	timer(5000, "freeimage",id)
end