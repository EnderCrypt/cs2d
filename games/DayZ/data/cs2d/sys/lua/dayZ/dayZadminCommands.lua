--addhook("say","dayZ.commandsSay")
function dayZ.commandsSay(id,text)
	--dayZ.playerSpeak(id,text)
	command = misc.commandSplit(text)--MADmin.split_text(text)
	if command[1] == "!nav" then -- targets a coordinate, using arrow
		local x = tonumber(command[2])
		local y = tonumber(command[3])
		if is_valid(x) and is_valid(y) then
			i = 1
			while ((dayZ.navArrow[i] == nil) == false) do
				i = i + 1
			end
			dayZ.navArrow[i] = image("gfx/hud_arrow.bmp",player(id,"x"),player(id,"y"),1,id)
			local dir = point_direction(player(id,"tilex"),player(id,"tiley"),x,y)
			imagepos(dayZ.navArrow[i],player(id,"x"),player(id,"y"),dir)
			tween_scale(dayZ.navArrow[i],2500,1,5)
			imagecolor(dayZ.navArrow[i],0,0,255)
			timer(3000,"dayZ.fadeNavArrow",tostring(i))
			timer(5000,"dayZ.removeNavArrow",tostring(i))
		end
		return 1
	end
	if command[1] == "!loc" then -- shows everyone your location (text)
		msg(player(id,"name").." is located at "..player(id,"tilex")..", "..player(id,"tiley"))
	end
	if command[1] == "!shout" then -- shows everyone your location (arrow)
		if dayZ.readyToShout[id] then
			dayZ.readyToShout[id] = false
			parse("sv_sound scream.ogg")
			local pl = player(0,"tableliving")
			x = player(id,"tilex")
			y = player(id,"tiley")
			local i = 0
			while (i < #pl) do
				i = i + 1
				if (pl[i] == id) == false then
					local ii = 1
					while ((dayZ.navArrow[ii] == nil) == false) do
						ii = ii + 1
					end
					dayZ.navArrow[ii] = image("gfx/hud_arrow.bmp",player(pl[i],"x"),player(pl[i],"y"),1,pl[i])
					local dir = point_direction(player(pl[i],"tilex"),player(pl[i],"tiley"),x,y)
					imagepos(dayZ.navArrow[ii],player(pl[i],"x"),player(pl[i],"y"),dir)
					tween_scale(dayZ.navArrow[ii],2500,1,5)
					imagecolor(dayZ.navArrow[ii],0,0,255)
					timer(3000,"dayZ.fadeNavArrow",tostring(ii))
					timer(5000,"dayZ.removeNavArrow",tostring(ii))
				end
			end
			msg("�000999999"..player(id,"name").." shouted his location!")
			timer(10000,"dayZ.readyShout",tostring(id))
		else
			msg2(id,"please wait atleast 10 seconds")
			return 1
		end
	end
	if command[1] == "!travel" then -- targets a coordinate, teleports
		local tx = tonumber(command[2])
		local ty = tonumber(command[3])
		if is_valid(tx) and is_valid(ty) then
			if (tile(tx,ty,"frame") > 0) and (tile(tx,ty,"walkable")) then
				local consumeID = nil
				if (dayZ.item_count(id,1012) > 0) then
					consumeID = 1012
				end
				if (dayZ.item_count(id,1037) > 0) then
					consumeID = 1037
				end
				if (consumeID ~= nil) then
					dayZ.use_item(id,consumeID)
					dayZ.remove_item(id,consumeID,1)
					recalculate_itemAmount(id)
					msg3(id,player(id,"name").." fast traveled to some place")
					msg2(id,"you fast travelled to "..tx..", "..ty)
					local x = misc.tile_to_pixel(tx)
					local y = misc.tile_to_pixel(ty)
					cs2d.setpos.call(id,x,y)
				else
					msg2(id,"Cant fast travel (no gasoline/diesel available)")
				end
			else
				msg2(id,"Cant fast travel (not valid destination)")
			end
		else
			msg2(id,"command must be !travel x y")
		end
		return 1
	end
	if command[1] == "!leave" then -- leave life (and save)
		dayZ.playerLeftValid = true
		dayZsave.leave(id, "")
		dayZ.leaveGame(id)
		--parse("strip "..id.." 0") -- need gto be removed?
		parse("customkill 0 \"\left >> \" "..id)
		return 1
	end
	if command[1] == "!dropall" then -- drops all inventory items
		local worker = LuaWorker.new(function(worker, workspace)
			local id = workspace.id
			local inventory = dayZ.inventory[id]
			local item = inventory[#inventory]
			if (item == nil) then
				msg2(id, "You finished dropping of all items!")
				worker:delete()
			else
				dayZ.spawn_item(item.id,player(id,"tilex"),player(id,"tiley"))
				dayZ.remove_item(id,item.id,1)
			end
		end)
		worker.workspace.id = id
		worker:setFrequency(LuaWorkerData.always)
		worker:start()
		return 1
	end
	if command[1] == "!usemoney" then -- drops all inventory items
		local moneyItems = {66,67,68}
		local i = 0
		while (i < #dayZ.inventory[id]) do
			i = i + 1
			local item = dayZ.inventory[id][i]
			for _,itemID in ipairs(moneyItems) do
				if (item.id == itemID) then
					parse("equip "..id.." "..item.id)
				end
			end
		end
		return 1
	end
	if MADmin.is_admin(id) then
		if command[1] == "!help" then -- HELP
		msg2(id,"() means you must input a value")
		msg2(id,"[] optional value")
		msg2(id,"!kick (number: id)")
		msg2(id,"!ban (number: id)")
		msg2(id,"!reload [number: time]")
		msg2(id,"!z saveitems (text: filename)")
		msg2(id,"!z loaditems (text: filename)")
		msg2(id,"!z pvp (number: pvp factor (0 or more))")
		msg2(id,"!z spawnrate (number: spawn chance (0 to 10%))")
		msg2(id,"!z loot (number: loot chance (0 to 100%))")
		msg2(id,"!z items (shows all avaiable item)")
		msg2(id,"!z items count (the amount of custom items on ground)")
		msg2(id,"!z give (number: pid) (number: iid) (number: amount)")
		msg2(id,"!z remove (number: pid) (number: iid) (number: amount)")
		msg2(id,"!z spawnitem (number: iid) (number: tileX) (number: tileY)")
		msg2(id,"!tp (number: pid1) (number: pid2)")
		msg2(id,"!sp tresh (number: pid1) speed limit")
		msg2(id,"!sp set (number: pixel) where to set player if moving to fast")
		msg2(id,"!sp kick (number: kick) when to kick (piels overflow)")
		
		end
		if command[1] == "!tp" then -- RELOAD
			local pl1 = tonumber(command[2])
			local pl2 = tonumber(command[3])
			if is_valid(pl1) and is_valid(pl2) then
				if (player(pl1,"health") > 0) and (player(pl2,"health") > 0) then
					parse("setpos "..(pl1).." "..player(pl2,"x").." "..player(pl2,"y"))
				else
					msg2(id,"�999000000WARNING, invalid USERS")
				end
			else
				msg2(id,"�999000000WARNING, invalid INPUT")
			end
			return 1
		end
		if command[1] == "!status" then -- STATUS
			local pid = tonumber(command[2])
			if is_valid(pid) then
				if player(pid,"exists") == true then
					local usgn = player(pid,"usgn")
					if usgn == MADmin.MasterUsgn then
						msg("�000999000\""..player(pid,"name").."\" was VERIFIED AS MASTER ADMINISTRATOR!")
						return 0
					end
					local i = 0
					while (i < #MADmin.List) do
						i = i + 1
						if MADmin.List[i].usgn == usgn then
							msg("�000999000\""..player(pid,"name").."\" was VERIFIED AS ADMINISTRATOR!")
							return 0
						end
					end
					if usgn == 0 then
						msg("�999000000\""..player(pid,"name").."\" doesent have any U.S.G.N")
					else
						msg("�999000000\""..player(pid,"name").."\" doesent have any admin STATUS")
					end
				else
					msg2(id,"�999000000WARNING, invalid INPUT")
				end
			else
				local usgn = player(id,"usgn")
				if usgn == MADmin.MasterUsgn then
					msg("�000999000\""..player(id,"name").."\" was VERIFIED AS MASTER ADMINISTRATOR!")
					return 0
				end
				local i = 0
				while (i < #MADmin.List) do
					i = i + 1
					if MADmin.List[i].usgn == usgn then
						msg("�000999000\""..player(id,"name").."\" was VERIFIED AS ADMINISTRATOR!")
						return 0
					end
				end
				if usgn == 0 then
					msg("�999000000\""..player(id,"name").."\" doesent have any U.S.G.N")
				else
					msg("�999000000\""..player(id,"name").."\" doesent have any admin STATUS")
				end
			end
			return 1
		end
		if command[1] == "!kick" then -- KICK
			local aid = tonumber(command[2])
			if is_valid(aid) then
				if is_admin(aid) then
					msg2(id,"cant KICK admins")
				else
					parse("kick "..aid)
				end
			else
				msg2(id,"�999000000WARNING, invalid INPUT")
			end
			return 1
		end
		if command[1] == "!ban" then -- BAN
			local aid = tonumber(command[2])
			if is_valid(aid) then
				if is_admin(aid) then
					msg2(id,"cant BAN admins")
				else
					if player(aid,"usgn") > 0 then
						parse("banusgn "..aid.."")
					else
						parse("banip "..player(aid,"ip").."")
					end
				end
			else
				msg2(id,"�999000000WARNING, invalid INPUT")
			end
			return 1
		end
		if command[1] == "!reload" then -- RELOAD [time(in seconds)]
			local reload_time = tonumber(command[2])
			if is_valid(reload_time) then
				reload_time = reload_time
			else
				reload_time = 3
			end
			timer(reload_time*1000,"reload_map")
			msg("SERVER RELOAD IN "..reload_time.." SECONDS!")
			return 1
		end
		if command[1] == "!z" then -- DAYZ
			if command[2] == "saveitems" then -- saves all items on ground (third argument is save file)
				local file = io.open("sys/lua/dayZ/itemSave/"..command[3],"w")
				for locationIndex, itemStack in pairs(dayZ.items_onground) do
					for stackIndex, item in ipairs(itemStack) do
						file:write(locationIndex.." "..(item.id+1000).."\n")
					end
				end
				local allItems = item(0,"table")
				for index, itemValue in ipairs(allItems) do
					local itemX = item(itemValue,"x")
					local itemY = item(itemValue,"y")
					local itemType = item(itemValue,"type")
					file:write(itemX.."|"..itemY.." "..itemType.."\n")
				end
				file:close()
			end
			if command[2] == "loaditems" then -- load all items to the ground (this will not remove previous items)
				local file = io.open("sys/lua/dayZ/itemSave/"..command[3],"r")
				local line = file:read()
				while (line ~= nil) do
					local data = misc.customSplit(line,{"|", " "})
					local x = tonumber(data[1])
					local y = tonumber(data[2])
					local itemId = tonumber(data[3])
					dayZ.spawn_item(itemId,x,y)
					line = file:read()
				end
				file:close()
			end
			if command[2] == "pvp" then -- PVP
				local factor = tonumber(command[3])
				if is_valid(factor) then
					if factor > 0 then
						dayZ.pvp_damageFactor = factor
						msg("�999000000(PVP) Damage Factor set to "..factor)
					else
						dayZ.pvp_damageFactor = 0
						msg("�000999000(PVP is now OFF)")
					end
				else
					if command[3] == nil then
						msg("�000999000(PVP) factor currently set to "..dayZ.pvp_damageFactor)
					else
						msg2(id,"�999000000WARNING, invalid INPUT")
					end
				end
				return 1
			end
			if command[2] == "spawnrate" then -- spawnrate
				local factor = tonumber(command[3])
				if is_valid(factor) then
					if factor >= 0 and factor <= 10 then
						dayZ.ZombieSpawnChance = factor
						msg("�000999000(ZOMBIE'S) spawn chance set to "..factor.."%")
					end
				else
					if command[3] == nil then
						msg("�000999000(ZOMBIE'S) spawn chance currently set to "..dayZ.ZombieSpawnChance.."%")
					else
						msg2(id,"�999000000WARNING, invalid INPUT")
					end
				end
				return 1
			end
			if command[2] == "loot" then -- lootchance
				local factor = tonumber(command[3])
				if is_valid(factor) then
					if factor > 0 and factor <= 100 then
						dayZ.spawnLootChance = factor
						msg("�000999000(loot) spawn chance set to "..factor.."%")
					end
				else
					if command[3] == nil then
						pl = player(0,"tableliving")
						msg("�000999000(loot) spawn chance currently set to "..dayZ.spawnLootChance.."% ("..dayZ.spawnLootChance.."x"..#pl.."="..dayZ.spawnLootChance*#pl.."%)")
					else
						msg2(id,"�999000000WARNING, invalid INPUT")
					end
				end
				return 1
			end
			if command[2] == "items" then -- list item IDs
				if command[3] == "count" then
				msg("�000999000there is currently "..dayZ.CustomItemsCount.." custom items on map!")
				else
					msg2(id,"----- item IDs -----")
					local i = 0
					while (i < #dayZ.items) do
						i = i + 1
						msg2(id,"ID: "..(i+1000).." | name: "..dayZ.items[i].name)
					end
					msg2(id,"----- END -----")
				end
				return 1
			end
			if command[2] == "give" then -- give to player -- pid = player id
				local pid = tonumber(command[3])
				local iid = tonumber(command[4])
				local amount = tonumber(command[5])
				if is_valid(pid) and is_valid(iid) and is_valid(amount) then
					if iid > dayZ.reservedItemsId then
						msg(player(pid,"name").." was given "..amount.." "..dayZ.items[iid-dayZ.reservedItemsId].name.." from (admin) "..player(id,"name"))
					else
						msg(player(pid,"name").." was given "..amount.." "..itemtype(iid,"name").." from (admin)"..player(id,"name"))
					end
					dayZ.add_item(pid,iid,amount)
				else
					msg2(id,"�999000000WARNING, invalid INPUT")
				end
				return 1
			end
			if command[2] == "remove" then -- remove from player
				local pid = tonumber(command[3])
				local iid = tonumber(command[4])
				local amount = tonumber(command[5])
				if is_valid(pid) and is_valid(iid) and is_valid(amount) then
					if iid > dayZ.reservedItemsId then
						msg2(pid,""..amount.." "..dayZ.items[iid-dayZ.reservedItemsId].name.." was removed from you, by (admin) "..player(id,"name"))
					else
						msg2(pid,""..amount.." "..itemtype(iid,"name").." was removed from you, by (admin)"..player(id,"name"))
					end
					dayZ.remove_item(pid,iid,amount)
				else
					msg2(id,"�999000000WARNING, invalid INPUT")
				end
				return 1
			end
			if command[2] == "spawnitem" then -- spawn on ground
				local iid = tonumber(command[3])
				local x = tonumber(command[4])
				local y = tonumber(command[5])
				if is_valid(iid) and is_valid(x) and is_valid(y) then
					if iid > dayZ.reservedItemsId then
						dayZ.item_spawn(iid-dayZ.reservedItemsId,x,y)
					else
						parse("spawnitem "..iid.." "..x.." "..y)
					end
				else
					msg2(id,"�999000000WARNING, invalid INPUT")
				end
				return 1
			end
		end
		if command[1] == "!sp" then -- SpeedPreventor
			if not(speedPreventor == nil) then
				if command[2] == "tresh" then
					local factor = tonumber(command[3])
					if is_valid(factor) then
						speedPreventor.threshold = factor
						msg("�999000000(SpeedPreventor) treshhold set to "..factor)
					else
						if command[3] == nil then
							msg("�000999000(SpeedPreventor) treshhold currently set to "..speedPreventor.threshold)
						else
							msg2(id,"�999000000WARNING, invalid INPUT")
						end
					end
					return 1
				end
				if command[2] == "set" then
					local factor = tonumber(command[3])
					if is_valid(factor) then
						speedPreventor.thresholdSet = factor
						msg("�999000000(SpeedPreventor) thresholdSet set to "..factor)
					else
						if command[3] == nil then
							msg("�000999000(SpeedPreventor) treshhold currently set to "..speedPreventor.thresholdSet)
						else
							msg2(id,"�999000000WARNING, invalid INPUT")
						end
					end
					return 1
				end
				if command[2] == "kick" then
					local factor = tonumber(command[3])
					if is_valid(factor) then
						speedPreventor.overflowKick = factor
						msg("�999000000(SpeedPreventor) kick set to "..factor)
					else
						if command[3] == nil then
							msg("�000999000(SpeedPreventor) kick currently set to "..speedPreventor.overflowKick)
						else
							msg2(id,"�999000000WARNING, invalid INPUT")
						end
					end
					return 1
				end
			end
		end
	else
		if text == "!help" then -- HELP
			msg2(id,"!leave (die whitout lossing your save)")
			msg2(id,"!loc (displays your location)")
			msg2(id,"!nav [x] [y] (shows arrow towards target)")
			msg2(id,"!leave (die whitout losing save)")
			msg2(id,"!dropall (drops all items in inventory)")
		end
	end
end

function reload_map()
	pl = player(0,"tableliving")
	local i = 0
	while (i < #pl) do
		i = i + 1
		dayZsave.leave(pl[i],"")
	end
	timer(1,"parse","map "..map("name"))
end

function is_valid(value)
	if value == nil then
		return false
	else
		return true
	end
end

function is_alive(id)
id = tonumber(id)
if id == nil then
		return false
	else
		if player(id,"health") > 0 then
			return true
		else
			return false
		end
	end
end