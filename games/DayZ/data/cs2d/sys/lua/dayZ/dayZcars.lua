dayZ.cars = {}
dayZ.cars.list = {}
dayZ.playerMountedOn = {}
dayZ.cars.images = {"gfx/dayZ/jeep.png"}
misc.addServerTransfer(dayZ.cars.images[1])
dayZ.cars.speedFacter = 3

function dayZ.cars.spawn(x,y)
	local temp = {}
	temp.x = x
	temp.y = y
	temp.lastx = x
	temp.lasty = y
	temp.speed = 0
	temp.turnMotion = 0
	temp.dir = math.random(0,360)
	temp.force = {x=0,y=0}
	temp.img = image(dayZ.cars.images[math.random(1,#dayZ.cars.images)],x,y,1)
	imagescale(temp.img,0,0)
	tween_scale(temp.img,1000,1,1)
	temp.mounted = 0
	dayZ.cars.list[#dayZ.cars.list+1] = temp
end

function dayZ.cars.remove(id)
	freeimage(dayZ.cars.list[id].img)
	dayZ.cars.list[id] = dayZ.cars.list[#dayZ.cars.list]
	dayZ.cars.list[#dayZ.cars.list] = nil
end

addhook("use","dayZ.cars.use")
function dayZ.cars.use(id,event,data,x,y)
	if dayZ.playerMountedOn[id] == nil then
		i = 0
		while (i < #dayZ.cars.list) do
			i = i + 1
			dist = point_distance(player(id,"x"),player(id,"y"),dayZ.cars.list[i].x,dayZ.cars.list[i].y)
			if (dist < 100) then
				dayZ.cars.list[i].mounted = id
				dayZ.playerMountedOn[id] = i
				parse("setpos "..id.." "..dayZ.cars.list[i].x.." "..dayZ.cars.list[i].y)
			end
		end
	else
		dayZ.cars.list[dayZ.playerMountedOn[id]].mounted = 0
		dayZ.playerMountedOn[id] = nil
	end
end

addhook("ms100","dayZ.cars.ms100")
function dayZ.cars.ms100()
	i = 0
	while (i < #dayZ.cars.list) do
		i = i + 1
		if dayZ.cars.list[i].mounted > 0 then
			local x = player(dayZ.cars.list[i].mounted,"x")
			local y = player(dayZ.cars.list[i].mounted,"y")
			local vector_x = x-math.floor(dayZ.cars.list[i].x)
			local vector_y = y-math.floor(dayZ.cars.list[i].y)
			if vector_x > 12 then
				vector_x = 12
			end
			if vector_x < -12 then
				vector_x = -12
			end
			if vector_y > 12 then
				vector_y = 12
			end
			if vector_y < -12 then
				vector_y = -12
			end
			print("forward: "..-vector_y)
			print("turning: "..-vector_x)
			-- turning, and smoothness
			dayZ.cars.list[i].turnMotion = dayZ.cars.list[i].turnMotion - ((vector_x/10)*math.min(vector_y,10))
			dayZ.cars.list[i].turnMotion = dayZ.cars.list[i].turnMotion * 0.5
			dayZ.cars.list[i].dir = dayZ.cars.list[i].dir + dayZ.cars.list[i].turnMotion
			-- accelerate
			dayZ.cars.list[i].speed = dayZ.cars.list[i].speed + (vector_y*dayZ.cars.speedFacter)
			dayZ.cars.checkHitPlayer(i)
		end
		dayZ.cars.list[i].lastx = dayZ.cars.list[i].x
		dayZ.cars.list[i].lasty = dayZ.cars.list[i].y
		dayZ.cars.list[i].x = dayZ.cars.list[i].x + lengthdir_x(dayZ.cars.list[i].dir-90,dayZ.cars.list[i].speed)
		dayZ.cars.list[i].y = dayZ.cars.list[i].y + lengthdir_y(dayZ.cars.list[i].dir-90,dayZ.cars.list[i].speed)
		dayZ.cars.list[i].speed = dayZ.cars.list[i].speed * 0.5
		-- dayZ.cars.list[i].x = dayZ.cars.list[i].x + dayZ.cars.list[i].force.x
		-- dayZ.cars.list[i].y = dayZ.cars.list[i].y + dayZ.cars.list[i].force.y
		-- dayZ.cars.list[i].force.x = dayZ.cars.list[i].force.x * 0.75
		-- dayZ.cars.list[i].force.y = dayZ.cars.list[i].force.y * 0.75
		-- compile
		tween_move(dayZ.cars.list[i].img,100,dayZ.cars.list[i].x,dayZ.cars.list[i].y,dayZ.cars.list[i].dir)
		parse("setpos "..dayZ.cars.list[i].mounted.." "..dayZ.cars.list[i].x.." "..dayZ.cars.list[i].y)
	end
end

function dayZ.cars.hitWall(carId,id,x,y)
	msg2(id,"�999000000you hit a wall!")
	local p_x = player(id,"x")
	local p_y = player(id,"y")
	local dir = point_direction(x,y,p_x,p_y)
	dayZ.cars.list[i].x = dayZ.cars.list[i].lastx
	dayZ.cars.list[i].y = dayZ.cars.list[i].lasty
	dayZ.cars.list[i].x = dayZ.cars.list[i].x - lengthdir_x(dayZ.cars.list[i].dir-90,dayZ.cars.list[i].speed*0.5)
	dayZ.cars.list[i].y = dayZ.cars.list[i].y - lengthdir_y(dayZ.cars.list[i].dir-90,dayZ.cars.list[i].speed*0.5)
	dayZ.cars.list[i].speed = 0
end

function dayZ.cars.checkHitPlayer(carId)
	pl = player(0,"tableliving")
	local i = 0
	while (i < #pl) do
		i = i + 1
		if (pl[i] == dayZ.cars.list[carId].mounted) == false then
		dist = point_distance(player(pl[i],"x"),player(pl[i],"y"),dayZ.cars.list[carId].x,dayZ.cars.list[carId].y)
		if (dist < 100) then -- check distance
			local dir = dayZ.cars.list[carId].dir+90
			local x1 = dayZ.cars.list[carId].x + lengthdir_x(dir,32)
			local y1 = dayZ.cars.list[carId].y + lengthdir_y(dir,32)
			local x2 = dayZ.cars.list[carId].x + lengthdir_x(dir,-32)
			local y2 = dayZ.cars.list[carId].y + lengthdir_y(dir,-32)
			local dist1 = point_distance(player(pl[i],"x"),player(pl[i],"y"),x1,y1)
			local dist2 = point_distance(player(pl[i],"x"),player(pl[i],"y"),x2,y2)
			if dist1 < 64 then
				dir = point_direction(x1,y1,player(pl[i],"x"),player(pl[i],"y"))
				local x = x1 + lengthdir_x(dir,64)
				local y = y1 + lengthdir_y(dir,64)
				local dmg = math.floor(point_distance(0,0,dayZ.cars.list[carId].force.x+lengthdir_x(dayZ.cars.list[carId].dir-90,dayZ.cars.list[carId].speed),dayZ.cars.list[carId].force.y+lengthdir_x(dayZ.cars.list[carId].dir-90,dayZ.cars.list[carId].speed)))
				if dmg > 0 then
					msg2(pl[i],"�999000000You got hit by "..player(dayZ.cars.list[carId].mounted,"name").." for "..dmg.." points of bleeding damage")
					dayZ.blood_loss[pl[i]] = dayZ.blood_loss[pl[i]] + dmg
					dayZ.update_blood(pl[i])
				end
			else
				if dist2 < 64 then
					dir = point_direction(x2,y2,player(pl[i],"x"),player(pl[i],"y"))
					local x = x2 + lengthdir_x(dir,64)
					local y = y2 + lengthdir_y(dir,64)
					local dmg = math.floor(point_distance(0,0,dayZ.cars.list[carId].force.x+lengthdir_x(dayZ.cars.list[carId].dir-90,dayZ.cars.list[carId].speed),dayZ.cars.list[carId].force.y+lengthdir_x(dayZ.cars.list[carId].dir-90,dayZ.cars.list[carId].speed)))
					msg2(pl[i],"�999000000You got hit by "..player(dayZ.cars.list[carId].mounted,"name").." for "..dmg.." points of bleeding damage")
					dayZ.blood_loss[pl[i]] = dayZ.blood_loss[pl[i]] + dmg
					dayZ.update_blood(pl[i])
				end
			end
		end
		end
	end
end