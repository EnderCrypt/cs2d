-- extra functions --

function round(number)
	return math.floor(number+0.5)
end

function pixel_to_tile(pixel)
	return round((pixel-16)/32)
end

function tile_to_pixel(tile)
	return (tile*32)+16
end

function point_direction(x1,y1,x2,y2)
	return -math.deg(math.atan2(x1-x2,y1-y2))
end

function point_distance(x1,y1,x2,y2)
	return math.sqrt((x1-x2)^2 + (y1-y2)^2)
end

function lengthdir_x(dir,length)
	return math.cos(math.rad(dir - 90))*length
end

function lengthdir_y(dir,length)
	return math.sin(math.rad(dir - 90))*length
end

-- start --

dayZ.Ai = {}
dayZ.Ai.aiList = {}
dayZ.Ai.anger_distance = 32*7 -- the distance it takes for a zombie to start following a player (pixel)
dayZ.Ai.speed = 12.5 -- movement speed of zombies (players have an averange speed of 12.5)
dayZ.Ai.despawn_timer = 16 -- default despawn timer
dayZ.Ai.debugMode = false
dayZ.Ai.lagg = {}
dayZ.Ai.lagg.search_delay = 5 -- the higher value, the longer will a zombie wait, after hitting a wall

function debug_print(msg)
	if dayZ.Ai.debugMode then
		print(msg)
	end
end

function dayZ.Ai.add_ai(xpos,ypos,hp)
	img_name = "zombie_ ("..math.random(1,6)..")"
	if (hp == nil) then
		dayZ.Ai.aiList[#dayZ.Ai.aiList+1] = {despawn = dayZ.Ai.despawn_timer,search = 0,x = xpos, y = ypos, hp = 150, img = image("gfx/dayZ/"..img_name..".png",xpos,ypos,1), target = 0,dir = math.random(-180,180)}
	else
		dayZ.Ai.aiList[#dayZ.Ai.aiList+1] = {despawn = dayZ.Ai.despawn_timer,search = 0,x = xpos, y = ypos, hp = hp, img = image("gfx/dayZ/"..img_name..".png",xpos,ypos,1), target = 0,dir = math.random(-180,180)}
	end
	parse("hudtxt 1 \"active zombies: "..#dayZ.Ai.aiList.."\" 2 116 0")
end

function dayZ.Ai.remove_ai(id)
	freeimage(dayZ.Ai.aiList[id].img)
	dayZ.Ai.aiList[id] = dayZ.Ai.aiList[#dayZ.Ai.aiList]
	dayZ.Ai.aiList[#dayZ.Ai.aiList] = nil
	parse("hudtxt 1 \"active zombies: "..#dayZ.Ai.aiList.."\" 2 116 0")
end

addhook("second","dayZ.Ai.second")
function dayZ.Ai.second()
	pl = player(0,"table")
	i = 0
	while (i < #dayZ.Ai.aiList) do
		i = i + 1
		if dayZ.Ai.aiList[i].target == 0 then
			if dayZ.Ai.aiList[i].search == 0 then
				temp_targets = {}
				ii = 0
				while (ii < #pl) do
					ii = ii + 1
					if (player(pl[ii],"x")+dayZ.Ai.anger_distance > dayZ.Ai.aiList[i].x) then
					if (player(pl[ii],"x")-dayZ.Ai.anger_distance < dayZ.Ai.aiList[i].x) then
					if (player(pl[ii],"y")+dayZ.Ai.anger_distance > dayZ.Ai.aiList[i].y) then
					if (player(pl[ii],"y")-dayZ.Ai.anger_distance < dayZ.Ai.aiList[i].y) then
						if player(ii,"armor") == 206 then
							if math.random(0,7.5) < 1 then
								temp_targets[#temp_targets+1] = ii
							end
						else
							temp_targets[#temp_targets+1] = ii
						end
					end
					end
					end
					end
				end
				if #temp_targets > 0 then
					dayZ.Ai.aiList[i].target = pl[temp_targets[math.random(1,#temp_targets)]]
				end
			end
		end
	end
end

addhook("ms100","dayZ.Ai.ms100")
function dayZ.Ai.ms100()
	search_delay = 0
	i = 0
	while (i < #dayZ.Ai.aiList) do
		i = i + 1
		if dayZ.Ai.aiList[i].search > 0 then
			dayZ.Ai.aiList[i].search = dayZ.Ai.aiList[i].search - 1
			if dayZ.Ai.aiList[i].search == 0 then
				pl = player(0,"table")
				ii = 0
				temp_targets = {}
				while (ii < #pl) do
					ii = ii + 1
					if dayZ.Ai.collision_detection(player(pl[ii],"x"),player(pl[ii],"y"),dayZ.Ai.aiList[i].x,dayZ.Ai.aiList[i].y,dayZ.Ai.anger_distance) == true then
						temp_targets[#temp_targets+1] = pl[i]
					end
				end
				if #temp_targets > 0 then
					dayZ.Ai.aiList[i].target = temp_targets[math.random(1,#temp_targets)]
				end
			end
		else
			if dayZ.Ai.aiList[i].target == 0 then
				dayZ.Ai.aiList[i].dir = dayZ.Ai.aiList[i].dir + math.random(-32,32)
				next_x = (dayZ.Ai.aiList[i].x + math.cos(math.rad(dayZ.Ai.aiList[i].dir-90))*dayZ.Ai.speed)
				next_y = (dayZ.Ai.aiList[i].y + math.sin(math.rad(dayZ.Ai.aiList[i].dir-90))*dayZ.Ai.speed)
				if tile(pixel_to_tile(next_x),pixel_to_tile(next_y),"walkable") then
					imagepos(dayZ.Ai.aiList[i].img,dayZ.Ai.aiList[i].x,dayZ.Ai.aiList[i].y,dayZ.Ai.aiList[i].dir)
					tween_move(dayZ.Ai.aiList[i].img,100,next_x,next_y,dir)
					dayZ.Ai.aiList[i].x = next_x
					dayZ.Ai.aiList[i].y = next_y
				else
					dayZ.Ai.aiList[i].dir = dayZ.Ai.aiList[i].dir - 180
				end
				dayZ.Ai.aiList[i].despawn = dayZ.Ai.aiList[i].despawn - 1
				if dayZ.Ai.aiList[i].despawn <= 0 then -- despawn
					pl = player(0,"tableliving")
					ii = 0
					can_despawn = true
					while (ii < #pl) do
						ii = ii + 1
						--if dayZ.Ai.collision_detection(player(pl[ii],"x"),player(pl[ii],"y"),dayZ.Ai.aiList[i].x,dayZ.Ai.aiList[i].y,32*(dayZ.spawn_dist-2)) == true then
						if (misc.point_distance(player(pl[ii],"x"),player(pl[ii],"y"),dayZ.Ai.aiList[i].x,dayZ.Ai.aiList[i].y) < 32*(dayZ.spawn_dist-2)) then
							can_despawn = false
						end
					end
					if can_despawn == true then
						dayZ.Ai.remove_ai(i)
					end
				end -- end
			else
				if player(dayZ.Ai.aiList[i].target,"exists") and (player(dayZ.Ai.aiList[i].target,"health") > 0) then
					dayZ.Ai.aiList[i].despawn = dayZ.Ai.despawn_timer
					if dayZ.Ai.collision_detection(player(dayZ.Ai.aiList[i].target,"x"),player(dayZ.Ai.aiList[i].target,"y"),dayZ.Ai.aiList[i].x,dayZ.Ai.aiList[i].y,20) then
						dayZ.Ai.damagePlayer(dayZ.Ai.aiList[i].target,5)
						dayZ.Ai.aiList[i].search = 10
					end
					dayZ.Ai.aiList[i].dir = point_direction(dayZ.Ai.aiList[i].x,dayZ.Ai.aiList[i].y,player(dayZ.Ai.aiList[i].target,"x"),player(dayZ.Ai.aiList[i].target,"y"))
					next_x = (math.cos(math.rad(dayZ.Ai.aiList[i].dir-90))*dayZ.Ai.speed)
					next_y = (math.sin(math.rad(dayZ.Ai.aiList[i].dir-90))*dayZ.Ai.speed)
					if tile(pixel_to_tile(dayZ.Ai.aiList[i].x+next_x),pixel_to_tile(dayZ.Ai.aiList[i].y+next_y),"walkable") then
						imagepos(dayZ.Ai.aiList[i].img,dayZ.Ai.aiList[i].x,dayZ.Ai.aiList[i].y,dayZ.Ai.aiList[i].dir)
						tween_move(dayZ.Ai.aiList[i].img,100,dayZ.Ai.aiList[i].x + next_x,dayZ.Ai.aiList[i].y + next_y,dir)
						dayZ.Ai.aiList[i].x = dayZ.Ai.aiList[i].x + next_x
						dayZ.Ai.aiList[i].y = dayZ.Ai.aiList[i].y + next_y
					else
						imagepos(dayZ.Ai.aiList[i].img,dayZ.Ai.aiList[i].x,dayZ.Ai.aiList[i].y,dayZ.Ai.aiList[i].dir)
						tween_move(dayZ.Ai.aiList[i].img,100,dayZ.Ai.aiList[i].x - next_x,dayZ.Ai.aiList[i].y - next_y,dir)
						dayZ.Ai.aiList[i].x = dayZ.Ai.aiList[i].x - next_x
						dayZ.Ai.aiList[i].y = dayZ.Ai.aiList[i].y - next_y
						dayZ.Ai.aiList[i].despawn = 10
						dayZ.Ai.aiList[i].target = 0
						dayZ.Ai.aiList[i].search = search_delay
						search_delay = search_delay + dayZ.Ai.lagg.search_delay
					end
				else
					dayZ.Ai.aiList[i].despawn = 10
					dayZ.Ai.aiList[i].target = 0
					dayZ.Ai.aiList[i].search = search_delay
					search_delay = search_delay + dayZ.Ai.lagg.search_delay
				end
			end
		end
	end
end

function dayZ.Ai.damagePlayer(id,dmg)
	parse("sv_sound2 "..id.." player/zm_hit.wav")
	local i = 0
	while(i < #dayZ.armourValue) do
		i = i + 1
		if dayZ.armourValue[i].id == player(id,"armor") then
			total_dmg = dmg - ((5/100)*dayZ.armourValue[i].protection)
			dayZ.blood_loss[id] = dayZ.blood_loss[id] + total_dmg
			dayZ.update_blood(id)
			return 0
		end
	end
	-- user has no armour (or unknown type)
	dayZ.blood_loss[id] = dayZ.blood_loss[id] + dmg
	dayZ.update_blood(id)
end

addhook("attack","dayZ.Ai.attack")
function dayZ.Ai.attack(id)
	range = itemtype(player(id,"weapontype"),"range")*2
	local dir = player(id,"rot")
	dmg = itemtype(player(id,"weapontype"),"dmg")
	x_next = lengthdir_x(dir,8)
	y_next = lengthdir_y(dir,8)
	x = player(id,"x")
	y = player(id,"y")
	i = 0
	while ((i < range) and (tile(pixel_to_tile(x),pixel_to_tile(y),"wall") == false)) do
		i = i + 8
		x = x + x_next
		y = y + y_next
		ii = 0
		while (ii < #dayZ.Ai.aiList) do
			ii = ii + 1
			if dayZ.Ai.collision_detection(x,y,dayZ.Ai.aiList[ii].x,dayZ.Ai.aiList[ii].y,16) then
				if ((dayZ.dmgBonusWep[player(id,"weapontype")] == nil) == false) then
					dmg = dmg * dayZ.dmgBonusWep[player(id,"weapontype")]
				end
				dayZ.Ai.npcTakeDmg(id,ii,dmg)
				i = range
			end
		end
	end
end

function dayZ.Ai.npcTakeDmg(player_id,id,dmg)
	dayZ.Ai.aiList[id].hp = dayZ.Ai.aiList[id].hp - dmg
	if dayZ.Ai.aiList[id].target == 0 then
		dayZ.Ai.aiList[id].target = player_id
	end
	if dayZ.Ai.aiList[id].hp <= 0 then
		parse("sv_soundpos player/zm_die.wav "..dayZ.Ai.aiList[ii].x.." "..dayZ.Ai.aiList[ii].y)
		dayZ.zombie_kills[player_id] = dayZ.zombie_kills[player_id] + 1
		parse("hudtxt2 "..player_id.." 4 \"zombie kills: "..dayZ.zombie_kills[player_id].."\" 2 164 0")
		dayZ.Ai.remove_ai(id)
		if (BrutalBody ~= nil) then
			for index, imagePart in ipairs({"leg", "leg", "arm", "arm", "head"}) do
				BrutalBody.spawn(x, y, dmg/10, "t4_s_" ..imagePart)
			end
		end
	end
end

function dayZ.Ai.collision_detection(x,y,aix,aiy,detection_size)
	if (x+detection_size > aix) then
	if (x-detection_size < aix) then
	if (y+detection_size > aiy) then
	if (y-detection_size < aiy) then
		return true
	end
	end
	end
	end
	return false
end

dayZ.Ai.projectile_impact = {
{id=47,size=100,dmg=150},
{id=48,size=50,dmg=100},
{id=49,size=32,dmg=100},
{id=51,size=100,dmg=250},
{id=52,size=0,dmg=0},
{id=53,size=0,dmg=0},
{id=54,size=0,dmg=0},
{id=76,size=250,dmg=500},
{id=73,size=75,dmg=150},
{id=89,size=75,dmg=150}}

addhook("projectile","dayZ.Ai.projectile")
function dayZ.Ai.projectile(id,weapon,x,y)
	i = 0
	while(i < #dayZ.Ai.projectile_impact) do
		i = i + 1
		if dayZ.Ai.projectile_impact[i].id == weapon then
			debug_print("valid impact of weapon (id: "..dayZ.Ai.projectile_impact[i].id..") at position (X: "..x.."|Y: "..y..") explosion (size: "..dayZ.Ai.projectile_impact[i].size.."| dmg: "..dayZ.Ai.projectile_impact[i].dmg..")")
			ii = 0
			while (ii < #dayZ.Ai.aiList) do
				ii = ii + 1
				local dist = point_distance(x,y,dayZ.Ai.aiList[ii].x,dayZ.Ai.aiList[ii].y)
				if dist < dayZ.Ai.projectile_impact[i].size then
					--dmgByPixel = (dayZ.Ai.projectile_impact[i].dmg / dayZ.Ai.projectile_impact[i].size)
					--totalDamage = dmgByPixel * (dayZ.Ai.projectile_impact[i].size-dist)
					--if totalDamage > 0 then
						--debug_print("zombie npc (id: "..ii..") took "..totalDamage.." Damage")
						dayZ.Ai.npcTakeDmg(id,ii,totalDamage)
					--end
				end
			end
			return 0
		end
	end
	msg("�999000000WARNING: unknown projectile : "..itemtype(weapon,"name").." ("..weapon..")")
	return 0
end