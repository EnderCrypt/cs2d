dayZsave = {}

function dayZsave.load(id)
	dayZ.blood[id] = {}
	dayZ.hunger[id] = {}
	dayZ.thirst[id] = {}
	dayZ.inventory[id] = {}
	local file = io.open("sys/lua/dayZ/saves/usgn_"..player(id,"usgn")..".txt","r")
	load_verify = {}
	i = 0
	while (i < 10) do
		i = i + 1
		load_verify[i] = false
	end
	load_verify[9] = true
	load_verify[10] = true
	local alive_dead = file:read() -- alive/dead
	local data = file:read()
	local equip_slot = 0
	local i = 0
	while((data == nil) == false) do
		i = i + 1
		if data == "deaths" then
			dayZ.deaths[id] = tonumber(file:read())
			if dayZ.deaths[id] == nil then
				load_verify[1] = false
			else
				load_verify[1] = true
				msg2(id,"you have "..dayZ.deaths[id].." Deaths")
			end
		end
		if data == "blood" then
			dayZ.blood[id] = tonumber(file:read())
			if dayZ.blood[id] == nil then
				load_verify[2] = false
			else
				load_verify[2] = true
			end
		end
		if data == "bloodloss" then
			dayZ.blood_loss[id] = tonumber(file:read())
			if dayZ.blood_loss[id] == nil then
				load_verify[3] = false
			else
				load_verify[3] = true
			end
		end
		if data == "zkills" then
			dayZ.zombie_kills[id] = tonumber(file:read())
			if dayZ.zombie_kills[id] == nil then
				load_verify[4] = false
			else
				load_verify[4] = true
			end
			parse("hudtxt2 "..id.." 4 \"zombie kills: "..dayZ.zombie_kills[id].."\" 2 164 0")
		end
		if data == "hunger" then
			dayZ.hunger[id] = tonumber(file:read())
			if dayZ.hunger[id] == nil then
				load_verify[5] = false
			else
				load_verify[5] = true
			end
		end
		if data == "thirst" then
			dayZ.thirst[id] = tonumber(file:read())
			if type(dayZ.thirst[id]) == nil then
				load_verify[6] = false
			else
				load_verify[6] = true
			end
		end
		if data == "armour" then
			local armour = tonumber(file:read())
			if armour == nil then
				load_verify[7] = false
			else
				load_verify[7] = true
			end
			if armour > 0 then
				parse("equip "..id.." "..(armour-122))
			end
		end
		if data == "location" then
			local x = tonumber(file:read())
			local y = tonumber(file:read())
			if (x == nil) or (y == nil) then
				load_verify[8] = false
			else
				load_verify[8] = true
			end
			parse("setpos "..id.." "..x.." "..y.."")
		end
		if data == "equip" then -- 9 and 10
			equip_slot = equip_slot + 1
			dayZ.inventory[id][equip_slot] = {id=tonumber(file:read()),amount=tonumber(file:read())}
			if dayZ.inventory[id][equip_slot].id == nil then
				load_verify[9] = false -- this can only fail
			end
			if dayZ.inventory[id][equip_slot].amount == nil then
				load_verify[10] = false -- this can only fai
			end
		end
		data = file:read()
	end
	recalculate_itemAmount(id)
	-- load complete
	
	local valid = true
	i = 0
	while (i < 10) do
		i = i + 1
		if (load_verify[i] == false) then
			msg("�999000000-------------------------")
			msg("�999000000-- Failed to load save --")
			msg("�999000000-- Error location ("..i..")")
			msg("�999000000-- user: "..player(id,"name").." usgn: "..player(id,"usgn"))
			valid = false
		end
	end
	if valid == false then -- move to trash
		msg("�999000000-------------------------")
		msg2(id,"�999000000your save is CORRUPT/INVALID, and was copied to the trash")
		msg2(id,"�999000000please contact admin in order to restore your save file")
		file = io.open("sys/lua/dayZ/saves/usgn_"..player(id,"usgn")..".txt","r")
		local trash = io.open("sys/lua/dayZ/saves/trash.txt","a")
		
		trash:write("usgn_"..player(id,"usgn").." ("..player(id,"name")..") \n")
		
		i = 0
		while (i < 10) do
			i = i + 1
			if (load_verify[i] == nil) then
				trash:write("Error Location: ("..i..")\n")
			end
		end
		
		trash:write("--- save file data ---\n")
		data = file:read()
		while((data == nil) == false) do
			trash:write(data.."\n")
			data = file:read()
		end
		trash:write("----------\n")
		file:close()
		trash:close()
		--os.remove("sys/lua/dayZ/saves/usgn_"..player(id,"usgn")..".txt")
		
		dayZ.deaths[id] = 0
		dayZ.default_spawn(id)
	end
	parse("hudtxt 1 \"active zombies: "..#dayZ.Ai.aiList.."\" 2 116 0")
	parse("hudtxt2 "..id.." 5 \"Inventory: ("..dayZ.inventory[id].itemAmount.." out of "..dayZ.inventory_limit..")\" 2 180 0")
	dayZ.update_blood(id)
	dayZ.update_hunger_thirst(id)
end

addhook("leave","dayZsave.leave")
function dayZsave.leave(id, reason)
	if player(id,"health") <= 0 then
		return 1
	end
	if player(id,"usgn") == 0 then
		return 1
	end
	local file = io.open("sys/lua/dayZ/saves/usgn_"..player(id,"usgn")..".txt","w")
	file:write("ALIVE\n")
	file:write("deaths\n")
	file:write((dayZ.deaths[id]).."\n")
	file:write("blood\n")
	file:write((dayZ.blood[id]).."\n")
	file:write("bloodloss\n")
	file:write((dayZ.blood_loss[id]).."\n")
	file:write("zkills\n")
	file:write((dayZ.zombie_kills[id]).."\n")
	file:write("hunger\n")
	file:write((dayZ.hunger[id]).."\n")
	file:write("thirst\n")
	file:write((dayZ.thirst[id]).."\n")
	file:write("armour\n")
	file:write((player(id,"armor")).."\n")
	file:write("location\n")
	file:write((player(id,"x")).."\n")
	file:write((player(id,"y")).."\n")
	i = 0
	while (i < #dayZ.inventory[id]) do
		i = i + 1
		file:write("equip\n")
		file:write(dayZ.inventory[id][i].id.."\n")
		file:write(dayZ.inventory[id][i].amount.."\n")
	end
	file:close()
end

--addhook("die","dayZsave.die") -- manually triggered from dayZ.die
function dayZsave.die(victim,killer,weapon,x,y)
	dayZ.deaths[victim] = dayZ.deaths[victim] + 1
	if player(victim,"usgn") > 0 then
		local file = io.open("sys/lua/dayZ/saves/usgn_"..player(victim,"usgn")..".txt","w")
		file:write("DEAD\n")
		file:write("deaths\n")
		file:write((dayZ.deaths[victim]).."\n")
		file:close()
	end
end
