function dayZ.setValue(playerVariable, id, value, max)
  if (value < 0) then
    value = 0
  end
  if (value > max) then
    value = max
  end
  playerVariable[id] = value
end

function dayZ.addBloodLoss(id, value)
  dayZ.setBloodLoss(id, dayZ.blood_loss[id] + value)
end

function dayZ.setBloodLoss(id, value)
  dayZ.setValue(dayZ.blood_loss, id, value, dayZ.blood.max)
  dayZ.update_blood(id)
end

function dayZ.addBlood(id, value)
  dayZ.setBlood(id, dayZ.blood[id] + value)
end

function dayZ.setBlood(id, value)
  dayZ.setValue(dayZ.blood, id, value, dayZ.blood.max)
  dayZ.update_blood(id)
end

function dayZ.addHunger(id, value)
  dayZ.setHunger(id, dayZ.hunger[id] + value)
end

function dayZ.setHunger(id, value)
  dayZ.setValue(dayZ.hunger, id, value, 100)
  dayZ.update_hunger_thirst(id)
end

function dayZ.addThirst(id, value)
  dayZ.setThirst(id, dayZ.thirst[id] + value)
end

function dayZ.setThirst(id, value)
  dayZ.setValue(dayZ.thirst, id, value, 100)
  dayZ.update_hunger_thirst(id)
end

dayZ.items = {}
function dayZ.addItem(arguments)
  local data = {}
  data.name = arguments.name
  data.img = "gfx/dayZ/items/"..arguments.image
  misc.addServerTransfer(data.img)
  data.spawnRate = arguments.spawnRate
  data.use = arguments.onUse
  dayZ.items[#dayZ.items+1] = data
end

dayZ.addItem({ -- id: 1001
 name = "BloodBag",
 image = "bloodbag.png",
 spawnRate = 1,
 onUse = function(id)
  dayZ.setBlood(id, dayZ.blood.max)
  msg2(id,"Blood maxed to "..dayZ.blood.max.."!")
 end})

dayZ.addItem({ -- id: 1002
 name = "Can of Beans",
 image = "CanBeans.png",
 spawnRate = 1,
 onUse = function(id)
  dayZ.addHunger(id, -40)
  msg2(id,"hunger decreased by 40%")
 end})
 
dayZ.addItem({ -- id: 1003
 name = "Can of Tuna",
 image = "Tuna.png",
 spawnRate = 1,
 onUse = function(id)
  dayZ.addThirst(id, -10)
  dayZ.addHunger(id, -30)
  msg2(id,"hunger decreased by 30%, thirst decreased by 10%")
 end})
 
dayZ.addItem({ -- id: 1004
 name = "Pepsi",
 image = "pepsi.png",
 spawnRate = 1,
 onUse = function(id)
  dayZ.addThirst(id, -25)
  msg2(id,"thirst decreased by 25%")
 end})
 
dayZ.addItem({ -- id: 1005
 name = "Soup",
 image = "soup.png",
 spawnRate = 1,
 onUse = function(id)
  dayZ.addHunger(id, -35)
  dayZ.addThirst(id, -20)
  msg2(id,"hunger decreased by 35% and thirst by 20%")
 end})
 
dayZ.addItem({ -- id: 1006
 name = "Water",
 image = "water.png",
 spawnRate = 1,
 onUse = function(id)
  dayZ.addThirst(id, -75)
  msg2(id,"thirst decreased by 75%")
 end})
 
dayZ.addItem({ -- id: 1007
 name = "Orange Soda",
 image = "orangejuice.png",
 spawnRate = 1,
 onUse = function(id)
  dayZ.addThirst(id, -35)
  msg2(id,"thirst decreased by 35%")
 end})
 
dayZ.addItem({ -- id: 1008
 name = "Chocolate",
 image = "Chocolate.png",
 spawnRate = 1,
 onUse = function(id)
 dayZ.addBlood(id, 1000)
 dayZ.addThirst(id, 15)
 dayZ.addHunger(id, -25)
  msg2(id,"blood increased by 1000, hunger decreased by 25%, thirst INCREASED by 15%")
 end})
 
dayZ.addItem({ -- id: 1009
 name = "Apple",
 image = "apple.png",
 spawnRate = 1,
 onUse = function(id)
 dayZ.addHunger(id, -15)
  dayZ.addThirst(id, -15)
  msg2(id,"thirst decreased by 15%, hunger decreased by 15%")
 end})
 
dayZ.addItem({ -- id: 1010
 name = "Bread",
 image = "bread.png",
 spawnRate = 1,
 onUse = function(id)
  dayZ.addHunger(id, -35)
  msg2(id,"hunger decreased by 35%")
 end})
 
dayZ.addItem({ -- id: 1011
 name = "Carrots",
 image = "carrots.png",
 spawnRate = 1,
 onUse = function(id)
  dayZ.addHunger(id, -20)
  msg2(id,"hunger decreased by 20%")
 end})
 
dayZ.addItem({ -- id: 1012
 name = "Gasoline",
 image = "petrol.png",
 spawnRate = 1,
 onUse = function(id)
  msg2(id,"This item allows you to fast-travel using command !travel <x> <y>")
 end})
 
dayZ.addItem({ -- id: 1013
 name = "Energy Bar",
 image = "energybar.png",
 spawnRate = 1,
 onUse = function(id)
 dayZ.addBlood(id, 1500)
  dayZ.addHunger(id, -35)
  msg2(id,"hunger decreased by 15%, blood increased by 1500")
 end})
 
dayZ.addItem({ -- id: 1014
 name = "Duff Beer",
 image = "beer.png",
 spawnRate = 1,
 onUse = function(id)
  dayZ.addThirst(id, -20)
  msg2(id,"thirst decreased by 20%")
 end})
 
dayZ.addItem({ -- id: 1015
 name = "Pretzel",
 image = "pretzel.png",
 spawnRate = 1,
 onUse = function(id)
  dayZ.addHunger(id, -15)
  dayZ.addBlood(id, 200)
  msg2(id,"hunger decreased by 15%, blood increased by 200")
 end})
 
dayZ.addItem({ -- id: 1016
 name = "Plaster",
 image = "plaster.png",
 spawnRate = 1,
 onUse = function(id)
  dayZ.addBloodLoss(id, -250)
  parse("sethealth "..id.." "..player(id,"maxhealth"))
  msg2(id,"bleeding decreased by 250")
 end})
 
dayZ.addItem({ -- id: 1017
 name = "Chips",
 image = "chips.png",
 spawnRate = 1,
 onUse = function(id)
  dayZ.addHunger(id, -25)
  msg2(id,"hunger decreased by 25%")
 end})
 
dayZ.addItem({ -- id: 1018
 name = "Egg",
 image = "egg.png",
 spawnRate = 1,
 onUse = function(id)
  dayZ.addHunger(id, -10)
  dayZ.addThirst(id, -10)
  msg2(id,"hunger decreased by 10%, thirst decreased by 10%")
 end})
 
dayZ.addItem({ -- id: 1019
 name = "Mushroom",
 image = "mushroom.png",
 spawnRate = 1,
 onUse = function(id)
  dayZ.addHunger(id, -25)
  msg2(id,"hunger decreased by 25%")
 end})
 
dayZ.addItem({ -- id: 1020
 name = "StrawBerry",
 image = "StrawBerry.png",
 spawnRate = 1,
 onUse = function(id)
  dayZ.addHunger(id, -15)
  dayZ.addThirst(id, -15)
  msg2(id,"hunger decreased by 15%, thirst decreased by 15%")
 end})
 
dayZ.addItem({ -- id: 1021
 name = "Human meat",
 image = "human_meat.png",
 spawnRate = 1,
 onUse = function(id)
  dayZ.addHunger(id, -5)
  msg2(id,"Ughh... hunger decreased by 5%... Tastes like chicken!")
 end})
 
dayZ.addItem({ -- id: 1022
 name = "Donut",
 image = "donut.png",
 spawnRate = 1,
 onUse = function(id)
  dayZ.addHunger(id, -15)
  dayZ.addThirst(id, 5)
  dayZ.addBlood(id, 500)
  msg2(id,"hunger INCREASED by 5%, thirst decreased by 15%, blood increased by 500")
 end})
 
dayZ.addItem({ -- id: 1023
 name = "Choco Popsicle",
 image = "chpopsicle.png",
 spawnRate = 1,
 onUse = function(id)
  dayZ.addHunger(id, -15)
  msg2(id,"hunger decreased by 15%")
 end})
 
dayZ.addItem({ -- id: 1024
 name = "Pizza",
 image = "pizza.png",
 spawnRate = 1,
 onUse = function(id)
  dayZ.addHunger(id, -50)
  msg2(id,"hunger decreased by 50%")
 end})
 
dayZ.addItem({ -- id: 1025
 name = "Pumpkin",
 image = "pumpkin.png",
 spawnRate = 1,
 onUse = function(id)
  dayZ.addHunger(id, -35)
  msg2(id,"what pumpkin? hunger decreased by 35%")
 end})
 
dayZ.addItem({ -- id: 1026
 name = "Chili Pepper",
 image = "chilipepper.png",
 spawnRate = 1,
 onUse = function(id)
  dayZ.addHunger(id, -10)
  dayZ.addThirst(id, 15)
  msg2(id,"hunger INCREASED by 15%, thirst decreased by 10%")
 end})
 
misc.addServerTransfer("sfx/doot.ogg")
dayZ.addItem({ -- id: 1027
 name = "Trumpet",
 image = "trumpet.png",
 spawnRate = 1,
 onUse = function(id)
  local x = misc.round(player(id,"x"))
  local y = misc.round(player(id,"y"))
  msg2(id, "you play the trumpet!")
  for _,pid in ipairs(player(0,"tableliving")) do
  	local px = player(pid,"x")
  	local py = player(pid,"y")
  	if (misc.isInsideScreen(x,y,px,py)) then
  		if (id ~= pid) then
	  		msg2(pid, player(id,"name").." plays the trumpet!")
	  	end
  		parse("sv_sound2 "..pid.." doot.ogg")
  	end
  end
  dayZ.add_item(id,1027,1) -- this gives the item back to the player since using items deles them
 end})
 
dayZ.addItem({ -- id: 1028
 name = "Dry Pasta",
 image = "pasta.png",
 spawnRate = 1,
 onUse = function(id)
 dayZ.addHunger(id, -40)
 msg2(id, "hunger decreased by 40%")
 end})
 
dayZ.addItem({ -- id: 1029
 name = "Hot Cuppa",
 image = "hotcuppa.png",
 spawnRate = 1,
 onUse = function(id)
 dayZ.addThirst(id, -30)
 msg2(id, "thirst decreased by 30%")
 end})
 
dayZ.addItem({ -- id: 1030
 name = "Jelly Sandwich",
 image = "jellysandwich.png",
 spawnRate = 1,
 onUse = function(id)
 dayZ.addHunger(id, -20)
 msg2(id, "hunger decreased by 20%")
 end})
 
dayZ.addItem({ -- id: 1031
 name = "Jelly Beans",
 image = "jellybeans.png",
 spawnRate = 1,
 onUse = function(id)
 dayZ.addHunger(id, -15)
 msg2(id, "hunger decreased by 15%")
 end})
 
dayZ.addItem({ -- id: 1032
 name = "Nacho Jar",
 image = "nachos.png",
 spawnRate = 1,
 onUse = function(id)
 dayZ.addThirst(id, 10)
 dayZ.addHunger(id, -25)
 msg2(id, "thirst INCREASED by 10%, hunger decreased by 25%")
 end})
 
dayZ.addItem({ -- id: 1033
 name = "BlackBerry Pie",
 image = "bbpie.png",
 spawnRate = 1,
 onUse = function(id)
 dayZ.addHunger(id, -40)
 msg2(id, "hunger decreased by 40%")
 end})
 
 dayZ.addItem({ -- id: 1034
 name = "Beef Sausage",
 image = "beefsausage.png",
 spawnRate = 1,
 onUse = function(id)
 dayZ.addHunger(id, -40)
 msg2(id, "hunger decreased by 40%")
 end})
 
 dayZ.addItem({ -- id: 1035
 name = "Vodka",
 image = "vodka.png",
 spawnRate = 1,
 onUse = function(id)
 dayZ.addThirst(id, -40)
 msg2(id, "cheeki breeki i v damke! thirst decreased by 40%")
 end})
 
dayZ.addItem({ -- id: 1036
 name = "Cabbage",
 image = "cabbage.png",
 spawnRate = 1,
 onUse = function(id)
 dayZ.addThirst(id, -1)
 dayZ.addHunger(id, -25)
 msg2(id, "thirst decreased by 1%, hunger decreased by 25%")
 end})
 
dayZ.addItem({ -- id: 1037
 name = "Diesel",
 image = "biopetrol.png",
 spawnRate = 1,
 onUse = function(id)
  msg2(id,"This item allows you to fast-travel using command !travel <x> <y>")
 end})
 
 dayZ.addItem({ -- id: 1038
 name = "Burrito",
 image = "burrito.png",
 spawnRate = 1,
 onUse = function(id)
 dayZ.addHunger(id, -35)
 msg2(id, "hunger decreased by 35%")
 end})
 
 dayZ.addItem({ -- id: 1039
 name = "Canned Spam",
 image = "spam.png",
 spawnRate = 1,
 onUse = function(id)
 dayZ.addHunger(id, -40)
 msg2(id, "hunger decreased by 40%")
 end})
 
 dayZ.addItem({ -- id: 1040
 name = "Apple Juice Box",
 image = "applejuicebox.png",
 spawnRate = 1,
 onUse = function(id)
 dayZ.addThirst(id, -30)
 msg2(id, "thirst decreased by 30%")
 end})
 
dayZ.addItem({ -- id: 1041
 name = "Orange Juice",
 image = "borangejuice.png",
 spawnRate = 1,
 onUse = function(id)
 dayZ.addThirst(id, -50)
 msg2(id, "thirst decreased by 50%")
 end})
 
dayZ.addItem({ -- id: 1042
 name = "Whiskey",
 image = "whiskey.png",
 spawnRate = 1,
 onUse = function(id)
 dayZ.addThirst(id, -35)
 msg2(id, "thirst decreased by 35%")
 end})
 
dayZ.addItem({ -- id: 1043
 name = "Med Bag",
 image = "medbag.png",
 spawnRate = 1,
 onUse = function(id)
  dayZ.addBloodLoss(id, -350)
  dayZ.addBlood(id, 2500)
  msg2(id,"bleeding decreased by 350, blood increased by 2500")
 end})
 
dayZ.addItem({ -- id: 1044
 name = "Med Box",
 image = "medbox.png",
 spawnRate = 1,
 onUse = function(id)
  dayZ.addBloodLoss(id, -500)
  dayZ.addBlood(id, 4000)
  msg2(id,"bleeding decreased by 500, blood increased by 4000")
 end})
 
dayZ.addItem({ -- id: 1045
 name = "Fried Egg",
 image = "fryegg.png",
 spawnRate = 1,
 onUse = function(id)
  dayZ.addHunger(id, -25)
  msg2(id,"hunger decreased by 25%")
 end})
 
dayZ.addItem({ -- id: 1046
 name = "Chewing Gum",
 image = "gum.png",
 spawnRate = 1,
 onUse = function(id)
  dayZ.addHunger(id, -3)
  msg2(id,"You chew the gum... Not so satisfied, but hunger decreased by 3%")
 end})
 
dayZ.addItem({ -- id: 1047
 name = "Raspberry Jam",
 image = "raspbjam.png",
 spawnRate = 1,
 onUse = function(id)
  dayZ.addHunger(id, -30)
  msg2(id,"hunger decreased by 30%")
 end})
 
dayZ.addItem({ -- id: 1048
 name = "Jar Of Pickles",
 image = "pickles.png",
 spawnRate = 1,
 onUse = function(id)
  dayZ.addHunger(id, -40)
  dayZ.addThirst(id, -25)
  msg2(id,"hunger decreased by 40%, thirst by 25%")
 end})
 
dayZ.addItem({ -- id: 1049
 name = "Jar Of Berries",
 image = "jarberry.png",
 spawnRate = 1,
 onUse = function(id)
  dayZ.addHunger(id, -35)
  msg2(id,"hunger decreased by 35%")
 end})
 
dayZ.addItem({ -- id: 1049
 name = "Ketchup",
 image = "ketchup.png",
 spawnRate = 1,
 onUse = function(id)
  dayZ.addHunger(id, -25)
  msg2(id,"hunger decreased by 25%")
 end})
 
dayZ.addItem({ -- id: 1050
 name = "Cheese Sandwich",
 image = "cheesesandwich.png",
 spawnRate = 1,
 onUse = function(id)
  dayZ.addHunger(id, -35)
  msg2(id,"hunger decreased by 35%")
 end})
 
dayZ.addItem({ -- id: 1050
 name = "Grilled Cheese Sandwich",
 image = "gcsandwich.png",
 spawnRate = 1,
 onUse = function(id)
  dayZ.addHunger(id, -40)
  msg2(id,"hunger decreased by 40%")
 end})
 
dayZ.addItem({ -- id: 1051
 name = "Yoghurt",
 image = "yoghurt.png",
 spawnRate = 1,
 onUse = function(id)
  dayZ.addHunger(id, -10)
  msg2(id,"hunger decreased by 10%")
 end})
 
dayZ.addItem({ -- id: 1052
 name = "Cooked Bacon",
 image = "cookedbacon.png",
 spawnRate = 1,
 onUse = function(id)
  dayZ.addHunger(id, -30)
  msg2(id,"hunger decreased by 30%")
 end})
 
dayZ.addItem({ -- id: 1053
 name = "Burger",
 image = "burger.png",
 spawnRate = 1,
 onUse = function(id)
  dayZ.addHunger(id, -35)
  msg2(id,"hunger decreased by 35%")
 end})
 
dayZ.addItem({ -- id: 1054
 name = "Sandwich",
 image = "sandwich.png",
 spawnRate = 1,
 onUse = function(id)
  dayZ.addHunger(id, -35)
  msg2(id,"hunger decreased by 35%")
 end})
 
dayZ.addItem({ -- id: 1055
 name = "Pineapple",
 image = "pineapple.png",
 spawnRate = 1,
 onUse = function(id)
  dayZ.addHunger(id, -20)
  dayZ.addThirst(id, -5)
  msg2(id,"hunger decreased by 20%, thirst decreased by 5%")
 end})
 
dayZ.addItem({ -- id: 1056
 name = "Orange",
 image = "orange.png",
 spawnRate = 1,
 onUse = function(id)
  dayZ.addHunger(id, -10)
  dayZ.addThirst(id, -20)
  msg2(id,"hunger decreased by 10%, thirst decreased by 20%")
 end})
 
dayZ.addItem({ -- id: 1057
 name = "Hotdog",
 image = "hotdog.png",
 spawnRate = 1,
 onUse = function(id)
  dayZ.addHunger(id, -30)
  msg2(id,"hunger decreased by 30%")
 end})
 
dayZ.addItem({ -- id: 1058
 name = "French Fries",
 image = "frenchfries.png",
 spawnRate = 1,
 onUse = function(id)
  dayZ.addHunger(id, -25)
  msg2(id,"hunger decreased by 25%")
 end})
 
dayZ.addItem({ -- id: 1059
 name = "Banana",
 image = "banana.png",
 spawnRate = 1,
 onUse = function(id)
  dayZ.addHunger(id, -15)
  msg2(id,"hunger decreased by 15%")
 end})
 
dayZ.addItem({ -- id: 1060
 name = "Cooked Steak",
 image = "steakcooked.png",
 spawnRate = 1,
 onUse = function(id)
  dayZ.addHunger(id, -40)
  msg2(id,"hunger decreased by 40%")
 end})
 
dayZ.addItem({ -- id: 1061
 name = "Cooked Chicken",
 image = "cookedchicken.png",
 spawnRate = 1,
 onUse = function(id)
  dayZ.addHunger(id, -35)
  msg2(id,"hunger decreased by 35%")
 end})

dayZ.addItem({ -- id: 1062
 name = "Blood Syringe",
 image = "bloodsyringe.png",
 spawnRate = 1,
 onUse = function(id)
  dayZ.addBlood(id, 3000)
  msg2(id,"Blood increased by 3000")
 end})

dayZ.addItem({ -- id: 1063
 name = "Bloody Mary",
 image = "bloodymary.png",
 spawnRate = 1,
 onUse = function(id)
  dayZ.addThirst(id, -20)
  dayZ.addBlood(id, 500)
  msg2(id,"thirst decreased by 20%, blood increased by 500")
 end})
 
dayZ.addItem({ -- id: 1064
 name = "Lasagna",
 image = "lasagna.png",
 spawnRate = 1,
 onUse = function(id)
  dayZ.addHunger(id, -40)
  msg2(id,"hunger decreased by 40%")
 end})
 
dayZ.addItem({ -- id: 1065
 name = "Tortilla Chips",
 image = "tortillachips.png",
 spawnRate = 1,
 onUse = function(id)
  dayZ.addThirst(id, 10)
  dayZ.addHunger(id, -30)
  msg2(id,"thirst INCREASED by 10%, hunger decreased by 30%")
 end})
 
dayZ.addItem({ -- id: 1066
 name = "Apple Pie",
 image = "applepie.png",
 spawnRate = 1,
 onUse = function(id)
  dayZ.addHunger(id, -40)
  msg2(id,"hunger decreased by 40%")
 end})
 
dayZ.addItem({ -- id: 1067
 name = "Coca-Cola",
 image = "colasoda.png",
 spawnRate = 1,
 onUse = function(id)
  dayZ.addThirst(id, -35)
  msg2(id,"thirst decreased by 35%")
 end})
 
dayZ.addItem({ -- id: 1068
 name = "Root Beer",
 image = "rootbeer.png",
 spawnRate = 1,
 onUse = function(id)
  dayZ.addThirst(id, -35)
  msg2(id,"thirst decreased by 35%")
 end})
 
dayZ.addItem({ -- id: 1069
 name = "Dr. Pepper",
 image = "sodadrpepper.png",
 spawnRate = 1,
 onUse = function(id)
  dayZ.addThirst(id, -35)
  msg2(id,"thirst decreased by 35%")
 end})
 
dayZ.addItem({ -- id: 1070
 name = "Ginger Ale",
 image = "sodagingerale.png",
 spawnRate = 1,
 onUse = function(id)
  dayZ.addThirst(id, -35)
  msg2(id,"thirst decreased by 35%")
 end})
 
dayZ.addItem({ -- id: 1071
 name = "Apple Pop",
 image = "applepop.png",
 spawnRate = 1,
 onUse = function(id)
  dayZ.addThirst(id, -35)
  msg2(id,"thirst decreased by 35%")
 end})
 
dayZ.addItem({ -- id: 1072
 name = "Cherry Juice Box",
 image = "cherryjuicebox.png",
 spawnRate = 1,
 onUse = function(id)
  dayZ.addThirst(id, -30)
  msg2(id,"thirst decreased by 30%")
 end})
 
dayZ.addItem({ -- id: 1073
 name = "Grape Juice",
 image = "grapejuice.png",
 spawnRate = 1,
 onUse = function(id)
  dayZ.addThirst(id, -50)
  msg2(id,"thirst decreased by 50%")
 end})
 
dayZ.addItem({ -- id: 1074
 name = "Ginger Ale",
 image = "sodagingerale.png",
 spawnRate = 1,
 onUse = function(id)
  dayZ.addThirst(id, -35)
  msg2(id,"thirst decreased by 35%")
 end})
 
dayZ.addItem({ -- id: 1075
 name = "Pineapple Soda",
 image = "pineapplesoda.png",
 spawnRate = 1,
 onUse = function(id)
  dayZ.addThirst(id, -35)
  msg2(id,"thirst decreased by 35%")
 end})
 
dayZ.addItem({ -- id: 1076
 name = "Cup of Cola'n'Whiskey",
 image = "whiskeycola.png",
 spawnRate = 1,
 onUse = function(id)
  dayZ.addThirst(id, -30)
  msg2(id,"thirst decreased by 30%")
 end})
 
dayZ.addItem({ -- id: 1077
 name = "Cup Of Whiskey",
 image = "whiskeycup.png",
 spawnRate = 1,
 onUse = function(id)
  dayZ.addThirst(id, -20)
  msg2(id,"thirst decreased by 20%")
 end})
 
dayZ.addItem({ -- id: 1078
 name = "Grapefruit Soda",
 image = "sodagrapefruit.png",
 spawnRate = 1,
 onUse = function(id)
  dayZ.addThirst(id, -35)
  msg2(id,"thirst decreased by 35%")
 end})
 
dayZ.addItem({ -- id: 1079
 name = "Martini",
 image = "martini.png",
 spawnRate = 1,
 onUse = function(id)
  dayZ.addThirst(id, -10)
  msg2(id,"thirst decreased by 10%")
 end})
 
dayZ.addItem({ -- id: 1080
 name = "Lime Soda",
 image = "sodalime.png",
 spawnRate = 1,
 onUse = function(id)
  dayZ.addThirst(id, -35)
  msg2(id,"thirst decreased by 35%")
 end})
 
dayZ.addItem({ -- id: 1081
 name = "Grape Juice Box",
 image = "grapejuicebox.png",
 spawnRate = 1,
 onUse = function(id)
  dayZ.addThirst(id, -30)
  msg2(id,"thirst decreased by 30%")
 end})
 
dayZ.addItem({ -- id: 1082
 name = "Grape Soda",
 image = "sodagrape.png",
 spawnRate = 1,
 onUse = function(id)
  dayZ.addThirst(id, -35)
  msg2(id,"thirst decreased by 35%")
 end})
 
dayZ.addItem({ -- id: 1083
 name = "Orange Juice Box",
 image = "orangejuicebox.png",
 spawnRate = 1,
 onUse = function(id)
  dayZ.addThirst(id, -30)
  msg2(id,"thirst decreased by 30%")
 end})
