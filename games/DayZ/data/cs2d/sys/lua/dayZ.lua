dayZ = {}

dayZ.spawn_dist = 15
dayZ.pvp_damageFactor = 5
dayZ.ZombieSpawnChance = 3
dayZ.spawnLootChance = 1.5 --%
dayZ.lootOverflowPenalty = 50 -- decreases the chance of loot to spawn when theres this many items, when theres twice asmuch, the spawn multiplier is 50%
dayZ.deaths = {}
dayZ.blood = {}
dayZ.blood.max = 12000
dayZ.blood_loss = {}
dayZ.kills = {}
dayZ.inventory = {}
dayZ.inventory_limit = 32
dayZ.start_equipment = {{id=2,amount=1},{id=62,amount=1},{id=54,amount=1},{id=67,amount=1}}
dayZ.equip_prompt = {}
dayZ.zombie_kills = {}
dayZ.inv_size = 6
dayZ.hunger = {}
dayZ.thirst = {}
dayZ.decrease = {hunger=0.12,thirst=0.12}
dayZ.reservedItemsId = 1000
dayZ.items_onground = {}
dayZ.child_files = {"AI.lua","dayZitems.lua","dayZ_help.lua","dayZsave.lua","dayZadminCommands.lua"}
dayZ.baseDir = "sys/lua/dayZ/"
dayZ.backpack_image = {}
dayZ.spawned = {}
dayZ.dmgBonusWep = {}
dayZ.dmgBonusWep[10] = 5 --shotgun
dayZ.dmgBonusWep[11] = 5 --shotgun
dayZ.navArrow = {}
dayZ.readyToShout = {}
dayZ.playerLeftValid = false
dayZ.artifactItems = {}
dayZ.CustomItemsCount = 0

misc.mp_hud(true, true, false, true, true, true, true)

dayZ.armourValue = {{id=206,protection=0}, -- stealth armour
{id=205,protection=95}, -- super armour
{id=204,protection=0}, -- medic armour
{id=203,protection=75}, -- heavy armour
{id=202,protection=50}, -- medium armour
{id=201,protection=25}} -- light armour

function math.round(num,base)
	if base == nil then
		return math.floor(num+0.5)
	else
		return math.floor((num*base)+0.5)/base
	end
end

function file_exists(file_name)
	local file_id = io.open(file_name,"r")
	if file_id == nil then
		return false
	else
		file_id:close()
		return true
	end
end
-- file check --
msg("checking LUA files...")
-- i = 0
-- while (i < #dayZ.child_files) do
	-- i = i + 1
	-- msg(i)
	-- if (file_exists(dayZ.baseDir..dayZ.child_files[i])) then
		-- msg(dayZ.baseDir..dayZ.child_files[i])
		-- dofile(dayZ.baseDir..dayZ.child_files[i])
		-- msg("�000999000loaded: "..dayZ.baseDir..dayZ.child_files[i].." successfully!")
	-- else
		-- msg("�999000000WARNING: unable to find the file: "..dayZ.baseDir..child_files[i].."!")
	-- end
	-- msg(#dayZ.child_files)
-- end
dofile("sys/lua/dayZ/AI.lua")
dofile("sys/lua/dayZ/dayZitems.lua")
dofile("sys/lua/dayZ/dayZ_help.lua")
dofile("sys/lua/dayZ/dayZsave.lua")
dofile("sys/lua/dayZ/dayZadminCommands.lua")
--dofile("sys/lua/dayZ/dayZcars.lua")
-- TEMPORARY SOLUTION
msg("checking item images...")
i = 0
while (i < #dayZ.items) do
	i = i + 1
	if (file_exists(dayZ.items[i].img) == false) then
		msg(misc.rgb(255,0,0).."WARNING: unable to find the file: "..dayZ.items[i].img)
	end
end
msg("checking zombie images...")
i = 0
while (i < 6) do
	i = i + 1
	if (file_exists("gfx/dayZ/zombie_ ("..i..").png") == false) then
		msg(misc.rgb(255,0,0).."WARNING: unable to find the file: gfx/dayZ/zombie_ ("..i..").png")
	end
end
msg("checking help images...")
if (file_exists("gfx/dayZ/help_1.png") == false) then
	msg(misc.rgb(255,0,0).."WARNING: unable to find the file: gfx/dayZ/zombie_ ("..i..").png")
end

-- plugin --
if (MADmin == nil) == false then -- lua file MADmin.lua detected
	msg("MADmin was Detected!")
	addhook("say","dayZ.commandsSay") -- hook: "say"
end

function dayZ.playerSpeak(id,text)
	-- if text == "!leave" then
		-- dayZsave.leave(id,"")
		-- parse("strip "..id.." 0")
		-- parse("customkill 0 \"Leave\" "..id)
	-- end
end

addhook("startround","dayZ.startround")
function dayZ.startround(mode)
    parse("mp_damagefactor 1")
	parse("sv_gamemode 1")
	parse("mp_radar 0")
	parse("sv_fow 1")
	parse("mp_killbuildings 3")
	parse("mp_dispenser_money 200")
	parse("mp_startmoney 0")
	parse("mp_dmspawnmoney 0")
	parse("mp_weaponfadeout 120")
	parse("mp_randomspawn 1")
	parse("mp_turretdamage 5")
	parse("mp_idleaction 4")
	parse("mp_supply_items 67,69,85,51,54,79,65,74,80")
	parse("mp_building_limit dispenser 10")
	parse("mp_building_limit turret 50")
	parse("mp_building_limit supply 10")
	parse("mp_building_limit \"gate field\" 50")

	--parse("mp_building_limit \"teleport entrance\" 3")
	--parse("mp_building_limit \"teleport exit\" 3")
end
dayZ.startround(mode) -- server start

addhook("join","dayZ.join")
function dayZ.join(id)
	parse("hudtxt 1 \"active zombies: "..#dayZ.Ai.aiList.."\" 2 116 0")
end

addhook("spawn","dayZ.spawn")
function dayZ.spawn(id)
	dayZ.readyToShout[id] = true
	timer(1,"parse","strip "..id.." 0")
	dayZ.spawned[id] = true
	if player(id,"usgn") == 0 then
		msg2(id,misc.rgb(255,0,0).."you are not logged in to USGN, so your progress wont be saved!")
		dayZ.deaths[id] = 0
		dayZ.default_spawn(id)
	else
		if file_exists("sys/lua/dayZ/saves/usgn_"..player(id,"usgn")..".txt") then
			local file = io.open("sys/lua/dayZ/saves/usgn_"..player(id,"usgn")..".txt","r")
			data = file:read()
			if data == nil then
				msg2(id,misc.rgb(255,0,0).."your save is EMPTY")
				msg2(id,misc.rgb(255,0,0).."please contact admin")
				file:close()
				dayZ.deaths[id] = 0
				dayZ.default_spawn(id)
			else
				if data == "ALIVE" then
					file:close()
					msg2(id,misc.rgb(255,0,0).."welcome back, your save has been loaded")
					dayZsave.load(id)
				else -- DEAD
					msg2(id,misc.rgb(255,0,0).."welcome back, you where dead last time")
					local death_text = file:read() -- useless data
					dayZ.deaths[id] = tonumber(file:read())
					file:close()
					msg2(id,"you got a total of: "..dayZ.deaths[id].." deaths")
					dayZ.default_spawn(id)
				end
			end
		else
			msg2(id,"we dected that you have no save file (its your first time?)")
			msg2(id,misc.rgb(0,255,0).."WELCOME to our dayZ, please enjoy")
			if player(id,"usgn") > 0 then
				msg("Welcome "..player(id,"name").."! (first time playing!)")
			end
			dayZ.deaths[id] = 0
			dayZ.default_spawn(id)
		end
	end
	if (dayZ.backpack_image[id] == nil) == false then
		freeimage(dayZ.backpack_image[id])
		dayZ.backpack_image[id] = nil
	end
	dayZ.backpack_image[id] = image("gfx/dayZ/backpack.png",1,0,200+id)
end

function dayZ.default_spawn(id)
	if math.random(0,100) < 25 then
		if math.random(0,100) < 25 then
			dayZ.inventory[id] = {{id=68,amount=1}}
		else
			dayZ.inventory[id] = {{id=67,amount=1}}
		end
	else
		dayZ.inventory[id] = {}
	end
	recalculate_itemAmount(id)
	dayZ.blood[id] = dayZ.blood.max
	dayZ.blood_loss[id] = 0
	dayZ.zombie_kills[id] = 0
	dayZ.hunger[id] = 0
	dayZ.thirst[id] = 0
	parse("hudtxt 1 \"active zombies: "..#dayZ.Ai.aiList.."\" 2 116 0")
	dayZ.update_blood(id)
	dayZ.update_hunger_thirst(id)
	parse("hudtxt2 "..id.." 4 \"zombie kills: "..dayZ.zombie_kills[id].."\" 2 164 0")
	parse("hudtxt2 "..id.." 5 \"Inventory: ("..dayZ.inventory[id].itemAmount.." out of "..dayZ.inventory_limit..")\" 2 180 0")

	--parse("hudtxt2 "..id.." 6 \"Hunger: "..dayZ.hunger[id].."%\" 2 212 0")
	--parse("hudtxt2 "..id.." 7 \"Thirst: "..dayZ.thirst[id].."%\" 2 228 0")
end

function dayZ.deathArrow(id,x,y)
	local pl = player(0,"tableliving")
	local i = 0
	while (i < #pl) do
		i = i + 1
		if (pl[i] == id) == false then
			local ii = 1
			while ((dayZ.navArrow[ii] == nil) == false) do
				ii = ii + 1
			end
			dayZ.navArrow[ii] = image("gfx/hud_arrow.bmp",player(pl[i],"x"),player(pl[i],"y"),1,pl[i])
			local dir = point_direction(player(pl[i],"tilex"),player(pl[i],"tiley"),x,y)
			imagepos(dayZ.navArrow[ii],player(pl[i],"x"),player(pl[i],"y"),dir)
			tween_scale(dayZ.navArrow[ii],2500,1,5)
			imagecolor(dayZ.navArrow[ii],255,0,0)
			timer(3000,"dayZ.fadeNavArrow",tostring(ii))
			timer(5000,"dayZ.removeNavArrow",tostring(ii))
		end
	end
end

addhook("die","dayZ.die")
function dayZ.die(victim,killer,weapon,x,y)
	dayZ.deathArrow(victim,pixel_to_tile(x),pixel_to_tile(y))
	if dayZ.playerLeftValid == true then
		dayZ.playerLeftValid = false
		dayZ.leaveGame(victim)
	else
        dayZsave.die(victim,killer,weapon,x,y) -- trigger save (dayZsave.die)
        local to_spawn = math.random(1,5)
        local i = 0
        while (i < to_spawn) do
            i = i + 1
            dayZ.Ai.add_ai(x,y,200)
        end
        if dayZ.blood[victim] < 0 then
            msg(misc.rgb(255,0,0)..player(victim,"name").." Died while bleeding "..dayZ.blood_loss[victim].."!")
        end
        if dayZ.hunger[victim] > 100 then
            msg(misc.rgb(255,0,0)..player(victim,"name").." starved to death "..dayZ.blood_loss[victim].."!")
        end
        if dayZ.thirst[victim] > 100 then
            msg(misc.rgb(255,0,0)..player(victim,"name").." dehydrated to death "..dayZ.blood_loss[victim].."!")
        end
        dayZ.spawned[victim] = nil
        dayZ.hunger[victim] = 0
        dayZ.thirst[victim] = 0
        parse("sv_sound2 "..victim.." env/mystery.wav")
        dayZ.blood[victim] = "Death"
        dayZ.blood_loss[victim] = "Death"
        parse("hudtxt2 "..victim.." 2 \"Blood: (DEAD)\" 2 132 0")
        parse("hudtxt2 "..victim.." 3 \"Bleeding: (DEAD)\" 2 148 0")
        parse("hudtxt2 "..victim.." 5 \"Inventory: (DEAD)\" 2 180 0")
        parse("hudtxt2 "..victim.." 6 \"Hunger: (DEAD)\" 10 275 2")
        parse("hudtxt2 "..victim.." 7 \"Thirst: (DEAD)\" 10 300 2")
        parse("hudtxt2 "..victim.." 8 \"F3 : (DEAD)\" 2 196 0")
        x = pixel_to_tile(x)
        y = pixel_to_tile(y)
        if math.random(0,100) < 25 then
            dayZ.add_item(victim,1021,1) -- human flesh
        end
        local i = 0
        while (i < #dayZ.inventory[victim]) do
            i = i + 1
            local ii = 0
            while (ii < dayZ.inventory[victim][i].amount) do
                temp_x = math.random(-2,2)
                temp_y = math.random(-2,2)
                if tile(x+temp_x,y+temp_y,"walkable") and tile(x+temp_x,y+temp_y,"frame") > 0 then
                    ii = ii + 1
                    if dayZ.inventory[victim][i].id > dayZ.reservedItemsId then
                        dayZ.item_spawn(dayZ.inventory[victim][i].id-dayZ.reservedItemsId,x+temp_x,y+temp_y)
                    else
                        parse("spawnitem "..dayZ.inventory[victim][i].id.." "..x+temp_x.." "..y+temp_y.."")
                    end
                end
            end
        end
        dayZ.inventory[victim] = nil
        msg2(victim,misc.rgb(0,255,0).."you got "..dayZ.zombie_kills[victim].." zombie kills!")
        end_message(victim)
	end
end

function dayZ.leaveGame(id)
	dayZ.spawned[id] = nil
	dayZ.hunger[id] = 0
	dayZ.thirst[id] = 0
	dayZ.blood[id] = "Death"
	dayZ.blood_loss[id] = "Death"
	parse("hudtxt2 "..id.." 2 \"Blood: (DEAD)\" 2 132 0")
	parse("hudtxt2 "..id.." 3 \"Bleeding: (DEAD)\" 2 148 0")
	parse("hudtxt2 "..id.." 5 \"Inventory: (DEAD)\" 2 180 0")
	parse("hudtxt2 "..id.." 6 \"Hunger: (DEAD)\" 10 275 2")
	parse("hudtxt2 "..id.." 7 \"Thirst: (DEAD)\" 10 300 2")
	parse("hudtxt2 "..id.." 8 \"F3 : (DEAD)\" 2 196 0")
	dayZ.inventory[id] = nil
end

function end_message(id)
	if dayZ.zombie_kills[id] >= 100 then
		msg(player(id,"name").." killed "..dayZ.zombie_kills[id].." zombies!")
	end
	-- if dayZ.zombie_kills[id] >= 1000 then
		-- msg2(id,"�000999000your a PRO GOD, not even admin has gotten this many zombie kills!!! :O :O")
		-- return 0
	-- end
	-- if dayZ.zombie_kills[id] > 750 then
		-- msg2(id,"�000999000your a GOD survivor almost a 1000 KILLS")
		-- return 0
	-- end
	-- if dayZ.zombie_kills[id] > 500 then
		-- msg2(id,"�000999000your a PRO survivor!!! amazing work!!! :O")
		-- return 0
	-- end
	-- if dayZ.zombie_kills[id] > 250 then
		-- msg2(id,"�000999000your a PRO survivor! OMG :)")
		-- return 0
	-- end
	-- if dayZ.zombie_kills[id] > 150 then
		-- msg2(id,"�100999000thats some nice KILLING you got there!! :)")
		-- return 0
	-- end
	-- if dayZ.zombie_kills[id] > 100 then
		-- msg2(id,"�250750000quite ok killing!!! :|")
		-- return 0
	-- end
	-- if dayZ.zombie_kills[id] > 50 then
		-- msg2(id,"�500500000you brought some zombies down with you! :(")
		-- return 0
	-- end
	-- if dayZ.zombie_kills[id] > 25 then
		-- msg2(id,"�700300000ok for a newbie :(")
		-- return 0
	-- end
	-- if dayZ.zombie_kills[id] > 10 then
		-- msg2(id,"�800200000sad, you didnt do very well... :(")
		-- return 0
	-- end
	-- if dayZ.zombie_kills[id] > 5 then
		-- msg2(id,"�999000000your not good at this game... *sigh, atleast you got some down... D:")
		-- return 0
	-- end
	-- msg2(id,"�999000000*SIGH... YOUR A NOOB D: D: D:")
end

addhook("hit","dayZ.hit")
function dayZ.hit(id,source,weapon,hpdmg,apdmg)
    local rawDamage = hpdmg*dayZ.pvp_damageFactor
    local damageSourceName = "unknown"
    if (source > 0) and (player(source,"exists")) then
        damageSourceName = player(source,"name")
    end
    if dayZ.pvp_damageFactor == 0 then
        msg2(source,misc.rgb(0,255,0).."(PVP is OFF)")
        msg2(id,misc.rgb(0,255,0).."(PVP is OFF) took no damage from "..damageSourceName)
        return 1
    end
    local danage = misc.calcDamage(id,rawDamage)
    msg2(id,misc.rgb(255,0,0).."(PVP)"..damageSourceName.." made you bleed for "..danage)
    dayZ.blood_loss[id] = dayZ.blood_loss[id] + dmg
    dayZ.update_blood(id)
    return 1
end

addhook("walkover","dayZ.walkover")
function dayZ.walkover(id,iid,item_type,ain,a,mode)
	if dayZ.spawned[id] == nil then
		dayZ.spawn(id)
	end
	if (a+ain) == (itemtype(item_type,"ammo")+itemtype(item_type,"ammoin")) then
		if dayZ.inventory[id].itemAmount < dayZ.inventory_limit then
			dayZ.add_item(id,item_type,1)
			msg2(id,"picked up \""..itemtype(item_type,"name").."\"")
			parse("sv_sound2 "..id.." items/pickup.wav")
			parse("removeitem "..iid)
			if itemtype(item_type,"slot") > 0 then
				sme.createMenu(id,dayZ.walkoverEquipMenu,sme.noProcess,"Equip "..itemtype(item_type,"name").."?",false,{"Yes", "No"}, false, item_type)
			end
		end
		return 1
	else
		msg2(id,misc.rgb(255,0,0)..itemtype(item_type,"name").." cant be put in bag!")
		return 0
	end
end

function dayZ.walkoverEquipMenu(id, button, data, item)
	if (button == 1) then
		dayZ.remove_item(id,item,1)
		parse("equip "..id.." "..item)
		parse("setweapon "..id.." "..item)
	end
	return 0
end

addhook("use","dayZ.use")
function dayZ.use(id,event,data,x,y)
	x = player(id,"tilex")
	y = player(id,"tiley")
	local item = dayZ.get_item_id(x,y)
	if item > 0 then
		if dayZ.inventory[id].itemAmount < dayZ.inventory_limit then
			msg2(id,"picked up \""..dayZ.items[item].name.."\"")
			dayZ.add_item(id,item+dayZ.reservedItemsId,1)
			dayZ.item_remove(x,y)
			parse("sv_sound2 "..id.." items/pickup.wav")
		end
	end
end

addhook("movetile","dayZ.movetile")
function dayZ.movetile(id,x,y)
	parse("hudtxt2 "..id.." 9 \"Location: "..x..", "..y.."\" 2 212 0")
	-- local item = dayZ.get_item_id(x,y)
	-- if item > 0 then
		-- if dayZ.inventory[id].itemAmount < dayZ.inventory_limit then
			-- msg2(id,"picked up \""..dayZ.items[item].name.."\"")
			-- dayZ.add_item(id,item+dayZ.reservedItemsId,1)
			-- dayZ.item_remove(x,y)
			-- parse("sv_sound2 "..id.." items/pickup.wav")
		-- end
	-- end
end

function dayZ.item_spawn(id,tilex,tiley)
	local x = tile_to_pixel(tilex)
	local y = tile_to_pixel(tiley)
	dayZ.CustomItemsCount = dayZ.CustomItemsCount + 1
	if dayZ.items_onground[(tilex).."|"..(tiley)] == nil then
		dayZ.items_onground[(tilex).."|"..(tiley)] = {}
		dayZ.items_onground[(tilex).."|"..(tiley)][1] = {id=id,img=image(dayZ.items[id].img,x,y,0)}
	else
		local up_add = #dayZ.items_onground[(tilex).."|"..(tiley)]*3
		local mode = 0
		if #dayZ.items_onground[(tilex).."|"..(tiley)] > 8 then
			mode = 1
		end
		dayZ.items_onground[(tilex).."|"..(tiley)][#dayZ.items_onground[(tilex).."|"..(tiley)]+1] = {id=id,img=image(dayZ.items[id].img,x,y-up_add,mode)}
		scale = 1
		scale = scale - ((#dayZ.items_onground[(tilex).."|"..(tiley)]-1)*0.01)
		imagescale(dayZ.items_onground[(tilex).."|"..(tiley)][#dayZ.items_onground[(tilex).."|"..(tiley)]].img,scale,scale)
		imagepos(dayZ.items_onground[(tilex).."|"..(tiley)][#dayZ.items_onground[(tilex).."|"..(tiley)]].img,x,y-up_add,math.random(0,360))
	end
end

function dayZ.get_item_id(tilex,tiley)
	local groundItems = dayZ.items_onground[(tilex).."|"..(tiley)]
	if (groundItems == nil) == false then
		return groundItems[#groundItems].id -- last item on stack
	end
	return 0
end

function dayZ.item_remove(tilex,tiley)
	freeimage(dayZ.items_onground[(tilex).."|"..(tiley)][#dayZ.items_onground[(tilex).."|"..(tiley)]].img)
	dayZ.items_onground[(tilex).."|"..(tiley)][#dayZ.items_onground[(tilex).."|"..(tiley)]] = nil
	if #dayZ.items_onground[(tilex).."|"..(tiley)] == 0 then
		dayZ.items_onground[(tilex).."|"..(tiley)] = nil
	end
	dayZ.CustomItemsCount = dayZ.CustomItemsCount - 1
end

local map_name = string.lower(map("name"))
print("MAPNAME: "..map_name)
dayZ.spawn_loot_tile = {-1}
if (map_name == "zm_cs2dtown_kgb2d") then
	dayZ.spawn_loot_tile = {33,34,35}
end
if (map_name == "zm_cs2dtown_kgb2d_v5test") then
	dayZ.spawn_loot_tile = {33,34,35}
end
if (string.sub(map_name,1,14) == "zm_zoluthsamam") then
	dayZ.spawn_loot_tile = {33,34,35}
end
if (map_name == "dayz") then
	dayZ.spawn_loot_tile = {8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,38,39}
end
if (map_name == "dayZ zombie survival v.1") then
	dayZ.spawn_loot_tile = {6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,43,44,45,46,47,48,49,50,51,52,53,54,135,136,137,138,139,154}
end
if (map_name == "test_zone") then
	dayZ.spawn_loot_tile = {7}
end
if dayZ.spawn_loot_tile[1] == -1 then
	msg(misc.rgb(255,0,0).."WARNING: unable to find spawn tiles!")
else
	msg(misc.rgb(0,255,0).."Found the spawn tiles!")
end

dayZ.spawn_weapons = {1,2,3,4,5,6,10,11,20,21,22,23,24,30,31,32,33,34,35,36,37,38,39,40,41,46,47,48,49,51,52,53,54,57,58,59,60,61,62,64,65,66,67,68,69,72,73,74,76,77,79,80,81,82,84,85,87,89,90,91}
function spawn_loot(multiplier)
	if (math.random(0,100) < (dayZ.spawnLootChance*multiplier)) then
		attempt = 0
		while (attempt < 50) do
			attempt = attempt + 1
			x = math.random(0,map("xsize"))
			y = math.random(0,map("ysize"))
			if tile(x,y,"walkable") then
				frame = tile(x,y,"frame")
				local i = 0
				while(i < #dayZ.spawn_loot_tile) do
					i = i + 1
					if frame == dayZ.spawn_loot_tile[i] then
						--print("spawned item at X: "..x..",Y: "..y)
						--[[
						if math.random(0,100) < 35 then
							if math.random(0,100) < 40 then
								if math.random(0,100) < 50 then
									if math.random(0,100) < 25 then
										parse("spawnitem 64 "..x.." "..y) -- medkit
									else
										parse("spawnitem 65 "..x.." "..y) -- bandage
									end
								else
									parse("spawnitem 61 "..x.." "..y) -- primary ammo
								end
							else
								parse("spawnitem "..dayZ.spawn_weapons[misc.round(math.random(1,#dayZ.spawn_weapons))].." "..x.." "..y)
							end
						else
							dayZ.item_spawn(math.random(1,#dayZ.items),x,y)
						end
						]]--
						dayZ.spawnTileLoot(x, y)
						return 0
					end
				end
			end
		end
		print(misc.rgb(255,0,0).."WARNING: spawn item TIMEOUT! (50 failed attempts)")
	end
end

function dayZ.spawnTileLoot(x, y)
	if math.random(0,100) < 10 then
		-- health items
		if math.random(0,100) < 10 then
			-- bloodbag
			dayZ.item_spawn(1001,x,y)
		else
			-- bandage
			parse("spawnitem 65 "..x.." "..y)
		end
	else
		if math.random(0,100) < 20 then
			-- cs2d item
			parse("spawnitem "..dayZ.spawn_weapons[misc.round(math.random(1,#dayZ.spawn_weapons))].." "..x.." "..y)
		else
			-- DayZ item
			dayZ.item_spawn(math.random(1,#dayZ.items),x,y)
		end
	end
end

addhook("second","dayZ.second")
function dayZ.second()
    pl = player(0,"tableliving")
    local itemMultiplier = #pl/math.min(1,dayZ.CustomItemsCount/dayZ.lootOverflowPenalty)
    spawn_loot(itemMultiplier)
    i = 0
    while (i < #pl) do
        i = i + 1
        -- hunger/thirst
        dayZ.hunger[pl[i]] = dayZ.hunger[pl[i]] + dayZ.decrease.hunger
        if dayZ.hunger[pl[i]] >= 100 then
            --parse("customkill 0 \"Hunger\" "..pl[i])
            parse("sethealth "..pl[i].." "..player(pl[i],"health")-(dayZ.hunger[pl[i]]-100))
            msg2(pl[i],misc.rgb(255,0,0).."you are dying by hunger!")
        end
        dayZ.thirst[pl[i]] = dayZ.thirst[pl[i]] + dayZ.decrease.thirst
        if dayZ.thirst[pl[i]] >= 100 then
            --parse("customkill 0 \"Thirst\" "..pl[i])
            parse("sethealth "..pl[i].." "..player(pl[i],"health")-(dayZ.thirst[pl[i]]-100))
            msg2(pl[i],misc.rgb(255,0,0).."you are dying by thirst!")
        end
        if player(pl[i],"health") > 0 then
        dayZ.update_hunger_thirst(pl[i])
        -- blood loss
        if dayZ.blood_loss[pl[i]] > 0 then
            if (dayZ.blood_loss[pl[i]] > 25) then
                if (dayZ.blood_loss[pl[i]] > 100) then
                    if (dayZ.blood_loss[pl[i]] > 200) then
                        if (dayZ.blood_loss[pl[i]] > 500) then
                            msg2(pl[i],misc.rgb(250,0,0).."HOW AM I STILL ALIVE!!!")
                        else
                            msg2(pl[i],misc.rgb(150,50,0).."bleeding VERY MUCH!!")
                        end
                    else
                        msg2(pl[i],misc.rgb(50,150,0).."bleeding alot!")
                    end
                else
                    msg2(pl[i],misc.rgb(0,250,0).."bleeding a bit!")
                end
            end
            dayZ.blood[pl[i]] = math.round(dayZ.blood[pl[i]]-dayZ.blood_loss[pl[i]],10)

            --dayZ.update_blood(pl[i])
            if dayZ.blood[pl[i]] <= 0 then
                parse("customkill 0 \"BloodLoss\" "..pl[i])
            else
            	if (dayZ.blood_loss[pl[i]] ~= 0) then
                    dayZ.blood_loss[pl[i]] = dayZ.blood_loss[pl[i]] - 0.1
	                if player(pl[i],"armor") == 204 then -- medic armour
	                    dayZ.blood_loss[pl[i]] = dayZ.blood_loss[pl[i]] - 2.5
	                end
	                if dayZ.blood_loss[pl[i]] < 0 then
	                    dayZ.blood_loss[pl[i]] = 0
	                end
	                dayZ.update_blood(pl[i])
                end
            end
        end
        end
        -- spawning
        x = player(pl[i],"x")
        y = player(pl[i],"y")
        ii = 0
        while (ii < 25) do
            ii = ii + 1
            if math.random(0,100) < dayZ.ZombieSpawnChance then
                rnd_dir = math.random(0,360)
                temp_x = x+lengthdir_x(rnd_dir,32*dayZ.spawn_dist)
                temp_y = y+lengthdir_y(rnd_dir,32*dayZ.spawn_dist)
                local frame = tile(pixel_to_tile(temp_x),pixel_to_tile(temp_y),"frame")
                if tile(pixel_to_tile(temp_x),pixel_to_tile(temp_y),"walkable") and frame > 0 then
                    allow_spawn = true
                    if (allow_spawn) then
	                    for _,lootFrame in ipairs(dayZ.spawn_loot_tile) do
	                    	if (frame == lootFrame) then
	                    		allow_spawn = false
	                    		break
	                    	end
	                	end
	                end
                	if (allow_spawn) then
	                    iii = 0
	                    while (iii < #pl) do
	                        iii = iii + 1
	                        --if dayZ.Ai.collision_detection(player(pl[iii],"x"),player(pl[iii],"y"),temp_x,temp_y,32*5) == true then
	                        if (misc.point_distance(player(pl[iii],"x"),player(pl[iii],"y"),temp_x,temp_y) < 400) then
	                            allow_spawn = false
	                            break
	                        end
	                    end
	                end
                    if (allow_spawn) then
                        ii = 5
                        dayZ.Ai.add_ai(temp_x,temp_y)
                        iii = #pl
                    end
                end
            end
        end
    end
end

addhook("serveraction","dayZ.serveraction")
function dayZ.serveraction(id,action)
	if action == 1 then
		if player(id,"health") > 0 then
			sme.createMenu(id,dayZ.selectItem,dayZ.itemMenuProcessor,"DayZ Inventory",true,dayZ.inventory[id])
		end
	end
	if action == 2 then
		if (dayZ.item_count(id,65) <= 0) then
            msg2(id, "You dont have any bandages")
            return
        end
        if (dayZ.blood_loss[id] <= 0) then
            msg2(id, "You arent bleeding")
            return
        end
        dayZ.use_item(id,65)
        dayZ.remove_item(id,65,1)
        recalculate_itemAmount(id)
	end
end

function dayZ.itemMenuProcessor(index, item)
	local text = nil
	if (item.id > 0) then
		if (item.id > dayZ.reservedItemsId) then
			text = (dayZ.items[item.id-dayZ.reservedItemsId].name)
		else
			text = (itemtype(item.id,"name"))
		end
	end
	text = text.." (x"..item.amount..")"
	return text
end

function dayZ.selectItem(id, button, item)
	itemName = ""
	if (item.id > 0) then
		if (item.id > dayZ.reservedItemsId) then
			itemName = dayZ.items[item.id-dayZ.reservedItemsId].name
		else
			itemName = itemtype(item.id,"name")
		end
	end
	sme.createMenu(id,dayZ.itemAction,sme.noProcess,"Item: "..itemName,false,{"Use/Activate", "Drop 1", "Drop 5", "Drop all"}, false, item.id)
end

function dayZ.itemAction(id, button, item, itemId)
	if player(id,"health") <= 0 then
		return 0
	end
	if (button == 1) then
		if itemId > dayZ.reservedItemsId then
			dayZ.items[itemId-dayZ.reservedItemsId].use(id)
			dayZ.remove_item(id,itemId,1)
		else
			dayZ.remove_item(id,itemId,1)
			parse("equip "..id.." "..itemId)
			parse("setweapon "..id.." "..itemId)
			dayZ.use_item(id,itemId)
		end
		recalculate_itemAmount(id)
	end
	if (button == 2) then
		if itemId > dayZ.reservedItemsId then
			dayZ.item_spawn(itemId-dayZ.reservedItemsId,player(id,"tilex"),player(id,"tiley"))
			msg2(id,"You dropped 1 "..dayZ.items[itemId-dayZ.reservedItemsId].name)
		else
			parse("spawnitem "..itemId.." "..player(id,"tilex").." "..player(id,"tiley"))
			msg2(id,"You dropped 1 "..itemtype(itemId,"name"))
		end
		dayZ.remove_item(id,itemId,1)
	end
	if (button == 3) then
		local toDrop = math.min(5, dayZ.item_count(id,itemId))
		local i = 0
		while (i < toDrop) do
			i = i + 1
			dayZ.spawn_item(itemId,player(id,"tilex"),player(id,"tiley"))
		end
		dayZ.remove_item(id,itemId,toDrop)
		if itemId > dayZ.reservedItemsId then
			msg2(id,"You dropped "..toDrop.." "..dayZ.items[itemId-dayZ.reservedItemsId].name)
		else
			msg2(id,"You dropped "..toDrop.." "..itemtype(itemId,"name"))
		end
	end
	if (button == 4) then
		local org = dayZ.item_count(id,itemId)
		local i = org
		while (i > 0) do
			i = i - 1
			dayZ.spawn_item(itemId,player(id,"tilex"),player(id,"tiley"))
		end
		dayZ.remove_item(id,itemId,org)
		if itemId > dayZ.reservedItemsId then
			msg2(id,"You dropped "..org.." "..dayZ.items[itemId-dayZ.reservedItemsId].name)
		else
			msg2(id,"You dropped "..org.." "..itemtype(itemId,"name"))
		end
	end
	return 0
end

function dayZ.spawn_item(item,x,y) --works with both
	if item > dayZ.reservedItemsId then
		dayZ.item_spawn(item-dayZ.reservedItemsId,x,y)
	else
		parse("spawnitem "..item.." "..x.." "..y)
	end
end

function dayZ.add_item(player_id,item_type,amount)
	local free_item_slots = dayZ.inventory_limit - dayZ.inventory[player_id].itemAmount
	if amount > free_item_slots then
		amount = free_item_slots
	end
	local to_add = true
	local i = 0
	while (i < #dayZ.inventory[player_id]) do
		i = i + 1
		if dayZ.inventory[player_id][i].id == item_type then
			to_add = false
			dayZ.inventory[player_id][i].amount = dayZ.inventory[player_id][i].amount + amount
			dayZ.equip_prompt[player_id] = i
		end
	end
	if to_add == true then
		dayZ.inventory[player_id][#dayZ.inventory[player_id]+1] = {id=item_type,amount=amount}
		dayZ.equip_prompt[player_id] = #dayZ.inventory[player_id]
	end
	recalculate_itemAmount(player_id)
end

function dayZ.item_count(player_id,item_id)
	local i = 0
	while (i < #dayZ.inventory[player_id]) do
		i = i + 1
		if dayZ.inventory[player_id][i].id == item_id then
			return dayZ.inventory[player_id][i].amount
		end
	end
	return 0
end


function dayZ.remove_item(player_id,item_id,amount)
	local i = 0
	while (i < #dayZ.inventory[player_id]) do
		i = i + 1
		if dayZ.inventory[player_id][i].id == item_id then
			dayZ.inventory[player_id][i].amount = dayZ.inventory[player_id][i].amount - amount
			if dayZ.inventory[player_id][i].amount < 1 then
				-- remove item
				dayZ.inventory[player_id][i] = dayZ.inventory[player_id][#dayZ.inventory[player_id]]
				dayZ.inventory[player_id][#dayZ.inventory[player_id]] = nil
				-- remove item end
			end
		end
	end
	recalculate_itemAmount(player_id)
end

function recalculate_itemAmount(id)
	dayZ.inventory[id].itemAmount = 0
	local i = 0
	while (i < #dayZ.inventory[id]) do
		i = i + 1
		dayZ.inventory[id].itemAmount = dayZ.inventory[id].itemAmount + dayZ.inventory[id][i].amount
	end
	parse("hudtxt2 "..id.." 5 \"Inventory: ("..dayZ.inventory[id].itemAmount.." out of "..dayZ.inventory_limit..")\" 2 180 0")
	f3_item = 0
	if (dayZ.item_count(id,65) > 0) then
		f3_item = 65
	end
	if f3_item > 0 then
		parse("hudtxt2 "..id.." 8 \"F3 : "..itemtype(f3_item,"name").." x"..dayZ.item_count(id,65).."\" 2 196 0")
	else
		parse("hudtxt2 "..id.." 8 \"F3 : (noone)\" 2 196 0")
	end
end

function dayZ.use_item(id,item)
	if item == 64 then -- medkit
		msg2(id,misc.rgb(0,255,0).."Used MedKit!")
		dayZ.blood_loss[id] = 0
		dayZ.update_blood(id)
	end
	if item == 65 then -- bandage
		msg2(id,misc.rgb(0,255,0).."Used Bandage!")
		dayZ.blood_loss[id] = dayZ.blood_loss[id] - 500
		if (dayZ.blood_loss[id] < 0) then
			dayZ.blood_loss[id] = 0
		end
		dayZ.update_blood(id)
	end
end

addhook("buildattempt","dayZ.buildattempt")
function dayZ.buildattempt(id,type,x,y)
	-- msg2(id,"�999000000building is currently disabled")
	-- return 1
end

function dayZ.update_blood(id)
	parse("hudtxt2 "..id.." 2 \"Blood: "..dayZ.blood[id].."\" 2 132 0")
	extra = "status: not bleeding"
	if (dayZ.blood_loss[id] > 0) then
		local timer = math.round(dayZ.blood[id]/dayZ.blood_loss[id],10)
		if timer < 20 then
			extra = "Death IMMINENT In ~"..timer.." Seconds"
		else
			extra = "Death In ~"..timer.." Seconds"
		end
	end
	parse("hudtxt2 "..id.." 3 \"Bleeding: "..dayZ.blood_loss[id].." ("..extra..")\" 2 148 0")
end

function length_3(str)
	while (string.len(str) < 3) do
		str = "0"..str
	end
	return str
end

function dayZ.update_hunger_thirst(id)
	temp = math.round(dayZ.hunger[id]*2.55)
	temp2 = 255-temp
	temp = length_3(tostring(temp))
	temp2 = length_3(tostring(temp2))
	parse("hudtxt2 "..id.." 6 \""..misc.copyrightSymbol..temp..""..temp2.."000Hunger: "..math.round(dayZ.hunger[id]).."%\" 10 275 0")

	temp = math.round(dayZ.thirst[id]*2.55)
	temp2 = 255-temp
	temp = length_3(tostring(temp))
	temp2 = length_3(tostring(temp2))
	parse("hudtxt2 "..id.." 7 \""..misc.copyrightSymbol..temp..""..temp2.."000Thirst: "..math.round(dayZ.thirst[id]).."%\" 10 300 0")
end

function dayZ.fadeNavArrow(i)
	tween_alpha(dayZ.navArrow[tonumber(i)],2000,0)
end

function dayZ.removeNavArrow(i)
	freeimage(dayZ.navArrow[tonumber(i)])
	dayZ.navArrow[tonumber(i)] = nil
end

function dayZ.readyShout(id)
	dayZ.readyToShout[tonumber(id)] = true
end
