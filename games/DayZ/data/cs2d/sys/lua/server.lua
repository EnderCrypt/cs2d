-- library lua
dofile("sys/lua/MiscLibrary.lua")
dofile("sys/lua/timer2.lua")
dofile("sys/lua/servertransfer.lua")
dofile("sys/lua/BasicMenuEngine.lua")

-- secondary lua
dofile("sys/lua/ShieldCover.lua")
dofile("sys/lua/furniture.lua")

-- primary lua
dofile("sys/lua/dayZ.lua")

-- finalize
serverTransfer.finalize()
misc.mp_hud(true,true,true,true,true,true,true)
