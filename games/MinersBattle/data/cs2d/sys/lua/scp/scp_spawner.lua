addhook("startround","scp.hook.startround")
function scp.hook.startround(mode)
    local i = 0
    while (i < 5) do
        i = i + 1
        local timeout = 0
        while (timeout < 100) do
            timeout = timeout + 1
            local x = math.ceil(math.random()*const.map.xsize)
            local y = math.ceil(math.random()*const.map.ysize)
            if ((tile(x,y,"frame") > 0) and (tile(x,y,"walkable"))) then
                local scpID = scp.scps[math.ceil(math.random()*#scp.scps)]
                if (not(scpID == 106)) then
                    scp.spawn(scpID,x,y)
                    break;
                end
            end
        end
        if (timeout == 100) then
            print("spawning SCP failed!")
        end
    end
end

scp.hook.startround(0)