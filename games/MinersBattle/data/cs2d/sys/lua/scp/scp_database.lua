function scp.isInsideScreen(px,py,x,y)
		local screen_width = 640/2
		local screen_height = 480/2
		return ((px < x+screen_width) and (py < y+screen_height) and (px > x-screen_width) and (py > y-screen_height))
end

function scp.isSeeing(rot,px,py,x,y,vision_narrow)
		if (vision_narrow == nil) then
				vision_narrow = 75 -- used to determine the angle which the point we check against must be inside
		end
		local dir = misc.point_direction(px,py,x,y)
		angdiff = math.abs(((((rot - dir) % 360) + 540) % 360) - 180)
		if (angdiff < vision_narrow) then
				return true
		else
				return false
		end
end

function scp.isLineOfSight(x1,y1,x2,y2)
		local dist = misc.point_distance(x1,y1,x2,y2)
		local dir = misc.point_direction(x1,y1,x2,y2)
		local check_distance = 32
		local check_x = x1
		local check_y = y1
		local add_x = misc.lengthdir_x(dir,check_distance)
		local add_y = misc.lengthdir_y(dir,check_distance)
		local i = 0
		while (i < math.floor(dist/check_distance)) do
				i = i + 1
				check_x = check_x + add_x
				check_y = check_y + add_y
				--misc.debugImage(check_x,check_y)
				if tile(misc.pixel_to_tile(check_x),misc.pixel_to_tile(check_y),"wall") then
						return false
				end
		end
		return true
end

function scp.soundPosition(scp_num,fileName,x,y) --plays sound for any player with x,y inside their screen
		local pl = player(0,"tableliving")
		local i = 0
		while (i < #pl) do
				i = i + 1
				local id = pl[i]
				if (scp.isInsideScreen(player(id,"x"),player(id,"y"),x,y)) then
						parse("sv_sound2 "..id.." \"scp/"..scp_num.."/"..fileName.."\"")
				end
		end
end

--[[ TEMPLATE
scp.create(SCP_ID,SCP_NAME,function(me) --create

end,function(me,pl) --update

end,PIXEL_SIZE,function(me,pl,pl_touch) --touch

end,function(me,pl) --destroy

end)
]]--

--[[
the me variable contains:
scpID, the SCP id
x and y, the position (matters for automatic collision detection)
image, the image variable
destroy, a boolean variable set to false, switching it to true will delete the SCP automatically (this will trigger the onDestroy callback ps. dont freeimage(me.image), thats automatic)
data = {}, used for storing any data you wanna store
]]--

---------------
--- SCP 96 ---
---------------
misc.addServerTransfer("sfx/scp/96/scream.ogg")

scp.create(96,"The Shy Guy",function(me) --create
		me.data.locations_index = 0
		me.data.locations = {}
		me.data.anger = 0
		me.data.target = 0
		me.data.rot = math.random(360)-180
		tween_rotate(me.image,2500,me.data.rot)
end,function(me,pl) --update
		if (me.data.target == 0) then
				me.data.anger = me.data.anger - 1
				if (me.data.anger < 0) then
						me.data.anger = 0
				end
				local i = 0
				while (i < #pl) do
						i = i + 1
						if scp.isInsideScreen(pl[i].x,pl[i].y,me.x,me.y) then
								--local dist = misc.point_distance(pl[i].x,pl[i].y,me.x,me.y)
								--if (dist < 100) then
								local sensitivity = 64
								if ((pl[i].x < me.x+sensitivity) and (pl[i].y < me.y+sensitivity) and (pl[i].x > me.x-sensitivity) and (pl[i].y > me.y-sensitivity)) then
										local dir = misc.point_direction(me.x,me.y,pl[i].x,pl[i].y)
										-- MIGHT turn towards you...
										if (math.random() < 0.005) then
												me.data.rot = dir
												tween_move(me.image,1000,me.x,me.y,me.data.rot)
										end
										-- check
										local angdiff = math.abs(((((me.data.rot - dir) % 360) + 540) % 360) - 180)
										if (angdiff < 45) then
												if scp.isSeeing(player(pl[i].id,"rot"),pl[i].x,pl[i].y,me.x,me.y,35) then
														if scp.isLineOfSight(pl[i].x,pl[i].y,me.x,me.y) then
																if (math.random() < 0.5) then
																		--me.data.anger = me.data.anger + 10
																		--if (me.data.anger > 20) then
																				me.data.target = pl[i].id
																				parse("sv_sound2 "..me.data.target.." \"scp/96/scream.ogg\"")
																				me.data.locations_index = 0
																				me.data.locations = {}
																				me.data.anger = 0
																				--parse("explosion "..me.x.." "..me.y.." 32 0 0")
																		--end
																end
														end
												end
										end
								end
						end
				end
		else
				me.data.anger = me.data.anger + 1
				-- calculate
				if player(me.data.target,"exists") and (player(me.data.target,"health") > 0) then
						if (me.data.anger % 2 == 0) then
								me.data.locations[#me.data.locations+1] = {player(me.data.target,"x"),player(me.data.target,"y")}
						end
						if (me.data.anger < 100) then -- panic!
								local temp_x = me.x + math.random()*(me.data.anger*0.2)
								local temp_y = me.y + math.random()*(me.data.anger*0.2)
								local temp_rot = me.data.rot + math.random()*me.data.anger*2
								imagepos(me.image,temp_x,temp_y,temp_rot)
						else
								me.data.locations_index = me.data.locations_index + 1
								if (me.data.locations_index > #me.data.locations) then
										me.data.rot = misc.point_direction(me.x,me.y,player(me.data.target,"x"),player(me.data.target,"y"))
										scp.kill(me,me.data.target)
										--parse("customkill 0 \"SCP-96\" "..me.data.target)
										me.data.locations_index = 0
										me.data.target = 0
										me.data.anger = 0
								else
										local pl_position = me.data.locations[me.data.locations_index]
										local loc = me.data.locations_index
										while (misc.primitive_point_distance(me.x,me.y,pl_position[1],pl_position[2]) < 5) do
												me.data.locations_index = me.data.locations_index + 1
												pl_position = me.data.locations[me.data.locations_index]
										end
										--[[
										if (me.data.locations_index > loc) then
												msg("skipped: "..(me.data.locations_index-loc))
										end
										]]--
										me.data.rot = misc.point_direction(me.x,me.y,pl_position[1],pl_position[2])
										me.x = pl_position[1]
										me.y = pl_position[2]
										-- destroy buildings
										local tx = misc.pixel_to_tile(me.x)
										local ty = misc.pixel_to_tile(me.y)
										local object_id = objectat(tx, ty)
										if (object_id > 0) then
											local object_type = object(object_id, "type")
											if (object_type <= 21) then
												parse("explosion "..misc.tile_to_pixel(tx).." "..misc.tile_to_pixel(ty).." 10 10000 0")
											end
										end
										-- trigger doors
										if (entity(tx, ty, "exists") and entity(tx, ty, "typename") == "Func_DynWall" and entity(tx, ty, "state") == false) then
											parse("trigger "..entity(tx, ty, "name"))
										end
								end
								tween_move(me.image,100,me.x,me.y,me.data.rot)
						end
				else
						me.data.target = 0
						me.data.anger = 0
				end
		end
end,32,function(me,pl,pl_touch) --touch
		if ((me.data.locations_index > 0) and (me.data.target > 0)) then
				if (me.data.target == pl_touch.id) then
						if (me.data.anger >= 100) then
								me.data.rot = misc.point_direction(me.x,me.y,player(me.data.target,"x"),player(me.data.target,"y"))
								tween_move(me.image,100,me.x,me.y,me.data.rot)
								scp.kill(me,me.data.target)
								--parse("customkill 0 \"SCP-96\" "..me.data.target)
								me.data.target = 0
								me.data.anger = 0
						end
				else
						parse("sethealth "..pl_touch.id.." "..player(pl_touch.id,"health")-25)
				end
		end
end,function(me,pl) --destroy

end)

---------------
--- SCP 106 ---
---------------
misc.addServerTransfer("sfx/scp/106/laugh.ogg")
misc.addServerTransfer("sfx/scp/106/wall.ogg")

scp.create(106,"The Old Man",function(me) --create
		me.data.target = 0
		me.data.phased = false
		me.data.wall_check = function()
				if tile(misc.pixel_to_tile(me.x),misc.pixel_to_tile(me.y),"wall") then
						if not (me.data.phased) then -- phase
								tween_alpha(me.image,500,0.05)
								me.data.phased = true
								scp.soundPosition(106,"wall.ogg",me.x,me.y)
						end
				else
						if (me.data.phased) then -- unphase
								tween_alpha(me.image,5000,1)
								me.data.phased = false
								scp.soundPosition(106,"wall.ogg",me.x,me.y)
						end
				end
		end
end,function(me,pl) --update
		if (me.data.phased == false) then
				misc.effect("smoke",me.x,me.y,1,16,0,0,0)
		end
		if (me.data.target == 0) then
				-- try find a target
				local targets = {}
				local i = 0
				while (i < #pl) do
						i = i + 1
						if scp.isInsideScreen(pl[i].x,pl[i].y,me.x,me.y) then
								targets[#targets+1] = pl[i]
						end
				end
				if (#targets > 0) then
						me.data.target = targets[misc.round(math.random(1,#targets))].id
				else -- otherwise roam
						if (math.random() < 0.2) then
								local dir = math.random()*360
								me.x = me.x+misc.lengthdir_x(dir,4)
								me.y = me.y+misc.lengthdir_y(dir,4)
								tween_move(me.image,100,me.x,me.y,dir)
								me.data.wall_check()
						end
				end
		end
		if (me.data.target > 0) then
				if player(me.data.target,"exists") and (player(me.data.target,"health") > 0) then
						local dir = misc.point_direction(me.x,me.y,player(me.data.target,"x"),player(me.data.target,"y"))
						me.x = me.x+misc.lengthdir_x(dir,4)
						me.y = me.y+misc.lengthdir_y(dir,4)
						tween_move(me.image,100,me.x,me.y,dir)
						me.data.wall_check()
						-- check for new target
						if (math.random() < 0.05) then
								local max_dist = misc.point_distance(player(me.data.target,"x"),player(me.data.target,"y"),me.x,me.y)
								local new_target = 0
								local targets = {}
								local i = 0
								while (i < #pl) do
										i = i + 1
										if scp.isInsideScreen(pl[i].x,pl[i].y,me.x,me.y) then
												local dist = misc.point_distance(pl[i].x,pl[i].y,me.x,me.y)
												if ((dist*math.random(0.8,1.2)) < max_dist) then
														max_dist = dist
														new_target = pl[i].id
												end
										end
								end
								if (new_target > 0) then
										me.data.target = new_target
								end
						end
				else
						me.data.target = 0
				end
		end
end,32,function(me,pl,pl_touch) --touch
		scp.kill(me,pl_touch.id)
		parse("sv_sound2 "..pl_touch.id.." \"scp/106/laugh.ogg\"")
		--parse("customkill 0 \"SCP-106\" "..pl_touch.id)
end,function(me,pl) --destroy

end)

---------------
--- SCP 173 ---
---------------
misc.addServerTransfer("sfx/scp/173/horror_far.ogg")
misc.addServerTransfer("sfx/scp/173/horror_middle.ogg")
misc.addServerTransfer("sfx/scp/173/horror_near.ogg")
misc.addServerTransfer("sfx/scp/173/move_1.ogg")
misc.addServerTransfer("sfx/scp/173/move_2.ogg")
misc.addServerTransfer("sfx/scp/173/move_3.ogg")
misc.addServerTransfer("sfx/scp/173/neck.ogg")

scp.create(173,"The Sculpture",function(me) --create
		me.data.speed = 32
		me.data.isCharging = false
		me.roam = function()
				if (math.random() < 0.5) then
						local dir = math.random()*360
						local newx = me.x+misc.lengthdir_x(dir,32)
						local newy = me.y+misc.lengthdir_y(dir,32)
						if (tile(misc.pixel_to_tile(newx),misc.pixel_to_tile(newy),"walkable")) then
								me.x = newx
								me.y = newy
								tween_move(me.image,100,me.x,me.y,dir)
						end
						if (math.random() < 0.2) then
								scp.soundPosition(173,"move_"..(math.ceil(math.random()*3))..".ogg",me.x,me.y)
						end
				end
		end
end,function(me,pl) --update
		local targets = {}
		local isWhitinScreen = false
		local isSeen = false
		local isLined = false
		i = 0
		while (i < #pl) do
				i = i + 1
				if scp.isInsideScreen(pl[i].x,pl[i].y,me.x,me.y) then
						isWhitinScreen = true
						if scp.isSeeing(player(pl[i].id,"rot"),pl[i].x,pl[i].y,me.x,me.y) then
								if scp.isLineOfSight(me.x,me.y,pl[i].x,pl[i].y) then
										isSeen = true
								end
						end
						if scp.isLineOfSight(me.x,me.y,pl[i].x,pl[i].y) then
								isLined = true
								targets[#targets+1] = pl[i]
						end
				end
		end
		--check
		if (isWhitinScreen) then
				if (isSeen) then
						if (isLined) then
								if (me.data.isCharging) then
										local dist = misc.point_distance(me.x,me.y,targets[#targets].x,targets[#targets].y)
										if (dist < 64) then
												parse("sv_sound2 "..targets[#targets].id.." \"scp/173/horror_near.ogg\"")
										else
												if (dist < 128) then
														parse("sv_sound2 "..targets[#targets].id.." \"scp/173/horror_middle.ogg\"")
												else
														if (dist < 256) then
																parse("sv_sound2 "..targets[#targets].id.." \"scp/173/horror_far.ogg\"")
														end
												end
										end
								end
						else -- roam
								me.roam()
						end
						me.data.isCharging = false
				else -- move towards a target
						me.data.isCharging = false
						if (#targets > 0) then
								if (isLined) then
										--local target = targets[misc.round(math.random(1,#targets))]
										local target = targets[#targets]
										local dir = misc.point_direction(me.x,me.y,target.x,target.y)
										me.x = me.x+misc.lengthdir_x(dir,me.data.speed)
										me.y = me.y+misc.lengthdir_y(dir,me.data.speed)
										tween_move(me.image,100,me.x,me.y,dir)
										local dist = misc.point_distance(me.x,me.y,target.x,target.y)
										if (dist < 32) then
												scp.kill(me,target.id)
												parse("sv_sound2 "..target.id.." \"scp/173/neck.ogg\"")
												--parse("customkill 0 \"SCP-173\" "..target.id)
										else
												me.data.isCharging = true
										end
								else -- roam
										me.roam()
								end
						else -- roam
								me.roam()
						end
				end
		else -- roam
				me.roam()
		end
end,32,function(me,pl,pl_touch) --touch
		local dir = misc.point_direction(pl_touch.x,pl_touch.y,me.x,me.y)
		local newx = me.x+misc.lengthdir_x(dir,2)
		local newy = me.y+misc.lengthdir_y(dir,2)
		if (tile(misc.pixel_to_tile(newx),misc.pixel_to_tile(newy),"walkable")) then
				me.x = newx
				me.y = newy
				tween_move(me.image,100,me.x,me.y)
		end
end,function(me,pl) --destroy

end)
