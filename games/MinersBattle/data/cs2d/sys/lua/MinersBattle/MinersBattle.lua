-- strike the earth

function round(num)
    return math.floor(num+0.5)
end

parse("sv_gamemode 2")
parse("sv_specmode 1")
parse("mp_dispenser_health 5")
parse("mp_dispenser_money 100") --increased to 100 to solve the deflation issue
parse("mp_autoteambalance 0")
parse("mp_dmspawnmoney 0")
parse("mp_weaponfadeout 0")
parse("mp_infammo 0")
parse("mp_wpndmg laser 1000")
parse("mp_idleaction 0")
parse("mp_idletime 120")
parse("mp_randomspawn 0")
parse("mp_supply_items 51,52,53,54,77,66,30,39,86")
parse("mp_building_limit supply 25")
parse("mp_building_limit turret 10")
parse("mp_building_limit dispenser 5")
parse("mp_building_price supply 5000")
parse("mp_building_price \"teleporter entrance\" 2000")
parse("mp_building_price \"teleporter exit\" 1000")
parse("mp_building_limit supply 5")
parse("mp_building_limit \"gate field\" 100")
parse("mp_building_price \"gate field\" 1000")
parse("mp_killbuildings 3")

-- misc.mp_hud(Health,Armor,Time,Ammo,Money,Icons,Kill) --true or false
misc.mp_hud(true,true,true,true,true,true,true)

mb = {}
mb.player = {}
mb.locations = {}
mb.hudtxt = {}
mb.hudtxt.rep = misc.get_hudtxt()
mb.hudtxt.chem = misc.get_hudtxt()
mb.hudtxt.wood = misc.get_hudtxt()
mb.hudtxt.gold = misc.get_hudtxt()
mb.hudtxt.steel = misc.get_hudtxt()
mb.hudtxt.coal = misc.get_hudtxt()
mb.hudtxt.iron = misc.get_hudtxt()
mb.hudtxt.bank = misc.get_hudtxt()
--mb.locations.smelteryTile = 116
mb.locations.smelteryTile = 6
--mb.locations.weaponryTile = 115
mb.locations.weaponryTile = 9
--mb.locations.armoryTile = 117
mb.locations.armoryTile = 10
mb.grace = {}
mb.grace.max = 120
mb.grace.left = mb.grace.max
mb.grace.maxFlag = 60
mb.grace.leftFlag = mb.grace.maxFlag
mb.useMarket = true -- if MinersBattleMarket.lua is enabled, use it?
mb.broke = {}
--mb.broke.coal = 31
mb.broke.coal = 2
--mb.broke.iron = 32
mb.broke.iron = 3
--mb.broke.gold = 33
mb.broke.gold = 4
mb.broke.wood = 5
--mb.reset_loss = 0.5--%
mb.weaponry = {-- iron, steel, wood, gold, chemicals, create id
{1,0,0,0,0,1}, -- usp
{1,2,0,0,0,3}, -- deagle
{3,0,0,0,0,5}, -- elite
{2,0,0,0,0,6}, -- five seven
{12,0,0,1,0,10}, -- M3
{15,0,0,1,0,11}, -- XM1014
{3,0,0,0,0,20}, -- mp5
{5,0,0,0,0,24}, -- ump45
{7,2,0,0,0,22}, -- p90
{10,3,5,2,0,30}, -- ak-47
{22,4,0,0,0,31}, -- SG552
{15,2,0,2,0,32}, -- m4a1
{20,1,0,2,0,33}, -- aug
{17,1,0,3,0,34}, -- scout
{25,2,1,5,0,35}, -- awp
{25,2,1,5,0,36}, -- g3sg1
{25,2,1,5,0,37}, -- sg550
{10,0,0,2,0,38}, -- galil
{15,2,0,0,0,39}, -- famas
{20,10,0,0,0,40}, -- M249
{20,15,1,0,0,41}, -- tactical shield
{500,500,500,500,500,45}, -- laser (experimental)
{25,7,0,8,25,46}, -- Flamethrower
{30,0,15,10,10,47}, -- rpg launcher
{20,5,5,5,10,48}, -- rocket launcher
{15,5,5,3,5,49}, -- grenade launcher
{5,0,0,0,5,51}, -- he
{0,2,0,0,2,52}, -- flashbang
{1,0,1,0,0,53}, -- smoke grenade
{0,0,1,0,0,54}, -- flare
--{1,1,1,1,1,60}, -- gas mask
{20,0,0,0,0,69}, -- machete
{1,0,1,0,15,72}, -- gas grenade
{1,0,5,0,10,73}, -- molotov cocktail
{10,10,0,0,0,74}, -- wrench
{1,0,3,0,20,76}, -- air strike
{3,0,0,0,1,77}, -- mine
{25,25,0,0,0,85}, -- chainsaw
{10,5,0,1,0,87}, -- laser mine
{15,0,0,10,25,88}, -- portal gun
{15,0,0,0,15,89}, -- satchel charge
{50,0,0,0,0,90}, -- m134
{35,2,0,0,0,91}, -- fn 2000
{0,0,0,0,1,64}} -- medikit

mb.armory = {-- iron, steel, wood, gold, chemicals, create id
{0,10,0,0,0,79}, -- light armor
{0,20,0,0,0,80}, -- (medium) armor
{0,40,0,0,0,81}, -- heavy armor
{0,25,0,0,20,82}, -- medic armour
{5,5,0,0,15,84}} -- stealth armour

addhook("join","mb.join")
function mb.join(id)
    mb.player[id] = {}
    if (player(id,"bot")) then
        mb.player[id].welcome_message = false
    else
        mb.player[id].welcome_message = true
    end
    mb.player[id].teamBuildingDamage = 0

    mb.player[id].rep = 0
    mb.player[id].chem = 0
    mb.player[id].wood = 0
    mb.player[id].gold = 0
    mb.player[id].steel = 0
    mb.player[id].coal = 0
    mb.player[id].iron = 0
    mb.player[id].bank = 0

    mb.player[id].equip = {}
    mb.player[id].armor = 0

    local sfile = mb.getSaveFile(id)
    if sfile ~= nil then
        if (misc.fileExists(sfile) == false) then
            msg(misc.rgb(255,0,255)..player(id,"name").." is a new player!")
        end
        SaveEngine.open(sfile)
        mb.player[id].rep = SaveEngine.read(sfile,"Player","Reputation",100)
        mb.player[id].rep = misc.round(mb.player[id].rep,1)
        mb.player[id].chem = SaveEngine.read(sfile,"Player","Chemicals",0)
        mb.player[id].wood = SaveEngine.read(sfile,"Player","Wood",0)
        mb.player[id].gold = SaveEngine.read(sfile,"Player","Gold",0)
        mb.player[id].steel = SaveEngine.read(sfile,"Player","Steel",0)
        mb.player[id].coal = SaveEngine.read(sfile,"Player","Coal",0)
        mb.player[id].iron = SaveEngine.read(sfile,"Player","Iron",0)
        mb.player[id].bank = SaveEngine.read(sfile,"Player","Bank",0)
        -- load armor
        mb.player[id].armor = SaveEngine.read(sfile,"Player","Armor",0)
        -- load equipmnt
        local equip = SaveEngine.read(sfile,"Player","Equipment","")
        equip = misc.split(equip,"|")
        if ((equip[1] == nil) == false) then
            i = 0
            while (i < #equip) do
                i = i + 1
                wep = tonumber(equip[i])
                --print("equip: "..wep)
                mb.player[id].equip[#mb.player[id].equip+1] = wep
            end
        end
        --[[
        mb.player[id].chem = math.floor(mb.player[id].chem*mb.reset_loss)
        mb.player[id].wood = math.floor(mb.player[id].wood*mb.reset_loss)
        mb.player[id].gold = math.floor(mb.player[id].gold*mb.reset_loss)
        mb.player[id].steel = math.floor(mb.player[id].steel*mb.reset_loss)
        mb.player[id].coal = math.floor(mb.player[id].coal*mb.reset_loss)
        mb.player[id].iron = math.floor(mb.player[id].iron*mb.reset_loss)
        mb.player[id].bank = math.floor(mb.player[id].bank*mb.reset_loss)
        timer2(1000,{id},function (id)
            msg2(id,misc.rgb(255,0,0).."Save loaded!, you lost 50% of your resources!")
        end)
        --]]
    else
        if (player(id,"bot") == false) then
            msg(misc.rgb(255,0,255)..player(id,"name").." does not have U.S.G.N")
        end
    end
end

addhook("leave","mb.leave")
function mb.leave(id,reason)
    local sfile = mb.getSaveFile(id)
    if sfile == nil then
        mb.dropAllEquipment(id)
        print(misc.rgb(255,0,0)..player(id, "name").." dropped all equipment since he doesent have USGN/STEAM")
    else
        if (player(id,"team") > 0) then
            mb.riskEquipment(id)
        end
        mb.saveEquipment(id)
    end
    mb.player[id] = {} --nullified
end

function mb.saveEquipment(id)
	--call_stack()
	if (player(id,"health") > 0) then
		usgn = player(id,"usgn")

		local wep = playerweapons(id)
		i = 0
		while (i < #wep) do
			i = i + 1
			--msg("putting equip order on: "..wep[i])
			mb.player[id].equip[#mb.player[id].equip+1] = wep[i]
		end
		mb.player[id].equip[#mb.player[id].equip+1] = player(id,"armor") - 122

		local sfile = mb.getSaveFile(id)
		if sfile ~= nil then
			-- armor
			SaveEngine.write(sfile,"Player","Armor",player(id,"armor"))
			-- weapons
			wep = playerweapons(id)
			wep_string = ""
			i = 0
			while (i < #wep) do
					i = i + 1
					--print("saving equipment: "..wep[i])
					wep_string = wep_string.."|"..wep[i]
			end
			SaveEngine.write(sfile,"Player","Equipment",wep_string:sub(2,#wep_string))
			SaveEngine.save(sfile)
		end
    end
end

function mb.writeAndSave(id)
	local sfile = mb.getSaveFile(id)
	if sfile ~= nil then
		SaveEngine.write(sfile,"Player","Reputation",mb.player[id].rep)
		SaveEngine.write(sfile,"Player","Chemicals",mb.player[id].chem)
		SaveEngine.write(sfile,"Player","Wood",mb.player[id].wood)
		SaveEngine.write(sfile,"Player","Gold",mb.player[id].gold)
		SaveEngine.write(sfile,"Player","Steel",mb.player[id].steel)
		SaveEngine.write(sfile,"Player","Coal",mb.player[id].coal)
		SaveEngine.write(sfile,"Player","Iron",mb.player[id].iron)
		SaveEngine.write(sfile,"Player","Bank",mb.player[id].bank+player(id,"money"))
		SaveEngine.save(sfile)
	end
end

function mb.isFriendOfEnderCrypt(id)
	if superInventory == nil then
		return false
	end
	return superInventory.player[id]:contains("FRIEND_OF_ENDERCRYPT")
end

addhook("say","mb.say")
function mb.say(id,text)
    if text == "!reload" then
        if (player(id,"usgn") == 5783) then
            pl = player(0,"tableliving")
            local i = 0
            while (i < #pl) do
                i = i + 1
                mb.leave(pl[i])
            end
            parse("map "..map("name"))
        end
        return 1
    end
    if string.lower(text) == "strike the earth" then
        local rep = mb.player[id].rep
        local isFriend = mb.isFriendOfEnderCrypt(id)
        if ( (rep >= 100000) or (isFriend and (rep > 50000)) ) then
            local wep = player(id,"weapontype")
            local i = 0
            while (i < #mb.weaponry) do
                i = i + 1
                local wep_data = mb.weaponry[i]
                if (wep_data[6] == wep) then
                    local value = 0;
                    local ii = 0;
                    while (ii < 5) do
                        ii = ii + 1
                        value = value + (wep_data[ii]*math.floor(mbm.baseCost[mbm.quickfix[ii]]/mbm.market[mbm.quickfix[ii]]))
                    end
                    local money_return = 75
                    local sell_price = (value/100)*money_return
                    msg2(id,misc.rgb(0,255,100).." sold "..itemtype(wep,"name").." for "..sell_price.." money ("..money_return.."% of "..value..")")
                    mb.giveMoney(id,sell_price)
                    parse("strip "..id.." "..wep)
                    mb.writeAndSave(id)
                    break;
                end
            end
        else
            msg2(id,"requirments not met, you need atleast 100k rep")
            msg2(id,"or EnderCrypt frienship badge and 50k rep")
        end
        return 1
    end
    if text == "rank" then
        timer2(1,{id},function(id)
            msg(misc.rgb(0,255,100)..player(id,"name").."'s Reputation: "..misc.thousandMarks(mb.player[id].rep))
            if (mb.player[id].rep >= 0) then
                extraHP = math.floor(mb.player[id].rep/1000)
            else
                extraHP = math.ceil(mb.player[id].rep/100)
            end
            --[[
            if (mb.player[id].rep < 1000) then
                extraHP = "zero"
            end
            ]]--
            if (extraHP == 0) then
                msg(misc.rgb(255,0,100)..player(id,"name").." Gains no extra Health")
            else
                if (extraHP > 0) then
                    extraHP = math.min(extraHP,150)
                    msg(misc.rgb(0,255,100)..player(id,"name").." Gains "..extraHP.." extra Health")
                else
                    msg(misc.rgb(255,0,100)..player(id,"name").." Losses "..extraHP.." Health points")
                end
            end
        end)
    end
    if ((text == "!brag mat") or (text == "!brag res")) then
        msg("---[ "..player(id,"name").."'s Materials ]---")

        local value = 0
        local total = 0
        local i = 1
        value = math.ceil(mbm.baseCost[i]/mbm.market[i])*mb.player[id].chem
        total = total + value
        i = i + 1
        msg("�100255100has "..mb.player[id].chem.." Chem (worth: "..misc.thousandMarks(value)..")")
        value = math.ceil(mbm.baseCost[i]/mbm.market[i])*mb.player[id].wood
        total = total + value
        i = i + 1
        msg("�139069019has "..mb.player[id].wood.." Wood (worth: "..misc.thousandMarks(value)..")")
        value = math.ceil(mbm.baseCost[i]/mbm.market[i])*mb.player[id].gold
        total = total + value
        i = i + 1
        msg("�255255000has "..mb.player[id].gold.." Gold (worth: "..misc.thousandMarks(value)..")")
        value = math.ceil(mbm.baseCost[i]/mbm.market[i])*mb.player[id].steel
        total = total + value
        i = i + 1
        msg("�200200200has "..mb.player[id].steel.." Steel (worth: "..misc.thousandMarks(value)..")")
        value = math.ceil(mbm.baseCost[i]/mbm.market[i])*mb.player[id].coal
        total = total + value
        i = i + 1
        msg("�050050050has "..mb.player[id].coal.." Coal (worth: "..misc.thousandMarks(value)..")")
        value = math.ceil(mbm.baseCost[i]/mbm.market[i])*mb.player[id].iron
        total = total + value
        i = i + 1
        msg("�150150150has "..mb.player[id].iron.." Iron (worth: "..misc.thousandMarks(value)..")")
        msg(player(id,"name").."'s Materials have a total value of "..misc.thousandMarks(total))
        return 1
    end
    if text == "!brag equip" then
        local total_value = 0
        msg("---[ "..player(id,"name").."'s Equipment ]---")
        local wep = playerweapons(id)
        local total = 0
        i = 0
        while (i < #wep) do
            i = i + 1
            -- find weapon
            ii = 0
            while (ii < #mb.weaponry) do
                ii = ii + 1
                data = mb.weaponry[ii]
                if (wep[i] == data[6]) then
                    -- get cost
                    local cost = 0
                    iii = 0
                    while (iii < 5) do
                        iii = iii + 1
                        cost = cost + (data[iii]*math.floor(mbm.baseCost[mbm.quickfix[iii]]/mbm.market[mbm.quickfix[iii]]))
                    end
                    total = total + cost
                    msg(itemtype(data[6],"name").." worth "..misc.thousandMarks(cost))
                end
            end
        end
        msg(player(id,"name").."'s Weapons have a total value of: "..misc.thousandMarks(total))
        total_value = total_value + total
        -- armor
        armor = player(id,"armor")
        if (armor == 0) then
            msg("has no armor")
        else
            i = 0
            while (i < #mb.armory) do
                i = i + 1
                data = mb.armory[i]
                if (armor == (data[6]+122)) then
                    -- get cost
                    local cost = 0
                    ii = 0
                    while (ii < 5) do
                        ii = ii + 1
                        cost = cost + (data[ii]*math.floor(mbm.baseCost[mbm.quickfix[ii]]/mbm.market[mbm.quickfix[ii]]))
                    end
                    msg("has the armor "..itemtype(armor-122,"name").." worth "..misc.thousandMarks(cost))
                    total_value = total_value + cost
                end
            end
        end
        msg(player(id,"name").."'s Equipment have a total value of: "..misc.thousandMarks(total_value))
        return 1
    end
    if text == "!brag" then
        local total_value = 0
        msg("�000255100--{ "..player(id,"name").." }--")
        msg("�000255100has "..misc.thousandMarks(mb.player[id].rep).." Reputation!")
        msg("�000255000has "..misc.thousandMarks(mb.player[id].bank+player(id,"money")).." Money in bank+hand!")
        total_value = total_value + mb.player[id].bank+player(id,"money")
        -- get mat value
        local total = 0
        local i = 1
        value = math.ceil(mbm.baseCost[i]/mbm.market[i])*mb.player[id].chem
        total = total + value
        i = i + 1
        value = math.ceil(mbm.baseCost[i]/mbm.market[i])*mb.player[id].wood
        total = total + value
        i = i + 1
        value = math.ceil(mbm.baseCost[i]/mbm.market[i])*mb.player[id].gold
        total = total + value
        i = i + 1
        value = math.ceil(mbm.baseCost[i]/mbm.market[i])*mb.player[id].steel
        total = total + value
        i = i + 1
        value = math.ceil(mbm.baseCost[i]/mbm.market[i])*mb.player[id].coal
        total = total + value
        i = i + 1
        value = math.ceil(mbm.baseCost[i]/mbm.market[i])*mb.player[id].iron
        total = total + value
        msg("�000255000"..player(id,"name").."'s Materials have a total value of "..misc.thousandMarks(total))
        total_value = total_value + total
        -- get equip value
        local wep = playerweapons(id)
        local total = 0
        i = 0
        while (i < #wep) do
            i = i + 1
            -- find weapon
            ii = 0
            while (ii < #mb.weaponry) do
                ii = ii + 1
                data = mb.weaponry[ii]
                if (wep[i] == data[6]) then
                    -- get cost
                    local cost = 0
                    iii = 0
                    while (iii < 5) do
                        iii = iii + 1
                        cost = cost + (data[iii]*math.floor(mbm.baseCost[mbm.quickfix[iii]]/mbm.market[mbm.quickfix[iii]]))
                    end
                    total = total + cost
                    --msg(itemtype(data[6],"name").." worth "..misc.thousandMarks(cost))
                end
            end
            total_value = total_value + total
        end
        msg("�000255000"..player(id,"name").."'s Weapons have a total value of: "..misc.thousandMarks(total))
        total_value = total_value + total
        -- get armor value
        local cost = 0
        armor = player(id,"armor")
        if (armor > 0) then
            i = 0
            while (i < #mb.armory) do
                i = i + 1
                data = mb.armory[i]
                if (armor == (data[6]+122)) then
                    -- get cost
                    ii = 0
                    while (ii < 5) do
                        ii = ii + 1
                        cost = cost + (data[ii]*math.floor(mbm.baseCost[mbm.quickfix[ii]]/mbm.market[mbm.quickfix[ii]]))
                    end
                    total_value = total_value + cost
                end
            end
        end
        msg("�000255000"..player(id,"name").."'s Armor has a total value of: "..misc.thousandMarks(cost))
        -- display
        msg("�000255000"..player(id,"name").."'s TOTAL value is "..misc.thousandMarks(total_value).."")
        return 1
    end
end

function mb.getSaveFile(id)
	local usgn = player(id,"usgn")
	if usgn > 0 then
		return "sys/lua/MinersBattle/player_saves/usgn_"..usgn..".txt"
	end
	local steam = player(id,"steamid")
	if steam ~= "0" then
		return "sys/lua/MinersBattle/player_saves/steam_"..steam..".txt"
	end
	return nil
end

function mb.saveRep(id)
	local sfile = mb.getSaveFile(id)
	if sfile ~= nil then
		SaveEngine.write(sfile,"Player","Reputation",mb.player[id].rep)
		SaveEngine.save(sfile)
	end
end

function mb.addRep(id,rep,reason)
    if not(player(id,"bot")) then
        local rep = misc.round(rep,1)
        mb.player[id].rep = mb.player[id].rep + rep
        local usgn = player(id,"usgn")
        if usgn > 0 then
            mbrep.registerRep(player(id,"name"),usgn,mb.player[id].rep)
        end
        if rep > 0 then
            msg2(id,misc.rgb(0,255,0).."[+"..rep.." Rep] "..reason.."!")
        end
        if rep < 0 then
            msg2(id,misc.rgb(255,0,0).."["..rep.." Rep] "..reason.."!")
        end
        mb.drawrep(id)
        mb.saveRep(id)
    end
end

function mb.addRepAll(rep,reason)
    local pl = player(0,"tableliving")
    local i = 0
    while (i < #pl) do
        i = i + 1
        mb.addRep(pl[i],rep,reason)
    end
end

addhook("spawn","mb.spawn")
function mb.spawn(id)
    -- welcome message
    if (mb.player[id].welcome_message) then
        mb.player[id].welcome_message = false
        msg(misc.rgb(0,255,0).."Everyone welcome ("..id..") "..player(id,"name").." to the server!")
    end
    --
    local usgn = player(id,"usgn")
    if (usgn > 0) then
        msg2(id,misc.rgb(0,255,0).." You are logged in to USGN/STEAM and your data will automatically be saved!")
    else
        msg2(id,misc.rgb(255,0,0).." You are NOT logged in to USGN, your data will NOT BE SAVED!")
    end
    --
    mb.addRep(id,-1,"New Life")
    if (player(id,"bot") == false) then
        timer(1,"parse","strip "..id.." "..(3-player(id,"team")))
        mb.drawHud(id)
    end
    timer2(1,{id},function(id)
        if (mb.player[id].rep > 0) then
            parse("setmaxhealth "..id.." "..(100+math.floor(mb.player[id].rep/1000)))
        else
            parse("setmaxhealth "..id.." "..(100+math.ceil(mb.player[id].rep/100)))
        end
        --msg("amount of weapons: "..#mb.player[id].equip)
        i = 0
        while (i < #mb.player[id].equip) do
            i = i + 1
            wep = mb.player[id].equip[i]
            --msg("equip: "..wep)
            parse("equip "..id.." "..wep)
        end
        mb.player[id].equip = {}
        if (mb.player[id].armor > 0) then
            parse("setarmor "..id.." "..mb.player[id].armor)
            mb.player[id].armor = 0
        end
    end)
end

function mb.drawHud(id)
    mb.drawrep(id)
    mb.drawchem(id)
    mb.drawwood(id)
    mb.drawgold(id)
    mb.drawsteel(id)
    mb.drawcoal(id)
    mb.drawiron(id)
    mb.drawbank(id)
end

function mb.uiCoordinates(id, position)
	local width = player(id, "screenw")
	local height = player(id, "screenh")

	local result = {}
	result.x = width - 5
	result.y = height - 300 - (20 * position)

	return result
end

function mb.drawrep(id)
		local ui = mb.uiCoordinates(id, 8);
    misc.hudText(mb.hudtxt.rep,id,misc.thousandMarks(mb.player[id].rep).." Reputation", ui.x, ui.y, 2)
    misc.hudTextColor(mb.hudtxt.rep,id,1,0,255,100)
end

function mb.drawbank(id)
		local ui = mb.uiCoordinates(id, 7);
    misc.hudText(mb.hudtxt.bank,id,misc.thousandMarks(mb.player[id].bank).." Bank Cash", ui.x, ui.y, 2)
    misc.hudTextColor(mb.hudtxt.bank,id,1,0,255,0)
end

function mb.drawchem(id)
		local ui = mb.uiCoordinates(id, 5);
    misc.hudText(mb.hudtxt.chem,id,mb.player[id].chem.." Chemicals", ui.x, ui.y, 2)
    misc.hudTextColor(mb.hudtxt.chem,id,1,100,255,100)
end

function mb.drawwood(id)
		local ui = mb.uiCoordinates(id, 4);
    misc.hudText(mb.hudtxt.wood,id,mb.player[id].wood.." Wood", ui.x, ui.y, 2)
    misc.hudTextColor(mb.hudtxt.wood,id,1,139,69,19)
end

function mb.drawgold(id)
		local ui = mb.uiCoordinates(id, 3);
    misc.hudText(mb.hudtxt.gold,id,mb.player[id].gold.." Gold ore", ui.x, ui.y, 2)
    misc.hudTextColor(mb.hudtxt.gold,id,1,255,255,0)
end

function mb.drawsteel(id)
		local ui = mb.uiCoordinates(id, 2);
    misc.hudText(mb.hudtxt.steel,id,mb.player[id].steel.." Steel bar", ui.x, ui.y, 2)
    misc.hudTextColor(mb.hudtxt.steel,id,1,200,200,200)
end

function mb.drawcoal(id)
		local ui = mb.uiCoordinates(id, 1);
    misc.hudText(mb.hudtxt.coal,id,mb.player[id].coal.." Coal ore", ui.x, ui.y, 2)
    misc.hudTextColor(mb.hudtxt.coal,id,1,50,50,50)
end

function mb.drawiron(id)
		local ui = mb.uiCoordinates(id, 0);
    misc.hudText(mb.hudtxt.iron,id,mb.player[id].iron.." Iron ore", ui.x, ui.y, 2)
    misc.hudTextColor(mb.hudtxt.iron,id,1,150,150,150)
end

addhook("startround","mb.startround")
function mb.startround(mode)
    -- start grace
    msg(misc.rgb(0,255,0).."Grace period: "..mb.grace.max.." seconds")
    mb.grace.left = mb.grace.max
    mb.grace.leftFlag = mb.grace.maxFlag
end

addhook("endround","mb.endround")
function mb.endround(mode)
    -- calculate lost weapons
    local items = item(0,"table")
    local total = 0
    local unknown = 0
    local lasers = 0
    local i = 0
    while (i < #items) do
        i = i + 1
        local item_type = item(items[i],"type")
        local val = mb.calculateValue(item_type)
        if (val == 0) then
            unknown = unknown + 1
        else
            total = total + val
        end
        if (item_type == 45) then
            lasers = lasers + 1
        end
    end
    msg(misc.rgb(100,255,100).."Total lost items (on the ground): "..#items.." ("..(#items-unknown).." of them where valued)")
    if (unknown > 0) then
        msg(misc.rgb(100,255,100).."There was "..unknown.." un-craftable items on the ground")
    end
    msg(misc.rgb(100,255,100).."Total value of all items: "..misc.thousandMarks(total))
    if (lasers > 0) then
        msg(misc.rgb(100,255,100)..lasers.." Lasers where lost and void!")
    end
    -- reward: finished a round
    pl = player(0,"table")
    i = 0
    while (i < #pl) do
        i = i + 1
        parse("setmaxhealth "..pl[i].." 100")
        mb.addRep(pl[i],25,"Finishing a round")
    end
    --print("endround mode: "..mode)
    -- 50 = T captured all flags
    if (mode == 50) then
        msg("Terrorists win!")
        winners = player(0,"team1")
        losers = player(0,"team2")
    end
    -- 51 = CT captured all flags
    if (mode == 51) then
        msg("Counter-Terrorists win!")
        winners = player(0,"team2")
        losers = player(0,"team1")
    end
    -- process
    local i = 0
    while (i < #winners) do
        i = i + 1
        local p = winners[i]
        --mb.addRep(p,100,"You and your team won!")
        mb.addRep(p,30*#losers,"You and your team won!")
    end
    local i = 0
    while (i < #losers) do
        i = i + 1
        local p = losers[i]
        --mb.addRep(p,-50,"You and your team lost!!")
        mb.addRep(p,-20*#winners,"You and your team lost!!")
    end
end

addhook("break","mb.breakable",0,true) -- overridding extended addhook
function mb.breakable(x,y,id)
    if player(id,"bot") then
        return 0
    end
    if id == 0 then
        return 0
    end
    tileFrame = entity(x,y,"int0")
    --msg("player "..player(id,"name").." broke "..tile)
    if tileFrame == mb.broke.wood then --wood
        num = math.random(2,4)
        msg2(id,misc.rgb(139,69,19).."You found "..num.." wooden sticks!")
        mb.player[id].wood = mb.player[id].wood + num
        mb.drawwood(id)
    end
    if tileFrame == mb.broke.iron then --iron
        num = math.random(3,6)
        msg2(id,misc.rgb(150,150,150).."You found "..num.." iron ore!")
        mb.player[id].iron = mb.player[id].iron + num
        mb.drawiron(id)
    end
    if tileFrame == mb.broke.coal then --coal
        num = math.random(4,8)
        msg2(id,misc.rgb(50,50,50).."You found "..num.." coal ore!")
        mb.player[id].coal = mb.player[id].coal + num
        mb.drawcoal(id)
    end
    if tileFrame == mb.broke.gold then --gold
        num = math.random(1,3)
        msg2(id,misc.rgb(255,255,0).."You found "..num.." gold ore!")
        mb.player[id].gold = mb.player[id].gold + num
        mb.drawgold(id)
    end
end

addhook("hit","mb.hit")
function mb.hit(id,source,weapon,hpdmg,apdmg)
    if mb.grace.left > 0 then
        if source > 0 then
            msg2(source,"Grace period: pvp is OFF, please wait "..mb.grace.left.." sconds...")
            return 1
        end
    end
end

addhook("kill","mb.kill")
function mb.kill(killer,victim,weapon,x,y)
    if (not(player(victim,"team") == player(killer,"team"))) then
        if player(victim,"bot") then
            if weapon == 253 then
                mb.addRep(killer,0.1,"Killed an enemy bot using a turret")
            else
                mb.addRep(killer,1,"Killed an enemy bot")
            end
        else
            mb.addRep(victim,-10,"Died a brutal death")
            if weapon == 253 then
                mb.addRep(killer,3,"Killed an enemy player using a turret")
            else
                mb.addRep(killer,5,"Killed an enemy player")
            end
        end
        if (player(victim,"bot") == false) then
            local chem_get = 2
            mb.player[killer].chem = mb.player[killer].chem + chem_get
            mb.drawchem(killer)
            msg2(killer,misc.rgb(100,255,100).."You gained "..chem_get.." chemical for killing: "..player(victim,"name"))
        end
    end
end

addhook("die","mb.die")
function mb.die(victim,killer,weapon,x,y)
    --[[
    local usgn = player(victim,"usgn")
    if (usgn > 0) then
        msg2(victim,misc.rgb(0,255,0).." You are logged in to USGN and your data will automatically be saved!")
    else
        msg2(victim,misc.rgb(255,0,0).." You are NOT logged in to USGN, your data will NOT BE SAVED!")
    end
    ]]--
    --msg("clear")
    --mb.player[victim].equip = {}
    local armor = player(victim,"armor")
    local tx = player(victim,"tilex")
    local ty = player(victim,"tiley")
    if armor > 0 then
        armor = armor - 122
        --print(player(victim,"name").." died with armor: "..itemtype(armor,"name"))
        parse("spawnitem "..armor.." "..tx.." "..ty)
    end

    timer2(100,{victim},function(id)
        mb.saveEquipment(id)
        mb.writeAndSave(id)
    end)
end

addhook("serveraction","mb.serveraction")
function mb.serveraction(id,action)
    if player(id,"health") <= 0 then
        return 1
    end
    tileFrame = tile(player(id,"tilex"),player(id,"tiley"),"frame")
    if action == 1 then
        crafted = false
        if tileFrame == mb.locations.smelteryTile then
            sme.createMenu(id,mb.smeltMenu,sme.noProcess,"Smelter",false,{"Smelt STEEL","Smelt MONEY (GOLD)"},true)
            crafted = true
        end
        if tileFrame == mb.locations.weaponryTile then
            sme.createMenu(id,mb.craftWeapon,mb.processWeaponry,"Weaponry",true,mb.weaponry,true)
            crafted = true
        end
        if tileFrame == mb.locations.armoryTile then
            sme.createMenu(id,mb.craftArmor,mb.processWeaponry,"Armory",true,mb.armory,true) -- craftWeapon and processWeaponry should work here to
            crafted = true
        end
        if not crafted then
            msg2(id,misc.rgb(255,0,0).."Unable to craft anything (not whitin crafting area?)")
        end
    end
    if action == 2 then
        if not (mbm == nil) then
            if mb.useMarket then
                mbm.marketOpen(id)
            end
        end
    end
end

function mb.processWeaponry(id, data)
    rec = ""
    if data[1] > 0 then -- iron
        rec = rec.." "..data[1].."xIRON"
    end
    if data[2] > 0 then -- steel
        rec = rec.." "..data[2].."xSTEEL"
    end
    if data[3] > 0 then -- wood
        rec = rec.." "..data[3].."xWOOD"
    end
    if data[4] > 0 then -- gold
        rec = rec.." "..data[4].."xGOLD"
    end
    if data[5] > 0 then -- chemicals
        rec = rec.." "..data[5].."xCHEM"
    end
    if mb.player[id].iron >= data[1] and mb.player[id].steel >= data[2] and mb.player[id].wood >= data[3] and mb.player[id].gold >= data[4] and mb.player[id].chem >= data[5] then
        return itemtype(data[6],"name").."|"..rec
    else
        return "("..itemtype(data[6],"name").."|"..rec..")"
    end
end

function mb.craftArmor(id,num)
    if player(id,"health") <= 0 then
        msg2(id,misc.rgb(255,0,0).."your too dead to craft!")
        return 0
    end
    data = mb.armory[num]
    rec = ""
    if data[1] > 0 then -- iron
        rec = rec.." "..data[1].."xIRON"
    end
    if data[2] > 0 then -- steel
        rec = rec.." "..data[2].."xSTEEL"
    end
    if data[3] > 0 then -- wood
        rec = rec.." "..data[3].."xWOOD"
    end
    if data[4] > 0 then -- gold
        rec = rec.." "..data[4].."xGOLD"
    end
    if data[5] > 0 then -- chemicals
        rec = rec.." "..data[5].."xCHEM"
    end
    if mb.player[id].iron >= data[1] and mb.player[id].steel >= data[2] and mb.player[id].wood >= data[3] and mb.player[id].gold >= data[4] and mb.player[id].chem >= data[5] then
        if data[1] > 0 then
            mb.player[id].iron = mb.player[id].iron - data[1]
            mb.drawiron(id)
            msg2(id,misc.rgb(50,0,0).."used "..data[1].."xIRON")
        end
        if data[2] > 0 then
            mb.player[id].steel = mb.player[id].steel - data[2]
            mb.drawsteel(id)
            msg2(id,misc.rgb(50,0,0).."used "..data[2].."xSTEEL")
        end
        if data[3] > 0 then
            mb.player[id].wood = mb.player[id].wood - data[3]
            mb.drawwood(id)
            msg2(id,misc.rgb(50,0,0).."used "..data[3].."xWOOD")
        end
        if data[4] > 0 then
            mb.player[id].gold = mb.player[id].gold - data[4]
            mb.drawwood(id)
            msg2(id,misc.rgb(50,0,0).."used "..data[4].."xGOLD")
        end
        if data[5] > 0 then
            mb.player[id].chem = mb.player[id].chem - data[5]
            mb.drawwood(id)
            msg2(id,misc.rgb(50,0,0).."used "..data[5].."xCHEM")
        end
        msg2(id,misc.rgb(0,255,0).."you used"..rec.." to craft a "..itemtype(data[6],"name"))
        mb.giveArmor(id,data[6])
        --parse("equip "..id.." "..data[6])
        --parse("setweapon "..id.." "..data[6])
        -- rep
        local cost = 0
        i = 0
        while (i < 5) do
            i = i + 1
            cost = cost + (data[i]*(mbm.baseCost[mbm.quickfix[i]]/mbm.market[mbm.quickfix[i]]))
            --cost = cost + (data[i]*(mbm.baseCost[6-i]/mbm.market[6-i]))
        end
        msg2(id,misc.rgb(100,255,150).."Estimated armor cost (by market  price): "..misc.thousandMarks(misc.round(cost)))
        mb.addRep(id,cost/200,"Making a "..itemtype(data[6],"name"))
    else
        msg2(id,misc.rgb(255,0,0).."unable to craft armor, insufficient materials!")
    end
end

function mb.craftWeapon(id,num)
    if player(id,"health") <= 0 then
        msg2(id,misc.rgb(255,0,0).."your too dead to craft!")
        return 0
    end
    data = mb.weaponry[num]
    local wep = playerweapons(id)
    local primaries = 0
    local i = 0
    while (i < #wep) do
        i = i + 1
        if (itemtype(wep[i],"slot") == 1) then
            primaries = primaries + 1
            --[[
            if (primaries > 2) then
                msg2(id,misc.rgb(255,0,0).."you got too many primary weapons!")
                return 0
            end
            ]]--
        end
    end
    --[[
    if (primaries >= 1) then
        msg2(id,misc.rgb(255,0,0).."You already got a primary weapon")
        return 1
    end]]--
    rec = ""
    first = true
    if data[1] > 0 then -- iron
        rec = rec.." "..data[1].."xIRON"
    end
    if data[2] > 0 then -- steel
        rec = rec.." "..data[2].."xSTEEL"
    end
    if data[3] > 0 then -- wood
        rec = rec.." "..data[3].."xWOOD"
    end
    if data[4] > 0 then -- gold
        rec = rec.." "..data[4].."xGOLD"
    end
    if data[5] > 0 then -- chemicals
        rec = rec.." "..data[5].."xCHEM"
    end
    if mb.player[id].iron >= data[1] and mb.player[id].steel >= data[2] and mb.player[id].wood >= data[3] and mb.player[id].gold >= data[4] and mb.player[id].chem >= data[5] then
        if data[1] > 0 then
            mb.player[id].iron = mb.player[id].iron - data[1]
            mb.drawiron(id)
            msg2(id,misc.rgb(50,0,0).."used "..data[1].."xIRON")
        end
        if data[2] > 0 then
            mb.player[id].steel = mb.player[id].steel - data[2]
            mb.drawsteel(id)
            msg2(id,misc.rgb(50,0,0).."used "..data[2].."xSTEEL")
        end
        if data[3] > 0 then
            mb.player[id].wood = mb.player[id].wood - data[3]
            mb.drawwood(id)
            msg2(id,misc.rgb(50,0,0).."used "..data[3].."xWOOD")
        end
        if data[4] > 0 then
            mb.player[id].gold = mb.player[id].gold - data[4]
            mb.drawgold(id)
            msg2(id,misc.rgb(50,0,0).."used "..data[4].."xGOLD")
        end
        if data[5] > 0 then
            mb.player[id].chem = mb.player[id].chem - data[5]
            mb.drawwood(id)
            msg2(id,misc.rgb(50,0,0).."used "..data[5].."xCHEM")
        end
        msg2(id,misc.rgb(0,255,0).."you used"..rec.." to craft a "..itemtype(data[6],"name"))
        parse("equip "..id.." "..data[6])
        parse("setweapon "..id.." "..data[6])
        -- since the market had materials ordered in the wrong way, compared to this, a manual link is needed
        -- rep
        local cost = 0
        i = 0
        while (i < 5) do
            i = i + 1
            cost = cost + (data[i]*(mbm.baseCost[mbm.quickfix[i]]/mbm.market[mbm.quickfix[i]]))
            --print("material:"..i..", amount: "..data[i]..", base cost:"..mbm.baseCost[6-i]..", market: "..mbm.market[6-i]..", divide: "..(mbm.baseCost[quickfix[i]]/mbm.market[quickfix[i]]))
        end
        msg2(id,misc.rgb(100,255,150).."Estimated weapon cost (by market price): "..misc.thousandMarks(misc.round(cost)))
        mb.addRep(id,cost/200,"Making a "..itemtype(data[6],"name"))
    else
        msg2(id,misc.rgb(255,0,0).."unable to craft weapon, insufficient materials!")
    end
end
-- {100,100,100,100,100,45}, -- laser (experimental)
function mb.calculateValue(item_id)
    local i = 0
    while (i < #mb.weaponry) do
        i = i + 1
        if (mb.weaponry[i][6] == item_id) then
            data = mb.weaponry[i]
            if not (data == nil) then
                local cost = 0
                local ii = 0
                while (ii < 5) do
                    ii = ii + 1
                    cost = cost + (data[ii]*(mbm.baseCost[mbm.quickfix[ii]]/mbm.market[mbm.quickfix[ii]]))
                end
                return misc.round(cost)
            end
        end
    end
    local i = 0
    while (i < #mb.armory) do
        i = i + 1
        if (mb.armory[i][6] == item_id) then
            data = mb.armory[i]
            if not (data == nil) then
                local cost = 0
                local ii = 0
                while (ii < 5) do
                    ii = ii + 1
                    cost = cost + (data[ii]*(mbm.baseCost[mbm.quickfix[ii]]/mbm.market[mbm.quickfix[ii]]))
                end
                return misc.round(cost)
            end
        end
    end
    return 0
end

--[[
timer2(1,{},function()
    print("laser: "..mb.calculateValue(45))
end)
]]--

function mb.smeltMenu(id,num)
    if num == 1 then
        sme.createMenu(id,mb.smeltSteel,mb.processMenuSmeltSteel,"Smelter (Steel)",false,{1,2,3,4,5,6},true)
    end
    if num == 2 then
        sme.createMenu(id,mb.smeltGold,mb.processMenuSmeltGold,"Smelter (Gold)",false,{1,2,3,4,5,6},true)
    end
end

function mb.processMenuSmeltSteel(id,num)
    if num == 1 then
        if mb.player[id].iron >= 1 and mb.player[id].coal >= 1 then
            return "1xSTEEL|1xIRON + 1xCOAL"
        else
            return "(1xSTEEL|1xIRON + 1xCOAL)"
        end
    end
    if num == 2 then
        if mb.player[id].iron >= 5 and mb.player[id].coal >= 5 then
            return "5xSTEEL|5xIRON + 5xCOAL"
        else
            return "(5xSTEEL|5xIRON + 5xCOAL)"
        end
    end
    if num == 3 then
        if mb.player[id].iron >= 10 and mb.player[id].coal >= 10 then
            return "10xSTEEL|10xIRON + 10xCOAL"
        else
            return "(10xSTEEL|10xIRON + 10xCOAL)"
        end
    end
    if num == 4 then
        if mb.player[id].iron >= 50 and mb.player[id].coal >= 50 then
            return "50xSTEEL|50xIRON + 50xCOAL"
        else
            return "(50xSTEEL|50xIRON + 50xCOAL)"
        end
    end
    if num == 5 then
        if mb.player[id].iron >= 100 and mb.player[id].coal >= 100 then
            return "100xSTEEL|100xIRON + 100xCOAL"
        else
            return "(100xSTEEL|100xIRON + 100xCOAL)"
        end
    end
    if num == 6 then
        numSmelt = math.min(mb.player[id].iron,mb.player[id].coal)
        if numSmelt >= 1 then
            return numSmelt.."xIRON + "..numSmelt.."xCOAL = "..numSmelt.."xSTEEL"
        else
            return "("..numSmelt.."xIRON + "..numSmelt.."xCOAL = "..numSmelt.."xSTEEL)"
        end
    end
end

function mb.smeltSteel(id,num)
    tileFrame = tile(player(id,"tilex"),player(id,"tiley"),"frame")
    if not tileFrame == mb.locations.smelteryTile then
        msg2(id,misc.rgb(255,0,0).."you left the smelting area!")
        return 0
    end
    if num == 1 then
        smelt = 1
    end
    if num == 2 then
        smelt = 5
    end
    if num == 3 then
        smelt = 10
    end
    if num == 4 then
        smelt = 50
    end
    if num == 5 then
        smelt = 100
    end
    if num == 6 then
        smelt = math.min(mb.player[id].iron,mb.player[id].coal)
    end
    if smelt >= 1 then
        if mb.player[id].iron >= smelt and mb.player[id].coal >= smelt then
            mb.player[id].iron = mb.player[id].iron - smelt
            mb.drawiron(id)
            mb.player[id].coal = mb.player[id].coal - smelt
            mb.drawcoal(id)
            mb.player[id].steel = mb.player[id].steel + smelt
            mb.drawsteel(id)
            msg2(id,misc.rgb(200,200,200).."you smeleted "..smelt.." steel ores!")
            if smelt >= 10 then
                mb.addRep(id,smelt/5,"Aquiring hardware")
            end
        else
            msg2(id,misc.rgb(255,0,0).."Unable to smelt!")
        end
    end
end

function mb.processMenuSmeltGold(id,num)
    if num == 1 then
        if mb.player[id].gold >= 1 and mb.player[id].coal >= 1 then
            return "1KxMONEY|1xGOLD + 1xCOAL"
        else
            return "(1KxMONEY|1xGOLD + 1xCOAL)"
        end
    end
    if num == 2 then
        if mb.player[id].gold >= 5 and mb.player[id].coal >= 5 then
            return "5KxMONEY|5xGOLD + 5xCOAL"
        else
            return "(5KxMONEY|5xGOLD + 5xCOAL)"
        end
    end
    if num == 3 then
        if mb.player[id].gold >= 10 and mb.player[id].coal >= 10 then
            return "10KxMONEY|10xGOLD + 10xCOAL"
        else
            return "(10KxMONEY|10xGOLD + 10xCOAL)"
        end
    end
    if num == 4 then
        if mb.player[id].gold >= 50 and mb.player[id].coal >= 50 then
            return "50KxMONEY|50xGOLD + 50xCOAL"
        else
            return "(50KxMONEY|50xGOLD + 50xCOAL)"
        end
    end
    if num == 5 then
        if mb.player[id].gold >= 100 and mb.player[id].coal >= 100 then
            return "100KxMONEY|100xGOLD + 100xCOAL"
        else
            return "(100KxMONEY|100xGOLD + 100xCOAL)"
        end
    end
    if num == 6 then
        numSmelt = math.min(mb.player[id].gold,mb.player[id].coal)
        if numSmelt >= 1 then
            return numSmelt.."xGOLD + "..numSmelt.."KxMONEY = "..numSmelt.."xMONEY"
        else
            return "("..numSmelt.."xGOLD + "..numSmelt.."KxMONEY = "..numSmelt.."xMONEY)"
        end
    end
end

function mb.smeltGold(id,num)
    tileFrame = tile(player(id,"tilex"),player(id,"tiley"),"frame")
    if not tileFrame == mb.locations.smelteryTile then
        msg2(id,misc.rgb(255,0,0).."you left the smelting area!")
        return 0
    end
    if player(id,"health") <= 0 then
        msg2(id,misc.rgb(255,0,0).."your too dead to smelt!")
        return 0
    end
    if num == 1 then
        smelt = 1
    end
    if num == 2 then
        smelt = 5
    end
    if num == 3 then
        smelt = 10
    end
    if num == 4 then
        smelt = 50
    end
    if num == 5 then
        smelt = 100
    end
    if num == 6 then
        smelt = math.min(mb.player[id].gold,mb.player[id].coal)
    end
    if (smelt == nil) then
        return 0
    else
        if smelt >= 1 then
            if mb.player[id].gold >= smelt and mb.player[id].coal >= smelt then
                mb.player[id].gold = mb.player[id].gold - smelt
                mb.drawgold(id)
                mb.player[id].coal = mb.player[id].coal - smelt
                mb.drawcoal(id)
                mb.giveMoney(id,smelt*1000)
                msg2(id,misc.rgb(0,255,0).."you smeleted "..smelt.."K money!")
                mb.addRep(id,(smelt*(mbm.baseCost[3]/mbm.market[3]))/200,"Beautiful treasures")
                mb.writeAndSave(id) -- fix /retry bug
            else
                msg2(id,misc.rgb(255,0,0).."Unable to smelt!")
            end
        end
    end
end

--[[
LOL that rep!
you smeleted 891K money!
[+4617 Rep] Beautiful treasures!
]]--

function mb.removeMoney(id,money)
    if player(id,"money") < money then
        money = money - player(id,"money")
        parse("setmoney "..id.." 0")
        mb.player[id].bank = mb.player[id].bank - money
    else
        parse("setmoney "..id.." "..player(id,"money")-money)
    end
end

function mb.checkMoney(id)
    return player(id,"money")+mb.player[id].bank
end

function mb.giveMoney(id,money)
    local newmoney = player(id,"money")+money
    if newmoney > 10000 then
        bank = newmoney - 10000
        newmoney = 10000
        parse("setmoney "..id.." 10000")
        mb.player[id].bank = mb.player[id].bank + bank
        mb.drawbank(id)
    else
        parse("setmoney "..id.." "..newmoney)
    end
end

addhook("dominate","mb.dominate")
function mb.dominate(id,team,x,y)
    if mb.grace.leftFlag > 0 then
        msg2(id,misc.rgb(255,0,0).."You cant capture flags for another "..mb.grace.leftFlag.." seconds!")
        return 1
    else
        if (player(id,"bot") == false) then
            local i = 0
            while (i < #mbwe.dompoints) do
                i = i + 1
                local point = mbwe.dompoints[i]
                local px = point[1]
                local py = point[2]
                if (x == px) and (y == py) then
                    if (point[4] > 120) then -- limit to 120
                        point[4] = 120
                    end
                    if (point[4] >= 60) then -- required
                        point[4] = point[4] - 60
                    else
                        return 0
                    end
                end
            end
            mb.addRep(id,15,"Captured a flag")
            local flag_cap_chem = 10
            mb.player[id].chem = mb.player[id].chem + flag_cap_chem
            mb.drawchem(id)
            msg2(id,misc.rgb(100,255,100).."You gained "..flag_cap_chem.." chemical for capturing a flag!")
        end
    end
end

mb.randomRemovePrimary = 0
addhook("minute","mb.minute")
function mb.minute()
    if (math.random() < mb.randomRemovePrimary) then
        print("-----{ trying to rem slot 1 }-----")
        local pl = player(0,"tableliving")
        local i = 0
        while (i < #pl) do
            i = i + 1
            local id = pl[i]
            local weps = playerweapons(id)
            local slot_1_weps = {}
            -- check all player weapons for slot 1
            local ii = 0
            while (ii < #weps) do
                ii = ii + 1
                local wep = weps[ii]
                if (itemtype(wep,"slot") == 1) then
                    slot_1_weps[#slot_1_weps+1] = wep
                end
            end
            if (#slot_1_weps > 1) then
                -- remove current
                local carry_wep = player(id,"weapontype")
                local ii = 0
                while (ii < #slot_1_weps) do
                    ii = ii + 1
                    local lot_1_wep = slot_1_weps[ii]
                    if (carry_wep == lot_1_wep) then
                        slot_1_weps[ii] = slot_1_weps[#slot_1_weps]
                        slot_1_weps[#slot_1_weps] = nil
                    end
                end
                -- remove random
                local rem = slot_1_weps[math.ceil(math.random(#slot_1_weps))]
                print("removed "..itemtype(rem,"name").." from "..player(id,"name"))
                parse("strip "..id.." "..rem)
            end
        end
    end
end

addhook("second","mb.second")
function mb.second()
    -- grace period
    if mb.grace.left > 0 then
        mb.grace.left = mb.grace.left - 1
        if mb.grace.left % 10 == 0 and mb.grace.left > 0 then
            msg(misc.colorTextPercent((100/mb.grace.max)*mb.grace.left).." !!! PVP IS OFF !!!")
        end
        if mb.grace.left == round(mb.grace.max*0.5) then
            msg(misc.rgb(0,255,0).."Grace period: 50% of the time has passed ("..mb.grace.left.." seconds)")
        end
        if mb.grace.left <= 10 then
            msg(misc.rgb(255,0,0).."Grace period: "..mb.grace.left.." seconds left!")
            if mb.grace.left == 0 then
                msg(misc.rgb(255,0,0).."Grace period: has ended !!!PVP IS ON!!!")
                mb.addRepAll(20,"Time for COMBAT")
                mb.grace.leftFlag = mb.grace.maxFlag
                msg(misc.rgb(0,255,0).."Grace period for flags: "..mb.grace.leftFlag.." seconds")
            end
        end
    else
        if mb.grace.leftFlag > 0 then
            mb.grace.leftFlag = mb.grace.leftFlag - 1
            if mb.grace.leftFlag == 0 then
                msg(misc.rgb(255,0,0).."Grace period: you can now capture flags!")
            end
        end
    end
    -- player loop
    pl = player(0,"tableliving")
    i = 0
    while (i < #pl) do
        i = i + 1
        if (player(pl[i],"bot") == false) then
            -- bank system
            local money = player(pl[i],"money")
            local money_change = 10000 - money
            if money_change > 0 then
                money_change = math.min(money_change,mb.player[pl[i]].bank)
                if money_change > 0 then
                    mb.player[pl[i]].bank = mb.player[pl[i]].bank - money_change
                    mb.drawbank(pl[i])
                    parse("setmoney "..pl[i].." "..money+money_change)
                end
            else
                if money_change < 0 then
                    money_change = math.min(money_change,mb.player[pl[i]].bank)
                    mb.player[pl[i]].bank = mb.player[pl[i]].bank - money_change
                    mb.drawbank(pl[i])
                    parse("setmoney "..pl[i].." "..money+money_change)
                end
            end
            -- stealth speed boost
            if (player(pl[i],"armor") == 206) then
                local heal = 1
                if ((smokeNinja == nil) == false) then
                    if (smokeNinja.ninja[pl[i]]) then
                        heal = 3
                    end
                end
                parse("sethealth "..pl[i].." "..player(pl[i],"health")+heal)
                cs2d.speedmod.call(pl[i],7)
            else
                cs2d.speedmod.call(pl[i],0)
            end
            -- medic armor heals maxhealth
            -- maybe
        end
    end
    -- fumble with shield+laser
    for i,id in ipairs(player(0,"tableliving")) do
        if (math.random() < 0.2) then
            if (player(id,"weapontype") == 41) then
                local weps = playerweapons(id)
                for i, wep in ipairs(weps) do
                    if (wep == 45) then
                        msg2(id,"You fumble with your heavy weapons and accidently drop you shield")
                        parse("strip "..id.." ".." 41")
                        parse("spawnitem 41 "..player(id,"tilex").." "..player(id,"tiley"))
                    end
                end
            end
        end
    end
end

addhook("objectdamage","mb.objectdamage")
function mb.objectdamage(obj_id,damage,id)
    if mb.grace.left > 0 then
        if damage > 0 then
            if (object(obj_id,"type") == 2) or (object(obj_id,"type") == 6) then -- barbed wire or gatefield
                return 0
            else
                msg2(id,misc.rgb(255,0,0).."You cant damage structure during grace period!")
                return 1
            end
        end
    end
    if (not(id == object(obj_id,"player"))) then
        local limit = 50
        local rep_per_limit = 1
        local playerData = mb.player[id]
        if (player(id,"team") == object(obj_id,"team")) or (damage < 0) then
            playerData.teamBuildingDamage = playerData.teamBuildingDamage + damage
            local rep_change = 0
            while (playerData.teamBuildingDamage > limit) do
                playerData.teamBuildingDamage = playerData.teamBuildingDamage - limit
                rep_change = rep_change - (rep_per_limit*5)
            end
            while (playerData.teamBuildingDamage < -limit) do
                playerData.teamBuildingDamage = playerData.teamBuildingDamage + limit
                rep_change = rep_change + rep_per_limit
            end
            if rep_change > 0 then
                mb.addRep(id,rep_change,"Repaired a friendly building")
            end
            if rep_change < 0 then
                mb.addRep(id,rep_change*2,"Damaged a friendly building")
                if (mb.player[id].rep < 1000) then
                    msg2(id,misc.rgb(255,0,0).."Cant damage team building, rep too low!")
                    return 1
                end
                timer2(1,{obj_id},function(obj_id)
                    if not(object(obj_id,"exists")) then
                        mb.addRep(id,-25,"Destroyed a friendly building")
                    end
                end)
            end
        else
            if sdp.is_damagable(obj_id,damage,id) then
                playerData.teamBuildingDamage = playerData.teamBuildingDamage - damage
                local rep_change = 0
                while (playerData.teamBuildingDamage < -limit) do
                    playerData.teamBuildingDamage = playerData.teamBuildingDamage + limit
                    rep_change = rep_change + rep_per_limit
                end
                if rep_change > 0 then
                    mb.addRep(id,rep_change,"Damaged a enemy building")
                    timer2(1,{obj_id},function(obj_id)
                        if not(object(obj_id,"exists")) then
                            mb.addRep(id,10,"Destroyed an enemy building")
                        end
                    end)
                end
            else
                if ((object(obj_id,"type") == 2) or (object(obj_id,"type") == 6)) then --Barbed Wire or gate field
                    return 0
                else
                    if (id > 0) then
                        msg2(id,misc.rgb(255,0,0).."You cannot damage buildings outside your screen")
                    end
                    return 1
                end
            end
        end
    else
        return 0
    end
end

addhook("drop","mb.drop")
function mb.drop(id,iid,type_id,ain,a,mode,x,y)
    --msg("drop: "..itemtype(type_id,"name").." ("..type_id..")")
    if (type_id == 53) then -- smoke granate
        return 1
    end
    if player(id,"bot") then
        if type_id == (3-player(id,"team")) then
            return 1
        end
    end
    return 2
end

addhook("walkover","mb.walkover")
function mb.walkover(id,iid,type_id,ain,a,mode)
    --msg("walkover: "..itemtype(type_id,"name").." ("..type_id..")")
    user_armor = player(id,"armor")
    -- clear armour
    if (type_id >= 79) and (type_id <= 84) then --potential armor switching!
        local nearby_items = closeitems(id,0)
        local player_armor = user_armor-122
        local i = 0
        while (i < #nearby_items) do
            i = i + 1
            local item_type = item(nearby_items[i],"type")
            --msg("nearby: "..itemtype(item_type,"name").." ("..item_type..")")
            -- code
            if (item_type == player_armor) then
                parse("setarmor "..id.." 0")
                --msg("stripped armor ("..itemtype(player_armor,"name")..")")
            end
        end
    end
    -- continue normal
    if user_armor > 0 then
        if not((user_armor-122) == type_id) then
            for i=1,#mb.armory do
                local armor_id = mb.armory[i][6]
                if type_id == armor_id then
                    local tx = player(id,"tilex")
                    local ty = player(id,"tiley")
                    parse("spawnitem "..(user_armor-122).." "..tx.." "..ty)
                end
            end
        end
    end
end

function mb.giveArmor(id,armor_id)
    user_armor = player(id,"armor")
    if user_armor > 0 then
        local tx = player(id,"tilex")
        local ty = player(id,"tiley")
        parse("spawnitem "..(user_armor-122).." "..tx.." "..ty)
    end
    parse("equip "..id.." "..armor_id)
end

addhook("build","mb.build")
function mb.build(id,type_id,x,y,mode,objectid)
    local building = const.buildings[type_id]
    if not (building == nil) then
        mb.addRep(id,building.price/100,"Constructed a "..building.name)
    end
end

addhook("team","mb.team")
function mb.team(id, newTeam, look)
    local oldTeam = player(id,"team")
    if ((oldTeam == 0) or (newTeam == 0)) then
        if ((oldTeam == 1) or (oldTeam == 2)) then
            mb.riskEquipment(id)
        end
        mb.saveEquipment(id)
        mb.writeAndSave(id)
        parse("strip "..id.." 0")
        parse("setarmor "..id.." 0")
    else
        mb.addRep(id,-1,"Traitor")
    end
end

function mb.riskEquipment(id)
    local health = (100/player(id,"maxhealth"))*player(id,"health")
    if (health < 90) then
        health = (1/90)*health
        --print(player(id,"name")..": "..health)
        if (math.random() > health) then
            msg2(id,misc.rgb(255,0,0).."You switched to spec with too low hp, equipment lost!")
            mb.dropAllEquipment(id)
            print(misc.rgb(255,0,0)..name.." roll failed, equipment dropped")
        end
    end
end

function mb.dropAllEquipment(id)
    local armor = player(id,"armor")
    local tx = player(id,"tilex")
    local ty = player(id,"tiley")
    if armor > 0 then
        armor = armor - 122
        parse("spawnitem "..armor.." "..tx.." "..ty)
    end
    for _,wep in ipairs(playerweapons(id)) do
        if (wep ~= 50) then
            parse("spawnitem "..wep.." "..tx.." "..ty)
            parse("strip "..id.." "..wep)
        end
    end
    parse("strip "..id.." 0")
end
