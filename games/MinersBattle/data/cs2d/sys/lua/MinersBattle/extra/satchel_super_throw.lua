addhook("attack","satchel_super_throw_attack")
function satchel_super_throw_attack(id)
    weapon = player(id,"weapontype")
    if (weapon == 89) then
        x = player(id,"x")
        y = player(id,"y")
        i = 0
        while (i < 3) do
            i = i + 1
            dist = 300/i
            parse("spawnprojectile "..id.." 89 "..x.." "..y.." "..dist.." "..player(id,"rot")-(i*10))
            parse("spawnprojectile "..id.." 89 "..x.." "..y.." "..dist.." "..player(id,"rot")+(i*10))
        end
    end
end