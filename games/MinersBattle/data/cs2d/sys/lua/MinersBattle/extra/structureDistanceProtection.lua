sdp = {}
sdp.screen = {}
sdp.screen.width = 850
sdp.screen.height = 480

function sdp.is_damagable(object_id,damage,player_id)
    --msg("Structure id: "..object_id.." Dmg: "..damage.." Player: "..player)
    build_x = object(object_id,"x")+16 -- (in pixels)
    build_y = object(object_id,"y")+16 -- (in pixels)
    px = player(player_id,"x") -- (in pixels)
    py = player(player_id,"y") -- (in pixels)
    if (player_id > 0) then -- ignore non player damage
        if (player(player_id,"team") == object(object_id,"team")) == false then -- if on same team as building, ignore
            if (sdp.insideScreen(build_x,build_y,px,py)) then
                return true
            else
                return false
            end
        end
    end
end

function sdp.insideScreen(build_x,build_y,px,py)
    local width = (sdp.screen.width/2)+16 -- ~335
    local height = (sdp.screen.height/2)+16
    local x_dist = math.abs(build_x - px)
    local y_dist = math.abs(build_y - py)
    return ((x_dist < width) and (y_dist < height))
end