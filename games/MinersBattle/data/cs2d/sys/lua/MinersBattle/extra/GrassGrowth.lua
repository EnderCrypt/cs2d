GrassGrowth = {}
GrassGrowth.surrounding = {{-1,0},{1,0},{0,-1},{0,1}}
GrassGrowth.tile = {dirt=12,grass=13}
GrassGrowth.updateRate = 3

GrassGrowth.explosionPower = {}
GrassGrowth.explosionPower[47] = 40  -- RPG launcher
GrassGrowth.explosionPower[48] = 15  -- Rocket Launcher
GrassGrowth.explosionPower[51] = 20  -- HE
GrassGrowth.explosionPower[73] = 25  -- Molotov Cocktail
GrassGrowth.explosionPower[75] = 0   -- Snowball
GrassGrowth.explosionPower[76] = 250 -- Air Strike
GrassGrowth.explosionPower[86] = -1  -- Gut Bomb
GrassGrowth.explosionPower[89] = 20  -- Satchel Charge

freehook("always","GrassGrowth.always")
addhook("always","GrassGrowth.always")
function GrassGrowth.always()
	local i = 0
	while (i < GrassGrowth.updateRate) do
		i = i + 1
		GrassGrowth.update()
	end
end

freehook("projectile","GrassGrowth.projectile")
addhook("projectile","GrassGrowth.projectile")
function GrassGrowth.projectile(id,weapon,x,y)
	local power = GrassGrowth.explosionPower[weapon]
	if (power ~= nil) then
		for i=0,power do
			local dir = math.random()*360
			local length = math.random()*(i*2.5)
			local dx = x + misc.lengthdir_x(dir,length)
			local dy = y + misc.lengthdir_y(dir,length)
			local tx = misc.pixel_to_tile(dx)
			local ty = misc.pixel_to_tile(dy)
			GrassGrowth.setState(tx,ty,true)
		end
	else
		print("GrassGrowth: unknown projectile: "..weapon)
	end
end

freehook("attack","GrassGrowth.attack")
addhook("attack","GrassGrowth.attack")
function GrassGrowth.attack(id)
	local weapon = player(id,"weapontype")
	if (weapon == 46) then
		for i=0,5 do
			local x = player(id,"x")
			local y = player(id,"y")
			local dir = player(id,"rot")+(math.random()*32)-16
			local length = 32+(math.random()*150)
			local dx = x + misc.lengthdir_x(dir,length)
			local dy = y + misc.lengthdir_y(dir,length)
			local tx = misc.pixel_to_tile(dx)
			local ty = misc.pixel_to_tile(dy)
			GrassGrowth.setState(tx,ty,true)
		end
	end
end

function GrassGrowth.update()
	local x = math.ceil(math.random()*const.map.xsize)
	local y = math.ceil(math.random()*const.map.ysize)
	if (GrassGrowth.isBlocked(x,y)) then
		GrassGrowth.setState(x,y,true)
	else
		if (GrassGrowth.isSurroundedByGrass(x,y)) then
			GrassGrowth.setState(x,y,false)
		end
	end
end

function GrassGrowth.setState(x,y,block)
	local current = tile(x,y,"frame")
	if (block) then
		if (current == GrassGrowth.tile.grass) then
			parse("settile "..x.." "..y.." "..GrassGrowth.tile.dirt)
		end
	else
		if (current == GrassGrowth.tile.dirt) then
			parse("settile "..x.." "..y.." "..GrassGrowth.tile.grass)
		end
	end
end

function GrassGrowth.isSurroundedByGrass(x,y)
	local index = math.ceil(#GrassGrowth.surrounding * math.random())
	if index == 0 then
		index = index + 1
	end
	local dir = GrassGrowth.surrounding[index]
	local rx = x+dir[1]
	local ry = y+dir[2]
	if (tile(rx,ry,"frame") == GrassGrowth.tile.grass) then
		-- print(rx..", "..ry)
		if (GrassGrowth.isBlocked(rx,ry) == false) then -- double check that this other grass isnt blocked
			return true
		end
	end
	return false
end

--[[
function GrassGrowth.isSurroundedByGrass(x,y)
	local i = 0
	while (i < #GrassGrowth.surrounding) do
		i = i + 1
		local dir = GrassGrowth.surrounding[i]
		local rx = x+dir[1]
		local ry = y+dir[2]
		if (tile(rx,ry,"frame") == GrassGrowth.tile.grass) then
			if (GrassGrowth.isBlocked(rx,ry) == false) then -- double check that this other grass isnt blocked
				return true
			end
		end
	end
	return false
end
]]--

function GrassGrowth.isBlocked(x,y)
	if structureTracker == nil then
		print("structureTracker: "..tostring(structureTracker))
	end
	if structureTracker.map == nil then
		print("structureTracker.map: "..tostring(structureTracker.map))
	end
	local buildingID = structureTracker.map[x][y]
	if (buildingID == nil) then
		print(x..", "..y.." = "..tostring(buildingID))

	end
	if (buildingID > 0) then
		local buildingType = object(buildingID,"type")
		local buildingData = const.buildings[buildingType]
		if (buildingData ~= nil) then
			if (buildingData.blockingAir) then
				return true
			end
		end
	end
	if (entity(x,y,"typename") == "Env_Breakable") then
		return true
	end
	return false
end
