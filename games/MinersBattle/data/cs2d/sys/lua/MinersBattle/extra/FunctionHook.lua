--FunctionHook = {}

FunctionHook = function(func) -- create a new callback
    local data = {}
    data.call = function(...) -- call the function
        for i=1,#data.hooks do
            data.hooks[i](unpack({...}))
        end
        return func(unpack({...}))
    end
    data.hook = function(callBack) -- create a new hook
        data.hooks[#data.hooks+1] = callBack
    end
    data.hooks = {} -- stores all hooks
    return data
end

--[[ example
data = FunctionHook(function(name)
    print("hello "..name)
end)

data.hook(function(name)
    print("something is calling the hello function")
end)

data.call("magnus")

OUPUTS:
> "something is calling the hello function"
> "hello magnus"
--]]

--------------------
--{ CUSTOM HOOKS }--
--------------------

cs2d = {}

cs2d.speedmod = FunctionHook(function(id, speed)
    parse("speedmod "..id.." "..misc.round(speed))
end)

cs2d.setpos = FunctionHook(function(id, x, y)
    parse("setpos "..id.." "..misc.round(x).." "..misc.round(y))
end)

cs2d.sethealth = FunctionHook(function(id, health)
    parse("sethealth "..id.." "..health)
end)