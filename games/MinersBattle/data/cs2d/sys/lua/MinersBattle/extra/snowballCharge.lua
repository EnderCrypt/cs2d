snowballCharge = {}
snowballCharge.alwaysKnockback = true
snowballCharge.knockbackDistance = 16
snowballCharge.chargeKnockbackDistance = 32
snowballCharge.knockback = {}
snowballCharge.chargeTime = 7

addhook("join","snowballCharge.join")
function snowballCharge.join(id)
    snowballCharge.knockback[id] = false
end

addhook("attack","snowballCharge.attack")
function snowballCharge.attack(id)
    weapon = player(id,"weapontype")
    if (weapon == 75) then
        local worker = LuaWorker.new(function(self,workspace)
            local x = player(workspace.id,"x")
            local y = player(workspace.id,"y")
            local rot = player(workspace.id,"rot")
            if (x ~= workspace.position_x) or (y ~= workspace.position_y) then
                msg2(workspace.id, misc.rgb(255,0,0).."You moved, the charge was interrupted")
                self:delete()
                return
            end
            local angDiff = misc.angleDiffrence(workspace.position_rot, rot)
            if (math.abs(angDiff) > 20) then
                msg2(workspace.id, misc.rgb(255,0,0).."You rotated, the charge was interrupted")
                self:delete()
                return
            end
            workspace.position_rot = rot
            if (workspace.charge) then
                left_x = x + misc.lengthdir_x(rot-180+workspace.dir, 32)
                left_y = y + misc.lengthdir_y(rot-180+workspace.dir, 32)
                right_x = x + misc.lengthdir_x(rot-180-workspace.dir, 32)
                right_y = y + misc.lengthdir_y(rot-180-workspace.dir, 32)
                parse("spawnprojectile "..workspace.id.." 75 "..left_x.." "..left_y.." 16 "..rot-90+workspace.dir)
                parse("spawnprojectile "..workspace.id.." 75 "..right_x.." "..right_y.." 16 "..rot+90-workspace.dir)
                --
                if (workspace.dir >= (180-(180/snowballCharge.chargeTime))) then
                    workspace.charge = false
                end
                workspace.dir = workspace.dir + (180/snowballCharge.chargeTime)
            else
                if (player(id,"health") > 0) then
                    snowballCharge.knockback[workspace.id] = true
                    timer2(1000,{workspace.id},function(id)
                        snowballCharge.knockback[id] = false
                    end)
                    --[[
                    local i = 20
                    while (i > 0) do
                        local dist = 400 - (i* 10)
                        parse("spawnprojectile "..workspace.id.." 75 "..x.." "..y.." "..dist.." "..rot - i)
                        parse("spawnprojectile "..workspace.id.." 75 "..x.." "..y.." "..dist.." "..rot + i)
                        i = i - 1
                    end
                    ]]--
                    local i = 0.5
                    while (i < 20) do
                        local dist = 400 - (i* 10)
                        parse("spawnprojectile "..workspace.id.." 75 "..x.." "..y.." "..dist.." "..rot - i)
                        parse("spawnprojectile "..workspace.id.." 75 "..x.." "..y.." "..dist.." "..rot + i)
                        i = i * 1.1
                    end
                end
                self:delete()
                return
            end
        end)
        worker.workspace.id = id
        worker.workspace.charge = true
        worker.workspace.dir = 0
        worker.workspace.position_x = player(id,"x")
        worker.workspace.position_y = player(id,"y")
        worker.workspace.position_rot = player(id,"rot")
        worker:setFrequency(LuaWorkerData.ms100)
        worker:start()
    end
end

addhook("hit","snowballCharge.hit")
function snowballCharge.hit(id,src,weapon,hpdmg,apdmg,rawdmg)
    if (weapon == 75) then
        if (player(id,"weapontype") == 41) then
            local dir = player(id,"rot")
            local direction = misc.point_direction(player(id,"x"),player(id,"y"),player(src,"x"),player(src,"y"))
            local angleDiff = misc.angleDiffrence(dir, direction)
            if (math.abs(angleDiff) < 50) then
                local x = player(id,"x") + misc.lengthdir_x(dir, 16)
                local y = player(id,"y") + misc.lengthdir_y(dir, 16)
                local dist = 500
                parse("spawnprojectile "..id.." 75 "..x.." "..y.." "..dist.." "..(dir+angleDiff))
                return 1
            end
        else
            if ( (snowballCharge.alwaysKnockback) or (snowballCharge.knockback[src]) ) then
                local dir = misc.point_direction(player(src,"x"),player(src,"y"),player(id,"x"),player(id,"y"))
                local distance = snowballCharge.knockbackDistance
                if (snowballCharge.knockback[id]) then
                    distance = snowballCharge.chargeKnockbackDistance
                end
                local x = player(id,"x") + misc.lengthdir_x(dir,distance)
                local y = player(id,"y") + misc.lengthdir_y(dir,distance)
                local tx = misc.pixel_to_tile(x)
                local ty = misc.pixel_to_tile(y)
                if (tile(tx,ty,"walkable")) then
                    cs2d.setpos.call(id,x,y)
                end
            end
        end
    end
end