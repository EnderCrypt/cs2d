structureTracker = {}
structureTracker.map = nil

function structureTracker.init()
	-- clear
	structureTracker.map = {}
	local x = 0
	while (x <= const.map.xsize) do
		structureTracker.map[x] = {}
		local y = 0
		while (y <= const.map.ysize) do
			structureTracker.map[x][y] = 0
			y = y + 1
		end
		x = x + 1
	end
	-- update with buildings
	local objs = object(0,"table")
	local i = 0
	while (i < #objs) do
		i = i + 1
		local obj = objs[i]
		if (object(obj,"type") <= 23) then --building
			local x = object(obj,"tilex")
			local y = object(obj,"tiley")
			structureTracker.map[x][y] = obj
		end
	end
end

structureTracker.init() -- might be a double init

--freehook("startround","structureTracker.startround")
addhook("startround","structureTracker.startround")
function structureTracker.startround(mode)	
	timer2(1, {}, structureTracker.init)
end

--freehook("build","structureTracker.build")
addhook("build","structureTracker.build")
function structureTracker.build(id,type,x,y,mode,objectid)
	structureTracker.map[x][y] = objectid
end

--freehook("objectkill","structureTracker.objectkill")
addhook("objectkill","structureTracker.objectkill")
function structureTracker.objectkill(objectid,player)
	local x = object(objectid,"tilex")
	local y = object(objectid,"tiley")
	structureTracker.map[x][y] = 0
end
