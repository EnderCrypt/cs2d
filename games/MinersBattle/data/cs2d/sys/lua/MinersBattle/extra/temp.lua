-- addhook("say", "spectator_say")
function spectator_say(id, text)
	if player(id, "team") == 0 then
		msg(player(id, "name").." *SPEC*: "..text)
		return 1
	end
end


-- addhook("second", "bot_restart")
function bot_restart()
	if math.random() < 0.2 then
		parse("bot_freeze 0")
	end
end
