bot_auto = {}
bot_auto.minimum = 3

addhook("join","bot_auto.join")
function bot_auto.join(id)
    bot_auto.balance()
end

addhook("leave","bot_auto.leave")
function bot_auto.leave(id)
    bot_auto.balance()
end

addhook("team","bot_auto.team")
function bot_auto.team(id, team, look)
    bot_auto.balance()
end

function bot_auto.balance()
    timer2(100,{},function()
        bot_auto.balanceTeam(1,"t")
        bot_auto.balanceTeam(2,"ct")
    end)
end

-- ADD BOTS AT START
bot_auto.balance()

function bot_auto.balanceTeam(team_num,team_char)
    local pl = player(0,"team"..team_num)
    local players = 0
    local bots = 0
    local i = 0
    while (i < #pl) do
        i = i + 1
        if player(pl[i],"bot") then
            bots = bots + 1
        else
            players = players + 1
        end
    end
    target_bots = 3 - players
    target_bots = target_bots - bots
    --print("# bots to add: "..target_bots)
    local i = 0
    while (i < target_bots) do
        i = i + 1
        parse("bot_add_"..team_char)
    end
    local i = 0
    while (i > target_bots) do
        i = i - 1
        parse("bot_remove_"..team_char)
    end
end