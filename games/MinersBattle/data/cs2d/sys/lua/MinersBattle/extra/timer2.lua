--[[
timer(time,"func",["p"],[c])
timer2(time,{arg1,arg2},function()

end)

example:
timer2(1,{id},function(id)
    parse("equip "..id.." 45")
end)
-- this example would fit well in the spawn hook for example, since you cannot equip in spawn hook because it occurs before spawn

]]--


timer2_container = {}
function timer2(delay,args,callback)
    local index = #timer2_container+1
    local temp = {}
    temp.args = args
    temp.callback = callback
    timer2_container[index] = temp
    timer(delay,"timer2_handler",index)
end

function timer2_handler(index)
    local temp = timer2_container[tonumber(index)]
    temp.callback(unpack(temp.args))
    timer2_container[index] = nil
end