gutDrone = {}
gutDrone.itemID = 86
gutDrone.itemBulletID = 75
gutDrone.expectDistance = 64
gutDrone.maxShootingDistance = 300
gutDrone.jumpAngle = 25

addhook("projectile", "gutDrone.projectile")
function gutDrone.projectile(id, weapon, x, y)
	if (weapon == gutDrone.itemID) then
		local tx = misc.pixel_to_tile(x)
		local ty = misc.pixel_to_tile(y)
		if (tile(tx,ty,"walkable") == false) then
			return
		end
		local ox = player(id,"x")
		local oy = player(id,"y")
		-- check where to bounce to
		local pdist = misc.point_distance(ox,oy,x,y)
		local pdir = misc.point_direction(ox,oy,x,y)
		local target_x = ox + misc.lengthdir_x(pdir+gutDrone.jumpAngle,gutDrone.expectDistance)
		local target_y = oy + misc.lengthdir_y(pdir+gutDrone.jumpAngle,gutDrone.expectDistance)
		
		-- if collided with player
	    if (gutDrone.isHitPlayer(id, x, y)) then
	    	return
	    end

	    -- shot at enemies
	    local target = gutDrone.getNearbyEnemy(id) 
	    if (target ~= nil) then
	    	local ex = player(target,"x")
			local ey = player(target,"y")
	    	local edist = misc.point_distance(x,y,ex,ey)
	    	if (edist < gutDrone.maxShootingDistance) then
		        local dist = misc.point_distance(x,y,ex,ey)
		        local dir = misc.point_direction(x,y,ex,ey)
		        parse("spawnprojectile "..id.." "..gutDrone.itemBulletID.." "..x.." "..y.." "..(edist*2).." "..dir)
		    end
	    end

    	-- bounce
    	local nearby = gutDrone.getNearbyPlayer(target_x,target_y)
    	if ( (nearby.id ~= id) and (nearby.dist < 100) ) then
    		local dir = misc.point_direction(player(nearby.id,"x"),player(nearby.id,"y"),x,y)
    		target_x = x+misc.lengthdir_x(dir,105)
    		target_y = y+misc.lengthdir_y(dir,105)
    	end
	    local dist = misc.point_distance(x,y,target_x,target_y)
	    local dir = misc.point_direction(x,y,target_x,target_y)
	    parse("spawnprojectile "..id.." "..gutDrone.itemID.." "..x.." "..y.." "..math.min(dist,32).." "..dir)
	end
end


function gutDrone.getNearbyPlayer(x,y)
	local closest_id = nil
	local closest_dist = math.huge
	local pl = player(0,"tableliving")
	for i=1,#pl do
		local id = pl[i]
		local px = player(id,"x")
		local py = player(id,"y")
		local dist = misc.point_distance(x,y,px,py)
		if (dist < closest_dist) then
			closest_id = id
			closest_dist = dist
		end
	end
	return {id=closest_id,dist=closest_dist}
end

function gutDrone.getNearbyEnemy(id)
	local x = player(id,"x")
	local y = player(id,"y")
	local closest_id = nil
	local closest_dist = math.huge
	local team = player(id,"team")
	local pl = player(0,"team"..(3-team).."living")
	for i=1,#pl do
		local other = pl[i]
		local other_x = player(other,"x")
		local other_y = player(other,"y")
		local dist = misc.point_distance(x,y,other_x,other_y)
		if (dist < closest_dist) then
			closest_id = other
			closest_dist = dist
		end
	end
	return closest_id
end

function gutDrone.isHitPlayer(owner, x, y)
	local pl = player(0,"tableliving")
	for i=1,#pl do
		local other = pl[i]
		if (other ~= id) then
			local other_x = player(other,"x")
			local other_y = player(other,"y")
			local dist = misc.point_distance(x,y,other_x,other_y)
			if (dist < 30) then
				return true
			end
		end
	end
	return false
end