smokeBlink = {}
smokeBlink.smokeWeapon = 53
smokeBlink.smokeDist = 32
smokeBlink.players = {}

addhook("spawn","smokeBlink.spawn")
function smokeBlink.spawn(id)
    timer2(1,{id},function(id)
        smokeBlink.players[id] = {x=player(id,"x"),y=player(id,"y")}
    end)
end

function smokeBlink.getEscapeBlockage(id)
	local itemlist=playerweapons(id)
	for _,wid in pairs(itemlist) do
		if (wid == 45) then -- laser
			return "Unfortunately your laser is too heavy to teleport with!"
		end
	end
	return nil
end

addhook("projectile","smokeBlink.projectile")
function smokeBlink.projectile(id,weapon,x,y)
    if weapon == smokeBlink.smokeWeapon then
        dist = misc.point_distance(x,y,player(id,"x"),player(id,"y"))
        if (dist <= smokeBlink.smokeDist) then -- tp home
				local escapeBlockedMessage = smokeBlink.getEscapeBlockage(id)
				if escapeBlockedMessage == nil then
					msg2(id, misc.rgb(200,100,200).."you performed an Escape jump!")
					cs2d.setpos.call(id,smokeBlink.players[id].x,smokeBlink.players[id].y)
            else
					msg2(id, misc.rgb(255, 0, 0)..escapeBlockedMessage)
            end
        else -- blink
            msg2(id,misc.rgb(200,100,200).."you performed a Ninja blink!")
             cs2d.setpos.call(id,x,y)
            --parse("setpos "..id.." "..x.." "..y)
        end
        -- minersbattle specific code --
        if not (mbm == nil) then
            local chance = 0
            local rep_remove = 0
            local rep_remove_inc = 2
            local rep_remove_inc_inc = 0
            local temp_rep = mb.player[id].rep
            local count = 0
            while (temp_rep >= rep_remove) do
                count = count + 1
                chance = chance + ((100 - chance) / 100)
                rep_remove_inc_inc = rep_remove_inc_inc + 0.00025
                rep_remove_inc = rep_remove_inc + rep_remove_inc_inc
                rep_remove = rep_remove + rep_remove_inc
                temp_rep = temp_rep - rep_remove
            end
            local recovered_smoke = ((math.random()*100) < chance)
            
            if (recovered_smoke) then
                msg2(id,misc.rgb(0,255,100).."Your rep allowed you to instantly recover your smoke granade! ("..misc.round(chance,3).."%)")
                parse("equip "..id.." 53")
                parse("setweapon "..id.." 53")
            else
                msg2(id,misc.rgb(255,0,100).."You failed to recover your smoke due to low rep/bad luck ("..misc.round(chance,3).."%)")
            end
        end
        -- spedpreventor specific code --
        if not (speedPreventor == nil) then
            speedPreventor.immune[id] = true
        end
    end
end
