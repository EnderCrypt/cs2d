tileFire = {}
tileFire.flamethrower = 2 -- higher number equals more fire
tileFire.damageFactor = 1
tileFire.tiles = {}
tileFire.fires = {}

tileFire.explosiveFireCause = {}
tileFire.explosiveFireCause[47] = 150  -- RPG launcher
tileFire.explosiveFireCause[48] = 25  -- Rocket Launcher
tileFire.explosiveFireCause[49] = 10  -- Granade Launcher
tileFire.explosiveFireCause[51] = 50  -- HE
tileFire.explosiveFireCause[54] = 0 -- Flare
tileFire.explosiveFireCause[73] = 50  -- Molotov Cocktail
tileFire.explosiveFireCause[75] = 0   -- Snowball
tileFire.explosiveFireCause[76] = 750 -- Air Strike
tileFire.explosiveFireCause[86] = 0  -- GutBomb
tileFire.explosiveFireCause[89] = 20  -- Satchel Charge

function tileFire.addTile(startTile,energy,endTile)
	tileFire.tiles[startTile+1] = {energy=energy,tile=endTile}
end

tileFire.addTile(0,-1000,0)

function tileFire.add(x, y, energy)
	local fire = tileFire.fires[x..","..y]
	if (fire == nil) then
		local fire = {}
		fire.x = x
		fire.y = y
		fire.px = misc.tile_to_pixel(x)
		fire.py = misc.tile_to_pixel(y)
		fire.energy = energy
		fire.feed = 0
		fire.tile = tile(x,y,"frame")
		if (fire.tile == false) then
			fire.tile = 0
		end
		local t = tileFire.tiles[fire.tile+1]
		if (t ~= nil) then
			fire.feed = t.energy
		end
		tileFire.fires[x..","..y] = fire
	else
		fire.energy = fire.energy + energy
	end
end

function tileFire.fireGetDamage(fire)
	local dmg = fire.energy/100
	local frac = dmg - math.floor(dmg)
	dmg = math.floor(dmg)
	if (math.random() < frac) then
		dmg = dmg + 1
	end
	return dmg * tileFire.damageFactor
end

function tileFire.fireUpdate(fire)
	fire.energy = fire.energy - 0.1
	if (fire.energy <= 0) then
		return false
	end
	if (fire.feed ~= 0) then
		local change = fire.feed * 0.1
		fire.energy = fire.energy + change
		fire.feed = fire.feed - change
		if ( (fire.feed > -1) and (fire.feed < 1) ) then
			fire.feed = 0
			local t = tileFire.tiles[fire.tile+1]
			local toTile = t.tile
			if (fire.tile ~= toTile) then
				parse("settile "..fire.x.." "..fire.y.." "..toTile)
			end
		end
		if (fire.energy <= 0) then
			return false
		end
	end
	if (math.random() < fire.energy/1000) then
		local dirs = {{-1,0},{1,0},{0,-1},{0,1}}
		local dir = dirs[math.ceil(math.random()*#dirs)]
		local change = fire.energy/2
		tileFire.add(fire.x+dir[1], fire.y+dir[2], change)
		fire.energy = fire.energy - change
	end
	misc.effect("fire",fire.px,fire.py,tileFire.fireGetDamage(fire),16,0,0,0)
	return true
end

function tileFire.fireDamage(fire, id)
	local health = player(id,"health")
	local damage = tileFire.fireGetDamage(fire)
	health = health - misc.calcDamage(id, damage)
	if (health <= 0) then
		parse("customkill 0 \"Fire\" "..id)
	else
		parse("sethealth "..id.." "..health)
	end
end

addhook("ms100","tileFire.ms100")
function tileFire.ms100()
	-- update fires
	for index,fire in pairs(tileFire.fires) do
		if (tileFire.fireUpdate(fire) == false) then
			tileFire.fires[index] = nil
		end
	end
	-- affect players
	for _,id in ipairs(player(0,"tableliving")) do
		local x = player(id,"tilex")
		local y = player(id,"tiley")
		for _,fire in pairs(tileFire.fires) do
			if ( (x >= fire.x-1) and (y >= fire.y-1) and (x <= fire.x+1) and (y <= fire.y+1) ) then
				tileFire.fireDamage(fire, id)
			end
		end
	end
end

addhook("second","tileFire.second")
function tileFire.second()
	-- check players
	for _,id in ipairs(player(0,"tableliving")) do
		local x = player(id,"tilex")
		local y = player(id,"tiley")
		for _,fire in pairs(tileFire.fires) do
			if ( (x >= fire.x-1) and (y >= fire.y-1) and (x <= fire.x+1) and (y <= fire.y+1) ) then
				msg2(id,misc.rgb(255,0,0).."Warning: your burning!")
				break;
			end
		end
	end
end

addhook("attack","tileFire.attack")
function tileFire.attack(id)
	local weapon = player(id,"weapontype")
	if (weapon == 46) then
		for i=0,tileFire.flamethrower do
			local x = player(id,"x")
			local y = player(id,"y")
			local dir = player(id,"rot")+(math.random()*32)-16
			local length = 64+(math.random()*128)
			local dx = x + misc.lengthdir_x(dir,length)
			local dy = y + misc.lengthdir_y(dir,length)
			local tx = misc.pixel_to_tile(dx)
			local ty = misc.pixel_to_tile(dy)
			tileFire.add(tx, ty, 5)
		end
	end
end

addhook("projectile","tileFire.projectile")
function tileFire.projectile(id,weapon,x,y)
	local power = tileFire.explosiveFireCause[weapon]
	if (weapon == 54) then -- flare
		local LTW = LuaWorker.new(function(worker,workspace,arguments)
		    tileFire.add(workspace.x, workspace.y, 32)
		end)
		LTW.workspace.x = misc.pixel_to_tile(x)
		LTW.workspace.y = misc.pixel_to_tile(y)
		LTW:setTimeout(32)
		LTW:setFrequency(LuaWorkerData.second)
		LTW:start()
		return
	end
	if (power ~= nil) then
		local tx = misc.pixel_to_tile(x)
		local ty = misc.pixel_to_tile(y)
		tileFire.add(tx, ty, power)
		return
	end
	print("tileFire: unknown projectile: "..weapon)
end

addhook("startround", "tileFire.startround")
function tileFire.startround(mode)
	tileFire.fires = {}
end