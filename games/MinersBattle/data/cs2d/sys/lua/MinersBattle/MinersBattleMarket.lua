mbm = {}
mbm.resName = {"Chemicals","Wood","Gold","Steel","Coal","Iron"}
mbm.market = {}
mbm.colorSensitivity = 10 -- how sensitive the colors are for cost changes
mbm.sellPenalty = 5 --%
mbm.market[1] = 10000 -- chem
mbm.market[2] = 10000 -- wood
mbm.market[3] = 10000 -- gold
mbm.market[4] = 10000 -- steel
mbm.market[5] = 10000 -- coal
mbm.market[6] = 10000 -- iron
mbm.baseCost = {}
mbm.baseCost[1] = 10000000 -- chem
mbm.baseCost[2] = 10000000 -- wood
mbm.baseCost[3] = 10000000 -- gold
mbm.baseCost[4] = 10000000 -- steel
mbm.baseCost[5] = 10000000 -- coal
mbm.baseCost[6] = 10000000 -- iron
mbm.marketFile = "sys/lua/MinersBattle/market_save/MB_market.txt"
mbm.oldPrice = {}

-- used to fix bug between mm and mbm
mbm.quickfix = {}
mbm.quickfix[1] = 6
mbm.quickfix[2] = 4
mbm.quickfix[3] = 2
mbm.quickfix[4] = 3
mbm.quickfix[5] = 5
mbm.quickfix[6] = 1

SaveEngine.open(mbm.marketFile)
i = 0
while (i < #mbm.resName) do
    i = i + 1
    mbm.market[i] = SaveEngine.read(mbm.marketFile,"Quantity",mbm.resName[i],mbm.market[i])
end

i = 0
while (i < 6) do
    i = i + 1
    mbm.oldPrice[i] = misc.round(mbm.baseCost[i]/mbm.market[i],5)
end

function mbm.marketOpen(id)
    sme.createMenu(id,mbm.marketMenu,sme.noProcess,"Gobal Market",false,mbm.resName,true)
end

function mbm.marketMenu(id,num)
	if num == 0 then -- user cancelled menu
		return nil
	end
	sme.createMenu(id,mbm.marketPerform,mbm.innerMarketProcess,"Gobal Market {"..mbm.resName[num].."}",false,{1,2,3,4,5,6,7,8,9},true,num)
end

function mbm.innerMarketProcess(id,button,res)
    if mbm.market[res] <= 0 then
        cost = mbm.baseCost[res]
    else
        cost = (mbm.baseCost[res]/mbm.market[res])
    end
    if res == 1 then
        has = mb.player[id].chem
    end
    if res == 2 then
        has = mb.player[id].wood
    end
    if res == 3 then
        has = mb.player[id].gold
    end
    if res == 4 then
        has = mb.player[id].steel
    end
    if res == 5 then
        has = mb.player[id].coal
    end
    if res == 6 then
        has = mb.player[id].iron
    end
    if button == 1 then
        m = math.ceil(cost*5)
        text = "Buy 5 "..mbm.resName[res].." for "..misc.thousandMarks(m)
        if mb.checkMoney(id) >= m and mbm.market[res] >= 5 then
            return text
        else
            return "("..text..")"
        end
    end
    if button == 2 then
        m = math.ceil(cost*10)
        text = "Buy 10 "..mbm.resName[res].." for "..misc.thousandMarks(m)
        if mb.checkMoney(id) >= m and mbm.market[res] >= 10 then
            return text
        else
            return "("..text..")"
        end
    end
    if button == 3 then
        m = math.ceil(cost*50)
        text = "Buy 50 "..mbm.resName[res].." for "..misc.thousandMarks(m)
        if mb.checkMoney(id) >= m and mbm.market[res] >= 50 then
            return text
        else
            return "("..text..")"
        end
    end
    if button == 4 then
        m = math.ceil(cost*100)
        text = "Buy 100 "..mbm.resName[res].." for "..misc.thousandMarks(m)
        if mb.checkMoney(id) >= m and mbm.market[res] >= 100 then
            return text
        else
            return "("..text..")"
        end
    end
    if button == 5 then
        m = math.floor((cost*5)*((100-mbm.sellPenalty)/100))
        text = "Sell 5 "..mbm.resName[res].." for "..misc.thousandMarks(m)
        if has >= 5 then
            return text
        else
            return "("..text..")"
        end
    end
    if button == 6 then
        m = math.floor((cost*10)*((100-mbm.sellPenalty)/100))
        text = "Sell 10 "..mbm.resName[res].." for "..misc.thousandMarks(m)
        if has >= 10 then
            return text
        else
            return "("..text..")"
        end
    end
    if button == 7 then
        m = math.floor((cost*50)*((100-mbm.sellPenalty)/100))
        text = "Sell 50 "..mbm.resName[res].." for "..misc.thousandMarks(m)
        if has >= 50 then
            return text
        else
            return "("..text..")"
        end
    end
    if button == 8 then
        m = math.floor((cost*100)*((100-mbm.sellPenalty)/100))
        text = "Sell 100 "..mbm.resName[res].." for "..misc.thousandMarks(m)
        if has >= 100 then
            return text
        else
            return "("..text..")"
        end
    end
    if button == 9 then
        m = math.floor((cost*1000)*((1000-mbm.sellPenalty)/1000))
        text = "Sell 1000 "..mbm.resName[res].." for "..misc.thousandMarks(m)
        if has >= 1000 then
            return text
        else
            return "("..text..")"
        end
    end
end

function mbm.marketPerform(id,button,data,res)
    print("button: "..button)
    print("res: "..res)
    if button == 0 then
        return 0
    end
    buy = nil
    amount = 0
    cost = (mbm.baseCost[res]/mbm.market[res])
    if button == 1 then
        buy = true
        amount = 5
        cost = cost*amount
    end
    if button == 2 then
        buy = true
        amount = 10
        cost = cost*amount
    end
    if button == 3 then
        buy = true
        amount = 50
        cost = cost*amount
    end
    if button == 4 then
        buy = true
        amount = 100
        cost = cost*amount
    end
    if button == 5 then
        buy = false
        amount = 5
        cost = (cost*amount)*((100-mbm.sellPenalty)/100)
    end
    if button == 6 then
        buy = false
        amount = 10
        cost = (cost*amount)*((100-mbm.sellPenalty)/100)
    end
    if button == 7 then
        buy = false
        amount = 50
        cost = (cost*amount)*((100-mbm.sellPenalty)/100)
    end
    if button == 8 then
        buy = false
        amount = 100
        cost = (cost*amount)*((100-mbm.sellPenalty)/100)
    end
    if button == 9 then
        buy = false
        amount = 1000
        cost = (cost*amount)*((1000-mbm.sellPenalty)/1000)
    end
    if buy then
        cost = math.ceil(cost)
        if mb.checkMoney(id) >= cost and mbm.market[res] >= amount then
            if res == 1 then
                mb.player[id].chem = mb.player[id].chem + amount
                mb.drawchem(id)
            end
            if res == 2 then
                mb.player[id].wood = mb.player[id].wood + amount
                mb.drawwood(id)
            end
            if res == 3 then
                mb.player[id].gold = mb.player[id].gold + amount
                mb.drawgold(id)
            end
            if res == 4 then
                mb.player[id].steel = mb.player[id].steel + amount
                mb.drawsteel(id)
            end
            if res == 5 then
                mb.player[id].coal = mb.player[id].coal + amount
                mb.drawcoal(id)
            end
            if res == 6 then
                mb.player[id].iron = mb.player[id].iron + amount
                mb.drawiron(id)
            end
            mb.removeMoney(id,cost)
            mbm.market[res] = mbm.market[res] - amount
            msg2(id,misc.rgb(0,255,0).."You bought "..amount.."x "..mbm.resName[res].." for "..cost)
        end
    else
        cost = math.floor(cost)
        if res == 1 then
            if mb.player[id].chem >= amount then
                mb.player[id].chem = mb.player[id].chem - amount
                mb.drawchem(id)
            end
        end
        if res == 2 then
            if mb.player[id].wood >= amount then
                mb.player[id].wood = mb.player[id].wood - amount
                mb.drawwood(id)
            end
        end
        if res == 3 then
            if mb.player[id].gold >= amount then
                mb.player[id].gold = mb.player[id].gold - amount
                mb.drawgold(id)
            end
        end
        if res == 4 then
            if mb.player[id].steel >= amount then
                mb.player[id].steel = mb.player[id].steel - amount
                mb.drawsteel(id)
            end
        end
        if res == 5 then
            if mb.player[id].coal >= amount then
                mb.player[id].coal = mb.player[id].coal - amount
                mb.drawcoal(id)
            end
        end
        if res == 6 then
            if mb.player[id].iron >= amount then
                mb.player[id].iron = mb.player[id].iron - amount
                mb.drawiron(id)
            end
        end
        mb.giveMoney(id,cost)
        mbm.market[res] = mbm.market[res] + amount
        msg2(id,misc.rgb(0,255,0).."You sold "..amount.."x "..mbm.resName[res].." for "..cost)
    end
end

function saveMarket()
    i = 0
    while (i < #mbm.resName) do
        i = i + 1
        SaveEngine.write(mbm.marketFile,"Quantity",mbm.resName[i],mbm.market[i])
    end
    SaveEngine.save(mbm.marketFile)
end

-- change: 1.4210854715202e-014

addhook("minute","mbm.minute")
function mbm.minute()
    saveMarket()
    msg(misc.rgb(0,255,0).."--| Global Market Status |--")
    i = 0
    while (i < 6) do
        i = i + 1
        newCost = misc.round(mbm.baseCost[i]/mbm.market[i],5)
        --change = ((100 / mbm.oldPrice[i]) * newCost)-100
        change = (((100/mbm.oldPrice[i])*newCost)-100)
        --print("old: "..mbm.oldPrice[i].." <> new: "..newCost)
        change_color = math.min(mbm.colorSensitivity,math.max(-mbm.colorSensitivity,change))
        change_color = (change_color + mbm.colorSensitivity)*(100/(mbm.colorSensitivity*2))
        --msg("old cost: "..mbm.oldPrice[i].." | new cost: "..newCost)
        --print("change: "..change)
        pre = "~"
        if change < 0 then
            pre = "-"
        end
        if change > 0 then
            pre = "+"
        end
        msg(misc.colorTextPercent(change_color)..mbm.resName[i].." cost: "..misc.round(newCost).." ("..pre..""..math.abs(misc.round(change,3)).."%)")
        print("Theres "..mbm.market[i].." of "..mbm.resName[i])
        mbm.oldPrice[i] = newCost
    end
end
--[[
addhook("say","mbm.say")
function mbm.say(id,text)
    if text == "!market" then
        msg2(id,misc.rgb(0,255,0).."--| Global Market Status |--")
        i = 0
        while (i < 6) do
            i = i + 1
            newCost = (mbm.baseCost[i]/mbm.market[i])
            change = ((100 / mbm.oldPrice[i]) * newCost)-100
            change_color = math.min(5,math.max(-5,change))
            change_color = (change_color + 5)*10
            pre = "~"
            if change < 0 then
                pre = ""
            end
            if change > 0 then
                pre = "+"
            end
            msg2(id,misc.colorTextPercent(change_color)..mbm.resName[i].." cost: "..math.ceil(newCost).." ("..pre..""..misc.round(change,2).."%)")
            --print("Theres "..mbm.market[i].." of "..mbm.resName[i])
            --mbm.oldPrice[i] = newCost
        end
        return 1
    end
end
]]--
