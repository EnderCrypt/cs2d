mbwe = {}
mbwe.delay = 30 -- in seconds
mbwe.counter = 0
mbwe.debug = false

mbwe.tolerance = 25

mbwe.autoTeamBalance = true

-- x, y, importance
local MAP_NAME = string.lower(map("name"))
if ((MAP_NAME == "minersbattle") or (MAP_NAME == "minersbattlev2")) then
    mbwe.dompoints = {{23,40,1},{23,60,1},{57,50,2},{91,40,1},{91,60,1}}
end
if ((MAP_NAME == "minersbattle2v5") or (MAP_NAME == "minersbattle2v6") or (MAP_NAME == "minersbattle2v7")) then
    mbwe.dompoints = {{33,46,1},{33,54,1},{83,46,1},{83,54,1},{58,50,2},{54,21,1},{62,21,1},{54,79,1},{62,79,1}}
end
if ((MAP_NAME == "minersbattle2v7_alt")) then
    mbwe.dompoints = {{33,46,1},{33,54,1},{83,46,1},{83,54,1},{58,50,2}}
end

mbwe.dompoint_value = 0

local i = 0
while (i < #mbwe.dompoints) do
    i = i + 1
    local point = mbwe.dompoints[i]
    local x = point[1]
    local y = point[2]
    local val = point[3]
    point[4] = 0
    if (entity(x,y,"type") == 17) then
        mbwe.dompoint_value = mbwe.dompoint_value + val
    else
        msg(misc.rgb(255,0,0).."WARNING: NOT A DOM POINT! ("..x..", "..y..")")
        mbwe.dompoints[i] = mbwe.dompoints[#mbwe.dompoints]
        mbwe.dompoints[#mbwe.dompoints] = nil
        i = i - 1
    end
end
msg("total dom flags #"..#mbwe.dompoints.." (value: "..mbwe.dompoint_value..")")

function mbwe.print(text)
    if (mbwe.debug) then
        print(text)
    end
end

addhook("startround","mbwe.startround")
function mbwe.startround(mode)
    mbwe.counter = 0
end

addhook("say","mbwe.say")
function mbwe.say(id,text)
    if (text == "!power") then
        local pow = mbwe.player(id)
        msg(misc.rgb(51,255,51)..player(id,"name").." has an estimated power of: "..misc.thousandMarks(misc.round(pow)))
    end
end

addhook("team","mbwe.teamchange")
function mbwe.teamchange(id,team,look)
    if (mbwe.autoTeamBalance) then
        if (team > 0) and (not(player(id,"bot"))) then
            local team1score = mbwe.getTeamScore(1)
            local team2score = mbwe.getTeamScore(2)

            local currentTeam = player(id,"team")
            local playerScore = mbwe.player(id)

            if (currentTeam == 1) then
                team1score = team1score - playerScore
            end
            if (currentTeam == 2) then
                team2score = team2score - playerScore
            end

            local team1chance = (100/team2score) * team1score
            local team2chance = (100/team1score) * team2score

            local imbalance = math.max(team1chance,team2chance) -  100

            if (imbalance > mbwe.tolerance) then
                if (team2chance > 100) then -- joining team 1 is better
                    if (team == 2) then
                        parse("maket "..id)
                        msg2(id,misc.rgb(255,25,25).."Teambalanced to Terrorist")
                        return 1;
                    end
                    if (team == 1) then
                        msg2(id,misc.rgb(25,25,255).."Terrorist's is a good team for you!")
                    end
                end
                if (team1chance > 100) then -- joining team 2 is better
                    if (team == 1) then
                        parse("makect "..id)
                        msg2(id,misc.rgb(25,25,255).."Teambalanced to Counter-Terrorist")
                        return 1;
                    end
                    if (team == 2) then
                        msg2(id,misc.rgb(255,25,25).."Counter-Terrorist's is a good team for you!")
                    end
                end
            else
                msg2(id,misc.rgb(25,255,25).."Join the team you wish to join")
            end
        end
    end
end

addhook("second","mbwe.second")
function mbwe.second()
    mbwe.minute()
    local i = 0
    while (i < #mbwe.dompoints) do
        i = i + 1
        local point = mbwe.dompoints[i]
        point[4] = point[4] + 1
    end
end

function mbwe.minute()
    mbwe.counter = mbwe.counter + 1
    if (mbwe.counter % mbwe.delay == 0) then
        -- get scores
        local t_score = mbwe.getTeamScore(1)
        local ct_score = mbwe.getTeamScore(2)
        -- calculate with flags
        t_score = mbwe.divideByDomPoints(t_score,1)
        ct_score = mbwe.divideByDomPoints(ct_score,2)
        -- calculates percentages
        local t_chance = misc.round(((100/ct_score)*t_score)-100,1)
        local ct_chance = misc.round(((100/t_score)*ct_score)-100,1)
        -- display
        if (t_chance > ct_chance) then -- t will win
            msg(misc.rgb(255,100,100).."Terrorists are "..t_chance.."% more likely to win!")
        end
        if (ct_chance > t_chance) then -- ct will win
            msg(misc.rgb(100,100,255).."Counter-Terrorists are "..ct_chance.."% more likely to win!")
        end
        if (ct_chance == t_chance) then -- equal chance
            msg(misc.rgb(100,255,100).."Counter-Terrorists and Terrorists have have EQUAL chance to win!")
        end
    end
    --mbwe.counter = mbwe.counter + 1
end

function mbwe.divideByDomPoints(points,team)
    local dom_points = 0
    local i = 0
    while (i < #mbwe.dompoints) do
        i = i + 1
        local point = mbwe.dompoints[i]
        local x = point[1]
        local y = point[2]
        local val = point[3]
        if (entity(x,y,"int0") == team) then
            dom_points = dom_points + val
        end
    end
    dom_points = math.max(math.sqrt(math.sqrt(dom_points)),1)
    mbwe.print("team"..team.." flag modifier: "..(100/mbwe.dompoint_value)*dom_points.."%")
    return (points/mbwe.dompoint_value)*dom_points
end

function mbwe.getTeamBuildings(team)
    local objs = object(0,"table")
    local team_objects = {}
    local i = 0
    while (i < #objs) do
        i = i + 1
        local obj = objs[i]
        if (object(obj,"team") == team) then
            team_objects[#team_objects+1] = obj
        end
    end
    return team_objects
end

function mbwe.getTeamScore(team)
    mbwe.print("Calculating team"..team.."'s score..")
    local score = 0

    -- players --
    local players = player(0,"team"..team)
    local i = 0
    while (i < #players) do
         i = i + 1
         local id = players[i]
         score = score + mbwe.player(id)
    end

    -- buildings --
    local buildings = mbwe.getTeamBuildings(team)
    local i = 0
    while (i < #buildings) do
         i = i + 1
         local id = buildings[i]
         score = score + (mbwe.building(id) * 0.5)
    end
    
    -- calculate --
    mbwe.print("Team total score: "..score.." for team"..team)
    return score
end

function mbwe.player(id)
    local score = 0
    -- health
    local maxhp = player(id,"maxhealth")
    local tp = maxhp*(maxhp/10)
    score = score + tp
    -- multiplier
    local multiplier = 0
    -- money (max: 15%)
    multiplier = multiplier + (15/100000)*math.min(mb.player[id].bank,100000)*0.01+1
    -- best weapon(max: ~52%)
    local highest_value = 0
    if (player(id,"health") <= 0) then
    
    else
        local wep = playerweapons(id)
        local i = 0
        while (i < #wep) do
            i = i + 1
            local wep_id = wep[i]
            local val = mb.calculateValue(wep_id)
            if (val > highest_value) then
                highest_value = val
            end
        end
    end
    multiplier = multiplier + (highest_value/250000)
    -- armor (max: 10%)
    local armor = player(id,"armor")
    armor = armor - 122
    if (armor > 0) then
        local worth = mb.calculateValue(armor)
        multiplier = multiplier + (((10/1000)*math.min(worth,1000))*0.01)
    end
    -- calculate
    score = score * multiplier
    -- output
    mbwe.print(player(id,"name").." is worth "..score.." points (multiplier: "..multiplier..")")
    return score
end

function mbwe.building(id)
    local type_id = object(id,"type")
    local score = 0
    if (type_id == 6) then -- gate field
        score = score + 50
    end
    if (type_id == 7) then -- dispener
        score = score + 250
    end
    if (type_id == 8) then -- turret
        score = score + 250
    end
    if (type_id == 11) then -- dual turret
        score = score + 1000
    end
    if (type_id == 13) then -- triple turret
        score = score + 3000
    end
    return score
end
