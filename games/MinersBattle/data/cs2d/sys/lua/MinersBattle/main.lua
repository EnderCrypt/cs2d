dofile("sys/lua/MinersBattle/extra/MiscLibrary.lua")
dofile("sys/lua/MinersBattle/extra/timer2.lua")
dofile("sys/lua/MinersBattle/extra/servertransfer.lua")
dofile("sys/lua/MinersBattle/extra/structureTracker.lua")
dofile("sys/lua/MinersBattle/extra/SaveEngineLinux.lua")
dofile("sys/lua/MinersBattle/extra/FunctionHook.lua")
dofile("sys/lua/MinersBattle/extra/BasicMenuEngine.lua")
dofile("sys/lua/MinersBattle/extra/ShieldCover.lua")
dofile("sys/lua/MinersBattle/extra/smokeBlink.lua")
dofile("sys/lua/MinersBattle/extra/structureDistanceProtection.lua")
dofile("sys/lua/MinersBattle/extra/bot_auto.lua")
dofile("sys/lua/MinersBattle/extra/satchel_super_throw.lua")
dofile("sys/lua/MinersBattle/extra/GutDrone.lua")
dofile("sys/lua/MinersBattle/extra/GrassGrowth.lua")
dofile("sys/lua/MinersBattle/extra/TileFire.lua")
dofile("sys/lua/MinersBattle/extra/snowballCharge.lua")

dofile("sys/lua/MinersBattle/MinersBattle.lua")
dofile("sys/lua/MinersBattle/MinersBattleReputation.lua")
dofile("sys/lua/MinersBattle/MinersBattleMarket.lua")
dofile("sys/lua/MinersBattle/MinersBattleWinEstimator.lua")

parse("mp_autoteambalance 0")
