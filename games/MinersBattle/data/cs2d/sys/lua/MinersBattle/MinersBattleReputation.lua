mbrep = {}
mbrep.file = "sys/lua/MinersBattle/reputation_highscore.txt"
SaveEngine.open(mbrep.file)

mbrep.highscore = {}
local i = 0
while (i < 10) do -- do one extra cause of the way this code works
    i = i + 1
    local temp = {}
    temp.name = "NO_NAME"
    temp.usgn = 0
    temp.rep = 0
    mbrep.highscore[i] = temp
end

function mbrep.pushHighscore(pos)
    local i = 11
    while (i > pos) do
        i = i - 1
        mbrep.highscore[i+1] = mbrep.highscore[i]
    end
end

function mbrep.registerRep(name,usgn,rep)
    local entry = mbrep.getEntry(usgn)
    if entry == 0 then
        mbrep.newEntry(name,usgn,rep)
    else
        mbrep.highscore[entry].name = name
        mbrep.highscore[entry].rep = rep
        mbrep.modifyEntry(entry,rep)
    end
end

function mbrep.getEntry(usgn)
    local i = 10
    while (i > 0) do
        if usgn == mbrep.highscore[i].usgn then
            break
        end
        i = i - 1
    end
    return i
end

function mbrep.newEntry(name,usgn,rep)
    local i = 10
    while (i > 0) do
        local temp = mbrep.highscore[i]
        if rep < temp.rep then
            break
        end
        i = i - 1
    end
    -- push the people under
    local pos = i + 1
    if pos <= 10 then
        mbrep.pushHighscore(pos)
        local temp = {}
        temp.name = name
        temp.usgn = usgn
        temp.rep = rep
        mbrep.highscore[pos] = temp
    end
end

function mbrep.modifyEntry(entry)
    while ((entry > 1) and (mbrep.highscore[entry].rep >= mbrep.highscore[entry-1].rep)) do
        mbrep.moveEntryUp(entry)
        entry = entry - 1
    end
    while ((entry < 10) and (mbrep.highscore[entry].rep < mbrep.highscore[entry+1].rep)) do
        mbrep.moveEntryDown(entry)
        entry = entry + 1
    end
end

function mbrep.moveEntryUp(entry) -- towards 1
    local temp = mbrep.highscore[entry-1]
    mbrep.highscore[entry-1] = mbrep.highscore[entry]
    mbrep.highscore[entry] = temp
end

function mbrep.moveEntryDown(entry)
    local temp = mbrep.highscore[entry+1]
    mbrep.highscore[entry+1] = mbrep.highscore[entry]
    mbrep.highscore[entry] = temp
end
--[[
mbrep.registerRep("test 1",1,10)
mbrep.registerRep("test 2",2,15)
mbrep.registerRep("test 3",3,5)
mbrep.registerRep("test 2",2,8)
mbrep.registerRep("test 3_1",3,5)
]]--

--superDump.dump("mbrep.highscore","Highscore")
