iniSave = {}
iniSave.cache = {}

function iniSave.open(file,force)
	if (iniSave.cache[file] == nil) or (force == true) then
		if not iniSave.fileExists(file) then
			iniSave.cache[file] = {}
		end
		local file_load = io.open(file,"r")
		if file_load == nil then
			return false
		end
		iniSave.cache[file] = {}
		local currentLabel = nil
		input = file_load:read()
		line_number = 0
		while not (input == nil) do
			line_number = line_number + 1
			if string.sub(input,1,1) == "[" and string.sub(input,string.len(input),string.len(input)) then
				currentLabel = string.sub(input,2,string.len(input)-1)
				iniSave.cache[file][currentLabel] = {}
			else
				if currentLabel == nil then
					file_load:close()
					return false
				end
				local equal_sign = string.find(input,"=")
				local first = string.sub(input,1,equal_sign-1)
				local var_type_name = string.find(first,":")
				local var_type = string.sub(first,1,var_type_name-1)
				local var = string.sub(first,var_type_name+1,string.len(first))
				local value = string.sub(input,equal_sign+1,string.len(input))
				if var_type == "string" then
					iniSave.cache[file][currentLabel][var] = value
				end
				if var_type == "number" then
					iniSave.cache[file][currentLabel][var] = tonumber(value)
				end
				if var_type == "boolean" then
					if value then
						iniSave.cache[file][currentLabel][var] = true
					else
						iniSave.cache[file][currentLabel][var] = false
					end
				end
			end
			input = file_load:read()
		end
		file_load:close()
	end
	return true
end

function iniSave.fileExists(file)
	local file_check = io.open(file,"r")
	if file_check == nil then
		return false
	end
	file_check:close()
	return true
end

function iniSave.save(file)
	if iniSave.cache[file] == nil then
		return false
	end
	local file_save = io.open(file,"w")
	if file_save == nil then
		return false
	end
	for label in pairs(iniSave.cache[file]) do
		file_save:write("["..label.."]\n")
		for variable in pairs(iniSave.cache[file][label]) do
			local var_type = type(iniSave.cache[file][label][variable])
			local var_name = variable
			local value = iniSave.cache[file][label][variable]
			file_save:write(var_type..":"..var_name.."="..value.."\n")
		end
	end
	file_save:close()
	return true
end

function iniSave.saveAll()
	if iniSave.cache[file] == nil then
		return false
	end
	for file in pairs(iniSave.cache) do
		iniSave.save(file)
	end
	return true
end

function iniSave.write(file,label,variable,newValue)
	if iniSave.cache[file] == nil then
		iniSave.cache[file] = {}
		iniSave.cache[file][label] = {}
	end
	if iniSave.cache[file][label] == nil then
		iniSave.cache[file][label] = {}
	end
	iniSave.cache[file][label][variable] = newValue
	return true
end

function iniSave.read(file,label,variable,default)
	if iniSave.cache[file] == nil then
		iniSave.cache[file] = {}
		iniSave.cache[file][label] = {}
		iniSave.cache[file][label][variable] = default
	end
	if iniSave.cache[file][label] == nil then
		iniSave.cache[file][label] = {}
		iniSave.cache[file][label][variable] = default
	end
	if iniSave.cache[file][label][variable] == nil then
		iniSave.cache[file][label][variable] = default
	end
	if iniSave.cache[file][label][variable] == nil then
		return default
	else
		return iniSave.cache[file][label][variable]
	end
end

function iniSave.getLabelNames(file)
	local temp = {}
	for label in pairs(iniSave.cache[file]) do
		temp[#temp+1] = label
	end
	return temp
end

function iniSave.getVarNames(file,label)
	local temp = {}
	for label in pairs(iniSave.cache[file]) do
		for variable in pairs(iniSave.cache[file][label]) do
			temp[#temp+1] = variable
		end
	end
	return temp
end