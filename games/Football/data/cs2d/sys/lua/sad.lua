sadViolin = {}
sadViolin.playOnDeath = true
sadViolin.file = "sad.ogg"

misc.addServerTransfer("sfx/"..sadViolin.file)

addhook("die","sadViolin.die")
function sadViolin.die(victim,killer,weapon,x,y)
	if (sadViolin.playOnDeath) then
		if (killer > 0) then
			sadViolin.play(victim)
		end
	end
end

function sadViolin.playTeam(teamID) --living
	local pl = player(0,"team"..teamID.."living")
	for _,id in ipairs(pl) do
		sadViolin.play(id)
	end
end

function sadViolin.play(id)
	if (player(id,"bot") == false) then
		parse("sv_sound2 "..id.." "..sadViolin.file)
	end
end