-- library lua
dofile("sys/lua/MiscLibrary.lua")
dofile("sys/lua/Class/Class.lua")
dofile("sys/lua/vector.lua")
dofile("sys/lua/FunctionHook.lua")
dofile("sys/lua/timer2.lua")
dofile("sys/lua/iniSave.lua")
dofile("sys/lua/servertransfer.lua")

-- secondary lua
dofile("sys/lua/sad.lua")

-- primary lua
dofile("sys/lua/football2/football2.lua")
dofile("sys/lua/football2/football2Indicator.lua")

-- finalize
serverTransfer.finalize()
misc.mp_hud(false,false,true,false,false,false,false)
