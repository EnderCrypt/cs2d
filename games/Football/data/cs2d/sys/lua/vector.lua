-- require MiscFunctions.lua

vector = {}
Class(vector,function(direction, length)
    if (direction == nil) then
        direction = 0
    end
    if (length == nil) then
        length = 0
    end
    self.direction = direction
    self.length = length
    self.cache = nil
end)

-- ADD

function vector:addVector(other)
    self:addDir(other.direction,other.length)
end

function vector:addDir(dir, length)
    local x = misc.lengthdir_x(dir,length)
    local y = misc.lengthdir_y(dir,length)
    self:addXY(x,y)
    self.cache = nil
end

function vector:addXY(other_x,other_y)
    local x, y = self:get()
    x = x + other_x
    y = y + other_y
    self.direction = misc.point_direction(0,0,x,y)
    self.length = misc.point_distance(0,0,x,y)
    self.cache = nil
end

-- SET

function vector:setVector(vector)
    self:setDir(vector.direction,vector.length)
    self.cache = nil
end

function vector:setDir(dir, length)
    self.direction = dir
    self.length = length
    self.cache = nil
end

function vector:setXY(other_x,other_y)
    self.direction = misc.point_direction(0,0,x,y)
    self.length = misc.point_distance(0,0,x,y)
    self.cache = nil
end

-- MISC

function vector:multiply(multiplier)
    self.length = self.length * multiplier
    self.cache = nil
end

function vector:bounce(bounceDirection)

    --local addDir = self.direction - bounceDirection
    --self.direction = bounceDirection + addDir

    self.direction = (bounceDirection - 180) - (self.direction - bounceDirection)
    self.cache = nil
end

-- GET

function vector:get()
    if (self.cache == nil) then
        local x = misc.lengthdir_x(self.direction,self.length)
        local y = misc.lengthdir_y(self.direction,self.length)
        self.cache = {x,y}
    end
    return self.cache[1], self.cache[2]
end
