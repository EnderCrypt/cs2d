football.indicator = {}
football.indicator.image = "gfx/arrowhead.png"

football.indicator.hud = {}

misc.addServerTransfer(football.indicator.image)

addhook("ms100","football.indicator.ms100")
function football.indicator.ms100()
	local pl = player(0,"tableliving")
	for _,id in ipairs(pl) do
		if (player(id,"bot") == false) then
			local entity = football.indicator.getClosestFootball(id)
			if (entity == nil) then
				football.indicator.closeHud(id)
			else
				football.indicator.updateHud(id,entity)
			end
		end
	end
end

function football.indicator.getClosestFootball(id)
	local x = player(id,"x")
	local y = player(id,"y")
	local closes_dist = math.huge
	local closes_ball = nil
	for i=1,#football.entities do
		local entity = football.entities[i]
		local dist = misc.primitive_point_distance(x,y,entity.x,entity.y)
		if (dist < closes_dist) then
			closes_dist = dist
			closes_ball = entity
		end
	end
	return closes_ball
end

function football.indicator.updateHud(id,entity)
	local px = player(id,"x")
	local py = player(id,"y")
    local width = player(id, "screenw")
    local height = player(id, "screenh")
	if (misc.isInsideScreen(entity.x,entity.y,px,py)) then
		football.indicator.closeHud(id)
	else
		local dist = misc.point_distance(px,py,entity.x,entity.y)
		local dir = misc.point_direction(px,py,entity.x,entity.y)
		-- calculate appropriate position
		local x = width/2
		local y = height/2
		local min = 200
		local move_x = misc.lengthdir_x(dir,1)
		local move_y = misc.lengthdir_y(dir,1)
		x = x + (move_x * min)
		y = y + (move_y * min)
		local div = 2
		local jump = math.pow(div,5)
		while (jump > 1) do
			local nx = x + (move_x * jump)
			local ny = y + (move_y * jump)
			local margin = 16
			if  ((nx >= margin) and (ny > margin) and (nx < width-margin) and (ny < height-margin)) then -- is inside screen
				x = nx
				y = ny
			else
				jump = jump / div
			end
		end
		--
		local football_dir = entity.vector.direction - 90
		if (football.indicator.hud[id] == nil) then
			football.indicator.hud[id] = image(football.indicator.image,0,0,2,id)
			imagepos(football.indicator.hud[id],x,y,football_dir)
			imagealpha(football.indicator.hud[id],0)
		else
			tween_move(football.indicator.hud[id],100,x,y,football_dir)
		end
		local alpha = (1 / 700) * (1000 - dist)
		alpha = math.min(math.max(alpha,0.1),1)
		tween_alpha(football.indicator.hud[id],100,alpha)
	end
end

function football.indicator.closeHud(id)
	local hud = football.indicator.hud[id]
	if (hud ~= nil) then
		freeimage(hud)
		football.indicator.hud[id] = nil
	end
end

addhook("die","football.indicator.die")
function football.indicator.die(victim,killer,weapon,x,y)
	football.indicator.closeHud(victim)
end

addhook("leave","football.indicator.leave")
function football.indicator.leave(id, reason)
	football.indicator.closeHud(id)
end
