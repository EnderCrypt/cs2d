football = {}
football.image = "gfx/football.png"
football.entities = {}
football.friction = 0.99
football.goalTile = {t=197,ct=196}
football.effect = {
-- type, multiplier, r, g, b
	{"smoke",1,0,0,0},
	{"fire",1,0,0,0},
	{"flare",8,255,0,0},
}

football.goals = {t=0,ct=0}
football.goalsRequired = 10
football.victories = {t=0,ct=0}

football.save = {}

football.hud = {}
football.hud.goals = misc.get_hudtxt()
football.hud.victories = misc.get_hudtxt()

parse("mp_damagefactor 0")
parse("mp_autoteambalance 1")
parse("sv_gamemode 2")
parse("mp_idleaction 1") -- make spectator

 misc.mp_hud(false,false,false,false,false,false,false) 

misc.addServerTransfer(football.image)

timer2(500,{},function()
	parse("bot_add_t")
	parse("bot_add_ct")
	parse("bot_add_t")
	parse("bot_add_ct")
	parse("bot_add_t")
	parse("bot_add_ct")
end)

timer2(1000,{},function()
	football.restartMatch()
	football.updateHUD()
end)

function football.spawn(x,y)
	football.entities[#football.entities+1] = footballClass.new(x,y)
end

addhook("join","football.join")
function football.join(id)
	football.save[id] = football2StatsClass.new(id)
end

addhook("leave","football.leave")
function football.leave(id)
	if (football.save[id] ~= nil) then
		football.save[id]:save()
	end
	football.save[id] = nil
end

addhook("die","football.die")
function football.die(victim,killer,weapon,x,y)
	football.save[victim]:save()
end

addhook("say","football.say")
function football.say(id,text)
	if (text == "!status") then
		football.save[id]:say()
	end
end

addhook("startround","football.startround")
function football.startround(mode)
    football.spawn(map("xsize")/2,map("ysize")/2)
end

addhook("endround","football.endround")
function football.endround(mode)
	for i=1,#football.entities do
		football.entities[i]:destroy()
	end
	football.entities = {}
end

function football.restartMatch()
	-- end round
	football.endround(nil)
	-- start round
	football.startround(nil)
    -- position players
    local placePlayers = function(players,x)
    	local yTiles = map("ysize")
    	local yPixels = misc.tile_to_pixel(yTiles)
    	local between = yPixels / (#players + 1)
    	for i=1,#players do
    		local id = players[i]
    		cs2d.setpos.call(id,x,between*i)
    		cs2d.speedmod.call(id,-25)
    	end
	end
	local xCenter = misc.tile_to_pixel(map("xsize")) / 2
	placePlayers(misc.shuffle_array(player(0,"team1living")),xCenter-500)
	placePlayers(misc.shuffle_array(player(0,"team2living")),xCenter+500)
end

addhook("attack","football.attack")
function football.attack(id)
	football.playerKick(id,true,32)
end

addhook("attack2","football.attack2")
function football.attack2(id)
	local speed = player(id,"speedmod")
	speed = speed + 32
	cs2d.speedmod.call(id,speed)
	football.save[id].stats.sprints = football.save[id].stats.sprints + 1
end

function football.getPlayerAim(id)
	local rot = player(id,"rot")
	local x = player(id,"x")+misc.lengthdir_x(rot,16)
	local y = player(id,"y")+misc.lengthdir_y(rot,16)
	return x, y
end

addhook("always","football.always")
function football.always()
	football.playersKickAll()
	football.updateMotions()
end

addhook("ms100","football.ms100")
function football.ms100()
	football.tweenAllImages()
	football.speedPlayers()
end

addhook("minute","football.minute")
function football.minute()
	for id,data in pairs(football.save) do
		data:save()
	end
end

function football.speedPlayers()
	local pl = player(0,"tableliving")
	for i=1,#pl do
		local id = pl[i]
		local speed = player(id,"speedmod")
		speed = speed * 0.8
		cs2d.speedmod.call(id,speed)
	end
end

function football.playersKickAll()
	local pl = player(0,"tableliving")
	for i=1,#pl do
		local id = pl[i]
		football.playerKick(id,false,16)
	end
end

function football.removeEntity(other_entity)
	for i=1,#football.entities do
		local entity = football.entities[i]
		if (entity == other_entity) then
			football.removeIndex(i)
			return true
		end
	end
	return false
end

function football.removeIndex(index)
	local entity = football.entities[index]
	entity:destroy()
	football.entities[index] = football.entities[#football.entities]
	football.entities[#football.entities] = nil
end

function football.playerKick(id,primary,range)
	local x, y = football.getPlayerAim(id)
	for i=1,#football.entities do
		local entity = football.entities[i]
		local dist = misc.primitive_point_distance(x,y,entity.x,entity.y)
		if (dist < range) then
			entity:kick(id,primary)
			return true
		end
	end
end

function football.updateMotions()
	for i=1,#football.entities do
		local entity = football.entities[i]
		entity:update()
	end
end

function football.tweenAllImages()
	for i=1,#football.entities do
		local entity = football.entities[i]
		entity:tweenImage()
	end
end

function football.updateHUD()
	text = "Goals: T"..football.goals.t.." - "..football.goals.ct.."CT"
	misc.hudTextEveryone(football.hud.goals,text,16,120,const.align.left)
	--misc.hudTextColor(football.hud.goals,0,0,0,255,0)
	text = "Victories: T"..football.victories.t.." - "..football.victories.ct.."CT"
	misc.hudTextEveryone(football.hud.victories,text,16,140,const.align.left)
	--misc.hudTextColor(football.hud.victories,0,0,0,255,0)
end

-- CLASS --

footballClass = {}
Class(footballClass,function(x,y)
	self.x = misc.tile_to_pixel(x)
	self.y = misc.tile_to_pixel(y)
	self.image = image(football.image,self.x,self.y,0)
	self.vector = vector.new()
	self.lastID = 0 -- anyone who kicks the ball, gets set to 0 id the ball goes under 5 speed
	self.lastSecondaryID = 0 -- gets set to anyone touching the ball, prefferably from opposite team of lastID
	self.lastBackupID = 0 -- if speed goes under 5, this gets set to what lastID was
end)

function footballClass:kick(id,primary,energy)
	if (primary) then
		football.save[id].stats.kicks = football.save[id].stats.kicks + 1
		self.lastID = id
		self.lastSecondaryID = 0
		energy = 10
	else
		if (self.lastSecondaryID ~= id) and (self.lastID ~= id) then
			if (self.lastID == 0) or (player(self.lastID,"team") ~= player(id,"team")) then
				self.lastSecondaryID = id
			end
		end
		energy = 0.5
	end
	local dir = player(id,"rot")
	self.vector:addDir(dir,energy)
	local speed = self.vector.length
	if (speed > football.save[id].stats.recordBallSpeed) then
		football.save[id].stats.recordBallSpeed = speed
		msg2(id,misc.rgb(100,255,150).."You scored a new max speed! ("..misc.round(speed,2)..")")
	end
	self:tweenImage()
end

function footballClass:update()
	self.vector:multiply(football.friction)
	-- fix id's
	if (self.vector.length <= 5) then
		if (self.lastID > 0) then
			self.lastBackupID = self.lastID
		end
		self.lastID = 0
	end
	-- move
	local oldx = self.x
	local oldy = self.y
	local x,y = self.vector:get()
	self.x = self.x + x
	self.y = self.y + y
	self:checkGoal()
	-- DEBUG: misc.debugImage(self.x, self.y)

	-- bounce
	local newxtile = misc.pixel_to_tile(self.x)
	local newytile = misc.pixel_to_tile(self.y)
	if (tile(newxtile,newytile,"wall") or tile(newxtile,newytile,"obstacle")) then
		self.lastSecondaryID = 0
		-- prescision bounce
		self.x = oldx
		self.y = oldy
		local precision = 1
		while (true) do
			oldx = self.x
			oldy = self.y
			self.x = oldx + (x / 2)
			self.y = oldy + (y / 2)
			local newxtile = misc.pixel_to_tile(self.x)
			local newytile = misc.pixel_to_tile(self.y)
			if (tile(newxtile,newytile,"wall") or tile(newxtile,newytile,"obstacle")) then
				precision = precision + 1
				if (precision > 10) then
					break
				else
					x = (x / 2)
					y = (y / 2)
					self.x = oldx
					self.y = oldy
				end
			end
		end
		--
		local oldxtile = misc.pixel_to_tile(oldx)
		local oldytile = misc.pixel_to_tile(oldy)
		local dir = misc.point_direction(oldxtile,oldytile,newxtile,newytile)
		self.vector.direction = (dir - 180) - (self.vector.direction - dir)
		self.vector.cache = nil
		self.x = oldx
		self.y = oldy
	end
	local speed = self.vector.length
	-- add distance to stats
	local owner = self.lastID
	if (owner > 0) then
		football.save[owner].stats.distanceKicked = football.save[owner].stats.distanceKicked + (speed / 32) -- convert into meters (tiles)
	end
	-- effect
	local index = 0
	local diff = 7.5
	while (speed > diff and index < #football.effect) do
		index = index + 1
		local effect = football.effect[index]
		local effect_name = effect[1]
		local multiplier = effect[2]
		local chance = speed * 0.1
		if (math.random() < chance) then
			misc.effect(effect_name,self.x,self.y,1*multiplier,8,effect[3],effect[4],effect[5])
		end
		speed = speed - diff
	end
end

function footballClass:checkGoal()
	local tx = misc.pixel_to_tile(self.x)
	local ty = misc.pixel_to_tile(self.y)

	if (player(self.lastID,"exists") == false) then
		self.lastID = 0
	end
	if (player(self.lastSecondaryID,"exists") == false) then
		self.lastSecondaryID = 0
	end
	if (player(self.lastBackupID,"exists") == false) then
		self.lastSecondaryID = 0
	end

	local id = self.lastID
	local second_id = self.lastSecondaryID
	if (id == 0) then
		id = self.lastBackupID
	end

	local frame = tile(tx,ty,"frame")
	local name = player(id,"name")
	local team = player(id,"team")
	if (id == 0) then
		name = "[MISSING PLAYER]"
		team = 0
	end

	local through = ""
	local second_id = self.lastSecondaryID
	if (second_id > 0) then
		local under_team = player(second_id,"team")
		local under_color = 0
		if (under_team == 1) then
			under_color = misc.rgb(255,0,0)
		end
		 if (under_team == 2) then
			under_color = misc.rgb(0,0,255)
		end
		through = " Through "..under_color..player(second_id,"name")
	end

	scoredID = -1
	scoreChange = 0
	if (frame == football.goalTile.t) then
		if (team == 1) then
			scoreChange = -1
			msg(misc.rgb(255,0,0)..name..misc.rgb(0,0,255).." Scored a self-goal"..through.."!")
			if (id > 0) then
				football.save[id].stats.selfTgoals = football.save[id].stats.selfTgoals + 1
			end
		else
			scoreChange = 1
			msg(misc.rgb(0,0,255)..name.." Scored a goal"..through.."!")
			if (id > 0) then
				football.save[id].stats.CTgoals = football.save[id].stats.CTgoals + 1
			end
		end
		football.goals.ct = football.goals.ct + 1
		sadViolin.playTeam(1)
		if (football.goals.ct >= football.goalsRequired) then
 			football.goals.t = 0
			football.goals.ct = 0
			football.victories.ct = football.victories.ct + 1
			msg(misc.rgb(0,255,0).."Match won by Counter-Terrorists!")
		end
		scoredID = id
	end
	if (frame == football.goalTile.ct) then
		if (team == 2) then
			scoreChange = -1
			msg(misc.rgb(0,0,255)..name..misc.rgb(255,0,0).." Scored a self-goal"..through.."!")
			if (id > 0) then
				football.save[id].stats.selfCTgoals = football.save[id].stats.selfCTgoals + 1
			end
		else
			scoreChange = 1
			msg(misc.rgb(255,0,0)..name.." Scored a goal"..through.."!")
			if (id > 0) then
				football.save[id].stats.Tgoals = football.save[id].stats.Tgoals + 1
			end
		end
		football.goals.t = football.goals.t + 1
		sadViolin.playTeam(2)
 		if (football.goals.t >= football.goalsRequired) then
 			football.goals.t = 0
			football.goals.ct = 0
			football.victories.t = football.victories.t + 1
 			msg(misc.rgb(0,255,0).."Match won by Terrorists!")
		end
		scoredID = id
	end
	if (scoredID > -1) then
		if (scoredID > 0) then
			local score = player(scoredID,"score")
			score = score + scoreChange
			parse("setscore "..scoredID.." "..score)
		end
		football.removeEntity(self)
		football.updateHUD()
		msg(misc.rgb(100,255,0).."Current match: T "..football.goals.t.." - "..football.goals.ct.." CT")
		football.restartMatch()
	end
end

function footballClass:tweenImage()
	local x = self.x
	local y = self.y
	local vx,vy = self.vector:get()
	--misc.debugImage(x + vx,y + vy)
	x = x + (vx * 10)
	y = y + (vy * 10)
	tween_move(self.image,200,x,y)
end

function footballClass:destroy()
	freeimage(self.image)
end

-- FootballStats

football2StatsClass = {}
Class(football2StatsClass,function(id)
	self.id = id
	self.usgn = player(id,"usgn")
	self.file = nil
	if (self.usgn > 0) then
		self.file = "sys/lua/football2/saves/usgn_"..self.usgn..".txt"
		iniSave.open(self.file)
	end
	self.stats = {}
	self:load()
end)

function football2StatsClass:load()
	-- Stats
	self.stats.kicks = 0
	self.stats.sprints = 0
	self.stats.recordBallSpeed = 0
	self.stats.distanceKicked = 0
	--Terrorist-stats
	self.stats.Tgoals = 0
	self.stats.selfTgoals = 0
	--CounterTerrorist-stats
	self.stats.CTgoals = 0
	self.stats.selfCTgoals = 0
	-- load
	if (self.usgn > 0) then
		self.stats.kicks = iniSave.read(self.file,"Stats","Kicks",self.stats.kicks)
		self.stats.sprints = iniSave.read(self.file,"Stats","Sprints",self.stats.sprints)
		self.stats.recordBallSpeed = iniSave.read(self.file,"Stats","Record ball kick speed",self.stats.recordBallSpeed)
		self.stats.distanceKicked = iniSave.read(self.file,"Stats","Distance kicked",self.stats.distanceKicked)
		self.stats.Tgoals = iniSave.read(self.file,"Terrorist-stats","Goals",self.stats.Tgoals)
		self.stats.selfTgoals = iniSave.read(self.file,"Terrorist-stats","Self goals",self.stats.selfTgoals)
		self.stats.CTgoals = iniSave.read(self.file,"CounterTerrorist-stats","Goals",self.stats.CTgoals)
		self.stats.selfCTgoals = iniSave.read(self.file,"CounterTerrorist-stats","Self goals",self.stats.selfCTgoals)
	end
end

function football2StatsClass:save()
	if (self.usgn > 0) then
		iniSave.write(self.file,"Stats","Kicks",self.stats.kicks)
		iniSave.write(self.file,"Stats","Sprints",self.stats.sprints)
		iniSave.write(self.file,"Stats","Record ball kick speed",self.stats.recordBallSpeed)
		iniSave.write(self.file,"Stats","Distance kicked",self.stats.distanceKicked)
		iniSave.write(self.file,"Terrorist-stats","Goals",self.stats.Tgoals)
		iniSave.write(self.file,"Terrorist-stats","Self goals",self.stats.selfTgoals)
		iniSave.write(self.file,"CounterTerrorist-stats","Goals",self.stats.CTgoals)
		iniSave.write(self.file,"CounterTerrorist-stats","Self goals",self.stats.selfCTgoals)
		iniSave.save(self.file)
	end
end

function football2StatsClass:getDistanceString()
	local distance = self.stats.distanceKicked
	local meassurments = {
	{name="Meters",min=0},
	{name="Kilometers",min=1000},
	{name="Miles",min=1000*10}}
	local name = nil
	local multiplier = 0
	for i=1,#meassurments do
		local m = meassurments[i]
		if (distance >= m.min) then
			name = m.name
			multiplier = m.min
		else
			break
		end
		i = i + 1
	end

	return misc.round(distance/math.max(1,multiplier),2).." "..name
end

function football2StatsClass:say()
	local c = misc.rgb(0,155,0)
	msg(c.."--{ "..player(self.id,"name").." }--")
	msg(c.."kicks: "..self.stats.kicks.." Sprints: "..self.stats.sprints)
	local metersPerSecond = (self.stats.recordBallSpeed * 10) / 32	
	msg(c.."Fastest ball kick: "..misc.round(self.stats.recordBallSpeed,2).." ("..misc.round(metersPerSecond,2).." M/Second)")
	msg(c.."Distance kicked: "..self:getDistanceString())
	msg(c.."(as T) Goals: "..self.stats.Tgoals.." | Self goals: "..self.stats.selfTgoals)
	msg(c.."(as CT) Goals: "..self.stats.CTgoals.." | Self goals: "..self.stats.selfCTgoals)
	msg(c.."(Total) Goals: "..(self.stats.Tgoals+self.stats.CTgoals).." | Self goals: "..(self.stats.selfTgoals+self.stats.selfCTgoals))
end


