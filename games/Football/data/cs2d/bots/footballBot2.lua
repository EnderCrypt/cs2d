bot = {}
bot.array = {}
bot.settings = {}
bot.settings.sprintUsage = 0.01

function ai_onspawn(id)
	bot.array[id] = BotClass.new(id)
end

function ai_update_living(id)
	local self = bot.array[id]
	local football = self:getClosestBall()
	if (football ~= nil) then
		if (self.attackBall) then
			local goaltx = misc.tile_to_pixel(self.target.x)
			local goalty = misc.tile_to_pixel(self.target.y)
			local dir_ball_to_goal = misc.point_direction(football.x,football.y,goaltx,goalty)
			local move_x = football.x - misc.lengthdir_x(dir_ball_to_goal,32)
			local move_y = football.y - misc.lengthdir_y(dir_ball_to_goal,32)
			local px = player(id,"x")
			local py = player(id,"y")
			self.walkDir = misc.point_direction(px,py,move_x,move_y)
			self:aim(football)
			local dist = misc.point_distance(px,py,football.x,football.y)
			if (dist < 50) then
				ai_attack(id)
			end
			if ((dist > 96) and (math.random() < bot.settings.sprintUsage)) then
				ai_attack(id,1)
			end
		else
			ai_rotate(id,self.walkDir)
			self.walkDir = self.walkDir + ((math.random()*20)-10)
		end
	end
	if (ai_move(id,self.walkDir) == 0) then
		self.walkDir = self.walkDir + 180
	end
end

function ai_update_dead(id)
	ai_respawn(id)
end

bot.hook = {}

addhook("ms100","bot.hook.ms100")
function bot.hook.ms100()
	local team = 1 + math.floor(math.random()*2) -- generates either 1 or 2
	bot.setAttackBot(team)
	team = math.abs(team-3) -- switches between 1 and 2
	bot.setAttackBot(team)
	--[[
	bot.setAttackBot(1)
	bot.setAttackBot(2)
	]]--
end

function bot.setAttackBot(team)
	local closest_dist = math.huge
	local closest_bot = nil
	for _,self in pairs(bot.array) do
		if (self.team == team) then
			self.attackBall = false
			if (self.distance < closest_dist) then
				closest_dist = self.distance
				closest_bot = self
			end
		end
	end
	if (closest_bot ~= nil) then
		closest_bot.attackBall = true
	end
end

addhook("die","bot.hook.die")
function bot.hook.die(victim,killer,weapon,x,y)
	bot.array[victim] = nil
end

addhook("leave","bot.hook.leave")
function bot.hook.leave(id,reasob)
	bot.array[id] = nil
end

-- CLASS --
BotClass = {}
Class(BotClass,function(id)
	self.id = id
	self.team = player(id,"team")
	self.distance = math.huge
	self.attackBall = false

	self.target = {}
	if (self.team == 1) then
		--self.target.x = 37
		self.target.x = 47
	end
	if (self.team == 2) then
		--self.target.x = 2
		self.target.x = 3
	end
	self.target.y = 9.5

	local goaltx = misc.tile_to_pixel(self.target.x)
	local goalty = misc.tile_to_pixel(self.target.y)
	self.walkDir = misc.point_direction(player(id,"x"),player(id,"y"),goaltx,goalty)
end)

function BotClass:aim(football)
	local enemyID = self:getClosestEnemy(football)
	if ((enemyID == nil) or (self.distance > 64)) then
		self:aimAtGoal(0)
	else
		local ox = player(enemyID,"x")
		local oy = player(enemyID,"y")
		local distance = misc.point_distance(football.x,football.y,ox,oy)
		if (distance < 64) then
			local maxOffset = 50
			self:aimAtGoal(math.random()*(maxOffset*2)-maxOffset)
		else
			self:aimAtGoal(0)
		end
	end
end

function BotClass:aimAtGoal(offset)
	local tx = player(self.id,"tilex")
	local ty = player(self.id,"tiley")
	local dir = misc.point_direction(tx,ty,self.target.x,self.target.y)
	ai_rotate(self.id,dir+offset)
end

function BotClass:getClosestEnemy(football)
	local pl = player(0,"team"..(3-self.team).."living")
	for i=1,#pl do
		local otherID = pl[i]
		local ox = player(otherID,"x")
		local oy = player(otherID,"y")
		local dist = misc.point_distance(football.x,football.y,ox,oy)
		if (dist < 64) then
			return otherID
		end
	end
	return nil
end
--[[
function BotClass:getActiveEnemy()
	local pl = player(0,"team"..(3-self.team).."living")
	for i=1,#pl do
		local otherID = pl[i]
		if (player(otherID,"bot")) then
			local other = bot.array[otherID]
			if (other.distance < math.huge) then
				return otherID
			end
		end
	end
	return nil
end
]]--

function BotClass:getClosestBall()
	local x = player(self.id,"x")
	local y = player(self.id,"y")
	local closes_dist = math.huge
	local closes_ball = nil
	for i=1,#football.entities do
		local entity = football.entities[i]
		local dist = misc.primitive_point_distance(x,y,entity.x,entity.y)
		if (dist < closes_dist) then
			closes_dist = dist
			closes_ball = entity
		end
	end
	if (closes_ball == nil) then
		self.distance = math.huge
	else
		self.distance = closes_dist
	end
	return closes_ball
end