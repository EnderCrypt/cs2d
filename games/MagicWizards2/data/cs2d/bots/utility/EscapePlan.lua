BotEscapePlan = {}

Class(BotEscapePlan,function(botID)
	self.botID = botID
	self.threatID = nil
	self.threatPosition = nil
	self.index = 1
	self.path = {}
	self.open = nil
	self.closed = nil
	self.valid = false
end)

function BotEscapePlan:planEscape(threatID, escapeTileDistance, timeout)
	if (timeout == nil) then
		timeout = math.huge
	end
	self.threatID = threatID
	self.open = {}
	self.closed = {}
	--
	self.threatPosition = {x=player(self.threatID,"tilex"),y=player(self.threatID,"tiley")}
	self.open[#self.open+1] = {player(self.botID,"tilex"),player(self.botID,"tiley"),0,nil} -- x, y, distFromThreat, previous
	local current = {}
	-- start iterate
	while ((#self.open > 0) and (timeout > 0)) do -- grab top tile (the ones furthest away from threat)
		timeout = timeout + 1
		current = self.open[#self.open]
		self.open[#self.open] = nil
		--
		if (current[3] >= escapeTileDistance) then -- minTiles tiles away from threat
			-- save results
			self.index = 1
			self.path = self:backTraceResults(current)
			self.valid = true
			return true
		end
		--
		self.closed[current[1]..","..current[2]] = true
		self:addTilePaths(current)
		--
	end
	return false
end

function BotEscapePlan:addTilePaths(current)
	self:addTile(current,current[1]+1,current[2])
	self:addTile(current,current[1]-1,current[2])
	self:addTile(current,current[1],current[2]+1)
	self:addTile(current,current[1],current[2]-1)
end

function BotEscapePlan:addTile(previous,x,y)
	if (tile(x,y,"walkable") == true) then -- if walkable tile
		if (self.closed[x..","..y] == nil) then -- if hasnt already been iterated through
			local dist = misc.primitive_point_distance(self.threatPosition.x,self.threatPosition.y,x,y)
			local index = #self.open+1
			while ( (index > 2) and (dist < self.open[index-1][3]) ) do
				index = index - 1
			end
			table.insert(self.open,index,{x,y,dist,previous}) -- sort it by putting the tiles that are furthest away on top of array
		end
	end
end

function BotEscapePlan:backTraceResults(endNode)
	local current = endNode
	local result = {}
	repeat
		table.insert(result,1,current)
		--misc.debugImage(misc.tile_to_pixel(current[1]),misc.tile_to_pixel(current[2]),100,100,100,2000)
		current = current[4] -- previous
	until (current == nil)
	return result
end

function BotEscapePlan:hasPlan()
	return self.valid
end

function BotEscapePlan:getThreat()
	return self.threatID
end

-- 0 goal reached
-- 1 path blocked
-- 2 still moving
function BotEscapePlan:escape()
	if (self.index <= #self.path) then
		local tx = self.path[self.index][1]
		local ty = self.path[self.index][2]
		local x = misc.tile_to_pixel(tx)
		local y = misc.tile_to_pixel(ty)
		local bx = player(self.botID,"x")
		local by = player(self.botID,"y")
		local primitiveDist = misc.primitive_point_distance(bx,by,x,y)
		if (primitiveDist < 32) then -- basically arrived, increase index
			self.index = self.index + 1
			if (self.index > #self.path) then
				self.valid = false
				return 0 -- goal reached
			end
		end
		local result = ai_move(self.botID,misc.point_direction(bx,by,x,y))
		if (result == 0) then -- path blocked
			self.valid = false
			return 1 -- path blocked
		else
			return 2 -- moving towards target
		end
	end
	self.valid = false
	return 0 -- goal reached
end