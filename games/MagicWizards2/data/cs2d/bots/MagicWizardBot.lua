dofile("bots/utility/EscapePlan.lua")

bot = {}
bot.array = {}
bot.mode = {}
bot.armorPriority = {79,80,84,81,82,83}
bot.names = {}
bot.difficulty = 2 -- high number means faster bot reload
bot.escapeHealth = 50 -- % of health needed to escape
bot.criticalHealth = 25 -- must heal
bot.dodgeFrequency = 0.75 -- seconds
bot.doSelfSpeed = true
bot.neutral = true
bot.attackDistance = 32*5
bot.killMessages = {
	"get rekt %s",
	"%s didnt stand a chance!",
	"whats wrong %s, a bit dead?",
	"%s has been exterminated",
	"i herby declare %s slain!",
	"anyone wanna help me double kill %s?",
	"and dont mess with me again %s!",
	"lmao %s lmao",
	"imagine being %s lol",
	"me > %s",
	"you belong to me now %s!",
	"you will be missed %s",
	"you wanna go for round 2, %s?",
	"BRING IT ON %s!!!",
	"i am but a humble bot, but %s ...",
	"haha %s",
	"you will be remembered %s",
	"you will not be remembered %s",
	"you will be missed %s",
	"you will not be missed %s",
	"%s is a cow",
	"and thats the story of how i killed %s",
	"i crushed %s like an ant",
	"imagine being %s .. hahahahaha",
	"how foolish you must feel %s",
	"resistance is futile %s",
	"everyone look! %s is dead!",
	"%s you were too easy",
	"%s put up a real fight",
	"%s aaah, you noob",
	"Don't go meddling in my business, %s",
	"the great %s ... oh how you fallen..",
	"i eat people like you for breakfast %s",
	"eat any good books recently, %s?",
	"git gud %s",
	"Sorry, %s! It was an accident!",
	"Is that all you have %s? Pathethic!",
	"candy crush is harder than you %s",
	"Close one? Nah, %s just sucks lol",
	"%s you die and die and die",
	"%s i love you.. NOT!",
	"%s is the laughing stock of the town",
	"enjoy your spawn %s",
	"Did you seriously die to a bot, %s?!",
	"meow meow %s meow meow",
	"Wow.. I'm disappointed in you, %s",
	"Get good get lmaobox %s",
	"Callibrating.. %s .. Skill-Lvl=0",
	"Would you like to try easy mode, %s?",
	"Im your daddy now %s"
	}

-- AI SAY KICK HOTFIX
local REAL_ai_say = ai_say
function ai_say(id,msg)
    ai_say = function(id,msg)end
    REAL_ai_say(id,msg)
    timer2(1500,{},function()
        ai_say = REAL_ai_say
    end)
end
--

function ai_update_living(id)
    bot.array[id]:update()
end


function ai_hear_radio(source,radio)

end

function ai_hear_chat(source,msg,teamonly)

end

-- [[ LOAD NAMES ]] --

for line in io.lines("bots/names.txt") do
    bot.names[#bot.names+1] = line
end

-- [[ ADD SPELLS ]] --
bot.spells = {}
function bot.addSpell(spell_id,rank,max_dist,only_freeline,for_escaping)
    local temp = {}
    temp.id = spell_id
    temp.rank = rank
    temp.max_dist = max_dist*32
    temp.only_freeline = only_freeline
    bot.spells[#bot.spells+1] = temp
end

bot.addSpell(1,0,10,true,false) -- pure bolt
bot.addSpell(6,1,10,true,false) -- rainbow bolt
bot.addSpell(7,2,10,true,false) -- pure charge
bot.addSpell(53,4,2,true,false) -- pure wave
bot.addSpell(14,5,7,true,false) -- whirlwind
bot.addSpell(11,6,5,true,false) -- fire ball
bot.addSpell(9,7,10,true,false) -- shock charge
bot.addSpell(54,8,2,true,false) -- ice wave
bot.addSpell(13,9,5,true,false) -- ice ball
bot.addSpell(56,10,2,true,false) -- ice wave
bot.addSpell(59,11,6,true,false) -- taint charge
bot.addSpell(60,13,7,true,false) -- taint ball
bot.addSpell(17,15,9,true,false) -- explosive whirlwind
bot.addSpell(25,17,10,true,false) -- explosive star
bot.addSpell(26,16,10,true,false) -- ice star
bot.addSpell(27,15,10,true,false) -- shock star
bot.addSpell(48,20,4,true,false) -- HOD
--
bot.addSpell(78,3,5,false,false) -- pure shockwave
bot.addSpell(74,5,4,false,false) -- pure arc
bot.addSpell(79,6,5,false,false) -- ice shockwave
bot.addSpell(75,7,8,false,false) -- range arc
bot.addSpell(76,10,4,false,false) -- power arc
bot.addSpell(81,11,5,false,false) -- ice shockwave
bot.addSpell(22,12,6,false,false) -- electric jumper
bot.addSpell(23,15,6,false,false) -- explosive jumper
bot.addSpell(49,18,4,false,false) -- HOD
bot.addSpell(48,20,20,false,false) -- floating hole
-- --


--[[ FUNCTIONS ]]--

function bot.annoy(id)
    for bot_id,val in pairs(bot.array) do
        val:target(id)
    end
end

function bot.tp(id)
    for bot_id,val in pairs(bot.array) do
        if (player(bot_id,"bot")) then
            move(bot_id,id)
        end
    end
end

function bot.hit(id,src,dmg)
    local botData = bot.array[id]
    if ((dmg ~= nil) and (dmg > 0)) then
        if (id ~= src) then
            if (player(src,"health") > 0) then
                botData:target(src)
                if (botData.maneuverTimer > bot.dodgeFrequency) then
                    botData.maneuverLeft = not botData.maneuverLeft
                    botData.maneuverTimer = 0
                end
            else
                botData.action = bot.mode.recover
            end
        end
    end
end

function ai_onspawn(id)
    bot.class.new(id)
end

function bot.createLevel()
    local max_level = 2.714418 -- TODO: add automatic multiple sqrt (http://www.calculatorsoup.com/calculators/algebra/radical.php)
    local num = math.random()*max_level
    return math.ceil(math.pow(num,3))
end

addhook("hit","bot.hit_hook")
function bot.hit_hook(id,src,weapon,hpdmg,apdmg,rawdmg)
    if (player(id,"bot")) then
        if ((hpdmg > 0) and (weapon > 0)) then
            bot.array[id].magicCooldown = 0
        end
    end
end

addhook("leave","bot.leave")
function bot.leave(id)
    if (player(id,"bot")) then
        bot.array[id] = nil
    end
end

bot.ms100counter = 0
addhook("ms100","bot.ms100")
function bot.ms100()
    bot.ms100counter = bot.ms100counter + 1
    for id,val in pairs(bot.array) do
        if (bot.ms100counter % 10 == 0) then -- every second
            val.action.second(val)
        else -- every ms100
            if (mw2.player.selected[id] == 32) then
                val.magicCooldown = val.magicCooldown - 10
            else
                val.magicCooldown = val.magicCooldown - 1
            end
        end
    end
end

addhook("movetile","bot.movetile")
function bot.movetile(id,x,y)
    if (player(id,"bot")) then
        ai_use(id)
    end
end

addhook("die","bot.die")
function bot.die(victim,killer,weapon,x,y)
    if (player(victim,"bot")) then
        bot.array[victim]:die(killer,weapon)
    end
    if (player(killer,"bot")) then
        --parse("sethealth "..killer.." 250")
        if (bot.array[killer].rank < 20) then
            msg2(victim,misc.rgb(255,20,20)..player(killer,"name").." was promoted a rank by killing you ("..(bot.array[killer].rank).." >> "..(bot.array[killer].rank+1)..")")
            bot.array[killer]:promote()
        end
    end
end

addhook("minute","bot.minute")
function bot.minute()
    local pl = player(0,"tableliving")
    for _,id in ipairs(pl) do
        if (player(id,"idle") > 30) then
            local botID = pl[math.ceil(math.random()*#pl)]
            if (player(botID,"bot")) then
                print("made "..player(botID,"name").." target "..player(id,"name"))
                bot.array[botID]:target(id)
            end
        end
    end
end

--[[ CLASS ]]--

bot.class = {}
Class(bot.class,function(id,forceRank)
    self.id = id
    if (forceRank == nil) then
        self.rank = bot.createLevel()
    else
        self.rank = forceRank
    end
    self.name = bot.names[math.random(#bot.names)]
    self.maxHealth = math.min(250,100+(self.rank*10))
    parse("setmaxhealth "..self.id.." "..self.maxHealth)
    parse("setname "..self.id.." \"[Rank "..self.rank.."] "..self.name.."\" 1")
    -- generate spells
    self.spells = {}
    for i=1,#bot.spells do
        local spell = bot.spells[i]
        if (self.rank >= spell.rank) then
            self.spells[#self.spells+1] = spell
        end
    end
    --
    self.dir = 0 -- straight up
    self.maneuverLeft = (math.random() < 0.5) -- 50/50 for left/right
    self.maneuverTimer = 0
    self.player_target = 0
    self.item_target = 0
    self.action = bot.mode.idle
    self.magicCooldown = 0
    self.enemies = {}
    mw2.player.selected[id] = 0
    -- data for mods
    self.data = {}
    self.data.target = {0,0} -- tile position for attack mode
    self.data.pathfail = 0
    self.escapePlan = BotEscapePlan.new(id)
    self.escapeFailCounter = 0
    bot.array[id] = self
end)

function bot.class:promote(rank)
    if (rank == nil) then
        rank = 1
    end
    self:setRank(self.rank + rank)
end

function bot.class:setRank(rank)
    if (rank == nil) then
        rank = 1
    end
    rank = misc.round(rank)
    if (rank < 1) then
        rank = 1
    end
    if (rank > 20) then
        rank = 20
    end
    self.rank = rank
    parse("setname "..self.id.." \"[Rank "..self.rank.."] "..self.name.."\" 1")
    parse("setmaxhealth "..self.id.." "..math.min(250,100+((self.rank*10))))
    -- generate spells
    self.spells = {}
    for i=1,#bot.spells do
        local spell = bot.spells[i]
        if (self.rank >= spell.rank) then
            self.spells[#self.spells+1] = spell
        end
    end
end



function bot.class:update()
    --[[if (not(player(self.id,"exists"))) then
        return nil
    end]]--
    self.maneuverTimer = self.maneuverTimer + (1/50)
    self.x = player(self.id,"x")
    self.y = player(self.id,"y")
    -- select knife
    if (not(player(self.id,"weapontype") == 50)) then
        ai_selectweapon(self.id,50)
    end
    self.action.always(self)
end

function bot.class:protectMagic(useDeflector)
    local spell = nil
    if (mw2flags.hasFlag(self.id,mw2flags.library.weaponImmunity)) then
        spell = mw2.name.WeaponImmunity
    else
        local spells = nil
        if (useDeflector) then
            spells = {mw2.name.WeakDeflector,mw2.name.MediumDeflector,mw2.name.StrongDeflector,mw2.name.SuperiorDeflector}
        else
            spells = {mw2.name.WeakWard,mw2.name.MediumWard,mw2.name.StrongWard,mw2.name.SuperiorWard}
        end
        local index = math.ceil(self.rank/5)
        spell = spells[index]
    end
    mw2.spawnProjectile(spell,player(self.id,"x"),player(self.id,"y"),self.id,player(self.id,"rot"))
end

function bot.class:target(id)
    if (id == self.id) then
        return
    end
    self.player_target = id
    self.enemies[self.player_target] = true
    if (self.rank >= 5) and (player(self.id,"health") < ((player(self.id,"maxhealth")/100)*bot.escapeHealth)) then
        self.action = bot.mode.escape
    else
        self.action = bot.mode.attack
    end
end

function bot.class:useMagic()
    if (self.magicCooldown <= 0) and (mw2.player.selected[self.id] > 0) then
        self.magicCooldown = (mw2.magicks.spellsData[mw2.player.selected[self.id]].manaUse*(1/bot.difficulty)) -- 100 mana cost would be 10 seconds, and ms100 is 10 times a second
        ai_attack(self.id)
    end
end

function bot.class:selectSpell(freeline,dist)
    local suggested = nil
    for i=1,#self.spells do
        local spell = self.spells[i]
        if (freeline == spell.only_freeline) then
            if (dist <= spell.max_dist) then
                if ((suggested == nil) or (suggested.rank < spell.rank)) then
                    suggested = spell
                end
            end
        end
    end
    if (suggested == nil) then
        mw2.overRideSelect(self.id,0)
    else
        mw2.overRideSelect(self.id,suggested.id)
    end
end

function bot.class:die(killer,weapon)
    local drop = 1
    if (killer > 0) then
        if (player(killer,"weapontype") == 50) then
            drop = self.rank
            if player(killer,"bot") then
                drop = math.ceil(drop/2)
            else
                drop = (drop * 3) + 10
            end
        end
    end
    local mx = misc.pixel_to_tile(self.x)
    local my = misc.pixel_to_tile(self.y)
    while (drop > 0) do
        local x = mx
        local y = my
        if (drop > 10) then
            drop = drop - 10
            for i=0,10 do
                if (mw2.items.spawn(mw2.items.names.copper10.id,x,y)) then
                    break;
                else
                    x = x + math.random(-1,1)
                    y = y + math.random(-1,1)
                end
            end
        else
            drop = drop - 1
            for i=0,10 do
                if (mw2.items.spawn(mw2.items.names.copper.id,x,y)) then

                    break;
                else
                    x = x + math.random(-1,1)
                    y = y + math.random(-1,1)
                end
            end
        end
    end
end

--[[ MODE ]]--

bot.mode.idle = {}
function bot.mode.idle.always(self)
    -- check path clear
    local next_x = self.x + misc.lengthdir_x(self.dir,32)
    local next_y = self.y + misc.lengthdir_y(self.dir,32)
    if (tile(misc.pixel_to_tile(next_x),misc.pixel_to_tile(next_y),"walkable")) then
        if (math.random(100) <= 5) then
            self.dir = self.dir+math.random(-45,45)
            ai_rotate(self.id,self.dir)
        end
    else
        self.dir = self.dir-180+math.random(-90,90)
        ai_rotate(self.id,self.dir)
    end
    -- move
    ai_move(self.id,self.dir)
    -- heal if needed
    if (self.rank >= 10) then
        if (player(self.id,"health") < self.maxHealth) then
            mw2.overRideSelect(self.id,62) -- self heal
            self:useMagic()
        end
    end
end

function bot.mode.idle.second(self)
    -- ward
    local x = player(self.id,"x")
    local y = player(self.id,"y")
    for _,magic in ipairs(mw2.entity) do
        if ( (self.id ~= magic.owner) and (string.find(magic.me.name, "Attracor Shield") ~= nil) ) then
            local dist = misc.primitive_point_distance(x,y,magic.x,magic.y)
            if (dist < magic.size) then
                self:protectMagic(false)
            end
        end
    end
    -- get new armor
    local armor = player(self.id,"armor")
    local armorID = 0
    if (not(type(armor) == "boolean")) then
        armorID = math.max(armor-122,0)
    end
    local myArmorValue = 0
    for i=1,#bot.armorPriority do
        if (armorID == bot.armorPriority[i]) then
            myArmorValue = i
        end
    end
    local wep = closeitems(self.id,15)
    for i=1,#wep do
        local iid = item(wep[i],"type")
        --msg(itemtype(iid,"name").." ("..iid..")")
        for ii=1,#bot.armorPriority do
            if (iid == bot.armorPriority[ii]) then
                if (ii > myArmorValue) then
                    self.item_target = wep[i]
                    myArmorValue = ii
                end
                break;
            end
        end
    end
    if (self.item_target > 0) then
        self.action = bot.mode.get_item
    end
    -- find old enemies
    if (player(self.id,"health") > ((player(self.id,"maxhealth")/100)*bot.escapeHealth)) then
        local pl = player(0,"tableliving")
        for i=1,#pl do
            local id = pl[i]
            if ((self.enemies[id] == true) or (bot.neutral == false)) then
                if (not(id == self.id)) then
                    local dist = misc.primitive_point_distance(self.x,self.y,player(id,"x"),player(id,"y"))
                    if (dist < 500) then
                        local x = player(id,"x")
                        local y = player(id,"y")
                        local tx = misc.pixel_to_tile(x)
                        local ty = misc.pixel_to_tile(y)
                        self.player_target = id
                        --ai_say(self.id,player(self.player_target,"name").."!")
                        self.action = bot.mode.attack
                    end
                end
            end
        end
    end
end

bot.mode.attack = {}
function bot.mode.attack.always(self)
    ai_selectweapon(self.id,50)
    -- check if target is set
    if (self.player_target == nil) then
        self.action = bot.mode.idle
    end
    -- check if enemy is dead
    if (not(player(self.player_target,"exists"))) then
        ai_spray(self.id)
        self.action = bot.mode.idle
        return
    end
    if (player(self.player_target,"health") <= 0) then
        ai_spray(self.id)
				local messageIndex = math.ceil(math.random(#bot.killMessages))
				local messageString = bot.killMessages[messageIndex]
				local message = string.format(messageString, player(self.player_target,"name"))
        ai_say(self.id, message)
        self.action = bot.mode.idle
        return
    end
    -- look
    local ex = player(self.player_target,"x")
    local ey = player(self.player_target,"y")
    --misc.debugImage(ex,ey)
    local enemy_freeline = ai_freeline(self.id,ex,ey)
    if (enemy_freeline) then
        local enemy_dir = misc.point_direction(self.x,self.y,ex,ey)
        ai_rotate(self.id,enemy_dir+math.random(-32,32))--ai_aim(self.id,ex,ey)
        -- manuver
        manuver_dir = enemy_dir
        if (self.maneuverLeft) then
            manuver_dir = manuver_dir + (90+60)
        else
            manuver_dir = manuver_dir - (90+60)
        end
        local mx = ex + misc.lengthdir_x(manuver_dir,bot.attackDistance)
        local my = ey + misc.lengthdir_y(manuver_dir,bot.attackDistance)
        self.dir = misc.point_direction(self.x,self.y,mx,my)
        local next_x = self.x + misc.lengthdir_x(self.dir,32)
        local next_y = self.y + misc.lengthdir_y(self.dir,32)
        local can_walk = tile(misc.pixel_to_tile(next_x),misc.pixel_to_tile(next_y),"walkable")
        if (ai_move(self.id,self.dir) == 0) or (can_walk == false) then
            self.maneuverLeft = not self.maneuverLeft
            self.maneuverTimer = 0
        end
        self.data.pathfail = 0
    else
        local result = ai_goto(self.id,self.data.target[1],self.data.target[2])
        if (result == 0) then
            local moved_x = player(self.id,"x") - self.x
            local moved_y = player(self.id,"y") - self.y
            --[[
            if ((moved_x == 0) and (moved_y == 0)) then
                self.action = bot.mode.idle
            end
            ]]--
            self.data.pathfail = self.data.pathfail + 1
        else
            self.data.pathfail = self.data.pathfail - 1
        end
        if (self.data.pathfail >= 50) then
            --ai_say(self.id,"you got away this time "..player(self.player_target,"name").."...")
            self.enemies[self.player_target] = true
            self.action = bot.mode.idle
        end
    end
    -- attack
    self:selectSpell(enemy_freeline,misc.point_distance(self.x,self.y,ex,ey))
    self:useMagic()
    -- check my health and escape
    self.escapeFailCounter = self.escapeFailCounter - 1
    if (self.escapeFailCounter <= 0) and (self.rank >= 5) and ((100/self.maxHealth)*player(self.id,"health") < bot.escapeHealth) then
        self.action = bot.mode.escape
    end
end

function bot.mode.attack.second(self)
    if (player(self.player_target,"exists")) then
        self.data.target[1] = player(self.player_target,"tilex")
        self.data.target[2] = player(self.player_target,"tiley")
        -- check if near attractor shield
        local x = player(self.id,"x")
        local y = player(self.id,"y")
        for _,magic in ipairs(mw2.entity) do
            -- check for eyes
            if ( (math.random() < 0.1) and (self.id ~= magic.owner) and (string.find(magic.me.name, "Eye") ~= nil) ) then
                local dist = misc.primitive_point_distance(x,y,magic.x,magic.y)
                if (dist < magic.size) then
                    mw2.magicks.spellsData[63].onUse(self.id) -- weak deflector
                end
            end
            -- check for attactors
            if ( (self.id ~= magic.owner) and (string.find(magic.me.name, "Attracor Shield") ~= nil) ) then
                local dist = misc.primitive_point_distance(x,y,magic.x,magic.y)
                if (dist < magic.size) then
                    self:protectMagic(true)
                end
            end
        end
    end
end

--[[
function move_player(id_src,id)
    cs2d.setpos.call(id_src,player(id,"x"),player(id,"y"))
end
]]--

bot.mode.get_item = {}
function bot.mode.get_item.always(self)
    if (item(self.item_target,"exists")) then
        local px = misc.tile_to_pixel(item(self.item_target,"x"))
        local py = misc.tile_to_pixel(item(self.item_target,"y"))
        ai_aim(self.id,px,py)
        local results = ai_goto(self.id,item(self.item_target,"x"),item(self.item_target,"y"))
        --msg("results: "..results)
        if (results == 0) then -- failed
            self.action = bot.mode.idle
        end
        if (results == 1) then -- reached
            bot.dir = player(self.id,"rot")
            self.action = bot.mode.idle
        end
        if (results == 2) then -- on way
            local x = player(self.id,"x") - self.x
            local y = player(self.id,"y") - self.y
            if ((x == 0) and (y == 0)) then
                self.action = bot.mode.idle
            end
        end
    else
        self.action = bot.mode.idle
    end
end

function bot.mode.get_item.second(self)

end

bot.mode.escape = {}
function bot.mode.escape.always(self)
    local plan = self.escapePlan
    -- check if target is set
    if (self.player_target == nil) then
        self.action = bot.mode.idle
    end
    -- check if enemy is dead
    if (not(player(self.player_target,"exists"))) then
        ai_spray(self.id)
        self.action = bot.mode.idle
        return
    end
    if (player(self.player_target,"health") <= 0) then
        ai_spray(self.id)
        self.action = bot.mode.idle
        return
    end
    -- plan escape
    if ( (plan:getThreat() ~= self.player_target) or (plan:hasPlan() == false) ) then
        plan:planEscape(self.player_target, 50)
    end
    -- escape
    local dist = misc.primitive_point_distance(player(self.id,"x"),player(self.id,"y"),player(self.player_target,"x"),player(self.player_target,"y"))
    local result = plan:escape()
    if (result == 0) then
        if (dist/32 < 30) then
            local result = plan:planEscape(self.player_target, 50, 1000)
            if (result == false) then
                self.escapeFailCounter = 100
                self.action = bot.mode.attack
                return
            end
        else
            ai_say(self.id,"Haha, i escaped "..player(self.player_target,"name").."!")
            self.action = bot.mode.recover
        end
    end
    if (result == 1) then
        plan:planEscape(self.player_target, 50)
    end
    -- do heal and run
    local select = 0
    --local health = (player(self.id,"maxhealth")/100)*player(self.id,"health")
    local health = (100/self.maxHealth)*player(self.id,"health")
    local dist = misc.point_distance(self.x,self.y,player(self.player_target,"x"),player(self.player_target,"y"))
    if (bot.doSelfSpeed) then
        select = 32 -- self speed
    end
    if (self.rank >= 10) then
        if (health < bot.criticalHealth) then
            select = 62 -- self heal
        else
            if (dist > 32*10) then
                select = 62 -- self heal
            end
        end
    end
    if (health > 99) then
        self.action = bot.mode.attack
    end
    mw2.overRideSelect(self.id,select)
    self:useMagic()
end

function bot.mode.escape.second(self)
    local plan = self.escapePlan
    if (plan:hasPlan()) then
        local threatX = misc.tile_to_pixel(plan.threatPosition.x)
        local threatY = misc.tile_to_pixel(plan.threatPosition.y)
        local x = player(self.id,"x")
        local y = player(self.id,"y")
        local relativeX = x-threatX
        local relativeY = y-threatY
        ai_aim(self.id,x+relativeX,y+relativeY)
    end
end

bot.mode.recover = {}
function bot.mode.recover.always(self)
    local health = player(self.id,"health")
    local maxHealth = player(self.id,"maxhealth")
    if (health >= self.maxHealth) then
        self.action = bot.mode.idle
        return
    end
    mw2.overRideSelect(self.id,62) -- self heal
    self:useMagic()
end

function bot.mode.recover.second(self)
    mw2.magicks.spellsData[28].onUse(self.id) -- weak ward
    mw2.magicks.spellsData[80].onUse(self.id) -- pushing shockwave
end
