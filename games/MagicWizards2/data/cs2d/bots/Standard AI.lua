function ai_update_dead(id)
    ai_respawn(id)
end

-- copy these
function ai_onspawn(id)

end

function ai_update_living(id)

end

function ai_hear_radio(source,radio)

end

function ai_hear_chat(source,msg,teamonly)

end

-- auto bot set
parse("bot_prefix [BOT]")
local ai_set = false
local MAP_NAME = string.lower(map("name"))

if (MAP_NAME == "magic wizards happy town") then
	dofile("bots/MagicWizardBot.lua")
	ai_set = true
end

if (ai_set == false) then
    dofile("bots/Standard cs2d AI.lua")
end

addhook("vote","BOT_vote")
function BOT_vote(id,mode,param)
    if (mode == 1) then
        local target = param
        if (player(target,"bot")) then
            parse("kick "..id.." \"Dont mess with bots\"")
        end
    end
end
