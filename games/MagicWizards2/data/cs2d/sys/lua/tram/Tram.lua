-- require 'MiscFunction.lua'
-- suggest 'SuperPixelOffset.lua'

dofile("sys/lua/tram/SuperPixelOffset.lua") -- OPTIONAL

ttrain = {}
if map("name") == "Magic Wizards Happy Town" then
    ttrain.tile = {5}
end

ttrain.track = {}
ttrain.cache = {}
ttrain.onTrain = {}
ttrain.player = {}
ttrain.player.trainControlling = 0
ttrain.player.controlImage = nil
ttrain.image = nil
ttrain.calculateTrackSuccess = false
misc.addServerTransfer("gfx/ttrain/tram.png")
misc.addServerTransfer("gfx/ttrain/speed-1.png")
misc.addServerTransfer("gfx/ttrain/speed0.png")
misc.addServerTransfer("gfx/ttrain/speed1.png")
misc.addServerTransfer("gfx/ttrain/speed2.png")
misc.addServerTransfer("gfx/ttrain/speed3.png")

ttrain.tram = {}
ttrain.tram.newSpeed = 0
ttrain.tram.currentSpeed = 0
ttrain.tram.speedCount = 0

function ttrain.init(x,y,dir)
    ttrain.image = Image.Class.new("gfx/ttrain/tram.png",0,0,0)
		ttrain.image:setAutoRefresh(true)
    ttrain.image:imagepos(misc.tile_to_pixel(x),misc.tile_to_pixel(y),dir)
    x_add = misc.round(misc.lengthdir_x(dir,1))
    y_add = misc.round(misc.lengthdir_y(dir,1))
    ttrain.calculateTrack(x,y,x-x_add,y-y_add)
    if (ttrain.calculateTrackSuccess) then
        ttrain.calculateTrackDirs()
        msg(misc.rgb(0,255,0).."ttrain: init completed!")
        --superDump.dump("ttrain.track","Export of pre loaded track data")
    else
        msg(msg.rgb(255,0,0).."ttrain: SEVERE: init failed!")
        freehook("ms100","ttrain.ms100")
        ttrain.image:freeimage()
    end
end

function ttrain.calculateTrack(x,y,lastx,lasty)
    local temp = {}
    temp.x = x
    temp.y = y
    if #ttrain.track > 0 then
        if (ttrain.track[1].x == temp.x) and (ttrain.track[1].y == temp.y) then
            print("Finished pre loading track... length: "..#ttrain.track)
            ttrain.calculateTrackSuccess = true
            return 0
        end
    end
    ttrain.track[#ttrain.track+1] = temp
    -- RECURSE --
    if not((x+1 == lastx) and (y == lasty)) then
        if ttrain.isTrack(x+1,y) then
            ttrain.calculateTrack(x+1,y,x,y)
        end
    end
    if not((x-1 == lastx) and (y == lasty)) then
        if ttrain.isTrack(x-1,y) then
            ttrain.calculateTrack(x-1,y,x,y)
        end
    end
    if not((x == lastx) and (y+1 == lasty)) then
        if ttrain.isTrack(x,y+1) then
            ttrain.calculateTrack(x,y+1,x,y)
        end
    end
    if not((x == lastx) and (y-1 == lasty)) then
        if ttrain.isTrack(x,y-1) then
            ttrain.calculateTrack(x,y-1,x,y)
        end
    end
end

function ttrain.calculateTrackDirs()
    i = 0
    while (i < #ttrain.track) do
        i = i + 1
        to = i + 2
        if to > #ttrain.track then
            to = to - #ttrain.track
        end
        dir = misc.point_direction(ttrain.track[i].x,ttrain.track[i].y,ttrain.track[to].x,ttrain.track[to].y)
        ttrain.track[i].dir = dir
    end
    print("Finished calculating train directions...")
end

addhook("use","ttrain.use")
function ttrain.use(id,event,data,x,y)
    if ttrain.onTrain[id] then
        if ttrain.player.trainControlling == id then
            msg2(id,misc.rgb(255,0,0).."You released the controls of the train!")
            freeimage(ttrain.player.controlImage)
            ttrain.player.controlImage = nil
            ttrain.player.trainControlling = 0
        else
            if ttrain.player.trainControlling > 0 then
                msg2(id,misc.rgb(255,0,0).."The train is currently controlled by "..player(ttrain.player.trainControlling,"name").."!")
            else
                ttrain.player.trainControlling = id
                msg2(id,misc.rgb(0,255,0).."You take control of the train!")
                msg2(id,misc.rgb(0,255,0).."say !s to slow down/reverse say !f to speedup")
                ttrain.changeSpeed(id,ttrain.tram.currentSpeed)
            end
        end
    end
end

function ttrain.changeSpeed(id,newSpeed)
    if ttrain.player.trainControlling == id then
        if not (ttrain.player.controlImage == nil) then
            freeimage(ttrain.player.controlImage)
        end
        ttrain.player.controlImage = image("gfx/hl2d/hud/train/speed"..newSpeed..".png",300,450,2,id)
    end
end

addhook("join","ttrain.join")
function ttrain.join(id)
    ttrain.onTrain[id] = false
end

addhook("leave","ttrain.leave")
function ttrain.leave(id)
    if ttrain.player.trainControlling == id then
        ttrain.player.trainControlling = 0
        freeimage(ttrain.player.controlImage)
        ttrain.player.controlImage = nil
    end
    ttrain.onTrain[id] = false
end

--[[
addhook("serveraction","ttrain.serveraction")
function ttrain.serveraction(id,action)
    if ttrain.player.trainControlling == id then
        if action == 1 then
            if ttrain.tram.newSpeed > -1 then
                ttrain.tram.newSpeed = ttrain.tram.newSpeed - 1
                msg2(id,misc.rgb(0,0,255).."Changed train speed to "..ttrain.tram.newSpeed)
            end
            ttrain.changeSpeed(id,ttrain.tram.newSpeed)
        end
        if action == 2 then
            if ttrain.tram.newSpeed < 3 then
                ttrain.tram.newSpeed = ttrain.tram.newSpeed + 1
                msg2(id,misc.rgb(0,0,255).."Changed train speed to "..ttrain.tram.newSpeed)
            end
            ttrain.changeSpeed(id,ttrain.tram.newSpeed)
        end
    end
end
]]
addhook("say","ttrain.say")
function ttrain.say(id,text)
    if ttrain.player.trainControlling == id then
        if text == "!s" then
            if ttrain.tram.newSpeed > -1 then
                ttrain.tram.newSpeed = ttrain.tram.newSpeed - 1
                msg2(id,misc.rgb(0,0,255).."Changed train speed to "..ttrain.tram.newSpeed)
            end
            ttrain.changeSpeed(id,ttrain.tram.newSpeed)
            return 1
        end
        if text == "!f" then
            if ttrain.tram.newSpeed < 3 then
                ttrain.tram.newSpeed = ttrain.tram.newSpeed + 1
                msg2(id,misc.rgb(0,0,255).."Changed train speed to "..ttrain.tram.newSpeed)
            end
            ttrain.changeSpeed(id,ttrain.tram.newSpeed)
            return 1
        end
    else
        if ((text == "!s") or (text == "!f")) then
            msg2(id,misc.rgb(255,0,0).."you cant control the tram whitout pressing E while on it first")
        end
    end
end

ttrain.trainPos = 0

addhook("ms100","ttrain.ms100")
function ttrain.ms100()
		if (#ttrain.track == 0) then
			print("ERROR TRAIN TRACK NOT INITIALIZED")
			return
		end
    -- MOVE TRAIN POSITION --
    if ttrain.trainPos >= 1 then
        old_x = ttrain.getX(ttrain.trainPos)
        old_y = ttrain.getY(ttrain.trainPos)
        old_dir = ttrain.track[math.floor(ttrain.trainPos)].dir
        old_x = misc.tile_to_pixel(old_x)
        old_y = misc.tile_to_pixel(old_y)
    end
    ttrain.trainPos = ttrain.trainPos + ((1/3)*ttrain.tram.currentSpeed)
    --msg("Tram position: "..ttrain.trainPos)
    if ttrain.trainPos > #ttrain.track then
        ttrain.trainPos = 1
    end
    if ttrain.trainPos < 1 then
        ttrain.trainPos = #ttrain.track
    end
    trainx = misc.tile_to_pixel(ttrain.getX(ttrain.trainPos))--misc.tile_to_pixel(ttrain.track[ttrain.trainPos].x)
    trainy = misc.tile_to_pixel(ttrain.getY(ttrain.trainPos))--misc.tile_to_pixel(ttrain.track[ttrain.trainPos].y)
    --msg("position: "..ttrain.trainPos.." > x: "..misc.round(trainx)..", y: "..misc.round(trainy))
    -- MOVE PLAYER POSITION --
    pl = player(0,"tableliving")
    i = 0
    while (i < #pl) do
        i = i + 1
        local id = pl[i]
        px = player(id,"x")
        py = player(id,"y")
        if (not(old_dir == nil)) then
            dist = misc.point_distance(old_x,old_y,px,py)
            dir = misc.point_direction(old_x,old_y,px,py)
            if (ttrain.isPlayerOn(id)) then
                ttrain.onTrain[id] = true
                dir = dir + (ttrain.track[math.floor(ttrain.trainPos)].dir-old_dir)
                local x = trainx+misc.lengthdir_x(dir,dist)
                local y = trainy+misc.lengthdir_y(dir,dist)
                --[ BEST SOLUTION ]--
                if (spo ~= nil) then
                    spo.setpos(id,x,y)
                else
                    if ( (cs2d ~= nil) and (cs2d.setpos ~= nil) ) then
                        cs2d.setpos.call(id,x,y)
                    else
                        if (server_setpos ~= nil) then
                            --[ PRIVATE SOLUTION ]--
                            server_setpos(id,x,y)
                        else
                            --[ WORST SOLUTION ]--
                            print("ttrain: warning: using a deprecated setpos technique")
                            parse("setpos "..id.." "..x.." "..x)
                        end
                    end
                end
            else
                if ttrain.player.trainControlling == pl[i] then
                    msg2(pl[i],misc.rgb(255,0,0).."You lost the train controls!")
                    freeimage(ttrain.player.controlImage)
                    ttrain.player.controlImage = nil
                    ttrain.player.trainControlling = 0
                end
                ttrain.onTrain[pl[i]] = false
            end
        end
    end
    -- TWEEN MOVE TRAIN --
    ttrain.tram.speedCount = ttrain.tram.speedCount - 1
   if (ttrain.tram.speedCount <= 0) then
        ttrain.tram.currentSpeed = ttrain.tram.newSpeed*0.5
        ttrain.tram.speedCount = 4-ttrain.tram.currentSpeed
    else
        return 1
    end
    if ttrain.tram.currentSpeed == 0 then -- why move when theres no movement

    else
        --if ttrain.tram.currentSpeed > 0 then
      		ttrain.image:tween_move(ttrain.tram.speedCount*100,trainx,trainy,ttrain.track[math.floor(ttrain.trainPos)].dir)
        --end
    end
    --misc.debugImage(misc.tile_to_pixel(ttrain.track[ms100pos].x),misc.tile_to_pixel(ttrain.track[ms100pos].y),255,0,0,300)
end

function ttrain.getX(pos)
    local mpos = (pos % 1)
    local floorPos = math.floor(pos)
    local position = ttrain.track[floorPos]
    tilePos = position.x+misc.lengthdir_x(position.dir,mpos)
    return tilePos--misc.tile_to_pixel(tilePos)
end

function ttrain.getY(pos)
    local mpos = (pos % 1)
    local floorPos = math.floor(pos)
    local position = ttrain.track[floorPos]
    tilePos = position.y+misc.lengthdir_y(position.dir,mpos)
    return tilePos--misc.tile_to_pixel(tilePos)
end

function ttrain.isPlayerOn(id)
    movePos = 64
    if dist < movePos then
        return true
    else
        movePos = movePos + ((32/3)*ttrain.tram.currentSpeed)
        if dist < movePos then
            dir_displacement = math.abs(dir-ttrain.track[math.floor(ttrain.trainPos)].dir)
            --msg("direction displacemet: "..dir_displacement)
            if dir_displacement <= 40 then
                --dmg = misc.round(math.pow(ttrain.tram.currentSpeed,5),2)
                --msg("dealt "..dmg.." damage to "..player(id,"name"))
                --parse("sethealth "..id.." "..player(id,"health")-dmg)
            end
        else
        return false
        end
    end
end

ttrain.cache.isTrack = {}
function ttrain.isTrack(x,y)
    tile_frame = ttrain.cache.isTrack[x.."|"..y]
    if tile_frame == nil then
        tile_frame = tile(x,y,"frame")
        ttrain.cache.isTrack[x.."|"..y] = tile_frame
    end
    i = 0
    while (i < #ttrain.tile) do
        i = i + 1
        if ttrain.tile[i] == tile_frame then
            return true
        end
    end
    return false
end

if map("name") == "Magic Wizards Happy Town" then
    ttrain.init(10,182,90)
end
if map("name") == "de_dust_track" then
    ttrain.init(30,7,90)
end
if map("name") == "track" then
    ttrain.init(10,6,90)
end

ttrain.cache.isTrack = nil --clearing cache
