spo = {}
spo.player = {}

addhook("join","spo.join")
function spo.join(id)
    spo.player = {x=0,y=0}
end

function spo.setpos(id,x,y)
    x_offset = x % 1
    y_offset = y % 1
    x = math.floor(x)
    y = math.floor(y)
    --msg("added offset: x: "..x_offset.." y: "..x_offset)
    spo.player.x = spo.player.x + x_offset
    spo.player.y = spo.player.y + y_offset
    if (spo.player.x >= 1) then
        spo.player.x = spo.player.x - 1
        x = x + 1
    end
    if (spo.player.y >= 1) then
        spo.player.y = spo.player.y - 1
        y = y + 1
    end
    parse("setpos "..id.." "..x.." "..y)
end
