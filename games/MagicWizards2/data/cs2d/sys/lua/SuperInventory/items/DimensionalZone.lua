superInventory.itemType.new({
	id="DIMENSIONAL_ZONE_TOGGLE",
	name="Orb of Dimensional shifting",
	require=DimensionalZone,
	onUse=function(id)
		if (DimensionalZone.isOn(id)) then
			DimensionalZone.stop(id)
		else
			DimensionalZone.start(id)
		end
		return false
	end
	})

superInventory.itemType.new({
	id="ONE_DIMENSIONAL_ZONE_TOGGLE",
	name="Limited orb of Dimensional shift",
	require=DimensionalZone,
	onUse=function(id)
		if (DimensionalZone.isOn(id) == false) then
			DimensionalZone.start(id)
			for i=39,49 do
				timer2(i*1000,{id},function(id)
					msg2(id,"Your dimensional shift is about to collapse! ("..(50-i).." seconds")
				end)
			end
			timer2(50000,{id},function(id)
				DimensionalZone.stop(id)
			end)
			msg2(id,misc.rgb(255,0,0).."The orb breaks and fades away the little energy in it is used up")
			return true
		end
	end
	})