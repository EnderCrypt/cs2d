superInventory.itemType.new({
	id="MW2_BOT_TRAINING_INF",
	name="Orb of the Bot challange",
	require=mw2BotTraining,
	onUse=function(id)
		if (mw2BotTraining.start(id)) then
			msg2(id,misc.rgb(0,255,0).."Good luck!")
		else
			msg2(id,misc.rgb(255,0,0).."All bots are currently busy")
		end
		return false
	end
	})

superInventory.itemType.new({
	id="MW2_BOT_TRAINING",
	name="Bot challanging card",
	require=mw2BotTraining,
	onUse=function(id)
		if (mw2BotTraining.start(id)) then
			msg2(id,misc.rgb(0,255,0).." Good luck!")
			return true
		else
			msg2(id,misc.rgb(255,0,0)..", All bots are currently busy")
			return false
		end
	end
	})

for i=5,50,5 do
	superInventory.itemType.new({
	id="MW2_BOT_BADGE_"..i,
	name="Bot Challange Badge "..i,
	require=mw2BotTraining,
	onUse=function(id)
		msg(misc.rgb(0,100,255)..player(id,"name").." Shows of their killing badge:")
		msg(misc.rgb(0,100,255).."Has reached "..i.." kills in a row in challange mode")
		return
	end
	})
end