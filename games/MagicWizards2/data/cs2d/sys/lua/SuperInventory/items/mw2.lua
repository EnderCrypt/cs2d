superInventory.itemType.new({
	id="MW2_INSTRUCTIONS",
	name="Magic Wizards 2 instructions",
	require=mw2,
	onUse=function(id)
		local worker = LuaWorker.new(function(worker,workspace)
			workspace.index = workspace.index + 1
			if (workspace.index > #workspace.msg) then
				worker:delete()
				return
			end
			msg2(workspace.id,misc.rgb(0,255,100).."[INSTR]"..worker.workspace.msg[workspace.index])
		end)
		worker.workspace.msg = {
		"Press F2 for full magic menu",
		"Press F3 for quick select magic menu",
		"Press F4 to buy scrolls",
		"You need multiple scrolls to unlock new spells",
		"You can find money and scrolls randomly spawning on the map"
		}
		worker.workspace.id = id
		worker.workspace.index = 0
		worker:setFrequency(LuaWorkerData.ms100*5)
		worker:start()
	    return false
	end
	})

superInventory.itemType.new({
	id="ICE_STORM_MINUTE",
	name="ice storm shard",
	require=mw2flag,
	onUse=function(id)
		playerMenuSelect.open(id,player(0,"tableliving"),function(id,target)
			mw2flag.new(target,id,mw2flags.library.iceStorm,60)
	    	msg2(id,misc.rgb(0,255,0).."You invoke the ice crystal into "..player(target,"name"))
	    	msg3(id,misc.rgb(255,0,0)..player(id,"name").." invoked an ice storm onto "..player(target,"name"))
		end)
		return true
	end
	})

superInventory.itemType.new({
	id="FIRE_STORM_MINUTE",
	name="fire storm shard",
	require=mw2flag,
	onUse=function(id)
		playerMenuSelect.open(id,player(0,"tableliving"),function(id,target)
			mw2flag.new(target,id,mw2flags.library.fireStorm,60)
	    	msg2(id,misc.rgb(0,255,0).."You invoke the fire crystal into "..player(target,"name"))
	    	msg3(id,misc.rgb(255,0,0)..player(id,"name").." invoked an fire storm onto "..player(target,"name"))
		end)
		return true
	end
	})

superInventory.itemType.new({
	id="IMMORTAL_MINUTE",
	name="Shard of immortality",
	require=mw2flag,
	onUse=function(id)
		mw2flag.new(id,0,mw2flags.library.immortal,60)
		return true
	end
	})

superInventory.itemType.new({
	id="STORM_CLOUD",
	name="storm shard",
	require=mw2,
	onUse=function(id)
		local x = player(id,"x")
		local y = player(id,"y")
		local dir = 0
		while (dir < 360) do
			dir = dir + 45
			mw2.spawnProjectile(mw2.name.CloudJumper,x,y,id,dir)
		end
		return true
	end
	})

superInventory.itemType.new({
	id="SCROLL",
	name="Scroll",
	require=mw2,
	onUse=function(id)
		mw2.items.spawn(mw2.items.names.scroll.id,player(id,"tilex"),player(id,"tiley"))
		return true
	end
	})

superInventory.itemType.new({
	id="SCROLL_DISPENSER",
	name="Scroll dispenser",
	require=mw2,
	onUse=function(id)
		if (mw2.items.spawn(mw2.items.names.scroll.id,player(id,"tilex"),player(id,"tiley")) == false) then
			msg2(id,misc.rgb(255,0,0).."The dispenser failed to dispenses a scroll...")
			return false
		end
		if (math.random() < 0.25) then
			msg2(id,misc.rgb(255,0,0).."The dispenser dispenses its last scroll..")
			return true
		else
			msg2(id,misc.rgb(0,255,0).."The dispenser seems to have more scrolls in it!")
			return false
		end
	end
	})

superInventory.itemType.new({
	id="LUCKY_BADGE_JACKPOT",
	name="Lucky badge (Jackpot)",
	require=false,
	onUse=function(id)
		msg(misc.rgb(0,255,0)..player(id,"name").." shows of their lucky badge (won from a jackpot!)")
		msg2(id,misc.rgb(0,255,0).."The badge shines, and you feel a healing glow surround you...")
		return false
	end
	})

superInventory.itemType.new({
	id="LOTTERY_TICKET",
	name="Lottery ticket",
	require=mw2,
	onUse=function(id)
		local money = 0
		if (math.random() < 0.5) then -- 50%
			if ( (math.random() < 0.02) and (player(id,"usgn") > 0) ) then -- 2%
				money = 5000
				msg(misc.rgb(255,0,0)..player(id,"name").." won the JACKPOT! (5000 MW2)")
				superInventory.get(id):add("LUCKY_BADGE_JACKPOT")
			else
				money = 100+(math.random()*150)
			end
		end
		money = misc.round(money)
		-- perform and display
		if (money == 0) then
			msg2(id,misc.rgb(255,0,0).."You won nothing...")
			msg3(id,misc.rgb(255,0,0)..player(id,"name").." won nothing on the lottery...")
		else
			msg2(id,misc.rgb(0,255,0).."You won "..money.." money! :D")
			msg3(id,misc.rgb(0,255,0)..player(id,"name").." won "..money.." on the lottery!")
			while (money > 0) do
				local x = player(id,"tilex")
		        local y = player(id,"tiley")
		        local toSpawnItem = nil
		        if (money > 500) then
		            money = money - 100
		            toSpawnItem = mw2.items.names.gold
		        else
			        if (money > 200) then
			            money = money - 50
			            toSpawnItem = mw2.items.names.silver
			        else
				        if (money > 10) then
				            money = money - 10
				            toSpawnItem = mw2.items.names.copper10
				        else
				            money = money - 1
				            toSpawnItem = mw2.items.names.copper
				        end
			        end
			    end
        		for i=0,32 do
	                if (mw2.items.spawn(toSpawnItem.id,x,y)) then
	                    break;
	                else
	                    x = x + math.random(-1,1)
	                    y = y + math.random(-1,1)
	                end
	            end
		    end
		end
		return true
	end
	})

superInventory.itemType.new({
	id="AURA_BLAST",
	name="Aura Bomb",
	require=mw2ma,
	onUse=function(id)
		local p = mw2ma.getGridStrengthLocation(Point.new(player(id,"tilex"),player(id,"tiley")))
		if (mw2ma.auraAdd(p,100)) then
			msg2(id,misc.rgb(0,255,0).."The bomb went off, releasing massive amounts of aura!")
			msg3(id,misc.rgb(0,255,0)..player(id,"name").." set off an aura bomb")
			return true
		end
		msg2(id,misc.rgb(255,0,0).."The bomb failed to go off")
		return false
	end
	})