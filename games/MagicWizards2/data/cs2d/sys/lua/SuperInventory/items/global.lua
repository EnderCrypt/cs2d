superInventory.itemType.new({
	id="SLAP",
	name="Slap-Hand",
	require=false,
	onUse=function(id)
		playerMenuSelect.open(id,player(0,"tableliving"),function(id,target)
			msg2(id,misc.rgb(255,0,0).."The slap-hand slapped "..player(target,"name").." quite hard!")
			if (id == target) then
				msg(misc.rgb(255,0,0)..player(id,"name").." slapped themself and liked it :O")
			else
				msg(misc.rgb(255,0,0)..player(id,"name").." slapped "..player(target,"name"))
			end
			parse("slap "..target)
		end)
	    return true
	end
	})

superInventory.itemType.new({
	id="PERMA_SLAP",
	name="SLAP-A-TRON",
	require=false,
	onUse=function(id)
		superInventory.itemList["SLAP"].onUse(id)
	    return false
	end
	})

superInventory.itemType.new({
	id="KICK",
	name="kick card",
	require=false,
	onUse=function(id)
		playerMenuSelect.open(id,player(0,"tableliving"),function(id,target)
			msg2(id,misc.rgb(0,255,0).."The orb glows for a moment, and "..player(target,"name").." fades away...")
		    msg(misc.rgb(0,255,0)..player(target,"name").." was kicked by "..player(id,"name").." using a kick card")
		    parse("kick "..target.." \"Kicked by "..player(id,"name").."\"")
		end)
		return true
	end
	})

superInventory.itemType.new({
	id="KILL",
	name="Kill Card",
	require=false,
	onUse=function(id)
		playerMenuSelect.open(id,player(0,"tableliving"),function(id,target)
			msg2(id,misc.rgb(255,0,0).."The kill card killed "..player(target,"name").."!")
			msg(misc.rgb(255,0,0)..player(id,"name").." killed "..player(target,"name"))
			parse("customkill "..id.." \"Kill Card\" "..target)
		end)
	    return true
	end
	})

superInventory.itemType.new({
	id="PERMA_KICK",
	name="Orb of kicking",
	require=false,
	onUse=function(id)
		superInventory.itemList["KICK"].onUse(id)
	    return false
	end
	})

superInventory.itemType.new({
	id="BAN_IP",
	name="Orb of Banishment/Banning",
	require=false,
	onUse=function(id)
		playerMenuSelect.open(id,player(0,"tableliving"),function(id,target)
			msg2(id,misc.rgb(0,255,0).."The orb glows for a moment, and "..player(target,"name").." fades away forever...")
			msg(misc.rgb(0,255,0)..player(target,"name").." was banned by "..player(id,"name").." using an Orb of banishment")
		    parse("banip "..player(target,"ip").." 0 \"Banned by "..player(id,"name").."\"")
		end)
	    return false
	end
	})

superInventory.itemType.new({
	id="USGN",
	name="U.S.G.N announcer",
	require=false,
	onUse=function(id)
		msg(misc.rgb(0,255,255)..player(id,"name").." Announces their USGN: "..misc.rgb(0,255,155)..player(id,"usgn"))
	    return false
	end
	})

superInventory.itemType.new({
	id="FRIEND_OF_ENDERCRYPT",
	name="EnderCrypt's friendship badge",
	require=false,
	onUse=function(id)
		msg(misc.rgb(0,255,0)..player(id,"name").." is an official friend of EnderCrypt!")
	    return false
	end
	})

superInventory.itemType.new({
	id="POOP",
	name="Pile of poop",
	require=false,
	onUse=function(id)
		msg2(id,misc.rgb(255,0,0).."this poop is cleaner than you, "..misc.rgb(255,0,0).."cheater!")
		msg3(id,misc.rgb(165,42,42)..player(id,"name").." is a horrible cheater and has poo in their inventory")
	    return false
	end
	})


superInventory.itemType.new({
	id="PLAYER_LOCATOR",
	name="Player GPS Locator",
	require=false,
	onUse=function(id)
		playerMenuSelect.open(id,player(0,"tableliving"),function(id,target)
			local dir = misc.point_direction(player(id,"x"),player(id,"y"),player(target,"x"),player(target,"y"))
			local imageID = image("gfx/hud_arrow.bmp",0,0,2,id)
			imagepos(imageID,320,240,dir)
			tween_move(imageID,2000,320+misc.lengthdir_x(dir,200),240+misc.lengthdir_y(dir,200),dir)
			tween_alpha(imageID,2000,0)
			timer2(2000,{imageID},function(imageID)
				freeimage(imageID)
			end)
		end)
		return false
	end
	})

superInventory.itemType.new({
	id="SATCHEL_CHARGE_360",
	name="Satchel Charge 360 throw",
	require=false,
	onUse=function(id)
		local LTW = LuaWorker.new(function(worker,workspace,arguments)
	        local x = player(workspace.id,"x")
	        local y = player(workspace.id,"y")
	        local dir = workspace.dir+(workspace.index*10)
	        parse("spawnprojectile "..workspace.id.." 89 "..x.." "..y.." 250 "..dir)
	        workspace.index = workspace.index + 1
	        if (workspace.index >= 36) then
	        	parse("equip "..workspace.id.." 89")
	        	parse("setweapon "..workspace.id.." 89")
	        	worker:delete()
	        end
		end)
		LTW.workspace.id = id
		LTW.workspace.index = 0
		LTW.workspace.dir = math.random()*360
		LTW:setFrequency(LuaWorkerData.always)
		LTW:start()
		return true
	end
	})

superInventory.itemType.new({
	id="SUPER_SATCHEL_CHARGE",
	name="Super Satchel Charge",
	require=false,
	onUse=function(id)
		local LTW = LuaWorker.new(function(worker,workspace,arguments)
	        local x = player(workspace.id,"x")
	        local y = player(workspace.id,"y")
	        parse("spawnprojectile "..workspace.id.." 89 "..x.." "..y.." "..workspace.dist.." "..workspace.dir)
	        workspace.dir = workspace.dir + 20
	        if (workspace.dir > 360) then
	        	workspace.dir = workspace.dir - 360
	        end
	        workspace.dist = workspace.dist + 1.5
        	if (workspace.dist > 256) then
	        	parse("equip "..workspace.id.." 89")
	        	parse("setweapon "..workspace.id.." 89")
	        	worker:delete()
	        end
		end)
		LTW.workspace.id = id
		LTW.workspace.dir = math.random()*360
		LTW.workspace.dist = 1
		LTW:setFrequency(LuaWorkerData.always)
		LTW:start()
		return true
	end
	})

superInventory.itemType.new({
	id="AUTO_SNOWBALL",
	name="Magic Snowball's",
	require=false,
	onUse=function(id)
		local LTW = LuaWorker.new(function(worker,workspace,arguments)
		   	local px = player(workspace.id,"x")
		    local py = player(workspace.id,"y")
		    local dist = workspace.dist*math.min(1,workspace.left/100)
		    parse("spawnprojectile "..workspace.id.." 75 "..px.." "..py.." "..dist.." "..player(workspace.id,"rot"))
			reqcld2(workspace.id,2,function(id,x,y,workspace)
			   	local px = player(workspace.id,"x")
			    local py = player(workspace.id,"y")
		    	workspace.dist = misc.point_distance(px,py,x,y)
			end,workspace)

	        workspace.left = workspace.left - 1
	        if (workspace.left <= 0) then
	        	worker:delete()
	        end
		end)
		LTW.workspace.id = id
		LTW.workspace.left = 500
		LTW.workspace.dist = 100
		LTW:setFrequency(LuaWorkerData.always)
		LTW:start()
		return true
	end
	})


superInventory.itemType.new({
	id="TILE_IDENTIFIER",
	name="Laser pointer",
	require=false,
	onUse=function(id)
		--reqcld2(id,2,function(id,x,y)
		    local x = player(id,"x")--misc.pixel_to_tile(x)
		    local y = player(id,"y")--misc.pixel_to_tile(y)
		    local tx = misc.pixel_to_tile(x)
		    local ty = misc.pixel_to_tile(y)
		    msg2(id,"Laser Pointer: "..tx..", "..ty.." ("..x..","..y..")")
		--end)
		return false
	end
	})

superInventory.itemType.new({
	id="VOTEKICK_SHIELD",
	name="Votekick Shield",
	require=false,
	onUse=function(id)
		msg2(id,"This shield protects against votekicks")
		return false
	end
	})

superInventory.itemType.new({
	id="MAP_RELOAD",
	name="Universal Imploder",
	require=false,
	onUse=function(id)
		msg(misc.rgb(255,0,0)..player(id,"name").." has initialized universal destruction")
		msg(misc.rgb(255,0,0).."The universe will implode and re-form in 5 seconds")
		timer2(5000,{},function()
			parse("map "..map("name"))
		end)
		return false
	end
	})

superInventory.itemType.new({
	id="MAP_CORRUPT",
	name="Map corruption machine",
	require=false,
	onUse=function(id)
		local mapCworker = LuaWorker.new(function(worker,workspace,arguments)
	        for i=0,5 do
	            x = misc.round(math.random()*map("xsize"))
	            y = misc.round(math.random()*map("ysize"))
	            tile_id = math.random()*15--466
	            parse("settile "..x.." "..y.." "..math.ceil(tile_id))
	        end
	    end)
	    mapCworker:setTimeout(LuaWorkerData.minute)
		mapCworker:setFrequency(LuaWorkerData.ms100)
		mapCworker:start()
		msg(player(id,"name").." started corrupting the map")
	    return true
	end
	})

superInventory.itemType.new({
	id="MAP_REPAIR",
	name="Map Repair machine",
	require=false,
	onUse=function(id)
		local repairWorker = LuaWorker.new(function(worker,workspace,arguments)
	        i = 0
		        while (i < 20) do
		            i = i + 1
		            workspace.recovered = workspace.recovered + 1
		            if (workspace.recovered > workspace.map5percent) then
		                workspace.recovered = 0
		                workspace.percentage = workspace.percentage + 5
		                msg("repairing map... "..workspace.percentage.."%")
		            end
		            if (tile(workspace.x,workspace.y,"hascustomframe")) then
		                originalTile = tile(workspace.x,workspace.y,"originalframe")
		                parse("settile "..workspace.x.." "..workspace.y.." "..originalTile)
		            end
		            workspace.x = workspace.x + 1
		            if (workspace.x > map("xsize")) then
		                workspace.x = 0
		                workspace.y = workspace.y + 1
		                if (workspace.y > map("ysize")) then
		                    worker:delete()
		                    msg("DONE REPAIRING MAP")
		                end
		            end
		        end
		    end)
		repairWorker.workspace.x = 0
		repairWorker.workspace.y = 0
		repairWorker.workspace.percentage = 0
		repairWorker.workspace.recovered = 0
		repairWorker.workspace.map5percent = map("xsize")*map("ysize")/100*5
		repairWorker:setFrequency(LuaWorkerData.always)
		repairWorker:start()
		msg(player(id,"name").." started repairing the map")
		
	    return false
	end
	})

superInventory.itemType.new({
	id="CHRISTMAS_PLAYER",
	name="Christmas Token",
	require=false,
	onUse=function(id)
		msg(misc.rgb(0,255,0)..player(id,"name").." Was in the Magic Wizards 2 Christmas Event!")
	    return false
	end
	})
