superInventory.itemType.new({
	id="SCP_96",
	name="scp-96 spawn card",
	require=scp,
	onUse=function(id)
		msg2(id,misc.rgb(255,0,0).."scp-96 will spawn on your current location in 3 seconds")
		timer2(3000,{player(id,"tilex"),player(id,"tiley")},function(x,y)
			scp.spawn(96,x,y)
		end)
		return true
	end
	})

superInventory.itemType.new({
	id="SCP_106",
	name="scp-106 spawn card",
	require=scp,
	onUse=function(id)
		msg2(id,misc.rgb(255,0,0).."scp-106 will spawn on your current location in 3 seconds")
		timer2(3000,{player(id,"tilex"),player(id,"tiley")},function(x,y)
			scp.spawn(106,x,y)
		end)
		return true
	end
	})

superInventory.itemType.new({
	id="SCP_173",
	name="scp-173 spawn card",
	require=scp,
	onUse=function(id)
		msg2(id,misc.rgb(255,0,0).."scp-173 will spawn on your current location in 3 seconds")
		timer2(3000,{player(id,"tilex"),player(id,"tiley")},function(x,y)
			scp.spawn(173,x,y)
		end)
		return true
	end
	})

superInventory.itemType.new({
	id="SCP_BANISH",
	name="scp banishing card",
	require=scp,
	onUse=function(id)
		local clostest_i = 0
		local closest_dist = 300--math.huge
		local x = player(id,"x")
		local y = player(id,"y")

	    local i = 0
	    while (i < #scp.entities) do
	        i = i + 1
	        local entity = scp.entities[i]
	        local scpID = entity.scpID
	        local dist = misc.point_distance(entity.x,entity.y,x,y)
	        if (dist <= closest_dist) then
	        	clostest_i = i
	        	closest_dist = dist
	        end
	    end

	    if (clostest_i == 0) then
	    	msg2(id,misc.rgb(255,0,0).."too far away from any scp")
	    	return false
	    else
	    	local entity = scp.entities[clostest_i]
	    	entity.destroy = true
	    	msg2(id,misc.rgb(0,255,0).."You banished the closest SCP (SCP-"..entity.scpID..")")
	    	return true
	    end
	end
	})

superInventory.itemType.new({
	id="SCP_BANISH_INF",
	name="scp banishing orb",
	require=scp,
	onUse=function(id)
		local clostest_i = 0
		local closest_dist = 300--math.huge
		local x = player(id,"x")
		local y = player(id,"y")

	    local i = 0
	    while (i < #scp.entities) do
	        i = i + 1
	        local entity = scp.entities[i]
	        local scpID = entity.scpID
	        local dist = misc.point_distance(entity.x,entity.y,x,y)
	        if (dist <= closest_dist) then
	        	clostest_i = i
	        	closest_dist = dist
	        end
	    end

	    if (clostest_i == 0) then
	    	msg2(id,misc.rgb(255,0,0).."too far away from any scp")
	    else
	    	local entity = scp.entities[clostest_i]
	    	entity.destroy = true
	    	msg2(id,misc.rgb(0,255,0).."You banished the closest SCP (SCP-"..entity.scpID..")")
	    end
	    return false
	end
	})