superInventory.itemType.new({
	id="SPEEDHACK_DOMINATOR_TOGGLE",
	name="Anti-Speedhack remote",
	require=speedDominator,
	onUse=function(id)
		local state = "unknown"
		if (speedDominator.enabled) then
			state = "disabled"
			speedDominator.disable()
		else
			state = "enabled"
			speedDominator.enable()
		end
		for _,pid in ipairs(player(0,"table")) do
			local inv = superInventory.get(pid)
			if (inv:contains("SPEEDHACK_DOMINATOR_TOGGLE")) then
				msg2(pid,player(id,"name").." "..state.." the anti-speedhack")
			end
		end
		return false
	end
	})

superInventory.itemType.new({
	id="SPEEDHACK_IMMUNE",
	name="Anti-Speedhack immunizer",
	require=speedDominator,
	onUse=function(id)
		return false
	end
	})