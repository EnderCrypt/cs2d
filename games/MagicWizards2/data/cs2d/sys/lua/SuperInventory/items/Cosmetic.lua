local jointGraphics = "gfx/Magic Wizard 2/inventory/xXxWeedSmoke360420666xXx.png"
misc.addServerTransfer(jointGraphics)

superInventory.itemType.new({
	id="JOINT",
	name="Joint",
	require=false,
	onUse=function(id)
		local worker = LuaWorker.new(function(worker,workspace)
			workspace.tick = workspace.tick + 1
			if (workspace.tick > 2*420) or (player(id,"exists") == false) then
				freeimage(workspace.image)
				worker:delete()
				return
			end
			local dir = player(id, "rot")
			local x = player(id, "x")
			local y = player(id, "y")
			x = x + misc.lengthdir_x(dir, 10)
			y = y + misc.lengthdir_y(dir, 10)
			misc.effect("smoke",x,y,3,1,0,0,0)
		end)
		worker.workspace.tick = 0
		worker.workspace.image = image(jointGraphics, 1, 0, 200+id)
		worker:setFrequency(LuaWorkerData.ms100*5)
		worker:start()
		msg(misc.rgb(100,255,100)..player(id,"name").." lights a joint, savage!")
		return true
	end
	})
