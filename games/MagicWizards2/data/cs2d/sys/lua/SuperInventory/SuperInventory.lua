superInventory = {}
superInventory.player = {}

dofile("sys/lua/SuperInventory/Classes/ItemType.class.lua")
dofile("sys/lua/SuperInventory/Classes/Inventory.class.lua")

function superInventory.reloadItemTypes()
	superInventory.itemList = {}
	dofile("sys/lua/SuperInventory/Globals.lua")
end
superInventory.reloadItemTypes()

superInventory.hooks = {}
addhook("join","superInventory.hooks.join")
function superInventory.hooks.join(id)
	superInventory.player[id] = superInventory.inventory.new(id)
end

addhook("say","superInventory.hooks.say")
function superInventory.hooks.say(id,text)
	texts = misc.split_text(text)
	if (texts[1] == "!inventory") then
		if (texts[2] == nil) then
			if (player(id,"health") > 0) then
				superInventory.open(id)
			end
			return 0
		end
		local target = tonumber(texts[2])
		if (target ~= nil) then
			if (player(target,"exists")) then
				msg2(id,misc.rgb(0,150,100).."-- {"..player(target,"name").."'s inventory} --")
				for _,itemId in ipairs(superInventory.player[target].inventory) do
					msg2(id,misc.rgb(0,150,100)..superInventory.processButton(nil,itemId))
				end
			end
			return 0
		end
	end
end

addhook("vote","superInventory.hooks.vote")
function superInventory.hooks.vote(id,mode,param)
    if (mode == 1) then
        local target = tonumber(param)
        local inventory = superInventory.get(target)
        if (inventory:contains("VOTEKICK_SHIELD")) then
            msg2(target,misc.rgb(255,0,0).."you where protected from votekick by "..player(id,"name").." by the votekick-shield")
            parse("kick "..id.." \"Voting protected player\"")
        end
    end
end

function superInventory.printItems()
	for id,data in pairs(superInventory.itemList) do
		print(id.." = "..data.name)
	end
end

function superInventory.get(id)
	return superInventory.player[id]
end

function superInventory.open(id)
	sme.createMenu(id,superInventory.clickItem,superInventory.processButton,"Global Inventory",true,superInventory.player[id].inventory,true)
end

function superInventory.processButton(id,data)
	local itemData = superInventory.itemList[data]
	if (itemData == nil) then
		return "(UNKNOWN ITEM: "..data..")"
	end
	if ((itemData.require == false) == false) then
		if (itemData.require == nil) then
			return "(MISSING LUA: "..data..")"
		end
	end
	return itemData.name
end

function superInventory.clickItem(id,itemIndex)
	if (player(id,"health") <= 0) then
		return false
	end
	local itemId = superInventory.player[id]:get(itemIndex)
	if (itemId == nil) then
		return false -- clicked outside inventory array
	end
	local itemData = superInventory.itemList[itemId]
	if (itemData == nil) then
		return false -- item id whitout corresponding data (should need cancelled out)
	end
	msg2(id,misc.rgb(0,100,255)..itemData.name.." was activated")
	local doRemove = itemData.onUse(id)
	if (doRemove == true) then
		msg2(id,misc.rgb(100,0,255)..itemData.name.." faded away...")
		superInventory.player[id]:remove(itemId)
	end
end

function giveItem(id,item,onlyIfDontHave)
	if (onlyIfDontHave == nil) then
		onlyIfDontHave = false
	end
	local inventory = superInventory.get(id)
	if ( ((onlyIfDontHave) and (inventory:contains(item))) == false) then
	    local result = inventory:add(item)
	    if (result == false) then
	        print("Failed to give "..tostring(item).." to "..player(id,"name"))
	    end
	end
end

function giveAll(item, onlyIfDontHave)
    print("Giving all players "..item)
    local pl = player(0,"tableliving")
    for _,id in ipairs(pl) do
        giveItem(id, item, onlyIfDontHave)
    end
end

-- POOP
local poopWorker = LuaWorker.new(function(worker,workspace)
	for _,id in ipairs(player(0,"tableliving")) do
		if (superInventory.player[id]:contains("POOP")) then
			-- message
			msg2(id,misc.rgb(165,42,42).."Something smells in your inventory... disgusting...")
			-- smoke
			misc.effect("colorsmoke",player(id,"x"),player(id,"y"),8,8,165,42,42)
			-- shake
			if (math.random() <= 0.25) then
				LTW = LuaWorker.new(function(worker,workspace)
					if (player(workspace.id,"exists") == false) then
						worker:delete()
						return
					end
			        local range = 2
			        local x = player(workspace.id,"x")
			        local y = player(workspace.id,"y")
			        x = x + (math.random()*(range*2))-range
			        y = y + (math.random()*(range*2))-range
			        cs2d.setpos.call(id,x,y)
			        if (math.random() < 0.05) then
			        	worker:delete()
			        end
			    end)
			    LTW.workspace.id = id
				LTW:setFrequency(LuaWorkerData.always)
				LTW:start()
		end
			-- money
			if (mw2 ~= nil) then
				local lose = misc.round(math.random()*2.5,1)
				mw2.player.money[id] = math.max(mw2.player.money[id]-lose,0)
				--mw2.drawMoney(id)
				mw2.playerSpeed(id,-3)
			end
		end
	end
end)
poopWorker:setFrequency(LuaWorkerData.second*2)
poopWorker:start()

-- GIVEAWAY
if (string.lower(map("name")) == "magic wizards happy town") then
	parse("mp_idleaction 1")
	local worker = LuaWorker.new(function(worker,workspace)
		msg(misc.rgb(0,255,0).."random giveaway!")
		local lotteryChance = 1
		while (math.random() < lotteryChance) do
			giveAll("LOTTERY_TICKET")
			lotteryChance = lotteryChance * 0.5
		end
		if (math.random() < 0.5) then
			giveAll("MW2_BOT_TRAINING")
		end
		if (math.random() < 0.5) then
			giveAll("JOINT")
		end
		if (math.random() < 0.25) then
			giveAll("ONE_DIMENSIONAL_ZONE_TOGGLE")
		end
		if (math.random() < 0.75) then
			giveAll("SLAP")
		end
		if (math.random() < 0.25) then
			giveAll("ICE_STORM_MINUTE")
		end
		if (math.random() < 0.25) then
			giveAll("FIRE_STORM_MINUTE")
		end
		if (math.random() < 0.1) then
			giveAll("IMMORTAL_MINUTE")
		end
		if (math.random() < 0.5) then
			giveAll("SCROLL")
		end
		if (math.random() < 0.25) then
			giveAll("STORM_CLOUD")
		end
	end)
	worker:setFrequency(LuaWorkerData.hour)
	worker:start()
end

-- LUCKY BADGE MONEY
if (string.lower(map("name")) == "magic wizards happy town") then
	local worker = LuaWorker.new(function(worker,workspace)
		for _,id in ipairs(player(0,"tableliving")) do
			local inv = superInventory.get(id)
			if (math.random() < 0.2) and (inv:contains("LUCKY_BADGE_JACKPOT")) then
				msg2(id,misc.rgb(0,255,0).."Your lucky badge starts vibrating...")
				timer2(1000,{id},function(id)
					msg2(id,misc.rgb(0,255,0).."Suddenly a rush of money is magically released from it!")
					local worker = LuaWorker.new(function(worker,workspace)
						workspace.money = workspace.money - 1
						if (workspace.money <= 0) or (player(id,"exists") == false) or (player(id,"health") <= 0) then
							worker:delete()
							return
						end
						mw2.items.spawnFloating(mw2.items.names.copper.id,player(id,"tilex"),player(id,"tiley"),16)
					end)
					worker.workspace.money = 32+(math.random()*32)
					worker:setFrequency(LuaWorkerData.ms100)
					worker:start()
				end)
			end
		end
	end)
	worker:setFrequency(LuaWorkerData.minute)
	worker:start()
end
