superInventory.inventory = {}

Class(superInventory.inventory,function(id)
	self.id = id
	self.usgn = player(id,"usgn")
	self.inventory = {}
	-- init
	self:load()
end)

function superInventory.inventory:add(item_id)
	if (superInventory.itemList[item_id] == nil) then
		return false
	end
	self.inventory[#self.inventory+1] = item_id
	self:save()
	msg2(self.id,misc.rgb(0,255,0).."A "..superInventory.itemList[item_id].name.." appered in your !inventory")
	return true
end

function superInventory.inventory:get(itemIndex)
	return self.inventory[itemIndex]
end

function superInventory.inventory:contains(itemIndex)
	local i = 0
	while (i < #self.inventory) do
		i = i + 1
		local item = self.inventory[i]
		if (item == itemIndex) then
			return true
		end
	end
	return false
end

function superInventory.inventory:remove(item_id)
	local i = 0
	while (i < #self.inventory) do
		i = i + 1
		if (self.inventory[i] == item_id) then
			self.inventory[i] = self.inventory[#self.inventory]
			self.inventory[#self.inventory] = nil
			self:save()
			return true
		end
	end
	return false
end

function superInventory.inventory:save()
	if (self.usgn > 0) then
		local saveFile = "sys/lua/SuperInventory/saves/usgn_"..self.usgn..".txt"
		if misc.fileExists(saveFile) then
			os.remove(saveFile)
		end
		local file = io.open(saveFile,"w")
		for index,item in ipairs(self.inventory) do
			file:write(item.."\n")
		end
		file:close()
	end
end

function superInventory.inventory:load()
	if (self.usgn > 0) then
		local saveFile = "sys/lua/SuperInventory/saves/usgn_"..self.usgn..".txt"
		if misc.fileExists(saveFile) then
			self.inventory = {}
			local file = io.open(saveFile,"r")
			local item = file:read()
			while ((item ~= nil) and (item ~= "")) do
				self.inventory[#self.inventory+1] = item
				item = file:read()
			end
			file:close()
		end
	end
	if (player(self.id,"bot") == false) then
		if (string.lower(map("name")) == "magic wizards happy town") then
			self:giveFundamental("MW2_INSTRUCTIONS")
		end
		if (self.usgn > 0) then
			self:giveFundamental("USGN")
		end
	end
end

function superInventory.inventory:giveFundamental(itemID)
	if (self:contains(itemID) == false) then
		self:add(itemID)
	end
end
