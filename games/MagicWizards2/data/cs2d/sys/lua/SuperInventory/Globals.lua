superInventory.itemFolder = "sys/lua/SuperInventory/items/"

function superInventory.addItemFile(fileName)
	dofile(superInventory.itemFolder..fileName..".lua")
end

superInventory.addItemFile("global")
superInventory.addItemFile("scp")
superInventory.addItemFile("mw2")
superInventory.addItemFile("mw2Bot")
superInventory.addItemFile("DimensionalZone")
superInventory.addItemFile("GutDrone")
superInventory.addItemFile("Speedhack")
superInventory.addItemFile("Cosmetic")
superInventory.addItemFile("Football")
