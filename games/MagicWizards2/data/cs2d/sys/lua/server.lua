-- primary
dofile("sys/lua/misc/MiscLibrary.lua")
dofile("sys/lua/misc/AddHook2.lua")
dofile("sys/lua/misc/reqcld2.lua")

-- class
dofile("sys/lua/Class/main.lua")

-- secondary misc
dofile("sys/lua/misc/SaveEngineLinux.lua")
dofile("sys/lua/misc/ServerTransfer.lua")
dofile("sys/lua/misc/Cs2dConsole.lua")
dofile("sys/lua/misc/Timer2.lua")
dofile("sys/lua/misc/LuaWorker.lua")
dofile("sys/lua/misc/SimpleMenuEngine.lua")
dofile("sys/lua/misc/PlayerMenuSelect.lua")


-- magic wizard 2
dofile("sys/lua/MagicWizard2/MagicWizard2.lua")
dofile("sys/lua/MagicWizard2/mw2_extra.lua")
dofile("sys/lua/MagicWizard2/mw2_scrollDirections.lua")
dofile("sys/lua/MagicWizard2/mw2_autofire.lua")
dofile("sys/lua/MagicWizard2/mw2_magicAreas.lua")
dofile("sys/lua/MagicWizard2/mw2_bars.lua")
dofile("sys/lua/MagicWizard2/mw2_frost.lua")
dofile("sys/lua/MagicWizard2/mw2_smokeblink.lua")
dofile("sys/lua/MagicWizard2/mw2_bottraining.lua")

-- others
dofile("sys/lua/chatbot3/ChatBot3.lua")
dofile("sys/lua/chatbot3/ChatBot3Follower.lua")
dofile("sys/lua/SuperInventory/SuperInventory.lua")
dofile("sys/lua/misc/SpeedhackDominator.lua")
dofile("sys/lua/misc/OnlyOneUsgn.lua")
dofile("sys/lua/misc/Furniture.lua")
dofile("sys/lua/misc/DimensionalZone.lua")
dofile("sys/lua/tram/Tram.lua")
dofile("sys/lua/misc/SpecChat.lua")
dofile("sys/lua/scp/scp.lua")
dofile("sys/lua/scp/secrets.lua")

-- add bots
timer2(10000,{},function()
	print("Adding bots!")
	parse("bot_add_ct")
	parse("bot_add_t")
	parse("bot_add_ct")
	parse("bot_add_t")
end)

serverTransfer.finalize()
superInventory.reloadItemTypes()
misc.mp_hud(true,true,true,true,true,true,true)
