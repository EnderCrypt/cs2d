DimensionalZone = {}
DimensionalZone.floorTile = 0
DimensionalZone.wallTile = 119

DimensionalZone.modifiedTiles = {}

function DimensionalZone.start(id)
    if (DimensionalZone.modifiedTiles[id] == nil) then
        DimensionalZone.modifiedTiles[id] = {}
        DimensionalZone.spawnBox(id,player(id,"tilex"),player(id,"tiley"))
    end
end

function DimensionalZone.stop(id)
    if (DimensionalZone.modifiedTiles[id] ~= nil) then
        DimensionalZone.recoverTiles(id)
        DimensionalZone.modifiedTiles[id] = nil
    end
end

function DimensionalZone.isOn(id)
    return (DimensionalZone.modifiedTiles[id] ~= nil)
end

addhook("movetile","DimensionalZone.movetile")
function DimensionalZone.movetile(id,x,y)
    if (DimensionalZone.modifiedTiles[id] ~= nil) then
        DimensionalZone.removeUsedModifiedTiles(id,x,y)
        DimensionalZone.recoverTiles(id)
        DimensionalZone.modifiedTiles[id] = {}
        DimensionalZone.spawnBox(id,x,y)
    end
end

addhook("die","DimensionalZone.die")
function DimensionalZone.die(victim,killer,weapon,x,y)
    if (DimensionalZone.modifiedTiles[id] ~= nil) then
        DimensionalZone.recoverTiles(id)
        DimensionalZone.modifiedTiles[id] = {}
    end
end

addhook("leave","DimensionalZone.leave")
function DimensionalZone.leave(id,reason)
    if (DimensionalZone.modifiedTiles[id] ~= nil) then
        DimensionalZone.stop(id)
    end
end

function DimensionalZone.removeUsedModifiedTiles(id,x,y)
    local modified = DimensionalZone.modifiedTiles[id]
    local i = 0
    while (i < #modified) do
        i = i + 1
        local tx = modified[i].x
        local ty = modified[i].y
        if ((tx >= x-2) and (ty >= y-2) and (tx <= x+2) and (ty <= y+2)) then
            modified[i] = modified[#modified]
            modified[#modified] = nil
            i = i - 1
        end
    end
end

function DimensionalZone.recoverTiles(id)
    local modified = DimensionalZone.modifiedTiles[id]
    local i = 0
    while (i < #modified) do
        i = i + 1
        local x = modified[i].x
        local y = modified[i].y
        local originalTile = tile(x,y,"originalframe")
        parse("settile "..x.." "..y.." "..originalTile)
    end
end

function DimensionalZone.spawnBox(id,sx,sy)
    -- creates floor
    for x=sx-1,sx+1 do
        for y=sy-1,sy+1 do
            DimensionalZone.modifyTile(id,x,y,DimensionalZone.floorTile)
        end
    end
    -- creates top walls
    for x=sx-2,sx+2 do
        DimensionalZone.modifyTile(id,x,sy-2,DimensionalZone.wallTile)
        DimensionalZone.modifyTile(id,x,sy+2,DimensionalZone.wallTile)
    end
    -- creates walls on left/right
    for y=sy-1,sy+1 do
        DimensionalZone.modifyTile(id,sx-2,y,DimensionalZone.wallTile)
        DimensionalZone.modifyTile(id,sx+2,y,DimensionalZone.wallTile)
    end
end

function DimensionalZone.modifyTile(id,x,y,tile)
    if ((x >= 0) and (y >= 0) and (x < map("xsize")+1) and (y < map("ysize")+1)) then
        parse("settile "..x.." "..y.." "..tile)
        local modified = DimensionalZone.modifiedTiles[id]
        --[[
        local i = 0
        while (i < #modified) do
            i = i + 1
            local tx = modified[i].x
            local ty = modified[i].y
            if ((x == tx) and (y == ty)) then
                return 0
            end
        end
        ]]--
        modified[#modified+1] = {x=x,y=y}
    end
end