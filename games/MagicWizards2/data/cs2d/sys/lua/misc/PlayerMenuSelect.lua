playerMenuSelect = {}
playerMenuSelect.pending = {}

function playerMenuSelect.open(id,targets,callback_function)
	playerMenuSelect.pending[id] = callback_function
	sme.createMenu(id,playerMenuSelect.select,playerMenuSelect.processor,"Select player",true,targets,false)
end

function playerMenuSelect.processor(id,data)
	return player(data,"name").."|"..data
end

function playerMenuSelect.select(id,button,data)
	playerMenuSelect.pending[id](id,data) -- (id,target)
	playerMenuSelect.pending[id] = nil
end