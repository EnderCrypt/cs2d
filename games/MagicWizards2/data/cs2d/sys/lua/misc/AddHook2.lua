ah2 = {}
ah2.hooks = {}
ah2.hook = {}
ah2.hook_caller = {}
ah2.temp = {}
ah2.debugLog = false
ah2.logFile = "sys/lua/misc/AddHook_Log.txt"
ah2.DebugHooks = false

if (ah2.debugLog) then
	io.open(ah2.logFile,"w"):close() -- reset file
end

ah2.original_addhook = addhook
function addhook(hook,func,priority,override)
    if (override) then
        ah2.original_addhook(hook,func,priority)
    else
        if ah2.hooks[hook] == nil then
            ah2.hooks[hook] = {}
            ah2.original_addhook(hook,"ah2.hook."..hook)
        end
        --ah2.hooks[hook][#ah2.hooks[hook]+1] = func
        if (priority == nil) then
            priority = 0
        end
			ah2.log("Addhook2: adding function \""..func.."\" to hook \""..hook.."\"")
			ah2.log(" - priority: "..priority)
        -- 
        ah2.insertHookData(hook, func, priority)
        --
        print(misc.rgb(0,100,255).."Addhook2: adding function \""..func.."\" to hook "..hook)
        ah2.formHook(hook)
    end
end

function ah2.log(message)
	if (ah2.debugLog) then
		local file = io.open(ah2.logFile, "a")
		file:write(message.."\n")
		file:close()
	end
end

function ah2.insertHookData(hook, func, priority)
    local loc = 1
    local i = 0
    while (i < #ah2.hooks[hook]) do
        i = i + 1
        local hook_data = ah2.hooks[hook][i]
        if (priority <= hook_data[2]) then
            loc = i+1
        end
    end
    -- 
    ah2.log(" - position: "..loc.." out of "..#ah2.hooks[hook])
    --
    local i = #ah2.hooks[hook]+1
    while (i >= loc) do
        i = i - 1
        ah2.hooks[hook][i+1] = ah2.hooks[hook][i]
    end
    --print("inserting at position "..loc.." out of "..#ah2.hooks[hook])
    ah2.hooks[hook][loc] = {func,priority}
end

function ah2.deleteHookData(hook, func)
    local i = 0
	while (i < #ah2.hooks[hook]) do
		i = i + 1
		if func == ah2.hooks[hook][i][1] then
            ah2.hooks[hook][i] = nil
            -- shift hooks
            local ii = i-1
            while (ii < #ah2.hooks[hook]) do
                ii = ii + 1
                ah2.hooks[hook][ii] = ah2.hooks[hook][ii+1]
            end
		end
	end
end

ah2.original_freehook = freehook
function freehook(hook,func)
	ah2.deleteHookData(hook, func)
    -- 
    ah2.log("Addhook2: removing function \""..func.."\" from hook "..hook)
    --
	print(misc.rgb(100,0,255).."Addhook2: removing function \""..func.."\" from hook "..hook)
	ah2.formHook(hook)
end

function ah2.formHook(hook)
    temp = {}
    ah2.writeNormalHook(hook)
    ah2.eval(temp,hook)
end

function ah2.writeNormalHook(hook)
	ah2.write("function ah2.hook."..hook.."(...)")
	ah2.write("     if (arg == nil) then")
	ah2.write("         arg = {}")
	ah2.write("     end")
	ah2.write("     local ret = {}")
    ah2.write("     local argIndex = #arg+1")
	local i = 0
	while (i < #ah2.hooks[hook]) do
		i = i + 1
        ah2.write("     arg[argIndex] = ret")
        ah2.write("     local returned = "..ah2.hooks[hook][i][1].."(unpack(arg))")
		ah2.write("     ah2.returnControl(ret,\""..hook.."\",returned)")
	end
	ah2.write("     return ah2.returnManager(ret)")
	ah2.write("end")
end

function ah2.returnManager(returnTable)
    local highestNumber = 0
    for i, v in pairs(returnTable) do
        local num = tonumber(i)
        if (not(num == nil)) then
            if (num > highestNumber) then
                highestNumber = num
            end
        end
    end
    return highestNumber
end

function ah2.write(text)
    temp[#temp+1] = text.."\n"
end

function ah2.returnControl(previousReturn,hook,newReturn)
    local index = tostring(newReturn)
    if (type(previousReturn[index]) == "number") then
        previousReturn[index] = previousReturn[index] + 1
    else
        previousReturn[index] = 1
    end
end

function ah2.debug() -- call this function to see what functions are missing
    print(misc.rgb(0,100,255).."Addhook2: Performing a debug check on all function/hooks...")
    local errors = 0
    for i,v in pairs(ah2.hooks) do
        for i2,v2 in ipairs(v) do
            local func = v2[1]
            --local priority = v2[1]
            local function_type = ah2.eval({"return type("..func..")"},i)
            if not(function_type == "function") then
                errors = errors + 1
                print(misc.rgb(255,0,0).."Addhook2: Error in hook: "..i..": missing function: \""..v2.."\" ("..function_type..")")
            end
        end
    end
    if errors == 0 then
        print(misc.rgb(0,255,100).."Addhook2: No missing functions found!")
    else
        print(misc.rgb(255,0,0).."Addhook2: Found a total of "..errors.." missing functions!")
    end
end

function ah2.print() -- call this function to all hooks
    print(misc.rgb(0,100,255).."Addhook2: Printing all hooks/functions")
    for i,v in pairs(ah2.hooks) do
        print(misc.rgb(0,100,200).."---{ \"Hook: "..i.."\" }---")
        for i2,v2 in ipairs(v) do
            print(misc.rgb(100,200,100).."Addhook2: Function: \""..v2.."\"")
        end
    end
end

function ah2.eval(code,hook)
    local fileName = "sys/lua/Addhook2_"..hook..".lua"
	local file = io.open(fileName,"w")
    if (ah2.DebugHooks) then
        file:write("--[[\n")
        file:write("NOTICE: these files (Addhook2_*.lua) can be deleted at anytime, as they are only temporary n")
        file:write("]]--\n")
    end
	i = 0
	while (i < #code) do
		i = i + 1
		file:write(code[i])
	end
	file:close()
	local ret = dofile(fileName)
    if (ah2.DebugHooks == false) then
        os.remove(fileName)
    end
	return ret
end
