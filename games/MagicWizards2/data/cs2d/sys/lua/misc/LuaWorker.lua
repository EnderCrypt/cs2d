LuaWorkerData = {}
LuaWorkerData.doDebug = false
LuaWorkerData.workers = {}

LuaWorkerData.frame = 1
LuaWorkerData.ms100 = 5
LuaWorkerData.second = 50
LuaWorkerData.minute = LuaWorkerData.second * 60
LuaWorkerData.hour = LuaWorkerData.minute * 60

LuaWorkerData.always = LuaWorkerData.frame

addhook("always","LuaWorkerData.update")
function LuaWorkerData.update()
    local i = 0
    while (i < #LuaWorkerData.workers) do
        i = i + 1
        local worker = LuaWorkerData.workers[i]
        if (worker.dispose) then
            worker:INTERNAL_dispose()
            i = i - 1
        else
            if (worker.active) then
                if ((worker.runtime % worker.frequency) == 0) then
                    worker.activations = worker.activations + 1
                    worker.workerFunction(worker,worker.workspace)
                    if (worker.activations == worker.timeout) then
                        worker:delete()
                    end
                end
            end
            worker.runtime = worker.runtime + 1
        end
    end
end

function LuaWorkerData.debug(text)
    if (LuaWorkerData.doDebug) then
        LuaWorkerData.print(text)
    end
end

function LuaWorkerData.print(text)
    print("LuaWorker> "..text)
end

-- worker class
LuaWorker = {}

Class(LuaWorker,function(workerFunction)
    if (type(workerFunction) == "table") then
        error("worker received invalid argument\n"..debug.traceback())
    end
    self.workerFunction = workerFunction
    self.name = "[UNTITLED]"
    self.frequency = LuaWorkerData.always -- how often the worker gets activated
    self.workspace = {} -- table of data that the worker can use
    self.runtime = 0 -- counts how long the worker has been active
    self.timeout = -1 -- how long untill the workerstops itself (default: never)
    self.activation_counter = 0 -- counts how many times the worker has been activaved
    self.active = false
    self.dispose = false
    self.index = #LuaWorkerData.workers+1

    LuaWorkerData.workers[self.index] = self
    LuaWorkerData.debug("Created new worker (ID: "..self.index..")")
end)

function LuaWorker:setName(name)
    self.name = name
    LuaWorkerData.debug("Changed name of worker (ID: "..self.index..") to "..self.name)
end

function LuaWorker:setFrequency(frequency)
    self.frequency = frequency
end

function LuaWorker:setTimeout(timeout)
    self.timeout = timeout
end

function LuaWorker:alive()
    return self.active
end

function LuaWorker:start()
    if (self:alive() == false) then
        LuaWorkerData.debug("Starting worker "..self.name.." (ID: "..self.index..")")
        self.active = true
        self.runtime = 0
        self.activations = 0
    else
        LuaWorkerData.debug("Worker "..self.name.." (ID: "..self.index..") already running, cant be started")
    end
end

function LuaWorker:stop()
    if (self:alive()) then
        LuaWorkerData.debug("Stopping worker "..self.name.." (ID: "..self.index..") after "..misc.round(self:getTime(),2).." Seconds of activity")
        self.active = false
    else
        LuaWorkerData.debug("Worker "..self.name.." (ID: "..self.index..") already stopped")
    end
end

function LuaWorker:delete()
    self.dispose = true
end

function LuaWorker:INTERNAL_dispose() -- INTERNAL
    local index = self.index
    LuaWorkerData.debug("Deleting worker "..self.name.."(ID: "..index..")")
    -- shift
    LuaWorkerData.workers[index] = LuaWorkerData.workers[#LuaWorkerData.workers]
    LuaWorkerData.workers[index].index = index
    LuaWorkerData.workers[#LuaWorkerData.workers] = nil
end

function LuaWorker:getTime() -- in seconds
    return (self.runtime / LuaWorkerData.second)
end

function LuaWorker:info()
    local frequency_text = self.frequency.." times a second"
    if (self.frequency == LuaWorkerData.always) then
        frequency_text = " Always"
    end
    if (self.frequency == LuaWorkerData.ms100) then
        frequency_text = " 10 times a second"
    end
    if (self.frequency == LuaWorkerData.second) then
        frequency_text = " Every second"
    end
    local active = "INACTIVE"
    local color = misc.rgb(255,100,00)
    if (self.active) then
        active = "ACTIVE"
        color = misc.rgb(100,2500,00)
    end
    return color.."LuaWorker "..self.name.." (ID: "..self.index..") State: ["..active.." "..self:getTime().."Sec, "..self.activations.."Execs] Freq: "..self.frequency.."/s (Speed: "..(100/self.frequency).."%)"
end