serverTransfer = {}
serverTransfer.refreshTimer = 10
serverTransfer.transferFile = "sys/servertransfer.lst"
serverTransfer.ignoreDuplicates = true
serverTransfer.ignoreUnexisting = false
serverTransfer.files = {}

function serverTransfer.print(text)
    serverTransfer.rawOutput(print, text)
end

function serverTransfer.error(text)
    serverTransfer.rawOutput(error, text)
end

function serverTransfer.rawOutput(outputFunction, text)
    outputFunction("[ServerTransfer] "..text)
end

function serverTransfer.import(file)
    serverTransfer.print("importing ServerTransfers from "..file)
    local f = io.open(file, "r")
    local input = f:read()
    while (input ~= nil) do
        serverTransfer.add(input)
        input = f:read()
    end
    f:close()
end

function serverTransfer.add(file)
    -- check that file exists
    
    if (misc.fileExists(file) == false) then
        if (serverTransfer.ignoreUnexisting) then
            serverTransfer.print("ignored unexisting file: "..file)
            return
        end
        serverTransfer.error("unexisting file: "..file)
    end
    
    -- check that its not a duplicate
    if (serverTransfer.contains(file)) then
        if (serverTransfer.ignoreDuplicates) then
            serverTransfer.print("ignored duplicate entry: "..file)
            return
        end
        serverTransfer.error("already added: "..file)
    end

    -- add file
    serverTransfer.files[#serverTransfer.files+1] = file
end

function serverTransfer.contains(file)
    for _,tFile in ipairs(serverTransfer.files) do
        if (tFile == file) then
            return true
        end
    end
    return false
end

function serverTransfer.writeTransfers()
    local f = io.open(serverTransfer.transferFile, "w")
    for _,file in ipairs(serverTransfer.files) do
        f:write(file.."\n")
    end
    f:close()
end

function serverTransfer.readTransfers()
    local transfers = {}
    local f = io.open(serverTransfer.transferFile, "r")
    local input = f:read()
    while (input ~= nil) do
        transfers[#transfers+1] = input
        input = f:read()
    end
    f:close()
    return transfers
end

function serverTransfer.transferComparisson(transferList)
    if (#serverTransfer.files ~= #transferList) then
        return false
    end
    for _,file in ipairs(transferList) do
        if (serverTransfer.contains(file) == false) then
            return false
        end
    end
    return true
end

function serverTransfer.finalize()
    -- read current transfers
    local transferList = serverTransfer.readTransfers()
    -- check if valid transfers
    if (serverTransfer.transferComparisson(transferList)) then
        serverTransfer.print("transfers are valid")
    else
        serverTransfer.print("transfers changed ( "..#transferList.." -> "..#serverTransfer.files.." ) map refreshing in "..serverTransfer.refreshTimer.." seconds")
        timer2(serverTransfer.refreshTimer*1000, {}, function()
            parse("map "..map("name"))
        end)
    end
    -- write current transfers
    serverTransfer.writeTransfers()
end



