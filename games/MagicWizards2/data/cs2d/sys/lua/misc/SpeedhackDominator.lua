speedDominator = {}
speedDominator.enabled = false -- DO NOT manually modify
speedDominator.monitorFps = false
speedDominator.serverFPS = 50
speedDominator.player = {}
speedDominator.hook = {}

speedDominator.settings = {}
speedDominator.settings.averageAmount = 50
speedDominator.settings.usgnTriggerPercentage = 175
speedDominator.settings.triggerPercentage = 1

addhook("spawn","speedDominator.hook.spawn")
function speedDominator.hook.spawn(id)
	local data = {}
	data.x = player(id,"x")
	data.y = player(id,"y")
	data.stack = Averager.new(speedDominator.settings.averageAmount)
	data.immune = 0
	if (superInventory.get(id):contains("SPEEDHACK_IMMUNE")) then
		data.immune = math.huge
	end
	data.isBot = player(id,"bot")
	speedDominator.player[id] = data
end

function speedDominator.getTriggerPercentage(id)
	if (player(id,"usgn") == 0) then
		return speedDominator.settings.triggerPercentage
	else
		return speedDominator.settings.usgnTriggerPercentage
	end
end

-- if started using console
local pl = player(0,"tableliving")
local i = 0
while (i < #pl) do
	i = i + 1
	speedDominator.hook.spawn(pl[i])
end

function speedDominator.enable()
	speedDominator.enabled = true
	local players = player(0,"tableliving")
	for _, id in ipairs(players) do
		local data = speedDominator.player[id]
		if (data.isBot == false) then
			data.stack:reset()
			speedDominator.checkPlayer(id)
		end
	end
end

function speedDominator.disable()
	speedDominator.enabled = false
end

addhook("second","speedDominator.hook.second")
function speedDominator.hook.second()
	if ( (speedDominator.enabled) and (speedDominator.monitorFps) ) then
		parse("fps")
	end
end

addhook("always","speedDominator.hook.always")
function speedDominator.hook.always()
	if (speedDominator.enabled) then
		local players = player(0,"tableliving")
		for _, id in ipairs(players) do
			if (speedDominator.player[id] == nil) then -- TEMP FIX FOR BUG?
				speedDominator.hook.spawn(id)
			end
			if (speedDominator.player[id].isBot == false) then
				speedDominator.checkPlayer(id)
			end
		end
	end
end

addhook("log","speedDominator.hook.log",0,true)
function speedDominator.hook.log(text)
	if (speedDominator.enabled) then
		local check = "Current FPS: "
		if (string.starts(text,check)) then
			local number = ""
			local i = string.len(check)+1
			local char = string.charAt(text,i)
			while ((tonumber(char) == nil) == false) do
				number = number..char
				i = i + 1
				char = string.charAt(text,i)
			end
			speedDominator.serverFPS = tonumber(number)
			return 1
		end
		return 0
	end
end

addhook("movetile","speedDominator.hook.movetile")
function speedDominator.hook.movetile(id,x,y)
	if (speedDominator.enabled) then
		if (speedDominator.player[id] ~= nil) then
			if (speedDominator.player[id].immune <= 0) then
				if (player(id,"x") == ((x*32)+16)) then
					if (player(id,"y") == ((y*32)+16)) then
						speedDominator.player[id].immune = 1
					end
				end
			end
		end
	end
end

cs2d.setpos.hook(function(id,x,y)
	if (speedDominator.player[id].immune <= 10) then
		speedDominator.player[id].immune = 10
	end
	speedDominator.player[id].stack:reset() -- TEST
end)

function speedDominator.checkPlayer(id)
	local data = speedDominator.player[id]
	local x = player(id,"x")
	local y = player(id,"y")
	if (data.immune <= 0) then
		local distanceMoved = misc.point_distance(x,y,data.x,data.y)
		local expected = speedDominator.getExpectedSpeed(id)
		local percentage = 100/expected*distanceMoved
		local stack = speedDominator.player[id].stack
		stack:push(misc.round(percentage))
		if (stack:isFilled()) then
			local average = stack:average()
			if (average >= speedDominator.getTriggerPercentage(id)) then
				data.stack:reset()
				speedDominator.punish(id)
			end
		end
	end
	-- save data for next iteration
	data.immune = data.immune - 1
	data.x = x
	data.y = y
end

function speedDominator.punish(id)
	--msg(misc.rgb(255,0,0)..player(id,"name").." was kicked for potential speedhacking")
	--parse("kick "..id.." \"speedhacking\"")
	--mw2flag.new(id,0,mw2flags.library.iceStorm,10)
	local worker = LuaWorker.new(function(worker,workspace)
		cs2d.setpos.call(workspace.id,workspace.x,workspace.y)
	end)
	worker.workspace.id = id
	worker.workspace.x = player(id,"x")
	worker.workspace.y = player(id,"y")
	worker:setTimeout(50*10)
	worker:setFrequency(LuaWorkerData.always)
	worker:start()
	if (mw2 ~= nil) then
		mw2.playerSpeedSet(id,-250)
	end
end

function speedDominator.getExpectedSpeed(id)
	local speed = 3.4--3.3
	-- compensate for speedmod
	local speedmd = player(id,"speedmod")
	if (speedmd ~= 0) then
		speed = speed + (speedmd * 0.12)
	end
	-- compensate for fps
	speed = speed * 50 / speedDominator.serverFPS
	--
	return speed
end
