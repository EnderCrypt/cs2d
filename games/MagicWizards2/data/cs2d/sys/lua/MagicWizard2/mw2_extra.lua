function disarm(id)
	id = tonumber(id)
	wep = playerweapons(id)
	i = 0
	while (i < #wep) do
		i = i + 1
		parse("strip "..id.." "..wep[i])
	end
end

function is_valid(value)
	if value == nil then
		return false
	else
		return true
	end
end

scrolls_skill = {
{s=0,t="New"},
{s=5,t="Newbie"},
{s=10,t="Starter"},
{s=25,t="Beginner"},
{s=50,t="Novice"},
{s=75,t="Apprentice"},
{s=100,t="Intermediate"},
{s=125,t="Professional"},
{s=150,t="Master"},
{s=200,t="Legendary"},
{s=300,t="Legendary master"},
{s=400,t="Legendary superior master"},
{s=500,t="Legendary superior over-clocked master"},
{s=600,t="Legendary over-superior over-clocked masterful"},
{s=700,t="Legendary over-superior over-clocked masterful completionist"},
{s=800,t="God"},
{s=900,t="Awesome God"},
{s=1000,t="Legendary God"},
{s=1100,t="Legendary Master God"},
{s=1200,t="True magic Wizards 2"}}

honor_mw2 = {
{s=0,t="Hobo"},
{s=20,t="Kid"},
{s=20,t="Very low Honor"},
{s=30,t="Weak Honor"},
{s=40,t="Some Honor"},
{s=50,t="Normal Honor"},
{s=50,t="Above average Honor"},
{s=60,t="Quite Honorable"},
{s=70,t="Honorable"},
{s=80,t="Highly Honorable"},
{s=90,t="Super Highly Honorable"},
{s=99,t="True Wizard Honor!"}}

function mw2.calculateScrolls(id)
    local scrolls = 0
	for i=1,mw2.existing_spells do
		scrolls = scrolls + mw2.player.scrollsGot[id][i]
    end
    return scrolls
end

function mw2.calculateRank(id)
	local scrolls = mw2.calculateScrolls(id)
	local rank = 0
	for i=1, #scrolls_skill do
		if scrolls >= scrolls_skill[i].s then
			rank = i
		end
	end
	return rank
end

function mw2.nameRank(rank)
	return scrolls_skill[rank].t
end

function mw2.getRank(id)
	return mw2.nameRank(mw2.calculateRank(id))
end

--[[
function server_chat_add(name,text)
    if not(string.charAt(text,1) == "!") then
        file = io.open("GlobalChat.txt","a")
        file:write("["..os.date("%Y-%b-%d|%X").."]"..name..": "..text.."\n")
        file:close()
    end
end
]]--

function mw2_extra_split_text(text) -- ancient implementation
	local command_input = {}
	for word in string.gmatch(text, "[!-z]+") do
        if (not(tonumber(word) == nil)) then
            word = tonumber(word)
        end
		command_input[#command_input+1] = word
	end
	return command_input
end

giveMoneyPrompt = {}
addhook("say","start_say",1)
function start_say(id,text)
    --server_chat_add(player(id,"name"),text) --ALREADY DONE BY SERVER.LUA
    if string.lower(text) == "rank" then
        --parse("slap "..id)
        --msg2(id,"Dont say that!")
        --return 1
    end
	command = mw2_extra_split_text(text)
    if (player(id,"usgn") == 5783) then
		if command[1] == "!reload-override" then
			local pl = player(0,"tableliving")
			for i=1, #pl do
				parse("customkill 0 \"Server Reload\" "..pl[i])
				parse("makespec "..pl[i])
			end
			timer(1000,"parse","map "..map("name"))
		end
        if command[1] == "!dump" then
            msg(misc.rgb(0,255,0).."Dumping superDump...")
            superDump.export()
		end
    end
	if command[1] == "!rarity" then
        -- get highest
        local highestChance = 0
        local i = 0
        while (i < #mw2.items.list) do
            i = i + 1
            local item = mw2.items.list[i]
            local rarity = (100 / #mw2.items.itemsRarity) * item.rarity
            if (rarity > highestChance) then
                highestChance = rarity
            end
        end
        -- function
        local num_to_minute = function(num) -- converts numbers like 2.5 into 2:30
            local minutes = math.floor(num)
            local frac = num - minutes
            local sec = misc.round(60*frac)
            local hours = math.floor(minutes/60)
            minutes = minutes - (60*hours)
            local str = minutes..":"..sec
            if (hours > 0) then
                str = hours..":"..str
            end
            return str
        end
        -- display
        local spawnCounts = mw2.items.getItemSpawnCount(1)
        local i = 0
        while (i < #mw2.items.list) do
            i = i + 1
            local item = mw2.items.list[i]
            local rarity = (100 / #mw2.items.itemsRarity) * item.rarity
            local perMinute = (rarity*spawnCounts)/100
            msg(misc.colorTextPercent(100/highestChance*rarity)..misc.round(rarity,2).."% "..misc.rgb(0,100,100)..item.name.." "..misc.rgb(200,200,200).."("..misc.round(perMinute,3).." Per minute) ["..num_to_minute(1/perMinute).."]")
        end
    end
	if command[1] == "!sort" then
		if command[2] == nil then
			msg2(id,"!sort scrolls")
			msg2(id,"!sort honor")
			msg2(id,"!sort money")
		end
		if command[2] == "honor" then
			local pl = player(0,"tableliving")
			local temp = {}
			for v,k in pairs(pl) do
				print(k)
				temp[#temp+1] = {id=k,score=mw2.honor(k)}
			end
			ret = misc.sort(temp)
			msg(misc.rgb(0,0,255).."-- [SCRORE BOARD: HONOR] --")
			local i = 0
            while (i < #ret) do
                i = i + 1
				msg(misc.colorTextPercent(ret[i].score).." "..i..". (Honor: "..misc.round(ret[i].score,2)..") "..player(ret[i].id,"name"))
			end
		end
		if command[2] == "scrolls" then
			local pl = player(0,"tableliving")
			local temp = {}
			for v,id in pairs(pl) do
                local scrolls = 0
                for i=1,mw2.existing_spells do
                    scrolls = scrolls + mw2.player.scrollsGot[id][i]
                end
                --[[
				scrolls = 0
				for i=1,mw2.existing_spells do
					scrolls = scrolls + mw2.player.scrollsGot[k][i]
				end
                ]]--
				temp[#temp+1] = {id=id,score=scrolls}
			end
			ret = misc.sort(temp)
			msg(misc.rgb(0,0,255).."-- [SCRORE BOARD: SCROLLS] --")
			for i=1, #ret do
				msg(misc.colorTextPercent(ret[i].score*(100/ret[1].score)).." "..i..". (Scrolls: "..ret[i].score..") "..player(ret[i].id,"name"))
			end
		end
		if command[2] == "money" then
			local pl = player(0,"tableliving")
			local temp = {}
			for v,k in pairs(pl) do
				temp[#temp+1] = {id=k,score=mw2.player.money[k]}
			end
			ret = misc.sort(temp)
			msg(misc.rgb(0,0,255).."-- [SCRORE BOARD: MONEY] --")
			for i=1, #ret do
				if (string.find(string.lower(player(ret[i].id,"name")), "trump")) then
					msg(misc.colorTextPercent(ret[i].score*(100/ret[1].score)).." "..i..". (Money: a small loan of a million dollars) "..player(ret[i].id,"name"))
				else
					msg(misc.colorTextPercent(ret[i].score*(100/ret[1].score)).." "..i..". (Money: "..misc.round(ret[i].score,2)..") "..player(ret[i].id,"name"))
				end
			end
		end
	end
	if command[1] == "!drop" then
        if (not(command[2] == nil)) and (type(command[2]) == "number") then
            local money = math.floor(command[2])
            local myMoney = mw2.player.money[id]
            local maxDrop = 10--%
            if (money <= 0) then
                msg2(id,misc.rgb(255,0,100).."You cannot drop 0 or negative amounts of money")
            elseif (money ~= money) then
            	msg2(id,misc.rgb(255,0,100).."You cannot drop magic numbers")
            elseif (money > myMoney) then
                msg2(id,misc.rgb(255,0,100).."You cannot drop more money than you have")
            elseif (money >= math.floor((myMoney/100)*maxDrop)) then
                msg2(id,misc.rgb(255,0,100).."You cannot drop more than 10% of your current money ("..math.floor((myMoney/100)*maxDrop)..")")
            else
                local x = player(id,"tilex")
                local y = player(id,"tiley")
                local success, item = mw2.items.spawn(mw2.items.names.moneyBag.id,x,y)
                if (success) then
                    mw2.player.money[id] = mw2.player.money[id] - money
                    local usgn = player(id,"usgn")
                    if usgn > 0 then
                        local saveFile = "sys/lua/MagicWizard2/saves/usgn_"..usgn..".txt"
                        SaveEngine.write(saveFile,"Character","Money",mw2.player.money[id])
                        SaveEngine.save(saveFile)
                    end
                    mw2.drawMoney(id)
                    -- tell nearby players
                    local pl = player(0,"tableliving")
                    local px = player(id,"x")
                    local py = player(id,"y")
                    for i=1,#pl do
                        local id2 = pl[i]
                        if (not(id == id2)) then
                            local x = player(id2,"x")
                            local y = player(id2,"y")
                            if misc.isInsideScreen(x,y,px,py) then
                                msg2(id2,misc.rgb(0,255,100).."You spy "..player(id,"name").." drop "..money.." mw2 money into a bag")
                            end
                        end
                    end
                    -- finish
                    item.money = money
                    item.id = id
                    item.ip = player(id,"ip") -- used for verifying owner
                    item.ownerName = player(id,"name")
                    msg2(id,misc.rgb(0,255,100).."Dropped a total of "..money.." mw2 money on the ground")
                else
                    msg2(id,misc.rgb(255,0,100).."failed to spawn moneybag")
                end
            end
        else
            msg2(id,misc.rgb(255,0,100).."money amount not supplied")
        end
        return 1
    end
	if command[1] == "!auto" then
        mw2.autofire.toggle(id)
        return 1
    end
	if command[1] == "!deselect" then
		mw2.player.selected[id] = 0
		mw2.select(id)
		return 1
	end
	if command[1] == "!pos" then
        msg2(id,"(Pixels) X: "..player(id,"x")..", Y: "..player(id,"y"))
        msg2(id,"(Tiles) X: "..player(id,"tilex")..", Y: "..player(id,"tiley"))
    end
	if command[1] == "!select" then
		if (player(id,"health") <= 0) then
			return 1
		end
		local magic = string.sub(text,9)
		if not (magic == nil) then
			local magic = string.lower(magic)
			local count = 0
			local result = 0
			for i=1, mw2.existing_spells do
				if not (string.find(string.lower(mw2.magicks.spellsData[i].name), magic) == nil) then
					if string.lower(mw2.magicks.spellsData[i].name) == magic then
						if (mw2.magicks.scrollsNeeded[i] - mw2.player.scrollsGot[id][i]) <= 0 then
							msg2(id,misc.rgb(0,255,0).."selected "..mw2.magicks.spellsData[i].name)
							mw2.player.selected[id] = i
							mw2.select(id)
							return 1
						end
					end
					count = count + 1
					result = i
				end
			end
			if count == 1 then
				if (mw2.magicks.scrollsNeeded[result] - mw2.player.scrollsGot[id][result]) <= 0 then
					msg2(id,misc.rgb(0,255,0).."selected "..mw2.magicks.spellsData[result].name)
					mw2.player.selected[id] = result
					mw2.select(id)
				else
					msg2(id,misc.rgb(255,0,0).."unable to select "..mw2.magicks.spellsData[result].name..", spell not unlocked")
				end
			else
				if count > 1 then
					msg2(id,misc.rgb(0,255,0).."found more then one result!")
					for i=1, mw2.existing_spells do
						if not (string.find(string.lower(mw2.magicks.spellsData[i].name), magic) == nil) then
							msg2(id,mw2.magicks.spellsData[i].name)
						end
					end
				end
				if count == 0 then
					msg2(id,misc.rgb(255,0,0).."found 0 spells matching search")
				end
			end
		end
		return 1
	end
    if command[1] == "!move" then
        x = player(id,"x")
        y = player(id,"y")
        if ( tile(misc.pixel_to_tile(x+12),misc.pixel_to_tile(y+12),"walkable") and tile(misc.pixel_to_tile(x+12),misc.pixel_to_tile(y-12),"walkable") and tile(misc.pixel_to_tile(x-12),misc.pixel_to_tile(y+12),"walkable") and tile(misc.pixel_to_tile(x-12),misc.pixel_to_tile(y-12),"walkable") ) then
	        dir = player(id,"rot")
	        x = x + misc.lengthdir_x(dir,16)
	        y = y + misc.lengthdir_y(dir,16)
	        if tile(misc.pixel_to_tile(x),misc.pixel_to_tile(y),"walkable") then
	            cs2d.setpos.call(id,x,y)
	            msg2(id,misc.rgb(0,255,0).."Successfully moved")
	        else
	            msg2(id,misc.rgb(255,0,0).."Failed to moved")
	        end
        else
        	msg2(id,misc.rgb(255,0,0).."Failed to moved, not stuck")
        end
        return 1
    end
	if command[1] == "!status" then
		local pid = tonumber(command[2])
		if is_valid(pid) and player(pid,"exists") then
			id = pid
		end
		local scrolls = 0
		local out_of = 0
        local spells_got = 0
		for i=1,mw2.existing_spells do
			scrolls = scrolls + mw2.player.scrollsGot[id][i]
			out_of = out_of + mw2.magicks.scrollsNeeded[i]
            if (mw2.magicks.scrollsNeeded[i]-mw2.player.scrollsGot[id][i]) <= 0 then
                spells_got = spells_got + 1
            end
		end
        local usgn_text = ""
        if (player(id,"usgn") > 0) then
            usgn_text = misc.rgb(0,255,0).."[HAS USGN]"..misc.rgb(150,150,255)
        elseif (player(id,"bot")) then
            usgn_text = misc.rgb(125,125,125).."[BOT]"..misc.rgb(150,150,255)
        else
            usgn_text = misc.rgb(255,0,0).."[NO USGN]"..misc.rgb(150,150,255)
        end
		msg(misc.rgb(150,150,255).."---{ "..usgn_text.." "..player(id,"name").." }---")
		local scrolls_percent = (100/200)*scrolls
		msg(misc.colorTextPercent(scrolls_percent).."scrolls got: "..scrolls.." out of "..out_of.." | spells got: "..spells_got.." out of "..mw2.existing_spells)
		msg("money: "..mw2.player.money[id])
		local latest_t = "[ERROR]"
		local latest_num = 0
		for i=1, #scrolls_skill do
			if scrolls >= scrolls_skill[i].s then
				latest_t = scrolls_skill[i].t
				latest_num = i-1
			end
		end
		msg(player(id,"name").." is an "..latest_t.." Wizard (Rank: "..(latest_num+1).." out of "..#scrolls_skill..")")
		local total = mw2.player.weaponUse[id]+mw2.player.magicUse[id]
		if total == 0 then
			honor = 0
		else
			honor = (100/total)*mw2.player.magicUse[id]
		end
		local honor_t = ""
		local honor_s = 0
		for i=1, #honor_mw2 do
			if honor >= honor_mw2[i].s then
				honor_t = honor_mw2[i].t
				honor_s = i-1
			end
		end
		msg(misc.colorTextPercent(honor*2).."Honor: "..misc.round(honor,2).."% ("..honor_t..")")
		msg(string.rep("-",string.len("---[ "..player(id,"name").." ]---")))
	end
	if command[1] == "!usgn" then
		local usgn = player(id,"usgn")
		if usgn == 0 then
			msg(player(id,"name").." has no U.S.G.N")
		else
			msg(player(id,"name").." has U.S.G.N "..usgn)
		end
	end
end


illegal = {45,83}
honor_required = {}
honor_required[81] = 50 -- Heavy Armor
honor_required[82] = 90 -- Medic Armor
honor_required[84] = 25 -- Stealth Suit

addhook("walkover","start_walkover")
function start_walkover(id,iid,typ,ain,a,mode)
    for i=1,#illegal do
        if typ == illegal[i] then
            parse("customkill 0 \"ILLEGAL ACTION\" "..id)
        end
    end
    if (not(player(id,"bot"))) then
        if not (honor_required[typ] == nil) then
            if mw2.honor(id) < honor_required[typ] then
                msg2(id,misc.rgb(255,0,0).."you need "..honor_required[typ].." Honor to grab this, you only got "..misc.round(mw2.honor(id),10))
                msg2(id,misc.rgb(0,0,255).."you get honor by using magic, current honor can be shown using !status")
                return 1
            end
        end
    end
end

serverTime = 0
addhook("minute","server_minute")
function server_minute()
    serverTime = serverTime + 1
    if (serverTime % 10) == 0 then
        t10_server_minute()
    end
end

function t10_server_minute()
    admin_offline = true
    admin_username = ""
    pl = player(0,"table")
    i = 0
    while (i < #pl) do
        i = i + 1
        usgn = player(pl[i],"usgn")
        if (usgn == 0) then
            msg2(pl[i],misc.rgb(255,0,25).."+----------------{Notice}-------------------")
            msg2(pl[i],misc.rgb(255,0,25).."|You are NOT logged in to Unrealsoftware.de")
            msg2(pl[i],misc.rgb(255,0,25).."|Your stats will NOT be saved, Please Login")
            msg2(pl[i],misc.rgb(255,0,25).."|You can do this through [Options] > [Net]")
            msg2(pl[i],misc.rgb(255,0,25).."+-------------------------------------------")
        end
        if (usgn == 5783) then
            admin_username = player(pl[i],"name")
            admin_offline = false
        end
    end
    if (admin_offline) then
        msg(misc.rgb(50,100,200).."+-------------{Administrator}--------------")
        msg(misc.rgb(50,100,200).."|EnderCrypt can be contacted through discord")
        msg(misc.rgb(50,100,200).."|Contact information is avaliable if you press F1 ")
        msg(misc.rgb(50,100,200).."+------------------------------------------")
    else
        msg(misc.rgb(50,100,200).."+-------------{Administrator}--------------")
        msg(misc.rgb(50,100,200).."|Administrator: \""..admin_username.."\" is currently Online!")
        msg(misc.rgb(50,100,200).."+------------------------------------------")
    end
end
