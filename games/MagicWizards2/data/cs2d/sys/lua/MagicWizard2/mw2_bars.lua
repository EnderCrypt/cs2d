mw2.bars = {}

mw2.bars.graphics = {}
mw2.bars.graphics.health = "gfx/Magic Wizard 2/bars_test/health.png"
mw2.bars.graphics.mana   = "gfx/Magic Wizard 2/bars_test/mana.png"
mw2.bars.graphics.width  = 100

serverTransfer.add(mw2.bars.graphics.health)
serverTransfer.add(mw2.bars.graphics.mana)

mw2.bars.time = {}
mw2.bars.time.life = 5.0
mw2.bars.time.fadein = 0.25
mw2.bars.time.fadeout = 1


mw2.bars.width = 50
mw2.bars.maxContainerAlpha = 0.25
mw2.bars.maxBarAlpha = 0.5
mw2.bars.minAlpha = 0.1

mw2.bars.players = {}

addhook("spawn", "mw2.bars.spawn")
function mw2.bars.spawn(id)
	local data = {}

	data.health = mw2.bars.generateBar(id, mw2.bars.graphics.health, {255, 0, 0}, function(id)
		return player(id, "maxhealth")
	end, function(id)
		return player(id, "health")
	end)

	data.mana = mw2.bars.generateBar(id, mw2.bars.graphics.mana, {0, 0, 255}, function(id)
		return mw2.maxPlayerMana
	end, function(id)
		return mw2.player.mana[id].mana
	end)

	mw2.bars.players[id] = data
end

addhook("leave", "mw2.bars.leave")
function mw2.bars.leave(id, reason)
	 mw2.bars.disableBars(id)
end

addhook("die", "mw2.bars.die")
function mw2.bars.die(victim, killer, weapon, x, y, killerobject)
	 mw2.bars.disableBars(victim)
end

addhook("ms100", "mw2.bars.ms100")
function mw2.bars.ms100()
	mw2.bars.updatePlayers(0.1)
end

function mw2.bars.disableBars(id)
	local data = mw2.bars.players[id]
	if (data == nil) then
		return
	end
	data.health.image.container:freeimage()
	data.health.image.bar:freeimage()
	data.mana.image.container:freeimage()
	data.mana.image.bar:freeimage()
end

function mw2.bars.generateBar(id, imagePath, color, maxGetter, valueGetter)
	local bar = {}

	bar.player = id

	bar.image = {}

	bar.image.container = Image.Class.new(imagePath, 0, 0, 132+id)
	bar.image.container:setAutoRefresh(true)
	bar.image.container:imagealpha(0)
	bar.image.container:imagescale(0, 1)

	bar.image.bar       = Image.Class.new(imagePath, 0, 0, 132+id)
	bar.image.bar:setAutoRefresh(true)
	bar.image.bar:imagealpha(0)
	bar.image.bar:imagescale(0, 1)
	bar.image.bar:imagecolor(color[1], color[2], color[3])

	bar.max = 0
	bar.maxGetter = maxGetter

	bar.value = 0
	bar.valueGetter = valueGetter

	bar.display = false
	bar.lifetime = 0

	return bar
end

function mw2.bars.updatePlayers(delta)
	for _,id in ipairs(player(0,"tableliving")) do
		mw2.bars.updatePlayer(id, delta)
	end
end

function mw2.bars.updatePlayer(id, delta)
	local data = mw2.bars.players[id]
	if (data == nil) then
		print("ERROR: missing bar for id "..id)
		return
	end
	mw2.bars.updateBar(data.health, delta)
	mw2.bars.updateBar(data.mana, delta)
end

function mw2.bars.updateBar(bar, delta)

	local deltaMs = delta * 1000

	local triggered = false

	local oldMax = bar.max
	bar.max = bar.maxGetter(bar.player)
	if (oldMax ~= bar.max) then
		triggered = true
		bar.image.container:tween_scale(deltaMs, (mw2.bars.width / mw2.bars.graphics.width) / 100 * bar.max, 1)
	end

	local oldValue = bar.value
	bar.value = bar.valueGetter(bar.player)
	if (oldValue ~= bar.value) then
		triggered = true
		bar.image.bar:tween_scale(deltaMs, (mw2.bars.width / mw2.bars.graphics.width) / 100 * bar.value, 1)
	end

	if (triggered) then
		bar.lifetime = 0
		if (bar.display == false) then
			if (bar.display == false) then
				bar.image.container:tween_alpha(mw2.bars.time.fadein*1000, mw2.bars.maxContainerAlpha)
				bar.image.bar:tween_alpha(mw2.bars.time.fadein*1000, mw2.bars.maxBarAlpha)
			end
			bar.display = true
		end
	end

	if (bar.display) then
		bar.lifetime = bar.lifetime + delta
		if (bar.lifetime > mw2.bars.time.life) then
			bar.display = false
			bar.image.container:tween_alpha(mw2.bars.time.fadeout*1000, mw2.bars.minAlpha)
			bar.image.bar:tween_alpha(mw2.bars.time.fadeout*1000, mw2.bars.minAlpha)
		end
	end

end
