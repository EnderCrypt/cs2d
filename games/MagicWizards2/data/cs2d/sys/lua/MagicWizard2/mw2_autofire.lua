mw2.autofire = {}
mw2.autofire.player = {}
mw2.autofire.frequency = 5

addhook("join","mw2.autofire.join")
function mw2.autofire.join(id)
	mw2.autofire.player[id] = {
			enabled = false,
			timer = 0
		}
end

addhook("leave","mw2.autofire.leave")
function mw2.autofire.leave(id)
	mw2.autofire.player[id] = nil
end

addhook("ms100","mw2.autofire.ms100")
function mw2.autofire.ms100()
	for id,data in pairs(mw2.autofire.player) do
		data.timer = data.timer - 1
		if (data.enabled) and (mw2.player.selected[id] > 0) and (data.timer < 0) then
			if (mw2.player.mana[id].mana > (mw2.maxPlayerMana*0.95)) then
				data.timer = mw2.autofire.frequency
				mw2.attack(id)
			end
		end
	end
end

function mw2.autofire.toggle(id)
	local data = mw2.autofire.player[id]
	data.enabled = not data.enabled
	msg2(id,misc.rgb(0,0,255).."Autofire mode: "..tostring(data.enabled))
end
