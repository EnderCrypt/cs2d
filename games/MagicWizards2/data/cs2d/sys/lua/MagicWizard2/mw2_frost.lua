mw2.frost = {}

mw2.frost.graphics = "gfx/Magic Wizard 2/effects/permafrost.png"
serverTransfer.add(mw2.frost.graphics)

mw2.frost.players = {}
mw2.frost.speedmodThreshold = -2

addhook("ms100", "mw2.frost.ms100")
function mw2.frost.ms100()
	for _,id in ipairs(player(0,"tableliving")) do
		local data = mw2.frost.get(id)
		local speed = player(id, "speedmod")
		local speedChange = speed ~= data.value
		data.value = speed
		if (speed >= mw2.frost.speedmodThreshold) then
			if (data.image ~= nil) then
				freeimage(data.image)
				data.image = nil
			end
		else
			if (data.image == nil) then
				data.image = image(mw2.frost.graphics, 0, 0, 100+id)
				imagealpha(data.image, 0)
				imagescale(data.image, 0, 0)
				speedChange = true
			end
			if (speedChange) then
				local strength = mw2.frost.getStrength(data.value)
				tween_alpha(data.image, 100, strength)
				tween_scale(data.image, 100, strength, strength)
			end
		end
	end
end

function mw2.frost.get(id)
	local data = mw2.frost.players[id]
	if (data == nil) then
		data = {}
		data.image = nil
		data.value = 0
		mw2.frost.players[id] = data
	end
	return data
end

function mw2.frost.getStrength(speedmod)
	local base = 1
	local mult = 0.1
	speedmod = math.abs(speedmod)
	local total = base + (speedmod * mult)
	local strength = 1 - (1 / total)
	strength = strength * strength
	return strength
end
