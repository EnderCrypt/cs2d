mw2BotTraining = {}
mw2BotTraining.busyBots = {}

addhook("die","mw2BotTraining.die")
function mw2BotTraining.die(victim,killer,weapon,x,y)
	if (player(victim,"bot")) then
		local data = mw2BotTraining.busyBots[victim]
		if (data ~= nil) then
			if (data.target == killer) then
				data.kills = data.kills + 1
				ai_say(victim,"nice! you now killed me "..data.kills.." times")
				timer2(1500,{victim, data.kills},function(botID, maxRank)
					local minRank = math.min(10, math.ceil(maxRank/2) )
					local rank = minRank + ( (maxRank-minRank)*math.random() )
					bot.array[botID]:setRank(rank)
				end)
				-- award badge
				--local data = mw2BotTraining.busyBots[victim]
				if (data.kills == 5) then
					ai_say(victim,"I will now play a bit harder, no wards allowed ;)")
				end
				if (data.kills % 5 == 0) then
					local inv = superInventory.get(data.target)
					local item = "MW2_BOT_BADGE_"..data.kills
					if (inv:contains(item) == false) then
						inv:add(item)
						inv:add("LOTTERY_TICKET")
						--msg(misc.rgb(0,255,0)..player(data.target).." Received a "..superInventory.itemList[item].name.."!")
					end
				end
			else
				-- something else killed the bot
			end
			--
		end
		return
	end
	for id,data in pairs(mw2BotTraining.busyBots) do
		if (data.target == victim) then
			mw2BotTraining.stop(data.target)
			return
		end
	end
end

function mw2BotTraining.start(targetID)
	local botID = mw2BotTraining.getBot(targetID)
	if (botID == false) then
		return false
	end
	local trainerWorker = LuaWorker.new(function(worker,workspace)
		mw2BotTraining.protect(workspace.botID)
		if ((worker.runtime % LuaWorkerData.second) == 0) then
			mw2BotTraining.process(worker,workspace.botID,workspace.targetID)
		end
	end)
	trainerWorker.workspace.botID = botID
	trainerWorker.workspace.targetID = targetID
	ai_say(botID,"I will now fight you in training mode, "..player(targetID,"name"))
	trainerWorker:setFrequency(LuaWorkerData.ms100)
	trainerWorker:start()

	bot.array[botID]:setRank(1)
	mw2BotTraining.busyBots[botID].worker = trainerWorker
	return true
end

function mw2BotTraining.stop(targetID)
	for id,data in pairs(mw2BotTraining.busyBots) do
		if (data.target == targetID) then
			ai_say(id,"You died, Excellent session, you killed me "..data.kills.." times!")
			ai_say(id,"training mode disabled!")
			data.worker:delete()
			mw2BotTraining.busyBots[id] = nil
			return true
		end
	end
	return false
end

function mw2BotTraining.getBot(targetID)
	local pl = player(0,"table")
	for _,botID in ipairs(pl) do
		if (player(botID,"bot")) then
			if (mw2BotTraining.busyBots[botID] == nil) then
				local data = {}
				data.kills = 0
				data.isDead = true
				data.target = targetID
				data.dist = 500
				mw2BotTraining.busyBots[botID] = data
				return botID
			end
		end
	end
	return false
end

function mw2BotTraining.protect(botID)
	local bx = player(botID,"x")
	local by = player(botID,"y")
	for i=1,#mw2.entity do
        local entity = mw2.entity[i]
        if (entity.owner ~= botID) then
	        if ((entity.me == mw2.name.handOfDoom) or (entity.me == mw2.name.FloatingHole)) then
	        	if (misc.point_distance(bx,by,entity.x,entity.y) <= 64) then
	        		mw2.magicks.spellsData[31].onUse(botID) -- superior ward
	        		return true
	        	end
	        end
	        if (mw2BotTraining.busyBots[botID].kills >= 5) then
		        if ((entity.me == mw2.name.WeakWard) or (entity.me == mw2.name.MediumWard) or (entity.me == mw2.name.StrongWard) or (entity.me == mw2.name.SuperiorWard) or (entity.me == mw2.name.WeakDeflector) or (entity.me == mw2.name.MediumDeflector) or (entity.me == mw2.name.StrongDeflector) or (entity.me == mw2.name.SuperiorDeflector)) then
		        	if (misc.point_distance(bx,by,entity.x,entity.y) <= 96) then
		        		mw2.magicks.spellsData[66].onUse(botID) -- superior deflector
		        		return true
		        	end
		        end
		    end
	    end
    end
    return false
end

function mw2BotTraining.process(worker,botID,targetID)
	if (player(targetID,"exists") == false) then
		worker:delete()
		return false
	end
	local px = player(targetID,"x")
	local py = player(targetID,"y")
	local bx = player(botID,"x")
	local by = player(botID,"y")
	bot.array[botID].escapeFailCounter = 100
	if (misc.point_distance(px,py,bx,by) > 500) then
		if (mw2BotTraining.teleport(botID,targetID)) then
			bot.array[botID]:target(targetID)
			cs2d.sethealth.call(botID,player(botID,"health")+100)
		end
	end
end

function mw2BotTraining.lineWalkable(x1,y1,x2,y2)
    precision = 16
    local dist = misc.point_distance(x1,y1,x2,y2)
    local dir = misc.point_direction(x1,y1,x2,y2)
    local check_x = x1
    local check_y = y1
    local add_x = misc.lengthdir_x(dir,precision)
    local add_y = misc.lengthdir_y(dir,precision)
    local i = 0
    while (i < math.floor(dist/precision)) do
        i = i + 1
        check_x = check_x + add_x
        check_y = check_y + add_y
        if (tile(misc.pixel_to_tile(check_x),misc.pixel_to_tile(check_y),"walkable") == false) then
            return false
        end
    end
    return true
end

function mw2BotTraining.teleport(botID,targetID)
	local x = player(targetID,"x")
	local y = player(targetID,"y")
	local dir_start = math.random()*360
	local dir = 0
	while (dir < 360) do
		dir = dir + 5
		local bx = x + misc.lengthdir_x(dir_start+dir,mw2BotTraining.busyBots[botID].dist)
		local by = y + misc.lengthdir_y(dir_start+dir,mw2BotTraining.busyBots[botID].dist)
		if (mw2BotTraining.lineWalkable(x,y,bx,by)) then
			local btx = misc.pixel_to_tile(bx)
			local bty = misc.pixel_to_tile(by)
			if (tile(btx,bty,"walkable")) then
				mw2BotTraining.busyBots[botID].dist = math.min(mw2BotTraining.busyBots[botID].dist+100,500)
				misc.effect("smoke",bx,by,100,32,0,0,0)
				cs2d.setpos.call(botID,bx,by)
				bot.array[botID]:target(targetID)
				mw2.playerSpeed(botID,mw2BotTraining.busyBots[botID].kills)
				mw2.magicks.spellsData[122].onUse(botID) -- weapon immunity
			end
			return true
		end
	end
	mw2BotTraining.busyBots[botID].dist = mw2BotTraining.busyBots[botID].dist - 100
	ai_say(botID,"Dont try to escape "..player(targetID,"name").."!")
	return false
end

--[[
function mw2BotTraining.attack(botID,targetID)
	--mw2.playerSpeedSet(botID,mw2.player.speed[targetID]+5)
	local minRank = mw2.calculateRank(targetID)
	minRank = minRank+misc.round(math.random()*2)-1
	if (bot.array[botID].rank < minRank) then
		bot.array[botID]:promote(minRank-bot.array[botID].rank)
	end
end
]]--

cs2d.speedmod.hook(function(id,speed)
	if (player(id,"bot") and (speed < 0)) then
		local rnd = math.random()*100
		if (rnd < math.abs(speed)) then
			mw2.magicks.spellsData[64].onUse(id) -- medium deflector
		end
	end
end)
