mw2sd = {}
mw2sd.radar = {}
mw2sd.radar.x = 54
mw2sd.radar.y = 54
mw2sd.counter = 0
mw2sd.everyXsecond = 120


mw2sd.counter = 0
addhook("second","mw2sd.second")
function mw2sd.second()
	mw2sd.counter = mw2sd.counter + 1
		if ((mw2sd.counter % mw2sd.everyXsecond) == 0) then
			mw2sd.showAllScrollDirections()
		end
end

function mw2sd.showAllScrollDirections()
		-- get all scrolls
		local scroll_locations = {}
		local i = 0
		while (i < #mw2.items.grid_list) do
				i = i + 1
				local item = mw2.items.grid_list[i]
				if (item.item_reference.id == mw2.items.names.scroll.id) then
						--print("scroll x:"..item.x.." y:"..item.y)
						local x = misc.tile_to_pixel(item.x)
						local y = misc.tile_to_pixel(item.y)
						scroll_locations[#scroll_locations+1] = {x = x, y = y}
				end
		end
		-- show scrolls for all players
		for _,id in ipairs(player(0,"tableliving")) do
			for _,scroll in ipairs(scroll_locations) do
					mw2sd.printScrollDirection(id,scroll)
			end
		end
end



function mw2sd.printScrollDirection(id,scroll)
	local px = player(id,"x")
	local py = player(id,"y")
	local width = player(id, "screenw")
	local height = player(id, "screenh")
	if (misc.isInsideScreen(scroll.x, scroll.y, px, py)) then
		return false
	end
	local dist = misc.point_distance(px,py,scroll.x,scroll.y)
	local dir = misc.point_direction(px,py,scroll.x,scroll.y)
	-- calculate appropriate position
	local x = width/2
	local y = height/2
	local min = 200
	local move_x = misc.lengthdir_x(dir,1)
	local move_y = misc.lengthdir_y(dir,1)
	x = x + (move_x * min)
	y = y + (move_y * min)
	local div = 2
	local jump = math.pow(div,5)
	while (jump > 1) do
		local nx = x + (move_x * jump)
		local ny = y + (move_y * jump)
		local margin = 16
		if	((nx >= margin) and (ny > margin) and (nx < width-margin) and (ny < height-margin)) then -- is inside screen
			x = nx
			y = ny
		else
			jump = jump / div
		end
	end
	-- draw scroll
	mw2sd.showScrollIcon(id, x, y)
end

function mw2sd.showScrollIcon(id, x, y)
	local img = image("gfx/Magic\ Wizard\ 2/items/Tile2303_C75_R9.png", x, y, 2, id)
	imagescale(img, 3, 3)
	imagealpha(img, 0)
	tween_scale(img, 1000, 1, 1)
	tween_alpha(img, 1000, 0.75)
	-- fade out
	timer2(4000,{},function()
		tween_alpha(img, 1000, 0)
	end)
	-- remove scroll
	timer2(5000,{},function()
		freeimage(img)
	end)
end

-- OLD CODE --

function mw2sd.tellScrollDirections_DEPRECATED(id,scroll_locations,skipTimer)
		local x = player(id,"tilex")
		local y = player(id,"tiley")
		local ii = 0
		--print("scrolls #"..#scroll_locations)
		while (ii < #scroll_locations) do
				ii = ii + 1
				local sx = scroll_locations[ii][1]
				local sy = scroll_locations[ii][2]
				--msg("x:"..x.." y: "..y)
				--msg("sx:"..sx.." sy: "..sy)
				local dir = misc.point_direction(x,y,sx,sy)
				local dist = misc.point_distance(x,y,sx,sy)
				dist = dist * 2
				dist = math.min(dist,50)
				--msg(dir)
				local draw_x = mw2sd.radar.x+misc.lengthdir_x(dir,dist)
				local draw_y = mw2sd.radar.y+misc.lengthdir_y(dir,dist)
				--print(draw_x..", "..draw_y)
				--msg("x:"..draw_x.." y: "..draw_y)
				mw2sd.player[id][#mw2sd.player[id]+1] = image("gfx/Magic\ Wizard\ 2/items/Tile2303_C75_R9.png",draw_x,draw_y,2,id)
				tween_alpha(mw2sd.player[id][#mw2sd.player[id]],5000,0)
				tween_scale(mw2sd.player[id][#mw2sd.player[id]],1000,0.5,0.5)
		end
		if (skipTimer == nil) then
				local pl = mw2sd.player[id]
				local ii = 0
				while (ii < #pl) do
						ii = ii + 1
						freeimage(pl[ii])
				end
				mw2sd.player[i] = {}
		end
end
