smokeBlink = {}
smokeBlink.smokeWeapon = 53
smokeBlink.smokeDist = 32
smokeBlink.players = {}

addhook("join","smokeBlink.join")
function smokeBlink.join(id)
    smokeBlink.players[id] = nil
end

addhook("say","smokeBlink.say")
function smokeBlink.say(id,text)
    if (text == "!home") then
        smokeBlink.players[id] = {x=player(id,"x"),y=player(id,"y")}
        return 1
    end
end

addhook("projectile","smokeBlink.projectile")
function smokeBlink.projectile(id,weapon,x,y)
    if weapon == smokeBlink.smokeWeapon then
        dist = misc.point_distance(x,y,player(id,"x"),player(id,"y"))
        if (dist <= smokeBlink.smokeDist) then -- tp home
            if (smokeBlink.players[id] == nil) then
                msg2(id,misc.rgb(200,100,200).."please set a home using !home")
            else
                msg2(id,misc.rgb(200,100,200).."you performed a home jump!")
                cs2d.setpos.call(id,smokeBlink.players[id].x,smokeBlink.players[id].y)
            end
        else -- blink
            msg2(id,misc.rgb(200,100,200).."you performed a Ninja blink!")
             cs2d.setpos.call(id,x,y)
        end
    end
end