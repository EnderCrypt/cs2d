mw2ma = {}
mw2ma.gridIndex = 1 -- switches between 1 and 2
mw2ma.otherGridIndex = (3-mw2ma.gridIndex) -- the opposite of previous
mw2ma.updateIndex = 0
mw2ma.doDebug = false
mw2ma.hud = misc.get_hudtxt()
-- updater
mw2ma.counter = 0
mw2ma.updateEvery = 3
-- settings
mw2ma.defaultMultiplier = 1
mw2ma.multiplierScrolls = 1000 --how many scrolls needed for 1 multiplier
mw2ma.scrollMultiplier = (1/mw2ma.multiplierScrolls)
-- size in tiles
mw2ma.gridSize = Point.new(10,10)
-- counts of areas
mw2ma.gridCount = Point.new(const.map.xsize/mw2ma.gridSize.x,const.map.ysize/mw2ma.gridSize.y):floor()
mw2ma.updateList = {} -- direct references to all grids
mw2ma.grid = {}
local x = 0
while (x < const.map.xsize-1) do
    x = x + 1
    mw2ma.grid[x] = {}
    local y = 0
    while (y < const.map.ysize-1) do
        y = y + 1
        mw2ma.grid[x][y] = nil
    end
end

function mw2ma.isValid(p)
    if ((p.x > 0) and (p.y > 0) and (p.x <= mw2ma.gridCount.x) and (p.y <= mw2ma.gridCount.y)) then
        return (not(mw2ma.grid[p.x][p.y] == nil))
    else
        return false
    end
end

function mw2ma.getGridStrengthLocation(p) -- turns a point like (153,43) into (16,5)
    return p:copy():divide(10):ceil()
end

function mw2ma.getGridStrength(p)
    if mw2ma.isValid(p) then
        return mw2ma.grid[p.x][p.y][mw2ma.gridIndex]
    else
        return mw2ma.defaultMultiplier
    end
end

function mw2ma.get(p,id) -- gets the aura from a tile location
    if ((not(id == nil)) and (mw2flags.hasFlag(id,mw2flags.library.contaminated))) then
        return 1/mw2ma.getGridStrength(mw2ma.getGridStrengthLocation(p))
    else
        return mw2ma.getGridStrength(mw2ma.getGridStrengthLocation(p))
    end
end

function mw2ma.addLocation(p1,p2)
    p1:divideP(mw2ma.gridSize)
    p1:ceil()--p1:floor()
    p2:divideP(mw2ma.gridSize)
    p2:ceil()
    local x = p1.x
    while (x < p2.x) do
        x = x + 1
        local y = p1.y
        while (y < p2.y) do
            y = y + 1
            if (mw2ma.grid[x][y] == nil) then
                mw2ma.grid[x][y] = {mw2ma.defaultMultiplier,mw2ma.defaultMultiplier}
                mw2ma.updateList[#mw2ma.updateList+1] = {x,y}
            end
        end
    end
end

function mw2ma.update(percent)
    local to_update = math.ceil((#mw2ma.updateList/100)*percent)
    local i = 0
    while ((i < to_update) and (mw2ma.updateIndex < #mw2ma.updateList)) do
        i = i + 1
        mw2ma.updateIndex = mw2ma.updateIndex + 1
        local loc = mw2ma.updateList[mw2ma.updateIndex]
        local x = loc[1]
        local y = loc[2]
        mw2ma.grid[x][y][mw2ma.otherGridIndex] = misc.average(mw2ma.getGridStrength(Point.new(x,y)),mw2ma.getGridStrength(Point.new(x-1,y-1)),mw2ma.getGridStrength(Point.new(x-1,y)),mw2ma.getGridStrength(Point.new(x-1,y+1)),mw2ma.getGridStrength(Point.new(x+1,y-1)),mw2ma.getGridStrength(Point.new(x+1,y)),mw2ma.getGridStrength(Point.new(x+1,y+1)),mw2ma.getGridStrength(Point.new(x,y+1)),mw2ma.getGridStrength(Point.new(x,y-1)))
    end
    if (mw2ma.doDebug) then
        print("updated "..mw2ma.updateIndex.."/"..#mw2ma.updateList.." chunks")
    end
    if (mw2ma.updateIndex >= #mw2ma.updateList) then -- if true, has finished
        mw2ma.updateIndex = 0
        mw2ma.gridIndex = mw2ma.otherGridIndex
        mw2ma.addLocationPlayerAuras()
        mw2ma.otherGridIndex = (3-mw2ma.gridIndex)
        return true
    end
    return false
end

function mw2ma.auraAdd(p,num)
    if (mw2ma.isValid(p)) then
        mw2ma.grid[p.x][p.y][mw2ma.gridIndex] = mw2ma.grid[p.x][p.y][mw2ma.gridIndex] + num
        return true
    end
    return false
end


function mw2ma.addLocationPlayerAuras()
    local pl = player(0,"tableliving")
    local i = 0
    while (i < #pl) do
        i = i + 1
        local id = pl[i]
        local p = mw2ma.getGridStrengthLocation(Point.new(player(id,"tilex"),player(id,"tiley")))
        -- calc
		local scrolls = 0
		for i=1,mw2.existing_spells do
			scrolls = scrolls + mw2.player.scrollsGot[id][i]
        end
        -- add
        --msg2(id,"you added "..scrolls*mw2ma.scrollMultiplier.." aura to this chunk")
        mw2ma.auraAdd(p,scrolls*mw2ma.scrollMultiplier)
        mw2ma.drawHud(id,p)
    end
end

mw2ma.addLocation(Point.new(0,0),Point.new(149,69)) -- main area
mw2ma.addLocation(Point.new(70,70),Point.new(108,90)) -- the arena entrance
mw2ma.addLocation(Point.new(6,69),Point.new(59,133)) -- the drop down area from the main area

-- start

mw2ma.worker = LuaWorker.new(
    function(worker,workspace)
        if (mw2ma.update(5)) then
            worker:stop()
        end
    end)
mw2ma.worker:setName("mw2ma.worker")
mw2ma.worker:setFrequency(LuaWorkerData.always)

addhook("second","mw2ma.second")
function mw2ma.second()
    mw2ma.counter = mw2ma.counter + 1
    if (mw2ma.counter % mw2ma.updateEvery == 0) then
        --mw2ma.addLocationPlayerAuras(mw2ma.gridIndex)
        mw2ma.worker:start()
    end
end

addhook("movetile","mw2ma.movetile")
function mw2ma.movetile(id,x,y)
    mw2ma.drawHud(id,mw2ma.getGridStrengthLocation(Point.new(x,y)))
end

function mw2ma.drawHud(id,location)
    local text = misc.rgb(255,50,255).."Aura: "
    if (mw2ma.isValid(location)) then
        --"("..p.x..", "..p.y..") Aura: "..misc.round(mw2ma.getGridStrength(p),2)
        text = text..mw2ma.icon(id,mw2ma.getGridStrength(location))--misc.rgb(0,255,0).." ("..location.x..", "..location.y..")"
    else
        text = text..misc.rgb(100,100,100).."None"
    end
    misc.hudText(mw2ma.hud,id,text,5,241,const.align.left)
end

function mw2ma.icon(id,aura)
    local percent = (100/mw2ma.defaultMultiplier)*aura
    if (percent >= 100) then
        local rank = math.floor(percent/100)
        local max_icons = 5
        local reminder = (percent-(rank*100))
        local count = math.floor(reminder/(1/(max_icons+1)*100))
        --msg("rank: "..rank.." <> percent: "..percent)
        return misc.rgb(50,255,50)..(string.char(64+rank)).." "..misc.rgb(0,255,0)..string.rep("+",count)
    else
        return misc.rgb(255,0,0)..string.rep("-",math.floor(math.abs(percent-100)/10))
    end
end

-- rcon live exec msg(mw2ma.getGridStrengthLocation(Point.new(player(6,"tilex"),player(6,"tiley"))).."Aura")

function mw2ma.reportAuraStability(id) --NOT IN USE
    local p = mw2ma.getGridStrengthLocation(Point.new(player(id,"tilex"),player(id,"tiley")))
    local x = p.x
    local y = p.y
    local surrounding = misc.average(mw2ma.getGridStrength(Point.new(x-1,y-1)),mw2ma.getGridStrength(Point.new(x-1,y)),mw2ma.getGridStrength(Point.new(x-1,y+1)),mw2ma.getGridStrength(Point.new(x+1,y-1)),mw2ma.getGridStrength(Point.new(x+1,y)),mw2ma.getGridStrength(Point.new(x+1,y+1)),mw2ma.getGridStrength(Point.new(x,y+1)),mw2ma.getGridStrength(Point.new(x,y-1)))
    local here = mw2ma.getGridStrength(Point.new(x,y))
    local stability = (100/here)*surrounding
    if (stability > 100) then
        stability = 100/(stability/100) -- wrap around
    end
    msg("stability report: "..misc.round(stability,1).."%")
end
