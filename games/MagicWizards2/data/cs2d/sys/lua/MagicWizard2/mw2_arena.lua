mw2.arena = {}
mw2.arena.fee = 10
mw2.arena.noDamageTimer = 10
mw2.arena.savefile = "sys/lua/MagicWizard2/arena_save.txt"
SaveEngine.open(mw2.arena.savefile)
mw2.arena.money = SaveEngine.read(mw2.arena.savefile,"Data","Money",0)
mw2.arena.weapons_container = {}
--mw2.arena.deathTransfer = 10
mw2.location = {}
-- mw2.location.trofe_x = 89
-- mw2.location.trofe_y = 80

-- mw2.location.tp_back_y = 176

-- mw2.location.arena_size_x = 100

mw2.location.trofe_x = 89
mw2.location.trofe_y = 80

mw2.location.tp_back_y = 176

mw2.location.arena_size_x = 100

addhook("movetile","mw2.arena.movetile")
function mw2.arena.movetile(id,x,y)
	if (y == mw2.location.tp_back_y) and (x < mw2.location.arena_size_x) then
		msg(misc.rgb(0,255,0)..player(id,"name").." Has Left the arena!")
		--mw2.player.weaponUse[id] = mw2.player.weaponUse[id] + 5
		--msg2(id,misc.rgb(0,255,0).."Lost some honor!")
		local dir = math.random(1,360)
		local px = mw2.location.trofe_x + misc.lengthdir_x(dir,2)
		local py = mw2.location.trofe_y + misc.lengthdir_y(dir,2)
		cs2d.setpos.call(id,misc.tile_to_pixel(px),misc.tile_to_pixel(py))
	end
	if  (x == mw2.location.trofe_x) and (y == mw2.location.trofe_y) then
        if (player(id,"bot")) then
            mw2.player.money[id] = mw2.player.money[id] + (mw2.arena.fee-(math.ceil(mw2.honor(id)/(100/mw2.arena.fee))))
            mw2.arena.joinArena(id,1)
        else
            mw2.arena.arenaPrompt(id)
        end
	end
end

function mw2.arena.arenaPrompt(id)
	cost = mw2.arena.fee-(math.ceil(mw2.honor(id)/(100/mw2.arena.fee)))
    if (cost < 0) then
        cost = 0
    end
	local yesB = "Yes (Costs "..cost..")"
	if mw2.player.money[id] < (cost+mw2.arena.fee) then
		yesB = "(Yes (Costs "..cost.." (+"..mw2.arena.fee.."))|you are required to have atleast "..(cost+mw2.arena.fee).." money))"
	end
	sme.createMenu(id,mw2.arena.joinArena,sme.noProcess,"Enter arena?",true,{yesB,"No"})
end

function mw2.arena.joinArena(id,button)
	if (button == 1) then
        cost = mw2.arena.fee-(math.ceil(mw2.honor(id)/(100/mw2.arena.fee)))
		if (player(id,"bot") or (mw2.player.money[id] >= cost+1)) then
			parse("equip "..id.." 79")
			if (cost < 0) then
                cost = 0
            end
			mw2.player.money[id] = mw2.player.money[id] - cost
			mw2.arena.money = mw2.arena.money + cost
			SaveEngine.write(mw2.arena.savefile,"Data","Money",mw2.arena.money)
			SaveEngine.save(mw2.arena.savefile)
			cs2d.setpos.call(id,misc.tile_to_pixel(math.random(1,mw2.location.trofe_x)),misc.tile_to_pixel(180))
			msg2(id,misc.rgb(255,0,0).."You Entered the ARENA(PVP ZONE!) good luck")
			msg(misc.rgb(255,0,0)..player(id,"name").." has entered the arena! (for a cost of "..cost..")")
			mw2.drawMoney(id)
			print("Arena now owns "..mw2.arena.money.." money")
		end
	end
end

addhook("say","mw2.arena.say")
function mw2.arena.say(id,text)
	if text == "!arena" then
        if (player(id,"tiley") < mw2.location.tp_back_y) then
        	if (mw2flags.hasFlag(id,mw2flags.library.inCombat)) then
        		msg2(id,misc.rgb(255,0,0).."You cannot enter the arena as you are currenly in combat")
        		return
	        end
	        mw2.arena.arenaPrompt(id)
        end
	end
end

addhook("build","mw2.arena.build")
function mw2.arena.build(id,typ,x,y,mode,objectid)
	if (typ == 13) or (typ == 14) then
		return 1
	end
end

addhook("die","mw2.arena.die")
function mw2.arena.die(victim,killer,weapon,x,y)
    --[[
    print("victim: "..victim)
    print("killer: "..killer)
    print("weapon: "..weapon)
    print("x: "..x)
    print("y: "..y)
    ]]--
	local x = player(victim,"tilex")
	local y = player(victim,"tiley")
	if (x < 100) and (y > mw2.location.tp_back_y) then
        local money = mw2.player.money[victim]
        local gain = 1
        local drop = 10
        while (money >= drop) do
            money = money - drop
            gain = gain + 1
            drop = drop + 10
        end
        gain = math.max(10,gain)
        --[[
        local gain = 10--math.ceil(mw2.player.money[victim]/100)
        if (killer > 0) then
            gain = 10--math.min(mw2.player.money[victim],mw2.player.money[killer])
            gain = gain+math.ceil(gain/100)
        end
        ]]--
        -- perform
        mw2.player.money[victim] = mw2.player.money[victim] - gain
        msg2(victim,misc.rgb(255,0,0).."You dropped "..gain.." money by getting killed")
        if (mw2.items.spawnMoneyBag(x,y,victim,gain) == false) then
            if (killer > 0) then
                if not (killer == victim) then
                    msg2(killer,misc.rgb(0,255,0).."You gained "..gain.." money by killing "..player(victim,"name"))
                    mw2.player.money[killer] = mw2.player.money[killer] + gain
                    mw2.drawMoney(killer)
                end
            end
        end
		mw2.drawMoney(victim)
		--mw2.items.spawn(3,x,y)
		
		local dir = math.random(1,360)
		local px = mw2.location.trofe_x + misc.lengthdir_x(2,dir)
		local py = mw2.location.trofe_y + misc.lengthdir_y(2,dir)
		mw2.arena.weapons_container[victim] = playerweapons(victim)
		timer(1,"parse","spawnplayer "..victim.." "..misc.tile_to_pixel(px).." "..misc.tile_to_pixel(py))
		timer(100,"mw2.arena.returnWeps",victim)
	end
end

function mw2.arena.returnWeps(id)
	id = tonumber(id)
	for i=1,#mw2.arena.weapons_container[id] do
		--msg(mw2.arena.weapons_container[id][i])
		parse("equip "..id.." "..mw2.arena.weapons_container[id][i])
	end
end