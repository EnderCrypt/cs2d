mw2flags = {}
mw2flags.hud = misc.get_hudtxt()
mw2flags.hook = {}

mw2flags.const = {}
mw2flags.const.infinite = {}
mw2flags.const.temporary = {}

mw2flags.flags = {} -- contains all flags [INTERNAL]
mw2flags.player = {} -- contains all player & active flags [INTERNAL]
mw2flags.library = {} -- contains all public flags

--[[ HOOKS ]]--

addhook("hit","mw2flags.hook.hit")
function mw2flags.hook.hit(id,source,weapon,hpdmg,apdmg)
	if (mw2flags.hasFlag(id,mw2flags.library.immortal)) then
        return 1
    end
    if (not(weapon == 251)) then --explosion
        if (mw2flags.hasFlag(id,mw2flags.library.weaponImmunity)) then
            return 1
        end
    end
    if (player(source,"exists")) then
        if (mw2flags.hasFlag(source,mw2flags.library.deathDoor) or mw2flags.hasFlag(source,mw2flags.library.deathChannel)) then -- zombies and deathChannel infect others with plauge
            mw2flag.new(id,source,mw2flags.library.deathDoor,60+(math.random()*60))
        end
        if (mw2flags.hasFlag(source,mw2flags.library.reanimated)) then -- zombies infect others with plauge (strong)
            mw2flag.new(id,source,mw2flags.library.deathDoor,60*5)
        end
        if (mw2flags.hasFlag(id,mw2flags.library.reanimated)) then -- makes it impossible for reanimated to hurt their source (of infection)
            if (mw2flags.isSource(source,id,mw2flags.library.reanimated)) then
                return 1
            end
        end
    end
end

addhook("join","mw2flags.hook.join")
function mw2flags.hook.join(id)
    mw2flags.resetPlayer(id)
    mw2flag.new(id,0,mw2flags.library.dead,mw2flags.const.infinite)
end

addhook("spawn","mw2flags.hook.spawn")
function mw2flags.hook.spawn(id)
    mw2flags.playerDie(id)
    mw2flag.new(id,0,mw2flags.library.immortal,5)
end

addhook("die","mw2flags.hook.die")
function mw2flags.hook.die(victim,killer,weapon,x,y)
    local flags_categories = mw2flags.player[victim]
    for i=1,#mw2flags.flags do -- get each flag category
        if (#flags_categories[i] > 0) then
            mw2flags.flags[i].onDeath(victim)
        end
    end
    mw2flags.playerDie(victim)
    mw2flag.new(victim,0,mw2flags.library.dead,mw2flags.const.infinite)
end

addhook("say","mw2flags.hook.say")
function mw2flags.hook.say(id,text)
    if (text == "!flags") then
        msg2(id,"--[[ Current status effects ]]--")
        local flags_categories = mw2flags.player[id]
        local flags_count = 0
        for i=1,#mw2flags.flags do -- get each flag category
            flags_count = flags_count + #flags_categories[i]
            if (#flags_categories[i] > 0) then
                local flag = mw2flags.getBiggestFlag(id,i)
                print("mw2_flags: "..tostring(flag))
                msg2(id,flag:getString()..misc.rgb(255,220,0).." "..misc.rgb(100,100,100).."\""..flag.flag.name.."\"")
                msg2(id,misc.rgb(200,200,200)..": "..flag.flag.description)
            end
        end
        if (flags_count == 0) then
            msg2(id,"You currently have no active flags")
        end
        return 1
    end
    if (text == "!all_flags") then
        msg2(id,"--[[ Available status effects ]]--")
        for i=1,#mw2flags.flags do -- get each flag category
            local flag = mw2flags.flags[i]
            msg2(id,flag.index..". "..flag.color..flag.acronym..misc.rgb(255,220,0).." "..misc.rgb(100,100,100).."\""..flag.name.."\"")
            msg2(id,misc.rgb(200,200,200)..": "..flag.description)
        end
        return 1
    end
	if (mw2flags.hasFlag(id,mw2flags.library.mute)) then
        msg2(id,misc.rgb(255,0,0).."You try to speak, but cant!")
        return 1
    end
end

function mw2flags.hook.second(pl)
    for i=1,#pl do
        local id = pl[i]
        local x = player(id,"tilex")
        local y = player(id,"tiley")
        if ((x > 173) and (y > 112) and (x < 192) and (y < 130)) then
            mw2flag.new(id,0,mw2flags.library.immortal,mw2flags.const.temporary)
        end
    end
end

--[[ MAIN CODE ]]--

function mw2flags.resetPlayer(id)
    mw2flags.player[id] = {}
    for i=1,#mw2flags.flags do
        mw2flags.player[id][i] = {}
    end
end

function mw2flags.playerDie(id)
    for i=1,#mw2flags.flags do
        local afterDeath = mw2flags.flags[i].afterDeath
        if (afterDeath == false) then
            mw2flags.player[id][i] = {}
        end
    end
end

function mw2flags.create(arguments) --name,acronym,color,description,onDeath,onDispose
    local flag = {}
    flag.index = #mw2flags.flags+1
    flag.name = arguments.name
    flag.acronym = arguments.acronym
    if (flag.acronym == nil) then
        flag.acronym = flag.name
    end
    flag.afterDeath = arguments.afterDeath
    if (flag.afterDeath == nil) then
        flag.afterDeath = false
    end
    flag.color = arguments.color
    flag.description = arguments.description
    flag.cancels = {}
    --end
    flag.onDeath = arguments.onDeath -- occurs when the player dies
    if (flag.onDeath == nil) then
        flag.onDeath = function()end
    end
    flag.onDispose = arguments.onDispose -- occurs when (the last of this kind of flag) is deleted
    if (flag.onDispose == nil) then
        flag.onDispose = function()end
    end
    flag.onSecond = arguments.onSecond -- occurs every second
    if (flag.onSecond == nil) then
        flag.onSecond = function()end
    end
    mw2flags.flags[flag.index] = flag
    return flag.index
end

function mw2flags.addCancellations(index,cancels)
    mw2flags.flags[index].cancels = cancels
end

addhook("second","mw2flags.update")
function mw2flags.update()
    local pl = player(0,"table")
    mw2flags.hook.second(pl)
    for i=1,#pl do
        local id = pl[i]
        if (mw2flags.player[id] == nil) then
            break
        end
        local display_string = ""
        local flags_categories = mw2flags.player[id]
        local has_flags = false
        for ii=1,#mw2flags.flags do -- get each flag category
            local flags = flags_categories[ii]
            local biggest_counter = 0
            local biggest = nil
            if (#flags > 0) then
                mw2flags.flags[ii].onSecond(id)
            end
            local iii = 0
            while (iii < #flags) do
                iii = iii + 1
                local flag = flags[iii]
                if (flag.infinite) then
                    display_string = display_string..flag:getString()
                    has_flags = true
                    biggest = nil
                    break
                else
                    flag.counter = flag.counter - 1
                    if (flag.counter <= 0) then
                        flags[iii]:stop()
                        iii = iii - 1
                    else
                        if (flag.counter > biggest_counter) then
                            flag.counter = flag.counter
                            has_flags = true
                            biggest = flag
                        end
                    end
                end
            end
            if (not(biggest == nil)) then
                display_string = display_string..biggest:getString()
            end
        end
        mw2flags.output(id,display_string,has_flags)
    end
end

addhook("rcon","mw2flags.rcon")
function mw2flags.rcon(cmds,id,ip,port)
    local cmd = misc.commandSplit(cmds)
    if (cmd[1] == "flag") then
        if (cmd[2] == "add") then
            --mw2flag.new(id,0,mw2flags.library.immortal,mw2flags.const.temporary)
            local flag = mw2flags.library[cmd[3]]
            if (flag == nil) then
                msg2(id,"Invalid command (flag add <flag> <id> <timer> [source-id])")
                return 1
            end
            local to = tonumber(cmd[4])
            if (to == nil) then
                to = id
            end
            local timer = tonumber(cmd[5])
            if (timer == nil) then
                timer = 60
            end
            local src = tonumber(cmd[6])
            if (src == nil) then
                src = 0
            end
            mw2flag.new(to,src,flag,timer)
        elseif (cmd[2] == "remove") then

        end
        return 1
    end
    return 0
end

function mw2flags.draw(id)
    local display_string = ""
    local flags_categories = mw2flags.player[id]
    local has_flags = false
    for ii=1,#mw2flags.flags do -- get each flag category
        local flags = flags_categories[ii]
        local biggest_counter = 0
        local biggest = nil
        local iii = 0
        while (iii < #flags) do
            iii = iii + 1
            local flag = flags[iii]
            if (flag.infinite or flag.temporary) then
                display_string = display_string..flag:getString()
                has_flags = true
                biggest = nil
                break
            else
                if (flag.counter > biggest_counter) then
                    flag.counter = flag.counter
                    has_flags = true
                    biggest = flag
                end
            end
        end
        if (not(biggest == nil)) then
            display_string = display_string..biggest:getString()
        end
    end
    mw2flags.output(id,display_string,has_flags)
end

function mw2flags.output(id,text,has_flags)
    local output = misc.rgb(0,200,0).."Flags: "
    if (has_flags) then
        output = output..text
    else
        output = output..misc.rgb(100,100,100).."None"
    end
    misc.hudText(mw2flags.hud,id,output,5,228,const.align.left)
end

function mw2flags.cancellationCheck(id)
    local flags_categories = mw2flags.player[id]
    for i=1,#mw2flags.flags do -- get each flag category
        if (#flags_categories[i] > 0) then
            local cancels = mw2flags.flags[i].cancels
            for ii=1,#cancels do
                --local cancel = cancels[ii]
                mw2flags.player[id][cancels[ii]] = {}
            end
        end
    end
end

function mw2flags.hasFlag(id,flag)
    return ((#mw2flags.player[id][flag]) > 0)
end

function mw2flags.isSource(id,source,flag_id) -- checks if source, is one of the creators of the flag flag_id in player id
    local flags = mw2flags.player[id][flag_id]
    local i = 0
    while (i < #flags) do
        i = i + 1
        local flag = flags[i]
        if (flag.source == source) then
            return true
        end
    end
    return false
end

function mw2flags.getBiggestFlag(id,flag_id)
    local biggest_counter = 0
    local biggest = nil
    local flags = mw2flags.player[id][flag_id]
    local i = 0
    while (i < #flags) do
        i = i + 1
        local flag = flags[i]
        if (flag.infinite or flag.temporary) then
            return flag
        else
            if (flag.counter >= biggest_counter) then
                biggest_counter = flag.counter
                biggest = flag
            end
        end
    end
    return biggest
end

mw2flag = {}
Class(mw2flag,function(id,source,flag_id,counter)
    self.id = id
    self.source = source -- source id
    self.flag = mw2flags.flags[flag_id]
    if (self.flag == nil) then
        error("unknown flag id: "..flag_id)
    end
    self.infinite = false
    self.temporary = false
    if (counter == mw2flags.const.infinite) then
        self.infinite = true
    elseif (counter == mw2flags.const.temporary) then
        self.temporary = true
        self.counter = 2
    else
        self.defaultCounter = misc.round(counter)
        self.counter = self.defaultCounter
    end
    self.index = (#mw2flags.player[id][flag_id])+1
    mw2flags.player[id][flag_id][self.index] = self
    mw2flags.cancellationCheck(id)
    mw2flags.draw(id)
end)

function mw2flag:reset()
    self.counter = self.defaultCounter
    mw2flags.draw(self.id)
end

function mw2flag:getTime()
    if (self.infinite) then
        return "Infinite"
    elseif (self.temporary) then
        return "Temp"
    else
        local s = self.counter
        local m = math.floor(s/60)
        s = s - (m*60)
        local text = s.."s"
        if (m > 0) then
            text = m.."m"..text
        end
        return text
    end
end

function mw2flag:getString()
    return self.flag.color.."["..self.flag.acronym.." "..self:getTime().."]"
end

function mw2flag:stop()
    local id = self.id
    local flag_type = self.flag.index
    local index = self.index
    if (#mw2flags.player[id][flag_type] == 1) then
        mw2flags.flags[flag_type].onDispose(id)
    end
    mw2flags.player[id][flag_type][index] = mw2flags.player[id][flag_type][#mw2flags.player[id][flag_type]]
    mw2flags.player[id][flag_type][#mw2flags.player[id][flag_type]] = nil
    if (not(mw2flags.player[id][flag_type][index] == nil)) then
        mw2flags.player[id][flag_type][index].index = index
    end
end

--[[ STANDARD FLAGS ]]-- (http://crawl.chaosforge.org/Status_effects)
--[[
lua mw2flag.new(1,0,mw2flags.library.,60)
]]--

--rcon live exec mw2flag.new(6,0,mw2flags.library.mute,300)
mw2flags.library.mute = mw2flags.create({
    name = "Muted",
    color = misc.rgb(255,0,50),
    description = "You been muted, your mouth wont open!",
    afterDeath = true
    })

mw2flags.library.fireStorm = mw2flags.create({
    name = "Fire Storm",
    acronym = "FStrm",
    color = misc.rgb(255,0,50),
    description = "A powerfull fire storm surrounds you",
    afterDeath = true,
    onSecond = function(id)
        local maxDist = 5
        for i=1,3 do
            local spells = {mw2.name.FireCharge,mw2.name.FireBall,mw2.name.explosiveShockwave,mw2.name.ExplosiveWind,mw2.name.FireFly}
            spell = spells[math.random(#spells)]
            local dir = math.random(360)
            local x = player(id,"tilex")
            local y = player(id,"tiley")
            local x_add = misc.lengthdir_x(dir,1)
            local y_add = misc.lengthdir_y(dir,1)
            local tiles = 0
            while ((tile(x,y,"wall") == false) and (tiles < (maxDist+1))) do
                tiles = tiles + 1
                x = x + x_add
                y = y + y_add
            end
            tiles = tiles - 1
            x = x - x_add
            y = y - y_add
            local inst = mw2.spawnProjectile(spell,misc.tile_to_pixel(x),misc.tile_to_pixel(y),0,dir-180)
        end
    end
    })

mw2flags.library.iceStorm = mw2flags.create({
    name = "Ice Storm",
    acronym = "IStrm",
    color = misc.rgb(255,0,50),
    description = "A powerfull ice storm surrounds you",
    afterDeath = true,
    onSecond = function(id)
        local maxDist = 5
        for i=1,3 do
            local spells = {mw2.name.IceCharge,mw2.name.IceBall,mw2.name.iceShockwave,mw2.name.FrozenWind,mw2.name.IceFly}
            spell = spells[math.random(#spells)]
            local dir = math.random(360)
            local x = player(id,"tilex")
            local y = player(id,"tiley")
            local x_add = misc.lengthdir_x(dir,1)
            local y_add = misc.lengthdir_y(dir,1)
            local tiles = 0
            while ((tile(x,y,"wall") == false) and (tiles < (maxDist+1))) do
                tiles = tiles + 1
                x = x + x_add
                y = y + y_add
            end
            tiles = tiles - 1
            x = x - x_add
            y = y - y_add
            local inst = mw2.spawnProjectile(spell,misc.tile_to_pixel(x),misc.tile_to_pixel(y),0,dir-180)
        end
    end
    })

mw2flags.library.dead = mw2flags.create({
    name = "Dead",
    color = misc.rgb(50,50,50),
    description = "You are dead..."
    })

mw2flags.library.immortal = mw2flags.create({
    name = "Immortal",
    color = misc.rgb(100,50,255),
    description = "You are 100% immortal and cant be killed by anything (natural...)"
    })

mw2flags.library.weaponImmunity = mw2flags.create({
    name = "Weapon Immune",
    acronym = "WpnImne",
    color = misc.rgb(100,50,255),
    description = "You are 100% immune against weapons"
    })

mw2flags.library.bleeding = mw2flags.create({
    name = "Bleeding",
    acronym = "Bldn",
    color = misc.rgb(200,50,50),
    description = "Your bleeding",
    onSecond = function(id)
        mw2.damagePlayer(0,id,5,"Bleeding")
    end
    })

mw2flags.library.christmas = mw2flags.create({
    name = "Christmas",
    acronym = "Chms",
    color = misc.rgb(0,255,25),
    description = "Happy christmas, you get free money!",
    onSecond = function(id)
        local x = player(id,"tilex")
        local y = player(id,"tiley")
        if (math.random() < 0.25) then
            mw2.items.spawn(mw2.items.names.copper.id,x,y)
        end
        if (math.random() < 0.25) then
            mw2.items.spawn(mw2.items.names.copper10.id,x,y)
        end
        if (math.random() < 0.1) then
            mw2.items.spawn(mw2.items.names.silver.id,x,y)
        end
        if (math.random() < 0.05) then
            mw2.items.spawn(mw2.items.names.gold.id,x,y)
        end
    end
    })

mw2flags.library.healing = mw2flags.create({
    name = "Healing",
    acronym = "Heal",
    color = misc.rgb(0,255,25),
    description = "Regenerating extra health points",
    onSecond = function(id)
        mw2.damagePlayer(0,id,-10,0)
    end
    })

mw2flags.library.teleportPrevention = mw2flags.create({
    name = "Teleport Prevention",
    acronym = "-Tele",
    color = misc.rgb(200,50,50),
    description = "Prevents you from using any kind of teleporting spells"
    })

mw2flags.library.confused = mw2flags.create({
    name = "Confused",
    acronym = "Conf",
    color = misc.rgb(200,50,50),
    description = "Your confused, casting magic might occaionally fail"
    })

mw2flags.library.contaminated = mw2flags.create({
    name = "Contaminated",
    acronym = "Contam",
    color = misc.rgb(100,100,100),
    description = "too much magic has contaminated your body, mana barely generates."
    })

mw2flags.library.innerFlame = mw2flags.create({
    name = "Inner Flame",
    acronym = "inrFlme",
    color = misc.rgb(255,0,50),
    description = "You are filled with an inner flame",
    onDeath = function(id)
        misc.explosion(player(id,"x"),player(id,"y"),250,250,id)
    end
    })

mw2flags.library.decay = mw2flags.create({
    name = "Decay",
    acronym = "Dcy",
    color = misc.rgb(255,255,0),
    description = "Your body is decaying quickly!",
    onDispose = function(id)
        parse("customkill 0 \"Decay\" "..id)
    end
    })

mw2flags.library.deathChannel = mw2flags.create({
    name = "Death Channel",
    acronym = "DChan",
    color = misc.rgb(255,0,230),
    description = "Anyone you hit will get reanimated into an unnatural human on death"
    })

mw2flags.library.deathDoor = mw2flags.create({
    name = "Death's Door",
    acronym = "DDoor",
    color = misc.rgb(255,0,230),
    description = "You'll be instantly reanimated if you where to die",
    onDeath = function(id)
        local source = mw2flags.getBiggestFlag(id,mw2flags.library.deathDoor).source
        local text = ""
        if (source > 0) then
            text = " by "..player(source,"name")
        end
        msg(misc.rgb(255,0,230)..player(id,"name").." reanimated"..text)
        timer2(1,{id},function(id)
            parse("spawnplayer "..id.." "..player(id,"x").." "..player(id,"y"))
        end)
        timer2(2,{id,source},function(id,source)
            mw2flag.new(id,source,mw2flags.library.reanimated,30+(math.random()*30))
        end)
    end,
    onSecond = function(id)
        misc.effect("flare",player(id,"x"),player(id,"y"),3,3,255,0,230)
    end
    })

mw2flags.library.reanimated = mw2flags.create({
    name = "Reanimated",
    acronym = "Reanim",
    color = misc.rgb(255,120,230),
    description = "You have reanimated and dying quickly, you cannot hurt your reanimator",
    onDispose = function(id)
        parse("customkill 0 \"De-Reanimation\" "..id)
    end,
    onSecond = function(id)
        misc.effect("flare",player(id,"x"),player(id,"y"),10,20,255,120,230)
    end
    })

mw2flags.library.inCombat = mw2flags.create({
    name = "Combat",
    acronym = "Cmbt",
    color = misc.rgb(255,0,0),
    description = "You are in active combat"
    })

--[[ Add Cancellations ]]--

mw2flags.addCancellations(mw2flags.library.contaminated,{
    mw2flags.library.innerFlame
    })

mw2flags.addCancellations(mw2flags.library.healing,{
    mw2flags.library.decay,
    mw2flags.library.bleeding
    })

mw2flags.addCancellations(mw2flags.library.deathChannel,{
    mw2flags.library.reanimated
    })

mw2flags.addCancellations(mw2flags.library.reanimated,{
    mw2flags.library.deathDoor,
    mw2flags.library.healing
    })
