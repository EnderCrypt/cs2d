mw2.items = {}
mw2.items.list = {}
mw2.items.grid = {}
mw2.items.itemsRarity = {}
mw2.items.count = {}
mw2.items.minute_count = 0
mw2.items.do_limit_items = false -- if the item limit should be used
mw2.items.names = {}
mw2.hud_id = misc.get_hudtxt()

mw2.items.grid_list = {}


function mw2.items.addItem(name,rarity,image,maxCount,onSpawn,onTake)
	local index = #mw2.items.list+1
	mw2.items.count[index] = 0
	local temp = {}
	temp.id = index
	temp.name = name
	temp.rarity = rarity
    misc.addServerTransfer("gfx/Magic\ Wizard\ 2/items/"..image..".png")
	temp.image = image
	temp.maxCount = maxCount
	temp.onSpawn = onSpawn
	temp.onTake = onTake
	-- rarity --

	local i = #mw2.items.itemsRarity
	local stop = #mw2.items.itemsRarity + rarity
	while (i < stop) do
		i = i + 1
		mw2.items.itemsRarity[i] = index
	end
	-- indexing --
	mw2.items.list[index] = temp
    return temp
end

dofile("sys/lua/MagicWizard2/items/mw2_items.lua")

function mw2.items.spawnMoneyBag(x,y,id,money)
	local success, item = mw2.items.spawn(mw2.items.names.moneyBag.id,x,y)
    if (success) then
        item.money = money
        item.id = id
        item.ip = player(id,"ip") -- used for verifying owner
        item.ownerName = player(id,"name")
    end
    return success
end

function mw2.items.getItemSpawnCountMoneyBag(x,y,from,money)
    local success, item = mw2.items.spawn(mw2.items.names.moneyBag.id,x,y)
    if (success) then
        item.money = money
        item.id = from
        item.ip = player(from,"ip") -- used for verifying owner
        item.ownerName = player(from,"name")
        return true
    end
    return false
end

function mw2.items.SuperSpawn(times)
	if (times == nil) then
		times = 100
	end
    local i = 0
    while (i < times) do
        i = i + 1
        mw2.items.SpawnRandom()
    end
end

function mw2.items.SpawnRandom()
	local index = mw2.items.itemsRarity[math.random(#mw2.items.itemsRarity)]
	local temp = {}
	x = math.random(1,map("xsize"))
	y = math.random(1,map("ysize"))
	local timeout = 0
	while ((entity(x,y,"exists") == true) or (tile(x,y,"walkable") == false) or (tile(x,y,"frame") == 0) or (not(mw2.items.grid[x.."|"..y] == nil))) do
		timeout = timeout + 1
		if timeout == 50 then
			break
		end
		x = math.random(1,map("xsize"))
		y = math.random(1,map("ysize"))
	end
	if timeout < 50 then
        if (mw2.items.spawn(index,x,y) == false) then
            print("spawnitem limit reached")
        end
	else
		print("spawnitem timeout")
	end
end

function mw2.items.spawn(index,x,y)
	if ((entity(x,y,"exists") == false) and (tile(x,y,"walkable") == true) and (tile(x,y,"frame") > 0) and (mw2.items.grid[x.."|"..y] == nil)) then
		if ((mw2.items.do_limit_items) and (mw2.items.list[index].maxCount > 0) and (mw2.items.count[index] > mw2.items.list[index].maxCount-1)) then
			return false
		end
        local temp = {}
		temp.image = image("gfx/Magic Wizard 2/items/"..mw2.items.list[index].image..".png",misc.tile_to_pixel(x),misc.tile_to_pixel(y),0)
		temp.item = mw2.items.list[index]
		temp.x = x
		temp.y = y
		mw2.items.grid[x.."|"..y] = temp
		mw2.items.count[index] = mw2.items.count[index] + 1
		mw2.items.list[index].onSpawn(temp)
        -- add to list
        local temp2 = {}
        temp2.item_reference = mw2.items.list[index]
        temp2.x = x
        temp2.y = y
        mw2.items.grid_list[#mw2.items.grid_list+1] = temp2
        return true, temp
    else
        return false
	end
end

function mw2.items.spawnFloating(index,x,y,timeout)
    while (true) do
        if ((entity(x,y,"exists") == false) and (tile(x,y,"walkable") == true) and (tile(x,y,"frame") > 0) and (mw2.items.grid[x.."|"..y] == nil)) then
            break
        end
        if (timeout <= 0) then
            return false
        end
        timeout = timeout - 1
        x = x + misc.round(math.random()*2)-1
        y = y + misc.round(math.random()*2)-1
    end
    mw2.items.spawn(index,x,y)
    return true
end

addhook("movetile","mw2.items.movetile")
function mw2.items.movetile(id,x,y)
	if not (mw2.items.grid[x.."|"..y] == nil) then
		local index = mw2.items.grid[x.."|"..y].item.id
		action = mw2.items.grid[x.."|"..y].item.onTake(id,mw2.items.grid[x.."|"..y])
        if not (action == false) then
            parse("sv_sound2 "..id.." items/pickup.wav")
            mw2.items.count[index] = mw2.items.count[index] - 1
            msg2(id,"Picked up "..mw2.items.grid[x.."|"..y].item.name)
            freeimage(mw2.items.grid[x.."|"..y].image)
            mw2.items.grid[x.."|"..y] = nil
            -- remove reference
            local i = 0
            while (i < #mw2.items.grid_list) do
                i = i + 1
                local item_ref = mw2.items.grid_list[i]
                if ((item_ref.x == x) and (item_ref.y == y)) then
                    mw2.items.grid_list[i] =  mw2.items.grid_list[#mw2.items.grid_list]
                    mw2.items.grid_list[#mw2.items.grid_list] = nil
                    --print("there are "..#mw2.items.grid_list.." items")
                end
            end
        end
	end
end

--[[
addhook("minute","mw2.items.minute")
function mw2.items.minute()
    local multiplier = 1
    -- mutiplier
    mw2.items.minute_count = mw2.items.minute_count + 1
    if (mw2.items.minute_count % (60*5) == 0) then
        multiplier = 25
    else
        if (mw2.items.minute_count % 60 == 0) then
            multiplier = 5
        else
            if (mw2.items.minute_count % 10 == 0) then
                multiplier = 2
            end
        end
    end
    if (multiplier > 1) then
        msg(misc.rgb(0,255,0).."x"..multiplier.." TIMES AS MUCH ITEMS just appered!")
    end
    --
    toSpawn = mw2.items.getItemSpawnCount(multiplier)
	for i=1,toSpawn do
		mw2.items.SpawnRandom()
	end
end
]]--

addhook("minute","mw2.items.minute")
function mw2.items.minute()
    local multiplier = 1
    -- mutiplier
    mw2.items.minute_count = mw2.items.minute_count + 1
    if (mw2.items.minute_count % (60*5) == 0) then
        multiplier = 25
    else
        if (mw2.items.minute_count % 60 == 0) then
            multiplier = 5
        else
            if (mw2.items.minute_count % 10 == 0) then
                multiplier = 2
            end
        end
    end
    if (multiplier > 1) then
        msg(misc.rgb(0,255,0).."x"..multiplier.." TIMES AS MUCH ITEMS just appered!")
    end
    --
    toSpawn = mw2.items.getItemSpawnCount(multiplier)
	for i=1,toSpawn do
		mw2.items.SpawnRandom()
	end
end

function mw2.items.getItemSpawnCount(multiplier)
    local toSpawn = 0
	local pl = player(0,"tableliving")
    for i=1,#pl do
        local id = pl[i]
        if player(id,"bot") then
            toSpawn = toSpawn + 0.2
        else
            toSpawn = toSpawn + 1
        end
    end
    return math.floor(toSpawn*multiplier)
end

function mw2.items.giveScroll(id)
	id = tonumber(id)

    mw2.addScrollRankCheck(id)

	local list = {}
	for i=1,mw2.existing_spells do
		local scrolls_left = mw2.magicks.scrollsNeeded[i]-mw2.player.scrollsGot[id][i]
		if scrolls_left > 0 then
			local scrolls_got = mw2.player.scrollsGot[id][i]+1
			local scrolls_total = mw2.magicks.scrollsNeeded[i]
			scrolls_left = scrolls_left + 1
			local inc = 100/scrolls_total
			local stop = math.ceil(scrolls_got*inc)
			local start = #list
			local ii = start
			while (ii < start+stop) do
				ii = ii + 1
				list[ii] = i
				--msg("chance for "..mw2.magicks.spellsData[i].name)
			end
		end
	end

	if #list > 0 then
		local ind = math.random(1,#list)
		msg(misc.rgb(0,0,255)..player(id,"name").." Received a scroll for "..mw2.magicks.spellsData[list[ind]].name)
		mw2.player.scrollsGot[id][list[ind]] = mw2.player.scrollsGot[id][list[ind]] + 1
		local scrolls_left = mw2.magicks.scrollsNeeded[list[ind]]-mw2.player.scrollsGot[id][list[ind]]
		if scrolls_left <= 0 then
			msg(misc.rgb(0,0,255)..player(id,"name").." JUST UNLOCKED "..string.upper(mw2.magicks.spellsData[list[ind]].name).."!!!")
		else
			msg2(id,misc.rgb(0,0,255).."you still need "..scrolls_left.." more scroll's to unlock "..mw2.magicks.spellsData[list[ind]].name)
		end
		local usgn = player(id,"usgn")
		if usgn > 0 then
			local saveFile = "sys/lua/MagicWizard2/saves/usgn_"..usgn..".txt"
			SaveEngine.write(saveFile,"Scrolls","MagicID_"..list[ind],mw2.player.scrollsGot[id][list[ind]])
			SaveEngine.save(saveFile)
		end
		mw2.regenerateSpellsList(id)
        return mw2.magicks.spellsData[list[ind]]
    else
        --[[
        msg2(id,misc.rgb(0,255,0).."You finished the game!, that doesent happen everyday!")
        msg2(id,misc.rgb(0,255,0).."I cant give you a scroll, but heres a 100 coins instead!")
        mw2.player.money[id] = mw2.player.money[id] + 100
        mw2.drawMoney(id)
        ]]--
        return false
	end
end

function mw2.drawMoney(id)
	misc.hudText(mw2.hud_id,id,"MW2 Money: "..mw2.player.money[id],5,215,-1)
end
