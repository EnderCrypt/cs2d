-- mw2.items.addItem(name,rarity,image,onSpawn,onTake) --

-- rcon live exec mw2.items.spawn(mw2.items.names.gift.id,30,30)

-- SCROLLS --
mw2.items.names.scroll = mw2.items.addItem("Magic Scroll",25,"Tile2303_C75_R9",20,function(me)
    msg(misc.rgb(0,0,255).."a magical scroll has spawned!")
    imagealpha(me.image,0)
    tween_alpha(me.image,2500,1)
end,function(id,me)
	local spell_got = mw2.items.giveScroll(id)
	--local spell_got = mw2.magicks.spellsData[math.random(#mw2.magicks.spellsData)]
    if (spell_got == false) then
        return false
    else
        parse("sv_sound mw2/notify.ogg")
        local wep_dir = "gfx/magic wizard 2/weapons/"
        local rot_speed = (math.random()*20)-10
        if (not(TempImage == nil)) then
            for i=1,4 do
                local img = TempImage.create(wep_dir..spell_got.file,0,0,1,5000,id)
                imagescale(img,0,0)
                tween_alpha(img,5000,0)
                tween_scale(img,5000,5,5)
                tween_rotateconstantly(img,rot_speed)
                imagepos(img,misc.tile_to_pixel(me.x),misc.tile_to_pixel(me.y),180+(i*90))
            end
        end
    end
end)

-- MONEY -

mw2.items.names.platinum = mw2.items.addItem("Platinum Coins",1,"Tile350_C12_R9",5,function()

end,function(id)
    msg(misc.rgb(0,0,255)..player(id,"name").." Found Platinum Coins, what a lucky player! (200)")
	mw2.player.money[id] = mw2.player.money[id] + 200
	local usgn = player(id,"usgn")
	if usgn > 0 then
		local saveFile = "sys/lua/MagicWizard2/saves/usgn_"..usgn..".txt"
		SaveEngine.write(saveFile,"Character","Money",mw2.player.money[id])
		SaveEngine.save(saveFile)
	end
	mw2.drawMoney(id)
end)

mw2.items.names.gold = mw2.items.addItem("Gold Coins",10,"Tile319_C11_R9",15,function()

end,function(id)
    msg(misc.rgb(0,0,255)..player(id,"name").." Found Gold Coins! (100)")
	mw2.player.money[id] = mw2.player.money[id] + 100
	local usgn = player(id,"usgn")
	if usgn > 0 then
		local saveFile = "sys/lua/MagicWizard2/saves/usgn_"..usgn..".txt"
		SaveEngine.write(saveFile,"Character","Money",mw2.player.money[id])
		SaveEngine.save(saveFile)
	end
	mw2.drawMoney(id)
end)

mw2.items.names.silver = mw2.items.addItem("Silver Coins",20,"Tile288_C10_R9",20,function()

end,function(id)
    msg(misc.rgb(0,0,255)..player(id,"name").." Found Silver Coins! (50)")
	mw2.player.money[id] = mw2.player.money[id] + 50
	local usgn = player(id,"usgn")
	if usgn > 0 then
		local saveFile = "sys/lua/MagicWizard2/saves/usgn_"..usgn..".txt"
		SaveEngine.write(saveFile,"Character","Money",mw2.player.money[id])
		SaveEngine.save(saveFile)
	end
	mw2.drawMoney(id)
end)

mw2.items.names.copper10 = mw2.items.addItem("Copper Coins",75,"Tile257_C9_R9",30,function()

end,function(id)
    msg2(id,misc.rgb(0,0,255).."You found copper Coins! (10)")
	mw2.player.money[id] = mw2.player.money[id] + 10
	local usgn = player(id,"usgn")
	if usgn > 0 then
		local saveFile = "sys/lua/MagicWizard2/saves/usgn_"..usgn..".txt"
		SaveEngine.write(saveFile,"Character","Money",mw2.player.money[id])
		SaveEngine.save(saveFile)
	end
	mw2.drawMoney(id)
end)

mw2.items.names.copper = mw2.items.addItem("Copper Coin",150,"Tile257_C9_R9_1",40,function()

end,function(id)
	msg2(id,misc.rgb(0,0,255).."You found a copper Coin (1)")
	mw2.player.money[id] = mw2.player.money[id] + 1
	local usgn = player(id,"usgn")
	if usgn > 0 then
		local saveFile = "sys/lua/MagicWizard2/saves/usgn_"..usgn..".txt"
		SaveEngine.write(saveFile,"Character","Money",mw2.player.money[id])
		SaveEngine.save(saveFile)
	end
	mw2.drawMoney(id)
end)

-- POTIONS --

mw2.items.names.manaPot = mw2.items.addItem("Mana Potion",50,"Tile537_C18_R10",15,function()

end,function(id)
	mw2.player.mana[id].mana = 100
	msg2(id,"Mana was restored to max")
	--local mana = mw2.player.mana[id].mana*0.01
	--tween_scale(mw2.player.mana[id].image,1000,mana,mana)
end)

mw2.items.names.speedPot = mw2.items.addItem("Speed Potion",50,"Tile785_C26_R10",15,function()

end,function(id)
	local effect = 25
	effect = math.ceil(effect * (mw2.honor(id)*0.01))
	msg2(id,"speed gained: "..misc.round(effect,10))
	mw2.player.speed[id] = mw2.player.speed[id]+misc.round(effect,10)
	parse("speedmod "..id.." "..mw2.player.speed[id])
end)

mw2.items.names.healthPot = mw2.items.addItem("Health Potion",50,"Tile1808_C59_R10",15,function()

end,function(id)
-- heal
	local effect = 50
	effect = math.ceil(effect * (mw2.honor(id)*0.01))
    msg2(id,"maxhealth gained: "..misc.round(effect,10))
	parse("sethealth "..id.." "..player(id,"maxhealth"))
	parse("setmaxhealth "..id.." "..player(id,"maxhealth")+misc.round(effect,10))
-- heal flag
    local affect_timer = 0
    local flag = mw2flags.getBiggestFlag(id,mw2flags.library.healing)
    if (flag == nil) then
        affect_timer = 10
    else
        affect_timer = flag.counter + 10
    end
    mw2flag.new(id,0,mw2flags.library.healing,10+math.floor(mw2.honor(id)*0.01))
end)

-- GIFT --
mw2.items.names.gift = mw2.items.addItem("Suprise Gift",1,"gift",5,function(id)
    msg(misc.rgb(100,255,100).."Something special just appeared somewhere!")
end,function(id,me)
    --[[
    local flow = function(spawnList,x,y)
        local timeout = 32
        repeat
            x = x + misc.round(math.random()*2)-1
            y = y + misc.round(math.random()*2)-1
            if (timeout <= 0) then
                return false
            end
            timeout = timeout - 1
        until ((entity(x,y,"exists") == false) and (tile(x,y,"walkable") == true) and (tile(x,y,"frame") > 0) and (mw2.items.grid[x.."|"..y] == nil))
        mw2.items.spawn(spawnList[math.random(#spawnList)],x,y)
        return true
    end
    ]]--
    -- generate custom spawnlist
    local spawnList = {}
    for i=1,#mw2.items.list do
        local item = mw2.items.list[i]
        if (string.find(string.lower(item.name),"potion") == nil) then
            for ii=1,item.rarity do
                spawnList[#spawnList+1] = item.id
            end
        end
    end
    -- spawn items
    --[[
    local count = 0
    local toSpawn = misc.round(10+(0.1*mw2.honor(id))) --10 to 20 depending on honor
    while (toSpawn > 0) do
        toSpawn = toSpawn - 1
        if flow(spawnList,me.x,me.y) then --mw2.items.itemsRarity
            count = count + 1
        end
    end
    ]]--
    local count = 0
    local toSpawn = misc.round(10+(0.25*mw2.honor(id))) --10 to 35 depending on honor
    while (toSpawn > 0) do
        toSpawn = toSpawn - 1
        local result = mw2.items.spawnFloating(spawnList[math.random(#spawnList)],me.x,me.y,10)
        if (result == true) then --mw2.items.itemsRarity
            count = count + 1
        end
    end
    msg(misc.rgb(100,255,100)..player(id,"name").." just opened a magical gift!")
    msg2(id,misc.rgb(100,255,100).."the gift was from the god "..misc.rgb(255,0,0).."Armok"..misc.rgb(100,255,100).." and "..count.." items gets thrown out!")
    mw2.spawnProjectile(mw2.name.multiFirework,player(id,"x"),player(id,"y"),id,math.random(360)-180)
    mw2.spawnProjectile(mw2.name.multiFirework,player(id,"x"),player(id,"y"),id,math.random(360)-180)
    mw2.spawnProjectile(mw2.name.multiFirework,player(id,"x"),player(id,"y"),id,math.random(360)-180)
    mw2.spawnProjectile(mw2.name.pushingShockwave,player(id,"x"),player(id,"y"),id,player(id,"rot"))
end)

mw2.items.names.moneyBag = mw2.items.addItem("Money Bag",0,"bag",-1,function()

end,function(id,me)
    if (me.money == nil) then
        msg2(id,misc.rgb(255,0,0).."Error, bag contained no money")
    else
        local ownerText = me.ownerName
        if ((me.id == id) and (player(id,"ip") == me.ip)) then
            ownerText = "yourself"
        end
        msg2(id,misc.rgb(0,0,255).."You found a bag containing "..me.money.." (mw2) coins from "..ownerText)
        mw2.player.money[id] = mw2.player.money[id] + me.money
        local usgn = player(id,"usgn")
        if usgn > 0 then
            local saveFile = "sys/lua/MagicWizard2/saves/usgn_"..usgn..".txt"
            SaveEngine.write(saveFile,"Character","Money",mw2.player.money[id])
            SaveEngine.save(saveFile)
        end
        mw2.drawMoney(id)
    end
end)
