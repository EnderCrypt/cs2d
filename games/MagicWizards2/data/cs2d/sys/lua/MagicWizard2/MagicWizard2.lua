-- script --
mw2 = {}
mw2.entity = {}
mw2.magicks = {}
mw2.magicks.category = {}

mw2.magicks.spellsListByCategory = {}
mw2.magicks.spellsData = {}
mw2.magicks.scrollsNeeded = {}

-- mw2.categoryList = {}
-- mw2.categorySpellList = {}
mw2.hud_selected_weapon = misc.get_hudtxt()
mw2.hud_estimate = misc.get_hudtxt()

mw2.existing_spells = 0
mw2.maxPlayerMana = 100

mw2.player = {}
mw2.player.selected = {}
mw2.player.validSave = {}
mw2.player.scrollsGot = {}
mw2.player.image = {}
mw2.player.mana = {}
mw2.player.money = {}
mw2.player.weaponUse = {}
mw2.player.magicUse = {}
mw2.player.knownSpells = {}
mw2.player.speed = {}

mw2.player.menuCategoryOpen = {}

dofile("sys/lua/MagicWizard2/mw2_arena.lua") -- arena first
dofile("sys/lua/MagicWizard2/mw2_flags.lua")
dofile("sys/lua/MagicWizard2/items/MagicWizard2_items.lua")

-- hudtxt 1 = money
-- hudtxt 2 = Selected spell
-- hudtxt 3 = scroll status
-- hudtxt 4 = Mana

--parse("transfer_speed 200")

parse("mp_hud "..(1+2+8+16+64)) -- https://www.cs2d.com/help.php?cat=all&cmd=mp_hud#cmd
parse("sv_maxplayers 32")
parse("mp_maxclientsip 2")
parse("sv_specmode 2")
parse("mp_autoteambalance 0")
parse("mp_radar 1")
parse("sv_fow 0")
parse("sv_gamemode 1")
parse("mp_dmspawnmoney 5000")
parse("mp_dispenser_health 5")
parse("mp_killbuildings 1") --4?
parse("mp_dispenser_money 1000")
parse("mp_weaponfadeout 120")
parse("mp_maxrconfails 10")
parse("mp_killteambuildings 1")
parse("mp_randomspawn 0")
parse("mp_idletime "..(60*10))
parse("mp_idleaction 4")
parse("mp_mapvoteratio 0") -- disable map vote

parse('mp_building_limit "supply" 10')
parse('mp_building_limit "turret" 3')
parse('mp_building_limit "gate field" 100')
parse('mp_building_limit "gate field" 100')

function mw2.addMagic(name,category,scrollsNeeded,manaUse,file,onUse)
	mw2.existing_spells = mw2.existing_spells + 1
	if mw2.magicks.category[category] == nil then
		mw2.magicks.category[category] = {}
		mw2.magicks.spellsListByCategory[category] = {}
		local index = #mw2.magicks.category+1
		mw2.magicks.category[index] = category
		-- DEBUG -- print("Created new magic category: "..category)
	end
	local index = #mw2.magicks.category[category]+1
	mw2.magicks.category[category][index] = category
	mw2.magicks.spellsListByCategory[category][index] = mw2.existing_spells
	if not (misc.fileExists("gfx/Magic\ Wizard\ 2/weapons/"..file)) then
		msg(misc.rgb(255,0,0).."error: unexisting staff image: "..file)
    else
        misc.addServerTransfer("gfx/Magic\ Wizard\ 2/weapons/"..file)
	end
	mw2.magicks.spellsData[mw2.existing_spells] = {name=name,manaUse=manaUse,category=category,onUse=onUse,file=file}
	mw2.magicks.scrollsNeeded[mw2.existing_spells] = scrollsNeeded
	-- DEBUG -- print("+ Created new magic: "..name)
end

mw2.magickProjectile = {}
mw2.name = {}
function mw2.addProjectile(name,magicHealth,collide_with,rot,speed,size,lifetime,image,onSpawn,onHitWall,onHitPlayer,onHitMagic,onUpdate,onDespawn)
	local index = #mw2.magickProjectile+1
	local temp = {}
	temp.typeId = index
	temp.index = index
	temp.name = name
	temp.magicHealth = magicHealth
	temp.rot = rot
	temp.collide_with = collide_with -- 1 collide with players, 2 collide with magic, 3 collide with both, 4 collide with all players
	temp.speed = speed
	temp.size = size
	temp.lifetime = lifetime
	if not (misc.fileExists("gfx/Magic\ Wizard\ 2/projectiles/"..image..".png")) then
		msg(misc.rgb(255,150,0).."error: unexisting projectile image: "..image..".png")
    else
        misc.addServerTransfer("gfx/Magic\ Wizard\ 2/projectiles/"..image..".png")
	end
	temp.image = image
	temp.onSpawn = onSpawn
	temp.onHitWall = onHitWall
	temp.onHitPlayer = onHitPlayer
	temp.onHitMagic = onHitMagic
	temp.onUpdate = onUpdate
	temp.onDespawn = onDespawn
	mw2.magickProjectile[index] = temp
	return temp
end

-- enhanced constructor
function mw2.addProjectileBuilder(data)
    return mw2.addProjectile(data.name, data.magicHealth, data.collideWith, data.rotation, data.speed, data.size, data.lifetime, data.image, data.onSpawn, data.onHitWall, data.onHitPlayer, data.onHitMagic, data.onUpdate, data.onDespawn)
end

function mw2.spawnProjectile(me,x,y,owner,direction)
	local index = #mw2.entity+1
	local temp = {}
	temp.scrollsNeeded = scrollsMax
	temp.me = me
	temp.magicHealth = me.magicHealth
	temp.lifetime = me.lifetime
	temp.size = me.size
	temp.x = x
	temp.y = y
	temp.dispose = false
    temp.originalOwner = owner
	temp.owner = owner
	temp.xmove = misc.lengthdir_x(direction,me.speed)
	temp.ymove = misc.lengthdir_y(direction,me.speed)
	temp.dir = direction
	temp.image = image("gfx/magic wizard 2/projectiles/"..me.image..".png",x,y,1)
	imagescale(temp.image,0,0)
    tween_scale(temp.image,100,1,1)
    if not (me.rot == false) then
		imagepos(temp.image,x,y,direction+me.rot)
	end
	tween_move(temp.image,me.lifetime*100,x+(temp.xmove*me.lifetime),y+(temp.ymove*me.lifetime))
	mw2.entity[index] = temp
	mw2.entity[index].me.onSpawn(mw2.entity[index])
end

function mw2.magicBack(magic)
	magic.x = magic.x - magic.xmove
	magic.y = magic.y - magic.ymove
end

function mw2.magicForward(magic)
	magic.x = magic.x + magic.xmove
	magic.y = magic.y + magic.ymove
end

addhook("ms100","mw2.ms100")
function mw2.ms100()
	-- update players
	for _,id in ipairs(player(0,"tableliving")) do
		-- base regen
		local mana_power = mw2.player.mana[id].mana+10
		-- msg2(id, "mana power: "..tostring(mana_power))
		local mana_inc = (mana_power*0.15)
		-- aura
		mana_inc = mana_inc * mw2ma.get(Point.new(player(id,"tilex"),player(id,"tiley")),id)
		-- slow due to ms100 instead of second
		mana_inc = mana_inc * 0.07
		-- contamination
		if (mw2flags.hasFlag(id,mw2flags.library.contaminated)) then
		    mana_inc = mana_inc / 3
		end
		-- add
		mw2.player.mana[id].mana = mw2.player.mana[id].mana + mana_inc
		-- limit
		if mw2.player.mana[id].mana > mw2.maxPlayerMana then
			mw2.player.mana[id].mana = mw2.maxPlayerMana
		end
		-- ready estimate
		mw2.drawReadyEst(id)
	end
	-- update magic
  local i = 0
  while (i < #mw2.entity) do
      i = i + 1
      local entity = mw2.entity[i]
      -- loop
      if ( (entity.owner == 0) or (player(entity.owner,"exists") == true) and (type(entity.x) == "number") ) then
          -- update
          entity.me.onUpdate(entity)
          -- move
          local primDist = misc.primitive_point_distance(0,0,entity.xmove,entity.ymove)
          local projectilePrecision = 20
          local div = 0
          if (entity.xmove == 0) and (entity.xmove == 0) then
              div = 1
          else
              div = math.ceil(primDist/projectilePrecision)
          end
          local xmove = entity.xmove/div
          local ymove = entity.ymove/div
          for i=1,div do
              entity.x = entity.x + xmove
              entity.y = entity.y + ymove
              mw2.updateProjectile(entity)
              if (entity.dispose) then
                  break
              end
          end
          -- lifetime throttler
          local lifetimeRemove = 1
          local remove_temp = 50
          local spells = #mw2.entity
          while (spells >= remove_temp) do
              lifetimeRemove = lifetimeRemove + 1
              spells = spells - remove_temp
              remove_temp = remove_temp - 10
              if (remove_temp < 10) then
                  remove_temp = 10
              end
          end
          entity.lifetime = entity.lifetime - lifetimeRemove
      else
          entity.dispose = true
      end
  end
  -- dispose spells
  local i = 0
  while (i < #mw2.entity) do
      i = i + 1
      local entity = mw2.entity[i]
      if ( (entity.dispose == true) or (entity.lifetime <= 0) or (entity.magicHealth <= 0) ) then
          entity.me.onDespawn(entity) -- onDespawn
          freeimage(mw2.entity[i].image)
          mw2.entity[i] = mw2.entity[#mw2.entity]
          mw2.entity[#mw2.entity] = nil
      end
  end
end

 -- 1 collide with players, 2 collide with magic, 3 collide with both, 4 collide with both and the owner
function mw2.updateProjectile(entity)
    -- colide with magic
    if ( (entity.me.collide_with == 2) or (entity.me.collide_with == 3) ) then
        for ii=1,#mw2.entity do
            local otherEntity = mw2.entity[ii]
            if (entity.owner ~= otherEntity.owner) then
                local dist = misc.primitive_point_distance(entity.x,entity.y,otherEntity.x,otherEntity.y)
                if (dist <= entity.size+otherEntity.size) then
                --if ( (otherEntity.x < entity.x+entity.size) and (otherEntity.x > entity.x-entity.size) and (otherEntity.y < entity.y+entity.size) and (otherEntity.y > entity.y-entity.size) ) then
                    ret = entity.me.onHitMagic(entity,otherEntity) -- onHitMagic (1 = increase magic use, 0 = dont increase)
                    if ((not (ret == 0)) and (entity.owner > 0)) then
                        mw2.player.magicUse[entity.owner] = mw2.player.magicUse[entity.owner] + 1
                    end
                end
            end
        end
    end
    -- collide with players
    if ( (entity.me.collide_with == 1) or (entity.me.collide_with == 3) or (entity.me.collide_with == 4) ) then
        local pl = player(0,"tableliving")
        for ii=1,#pl do
            local otherID = pl[ii]
            if (entity.me.collide_with == 4) or (entity.owner ~= otherID) then
                local dist = misc.primitive_point_distance(entity.x,entity.y,player(otherID,"x"),player(otherID,"y"))
                if (dist < 13+entity.size) then
                --if ( (player(otherID,"x") < entity.x+entity.size) and (player(otherID,"x") > entity.x-entity.size) and (player(otherID,"y") < entity.y+entity.size) and (player(otherID,"y") > entity.y-entity.size) ) then
                    ret = entity.me.onHitPlayer(entity,otherID) -- onHitPlayer
                    if ((not (ret == 0)) and (entity.owner > 0)) then
                        mw2.player.magicUse[entity.owner] = mw2.player.magicUse[entity.owner] + 1
                    end
                end
            end
        end
    end
    -- on hit wall
    if tile(misc.pixel_to_tile(entity.x),misc.pixel_to_tile(entity.y),"wall") then
        entity.me.onHitWall(entity)
    end
end
--[[
function mw2.ms100()
	disposeTable = {}
	for i=1,#mw2.entity do
        mw2.loopEntityIndex = i
		if ((player(mw2.entity[i].owner,"exists") == false) and (mw2.entity[i].owner > 0)) or (type(mw2.entity[i].x) == "boolean") then
			mw2.entity[i].dispose = true
		else
		--
        if (mw2.entity[i].magicHealth <= 0) then
            mw2.entity[i].dispose = true
        end
        --
		mw2.entity[i].x = mw2.entity[i].x + mw2.entity[i].xmove
		mw2.entity[i].y = mw2.entity[i].y + mw2.entity[i].ymove
		if (mw2.entity[i].me.collide_with == 2) or (mw2.entity[i].me.collide_with == 3) or (mw2.entity[i].me.collide_with == 4) then
			for ii=1,#mw2.entity do
				if not (mw2.entity[i].dispose) then
					if not (mw2.entity[i].owner == mw2.entity[ii].owner) then
						if mw2.entity[ii].x < mw2.entity[i].x+mw2.entity[i].size and mw2.entity[ii].x > mw2.entity[i].x-mw2.entity[i].size and mw2.entity[ii].y < mw2.entity[i].y+mw2.entity[i].size and mw2.entity[ii].y > mw2.entity[i].y-mw2.entity[i].size then
							ret = mw2.entity[i].me.onHitMagic(mw2.entity[i],mw2.entity[ii]) -- onHitMagic
							if ((not (ret == 0)) and (mw2.entity[i].owner > 0)) then
								mw2.player.magicUse[mw2.entity[i].owner] = mw2.player.magicUse[mw2.entity[i].owner] + 1
							end
						end
					end
				end
			end
		end
		if (mw2.entity[i].me.collide_with == 1) or (mw2.entity[i].me.collide_with == 3) or (mw2.entity[i].me.collide_with == 4) then
			local pl = player(0,"tableliving")
			for ii=1,#pl do
				if not (mw2.entity[i].dispose) then
					if (mw2.entity[i].me.collide_with == 4) then
						if player(pl[ii],"x") < mw2.entity[i].x+mw2.entity[i].size and player(pl[ii],"x") > mw2.entity[i].x-mw2.entity[i].size and player(pl[ii],"y") < mw2.entity[i].y+mw2.entity[i].size and player(pl[ii],"y") > mw2.entity[i].y-mw2.entity[i].size then
							ret = mw2.entity[i].me.onHitPlayer(mw2.entity[i],pl[ii]) -- onHitPlayer
							if ((not (ret == 0)) and (mw2.entity[i].owner > 0)) then
								mw2.player.magicUse[mw2.entity[i].owner] = mw2.player.magicUse[mw2.entity[i].owner] + 1
							end
						end
					else
                        if not (mw2.entity[i].owner == pl[ii]) then
                            if player(pl[ii],"x") < mw2.entity[i].x+mw2.entity[i].size and player(pl[ii],"x") > mw2.entity[i].x-mw2.entity[i].size and player(pl[ii],"y") < mw2.entity[i].y+mw2.entity[i].size and player(pl[ii],"y") > mw2.entity[i].y-mw2.entity[i].size then
                                ret = mw2.entity[i].me.onHitPlayer(mw2.entity[i],pl[ii]) -- onHitPlayer
                                if ((not (ret == 0)) and (mw2.entity[i].owner > 0)) then
                                    mw2.player.magicUse[mw2.entity[i].owner] = mw2.player.magicUse[mw2.entity[i].owner] + 1
                                end
                            end
                        end
					end
				end
			end
		end
        mw2.entity[i].me.onUpdate(mw2.entity[i])
		if tile(misc.pixel_to_tile(mw2.entity[i].x),misc.pixel_to_tile(mw2.entity[i].y),"wall") then
			mw2.entity[i].me.onHitWall(mw2.entity[i])
		end
		if mw2.entity[i].magicHealth <= 0 then
			mw2.entity[i].me.onDespawn(mw2.entity[i]) -- onDespawn
			mw2.entity[i].dispose = true
		end
        -- lifetime throttler
        local lifetimeRemove = 1
        local remove_temp = 50
        local spells = #mw2.entity
        while (spells >= remove_temp) do
            lifetimeRemove = lifetimeRemove + 1
            spells = spells - remove_temp
            remove_temp = remove_temp - 10
            if (remove_temp < 10) then
                remove_temp = 10
            end
        end
		mw2.entity[i].lifetime = mw2.entity[i].lifetime - lifetimeRemove
		if mw2.entity[i].lifetime <= 0 then
			mw2.entity[i].me.onDespawn(mw2.entity[i]) -- onDespawn
			mw2.entity[i].dispose = true
		end
		end
		if mw2.entity[i].dispose then
			disposeTable[#disposeTable+1] = i
		end
	end
	local i = #disposeTable
	while (i > 0) do
		freeimage(mw2.entity[disposeTable[i] ].image)
		if not (disposeTable[i] == #mw2.entity) then
			mw2.entity[disposeTable[i] ] = mw2.entity[#mw2.entity]
		end
		mw2.entity[#mw2.entity] = nil
		i = i - 1
	end
end
]]--
function mw2.getProjectiles(spell_index,id) -- get all entities of a certain type, optionally for ID only
    local entities = {}
    for i=1,#mw2.entity do
        local entity = mw2.entity[i]
        if (entity.me.index == spell_index) then
            if ((id == nil) or (entity.owner == id)) then
                entities[#entities+1] = entity
            end
        end
    end
    return entities
end

--[[
function mw2.getOtherProjectiles(magic) -- get all projetiles of same type and owner
    local entities = {}
    for i=1,#mw2.entity do
        local entity = mw2.entity[i]
        if (entity.me.index == magic.me.index) then
            if (entity.owner == magic.owner) then
                if (not()) -- unique identifier such as index
                entities[#entities+1] = entity
            end
        end
    end
    return entities
end
]]--

function mw2.projSetMotion(magic,dir,speed)
    magic.dir = dir
    magic.speed = speed
	magic.xmove = misc.lengthdir_x(magic.dir,magic.speed)
	magic.ymove = misc.lengthdir_y(magic.dir,magic.speed)
    tween_move(magic.image,magic.lifetime*100,magic.x+(magic.xmove*magic.lifetime),magic.y+(magic.ymove*magic.lifetime))
end

function mw2.playerSpeedSet(id,set)
    mw2.player.speed[id] = set
    cs2d.speedmod.call(id,set)
end

function mw2.playerSpeed(id,add)
    mw2.playerSpeedSet(id,mw2.player.speed[id] + add)
end

function mw2.explosion(x,y,size,dmg,owner)
	local pl = player(0,"tableliving")
	for i=1,#pl do
		local dist = misc.point_distance(x,y,player(pl[i],"x"),player(pl[i],"y"))
		if dist < size then
			dist = size-dist
            if (mw2.player.speed[pl[i]] < 0) then
                mw2.player.speed[pl[i]] = mw2.player.speed[pl[i]]+(dist*2)
                if (mw2.player.speed[pl[i]] > 0) then
                    mw2.player.speed[pl[i]] = 0
                end
                parse("speedmod "..pl[i].." "..misc.round(mw2.player.speed[pl[i]]))
            end
			mw2.hitBot(pl[i],owner)
		end
	end
	misc.explosion(x,y,size,dmg,owner)
end

function mw2.frezze(x,y,size)
	parse('effect "flare" '..x..' '..y..' '..size..' '..size..' 0 50 255')
	local pl = player(0,"tableliving")
	for i=1,#pl do
		local dist = misc.point_distance(x,y,player(pl[i],"x"),player(pl[i],"y"))
		if dist < size then
			dist = dist-size
			mw2.player.speed[pl[i]] = mw2.player.speed[pl[i]]+dist
			parse("speedmod "..pl[i].." "..mw2.player.speed[pl[i]])
		end
	end
end

function mw2.damageMaxHealth(src,id,dmg,wep)
    local dmg = mw2.damagePlayer(src,id,dmg,wep)
    local max = player(id,"maxhealth") - dmg
    mw2.triggerCombat(id,src)
    parse("setmaxhealth "..id.." "..max)
end

function mw2.triggerCombat(id,src)
    local triggerTime = 10
    if (id ~= src) then
        mw2flag.new(id,src,mw2flags.library.inCombat,triggerTime)
        if (src ~= 0) then
            mw2flag.new(src,id,mw2flags.library.inCombat,triggerTime)
        end
    end
end

function mw2.damagePlayer(src,id,dmg,wep)
    if (player(id,"exists")) then
        if (src > 0) then
            if (mw2flags.hasFlag(src,mw2flags.library.reanimated)) then
                if (mw2flags.isSource(src,id,mw2flags.library.reanimated)) then
                    return 0
                end
            end
            if (mw2flags.hasFlag(src,mw2flags.library.deathDoor) or mw2flags.hasFlag(src,mw2flags.library.deathChannel)) then -- zombies and deathChannel infect others with plauge
                mw2flag.new(id,src,mw2flags.library.deathDoor,60+(math.random()*60))
            end
        end

        if (player(src,"bot") and src == id) then
            return 0
        end

        local factoredDmg = misc.round(dmg)
        if (factoredDmg > 0) then
            factoredDmg = misc.calcDamage(id,dmg)
        end

        local health = player(id,"health")
        local newHealth = health-factoredDmg
        if (newHealth <= 0) then
            if (mw2flags.hasFlag(id,mw2flags.library.immortal)) then
                return 0
            end
            if (newHealth <= 0) then
                parse("customkill "..src.." \""..wep.."\" "..id)
            end
        end
        mw2.hitBot(id,src,factoredDmg)
        cs2d.sethealth.call(id,newHealth)
        if (factoredDmg > 0) then
            mw2.triggerCombat(id,src)
        end
        return factoredDmg
    end
end

function mw2.hitBot(id,src,dmg)
	if (bot ~= nil) then
        if (bot.hit ~= nil) then
            if (src > 0) then
                if (id ~= src) then
                    mw2.triggerCombat(id,src)
                    if (player(id,"bot")) then
                        bot.hit(id,src,dmg)
                    end
                end
            end
        end
	end
end

function mw2.drawReadyEst(id)
    local text = misc.rgb(255,255,0).."No spell selected"
    if (mw2.player.selected[id] > 0) then
        local spellData = mw2.magicks.spellsData[mw2.player.selected[id]]
        local manaAuraBoost = mw2ma.get(Point.new(player(id,"tilex"),player(id,"tiley")),id)
        if (mw2.player.mana[id].mana >= spellData.manaUse) then
            local times = math.floor(mw2.player.mana[id].mana/spellData.manaUse)
            local knifesPerSecond = 3.1 -- (186/60) you knife 186 times under a minute
            local tempMax = mw2.player.mana[id].mana
            local temp = tempMax-(spellData.manaUse*knifesPerSecond)
            temp = temp + ((temp+10)*0.15)
            temp = temp * manaAuraBoost
            if (mw2flags.hasFlag(id,mw2flags.library.contaminated)) then
                temp = temp / 3
            end
            if (temp >= tempMax) then
                text = misc.rgb(100,255,0).."Spell ready! xINFINITE"
            else
                text = misc.rgb(100,255,0).."Spell ready! x"..times
            end
        else
            i = 0
            m = mw2.player.mana[id].mana
            while (m < spellData.manaUse) do
                i = i + 1
                m = m + ((m+10)*0.15)*manaAuraBoost
            end
            text = misc.rgb(255,100,0).."Spell ready in "..i.." seconds"
        end
    end
    misc.hudText(mw2.hud_estimate,id,text,5,228+27,const.align.left)
end

addhook("second","mw2.second")
function mw2.second()
    -- lifetime throttler
    local lifetimeRemove = 1
    local remove_temp = 25
    local spells = #mw2.entity
    while (spells >= remove_temp) do
        lifetimeRemove = lifetimeRemove + 1
        spells = spells - remove_temp
        remove_temp = remove_temp - 10
        if (remove_temp < 10) then
            remove_temp = 10
        end
    end
    if (lifetimeRemove > 1) then
        print("There are "..#mw2.entity.." active projectiles, Throttling despawn by "..lifetimeRemove.."x")
    end
	local pl = player(0,"tableliving")
	for i=1, #pl do
		--local mana = mw2.player.mana[pl[i]].mana*0.01
		-- tween_scale(mw2.player.mana[pl[i]].image,1000,mana,mana)
		-- speed
		if not (mw2.player.speed[pl[i]] == 0) then
			if (mw2.player.speed[pl[i]] < 1) and (mw2.player.speed[pl[i]] > -1) then
				mw2.player.speed[pl[i]] = 0
			else
				if (mw2.player.speed[pl[i]] > 0) then
					mw2.player.speed[pl[i]] = mw2.player.speed[pl[i]] * 0.95
				else
					mw2.player.speed[pl[i]] = mw2.player.speed[pl[i]] * 0.9
				end
			end
			parse("speedmod "..pl[i].." "..mw2.player.speed[pl[i]])
			if mw2.player.speed[pl[i]] < 0 then
				parse('effect "flare" '..player(pl[i],"x")..' '..player(pl[i],"y")..' '..(-mw2.player.speed[pl[i]]*.5)..' '..(-mw2.player.speed[pl[i]]*.25)..' 0 50 255')
			end
			if mw2.player.speed[pl[i]] > 0 then
				parse('effect "smoke" '..player(pl[i],"x")..' '..player(pl[i],"y")..' '..(mw2.player.speed[pl[i]]*.5)..' '..(mw2.player.speed[pl[i]]*.5)..' 0 50 255')
			end
		end
		local maxhealth = player(pl[i],"maxhealth")
		if maxhealth < 100 then
			if maxhealth+1 < 100 then
				parse("setmaxhealth "..pl[i].." "..(maxhealth+1))
			else
				parse("setmaxhealth "..pl[i].." 100")
			end
		end
	end
end

dofile("sys/lua/MagicWizard2/magic/mw2_magic.lua")
dofile("sys/lua/MagicWizard2/magic/mw2_magicExtended1.lua")
dofile("sys/lua/MagicWizard2/magic/mw2_magicExtended2.lua")
dofile("sys/lua/MagicWizard2/magic/mw2_magicExtended3.lua")

function mw2.removeMana(id,amount)
	mw2.player.mana[id].mana = mw2.player.mana[id].mana - amount
	if mw2.player.mana[id].mana < 0 then
		mw2.player.mana[id].mana = 0
	end
	-- local mana = mw2.player.mana[id].mana*0.01
	-- tween_scale(mw2.player.mana[id].image,100,mana,mana)
end

addhook("hit","mw2.hit")
function mw2.hit(id,source,weapon,hpdmg,apdmg)
    if (hpdmg > 0) then
        mw2.triggerCombat(id,source)
    end
    --msg(source.." hit "..id)
	if source > 0 then
		if (not(weapon == 251)) then --explosion
			if weapon == 50 then --and mw2.player.selected[source] > 0 then
				--if (player(id,"bot") == true) or (player(source,"bot") == true) then
				if (player(source,"bot")) then
					-- hit with knife
                    if (math.random() < 0.75) then
                        return 1
                    end
					mw2.player.weaponUse[source] = mw2.player.weaponUse[source] + math.ceil(hpdmg/10)
				else
					return 1
				end
			else
				mw2.player.weaponUse[source] = mw2.player.weaponUse[source] + math.ceil(hpdmg/10)
			end
		end
        mw2.hitBot(id,source,hpdmg)
	end
end

addhook("join","mw2.join")
function mw2.join(id)
	mw2.player.mana[id] = nil
	mw2.player.validSave[id] = false
	mw2.saveNullifie(id)
end

function mw2.non_usgn(id)
	mw2.player.money[id] = 0
	mw2.player.weaponUse[id] = 1
	mw2.player.magicUse[id] = 1
	mw2.player.mana[id].mana = 100
	mw2.player.scrollsGot[id] = {}
	for i=1,mw2.existing_spells do
		mw2.player.scrollsGot[id][i] = 0
	end
	mw2.player.selected[id] = 0
end

function mw2.load(id,force)
	mw2.player.validSave[id] = false
	usgn = player(id,"usgn")
	if not (usgn == false) then
		if usgn > 0 then
			saveFile = "sys/lua/MagicWizard2/saves/usgn_"..usgn..".txt"
			if misc.fileExists(saveFile) then
				ret = SaveEngine.open(saveFile,force)
				if not (ret == true) then
                    if (tonumber(ret) == nil) then
                        ret = "UNKNOWN"
                    end
					msg(misc.rgb(255,0,0).."FAILED TO LOAD SAVE, ERROR CODE: "..ret)
					msg2(id,misc.rgb(255,0,0).." your data will not be saved")
					mw2.non_usgn(id)
					mw2.player.validSave[id] = false
				else
					mw2.player.scrollsGot[id] = {}
					for i=1,mw2.existing_spells do
						mw2.player.scrollsGot[id][i] = SaveEngine.read(saveFile,"Scrolls","MagicID_"..i,0)
                        if (mw2.player.scrollsGot[id][i] > mw2.magicks.scrollsNeeded[i]) then
                            mw2.player.scrollsGot[id][i] = mw2.magicks.scrollsNeeded[i]
                        end
					end
					mw2.player.mana[id].mana = SaveEngine.read(saveFile,"Character","Mana",100)
					mw2.player.money[id] = SaveEngine.read(saveFile,"Character","Money",100)
					mw2.player.weaponUse[id] = SaveEngine.read(saveFile,"Character","WeaponUse",0)
					mw2.player.magicUse[id] = SaveEngine.read(saveFile,"Character","MagicUse",0)
					mw2.drawMoney(id)
					mw2.player.selected[id] = 0
					mw2.player.validSave[id] = true
					msg2(id,misc.rgb(0,255,0).."Welcome (your save has been loaded)")
				end
			else
				mw2.non_usgn(id)
				mw2.player.validSave[id] = false
                msg(misc.rgb(0,150,150)..player(id,"name").." is a new player")
				msg2(id,misc.rgb(0,150,150).."Welcome, you are logged in to U.S.G.N")
			end
		else
			mw2.non_usgn(id)
			mw2.player.validSave[id] = false
            msg(misc.rgb(255,0,0)..player(id,"name").." has no U.S.G.N")
			msg2(id,misc.rgb(255,0,0).."Welcome (you have no U.S.G.N)")
		end
		mw2.regenerateSpellsList(id)
	end
end

addhook("spawn","mw2.spawn")
function mw2.spawn(id)
	if mw2.player.mana[id] == nil then
		mw2.player.mana[id] = {}
		mw2.load(id)
		-- mw2.player.mana[id].image = image("gfx/ROIW/circle.png",const.screen.width-50,480-(96*0.5)-75,2,id)
	else
		mw2.player.mana[id].mana = 0
	end

	mw2.player.speed[id] = 0

	-- imagecolor(mw2.player.mana[id].image,255,255,255)
	-- imagescale(mw2.player.mana[id].image,0,0)
	-- tween_color(mw2.player.mana[id].image,500,0,0,255)
	-- tween_scale(mw2.player.mana[id].image,500,mw2.player.mana[id].mana*0.01,mw2.player.mana[id].mana*0.01)

	timer(1,"parse","strip "..id.." 0")
	mw2.player.menuCategoryOpen[id] = 0
	mw2.select(id)
end

addhook("minute","mw2.minute")
function mw2.minute()
    local pl = player(0,"tableliving")
    for i=1,#pl do
        local id = pl[i]
        mw2.save(id)
    end
end

function mw2.save(id)
	if mw2.player.validSave[id] == true then
		usgn = player(id,"usgn")
		if usgn > 0 then
			saveFile = "sys/lua/MagicWizard2/saves/usgn_"..usgn..".txt"
			print("saving \""..player(id,"name").."\" (ID: "..id..") USGN: "..usgn)
			for i=1,mw2.existing_spells do
				if not (mw2.player.scrollsGot[id][i] == nil) then
					SaveEngine.write(saveFile,"Scrolls","MagicID_"..i,mw2.player.scrollsGot[id][i])
				end
			end
			if mw2.player.mana[id].mana == nil then
				SaveEngine.save(saveFile)
				parse("kick "..id.." \"Glitch\"")
				return 1
			end
			SaveEngine.write(saveFile,"Character","LastUsedName",player(id,"name"))
			SaveEngine.write(saveFile,"Character","Mana",mw2.player.mana[id].mana)
			SaveEngine.write(saveFile,"Character","Money",mw2.player.money[id])
			SaveEngine.write(saveFile,"Character","WeaponUse",mw2.player.weaponUse[id])
			SaveEngine.write(saveFile,"Character","MagicUse",mw2.player.magicUse[id])
			SaveEngine.save(saveFile)
		end
	end
end

function mw2.saveNullifie(id)
	mw2.player.scrollsGot[id] = nil
	mw2.player.validSave[id] = false
	mw2.player.mana[id] = nil
	mw2.player.money[id] = nil
	mw2.player.weaponUse[id] = nil
	mw2.player.magicUse[id] = nil
end

function mw2.removeMagic(id)
    local i = 0
    while (i < #mw2.entity) do
        i = i + 1
        local entity = mw2.entity[i]
        if (entity.owner == id) then
            entity.me.onDespawn(entity) -- onDespawn
            freeimage(mw2.entity[i].image)
            mw2.entity[i] = mw2.entity[#mw2.entity]
            mw2.entity[#mw2.entity] = nil
        end
    end
end


addhook("leave","mw2.leave")
function mw2.leave(id)
    mw2.removeMagic(id)
	mw2.save(id)
	mw2.saveNullifie(id)
end

addhook("die","mw2.die")
function mw2.die(victim,killer,weapon,x,y)
    --[[
    print("victim: "..victim)
    print("killer: "..killer)
    print("weapon: "..weapon)
    print("x: "..x)
    print("y: "..y)
    ]]--
	mw2.player.mana[victim].mana = 0
	mw2.save(victim)
end

addhook("serveraction","mw2.serveraction")
function mw2.serveraction(id,action)
	if player(id,"health") <= 0 then
		return 0
	end
	if action == 1 then
		sme.createMenu(id,mw2.selectCategory,sme.noProcess,"* Magic Category *",true,mw2.magicks.category)
	end
	if action == 2 then
		sme.createMenu(id,mw2.selectSpellQuick,mw2.menuQuickSelectProcess,"Quick Slect",true,mw2.player.knownSpells[id])
	end
	if action == 3 then
        local buyableCategories = {}
        for _,category in ipairs(mw2.magicks.category) do
            --[[
            for key,value in pairs(mw2.magicks.category) do
                print(key.." = "..tostring(value))
            end
            ]]--
            local spells = mw2.magicks.spellsListByCategory[category]
            local canBuyInCategory = false
            for _,spellID in ipairs(spells) do
                local spell = mw2.magicks.spellsData[spellID]
                local scrollsNeeded = (mw2.magicks.scrollsNeeded[spellID]-mw2.player.scrollsGot[id][spellID])
                if (scrollsNeeded > 0) then
                    canBuyInCategory = true
                    break
                end
            end
            if (canBuyInCategory) then
                --print(tostring(category))
                buyableCategories[#buyableCategories+1] = category
            end
        end

		-- if mw2.player.selected[id] > 0 then
			-- msg2(id,"Deselected Magick")
			-- mw2.player.selected[id] = 0
			-- mw2.select(id)
		-- end
		sme.createMenu(id,mw2.selectBuyCategory,sme.noProcess,"* Magic Scrolls Shop *",true,buyableCategories) -- mw2.magicks.category
	end
end

function mw2.regenerateSpellsList(id)
	mw2.player.knownSpells[id] = {}
	for i=1,mw2.existing_spells do
		if (mw2.magicks.scrollsNeeded[i] - mw2.player.scrollsGot[id][i]) <= 0 then
			mw2.player.knownSpells[id][#mw2.player.knownSpells[id]+1] = i
		end
	end
end

function mw2.menuQuickSelectProcess(id,data)
	return mw2.magicks.spellsData[data].name
end

function mw2.selectSpellQuick(id,spell)
	mw2.player.selected[id] = mw2.player.knownSpells[id][spell]
	mw2.select(id)
end

function mw2.selectCategory(id,category)
	local categoryName = mw2.magicks.category[category]
	mw2.player.menuCategoryOpen[id] = categoryName
	sme.createMenu(id,mw2.selectSpell,mw2.spellMenuButtonProcess,"Spells",true,mw2.magicks.spellsListByCategory[categoryName])
end

function mw2.spellMenuButtonProcess(id,data)
	local scrollsNeeded = (mw2.magicks.scrollsNeeded[data]-mw2.player.scrollsGot[id][data])
	if scrollsNeeded > 0 then
		return "("..mw2.magicks.spellsData[data].name.."|srolls needed: "..scrollsNeeded.." (got: "..mw2.player.scrollsGot[id][data].."))"
	else
		return mw2.magicks.spellsData[data].name.."|Unlocked"
	end
end

function mw2.selectBuyCategory(id,index,categoryName)
	mw2.player.menuCategoryOpen[id] = categoryName
	sme.createMenu(id,mw2.BuySpell,mw2.spellBuyMenuButtonProcess,"Buy Spells",true,mw2.magicks.spellsListByCategory[categoryName],true)
end

function mw2.spellBuyMenuButtonProcess(id,data)
	if player(id,"health") > 0 then
        local scrollsNeeded = (mw2.magicks.scrollsNeeded[data]-mw2.player.scrollsGot[id][data])
        if scrollsNeeded > 0 then
            if mw2.player.money[id] == nil then
                print("(ERROR OCCURED (1))"..player(id,"usgn"))
                return ("(ERROR OCCURED (1))"..player(id,"usgn"))
            end
            if scrollsNeeded == nil then
                print("(ERROR OCCURED (2))"..player(id,"usgn"))
                return ("(ERROR OCCURED (2))"..player(id,"usgn"))
            end
                est_total = 0
                i = 0
                while (i < scrollsNeeded) do
                    i = i + 1
                    est_total = est_total + mw2.byDiscount(id,95+(scrollsNeeded*5))
                end
                cost = mw2.byDiscount(id,95+(scrollsNeeded*5))
            if mw2.player.money[id] >= cost then
                return mw2.magicks.spellsData[data].name.."|scrolls needed: "..scrollsNeeded.." (cost: "..cost.."MW2/total cost: "..est_total..")"
            else
                return "("..mw2.magicks.spellsData[data].name.."|scrolls needed: "..scrollsNeeded.." (cost: "..cost.."MW2/total cost: "..est_total.."))"
            end
        else
            return "("..mw2.magicks.spellsData[data].name.."|Already Unlocked)"
        end
	end
end

function mw2.byDiscount(id,cost)
    discount_start = 80
    if mw2.honor(id) > discount_start then
        local h = mw2.honor(id)
        h = math.ceil(h - discount_start)
        cost = math.ceil(cost - (cost * (h*0.01)))
    end
    return cost
end

function mw2.selectSpell(id,spell)
	local selected = mw2.magicks.spellsListByCategory[mw2.player.menuCategoryOpen[id]][spell]
	mw2.player.selected[id] = selected
	mw2.select(id)
end

function mw2.BuySpell(id,spell)
	local selected = mw2.magicks.spellsListByCategory[mw2.player.menuCategoryOpen[id]][spell]
	local scrollsNeeded = (mw2.magicks.scrollsNeeded[selected]-mw2.player.scrollsGot[id][selected])
	local cost = (95+(scrollsNeeded*5))
	newCost = mw2.byDiscount(id,cost)
	if (not(cost == newCost)) then
        local h = mw2.honor(id)
        h = math.ceil(h - discount_start)
		msg2(id,"The "..h.."% Discount made the scroll cost "..newCost)
        cost = newCost
	end
	mw2.player.money[id] = mw2.player.money[id] - cost
	mw2.drawMoney(id)
	if mw2.player.money[id] >= 0 then
        mw2.addScrollRankCheck(id)
		mw2.player.scrollsGot[id][selected] = mw2.player.scrollsGot[id][selected] + 1
		msg2(id,"Bought a scroll of "..mw2.magicks.spellsData[selected].name.." for "..cost.." MW2 money")
		local scrollsNeeded = (mw2.magicks.scrollsNeeded[selected]-mw2.player.scrollsGot[id][selected])
		if scrollsNeeded == 0 then
			msg(misc.rgb(0,255,0)..player(id,"name").." Bought enough scrolls to unlock "..mw2.magicks.spellsData[selected].name.."!!!")
		end
		mw2.regenerateSpellsList(id)
	else
		msg2(id,misc.rgb(255,0,0).."Dont bug exploit")
	end
end

function mw2.addScrollRankCheck(id) --ACTIVATE before actually getting the scroll
	local scrolls = 0
	for i=1,mw2.existing_spells do
		scrolls = scrolls + mw2.player.scrollsGot[id][i]
	end
	latest_num = 0
	for i=1, #scrolls_skill do
		if scrolls >= scrolls_skill[i].s then
			latest_num = i-1
		end
	end
    honor_pre = latest_num
    scrolls = scrolls + 1
	local latest_t = "[ERROR]"
	local latest_num = 0
	for i=1, #scrolls_skill do
		if scrolls >= scrolls_skill[i].s then
			latest_t = scrolls_skill[i].t
			latest_num = i-1
        end
	end
    if (latest_num > honor_pre) then
        msg(misc.rgb(0,255,0)..player(id,"name").." Has ranked up to \""..latest_t.."\" ("..latest_num.." out of "..#scrolls_skill..")")
        msg2(id,misc.rgb(255,0,0).."Armok "..misc.rgb(100,255,100).."is proud over your rank up, and gave you a free lottery ticket")
        superInventory.get(id):add("LOTTERY_TICKET")
    end
end

function mw2.drawSelectedSpell(id)
	if mw2.player.selected[id] == 0 then
		misc.hudText(mw2.hud_selected_weapon,id,"Selected spell: noone",320,132,1)
		misc.hudTextColor(mw2.hud_selected_weapon,id,1000,0,255,0)
	else
		misc.hudText(mw2.hud_selected_weapon,id,"Selected spell: "..mw2.magicks.spellsData[mw2.player.selected[id]].name,320,132,1)
		misc.hudTextColor(mw2.hud_selected_weapon,id,1000,255,0,0)
	end
end

function mw2.honor(id)
	local total = mw2.player.weaponUse[id]+mw2.player.magicUse[id]
	if total == 0 then
		honor = 0
	else
		honor = (100/total)*mw2.player.magicUse[id]
	end
	return honor
end

addhook("attack","mw2.attack")
function mw2.attack(id)
    if player(id,"bot") then
        mw2.magicks.spellsData[mw2.player.selected[id]].onUse(id)
        return 0
    end
	if player(id,"weapontype") == 50 then
		if mw2.player.selected[id] > 0 then
			scrollsNeeded = (mw2.magicks.scrollsNeeded[mw2.player.selected[id]]-mw2.player.scrollsGot[id][mw2.player.selected[id]])
			scrollsMax = mw2.magicks.scrollsNeeded[mw2.player.selected[id]] -- used to determine power
            if scrollsNeeded <= 0 then
                local data = mw2.magicks.spellsData[mw2.player.selected[id]]
                if mw2.player.mana[id].mana >= data.manaUse then
                    mw2.removeMana(id,data.manaUse)
                    if (mw2flags.hasFlag(id,mw2flags.library.confused) and (math.random() < 0.5)) then
                        msg2(id,misc.rgb(200,50,50).."In confusion you fail to cast the spell")
                    else
                        data.onUse(id)
                        mw2.drawReadyEst(id)
                    end
                end
			end
		end
	end
	mw2.select(id)
end

addhook("select","mw2.select")
function mw2.select(id)
	mw2.drawSelectedSpell(id)
	if player(id,"weapontype") == 50 then
		if mw2.player.selected[id] > 0 then
			local wep_dir = "gfx/magic wizard 2/weapons/"
			if not (mw2.player.image[id] == nil) then
				freeimage(mw2.player.image[id])
				mw2.player.image[id] = nil
			end
			mw2.player.image[id] = image(wep_dir..""..mw2.magicks.spellsData[mw2.player.selected[id]].file,1,0,200+id)
		else
			if not (mw2.player.image[id] == nil) then
				freeimage(mw2.player.image[id])
				mw2.player.image[id] = nil
			end
		end
	else
		if not (mw2.player.image[id] == nil) then
			freeimage(mw2.player.image[id])
			mw2.player.image[id] = nil
		end
	end
end


function mw2.overRideSelect(id,spell)
	mw2.player.selected[id] = spell
	mw2.select(id)
end
