-- mw2.addProjectile(name,collide_with,rot,speed,size,lifetime,image,onSpawn,onHitWall,onHitPlayer,onHitMagic,onUpdate,onDespawn)
-- mw2.addMagic(name,category,scrollsNeeded,manaUse,file,onUse)

------------------------------
--[PHASE 1: ADD PROJECTILES]--
------------------------------

-------------
--[CHARGES]--
-------------

mw2.name.TaintCharge = mw2.addProjectile("Taint Charge",12,1,-90,32,25,32,"charges/tainted_charge",function(magic)--spawn

end,function(magic) --hit wall
	magic.dispose = true
end,function(magic,id) --hit player
    mw2.damagePlayer(magic.owner,id,25,magic.me.name)
    mw2.damageMaxHealth(magic.owner,id,20,"Taint Charge")
	magic.dispose = true
end,function(magic,otherMagic) --hit magic

end,function(magic) --update

end,function(magic) -- despawn

end)

-----------------
--[PROJECTILES]--
-----------------

mw2.name.TaintBall = mw2.addProjectile("Tainted Ball",30,1,false,32,32,32,"projectile/tainted_ball",function(magic)--spawn

end,function(magic) --hit wall
    mw2.magicBack(magic)
    mw2.explosion(magic.x,magic.y,50,50,magic.owner)
	magic.dispose = true
end,function(magic,id) --hit player
    mw2.damagePlayer(magic.owner,id,25,magic.me.name)
    mw2.damageMaxHealth(magic.owner,id,20,"Tainted Ball")
    --local maxhealth = player(id,"maxhealth")-20
    --parse("setmaxhealth "..id.." "..maxhealth)
	magic.dispose = true
end,function(magic,otherMagic) --hit magic

end,function(magic) --update

end,function(magic) -- despawn

end)

-----------
--[BUILD]--
-----------

mw2.name.antiBuild = mw2.addProjectile("Anti Build",15,1,0,16,32,16,"build/antiBuild",function(magic)--spawn

end,function(magic) --hit wall
    x = misc.tile_to_pixel(misc.pixel_to_tile(magic.x))
    y = misc.tile_to_pixel(misc.pixel_to_tile(magic.y))
    mw2.explosion(x,y,8,1000,magic.owner)
	magic.dispose = true
end,function(magic,id) --hit player

end,function(magic,otherMagic) --hit magic

end,function(magic) --update

end,function(magic) -- despawn

end)

--------------------
--[SELF AFFECTORS]--
--------------------

mw2.name.SelfHeal = mw2.addProjectile("Self Heal",5,0,false,0,32,10,"self/heal",function(magic)--spawn
    misc.effect("flare",magic.x,magic.y,25,32,255,0,0)
    local heal = mw2.honor(magic.owner) * 0.5
    if (mw2flags.hasFlag(magic.owner,mw2flags.library.inCombat)) then
        heal = heal * 0.2
    end
    if (player(magic.owner,"bot")) then
        heal = heal * 0.5
    end
    local health = player(magic.owner,"health")+heal
    local maxhp = player(magic.owner,"maxhealth") + (heal*0.25)
    parse("setmaxhealth "..magic.owner.." "..maxhp)
    parse("sethealth "..magic.owner.." "..health)
    tween_alpha(magic.image,500,0)
end,function(magic) --hit wall

end,function(magic,id) --hit player

end,function(magic,otherMagic) --hit magic

end,function(magic) --update

end,function(magic) -- despawn

end)

-----------
--[DEFLECTOR]--
-----------

local deflectorFunctions = {}

function deflectorFunctions.onSpawn(magic)
    magic.deflectorAlive = true
    tween_alpha(magic.image,magic.lifetime*100,0)
end

function deflectorFunctions.onWall(magic)

end

function deflectorFunctions.onPlayer(magic, id)

end

function deflectorFunctions.deflectMagic(magic, otherMagic)
    otherMagic.owner = magic.owner
    otherMagic.dir = otherMagic.dir+180
    if otherMagic.dir > 360 then
        otherMagic.dir = otherMagic.dir - 360
    end
    otherMagic.xmove = misc.lengthdir_x(otherMagic.dir,otherMagic.me.speed)
    otherMagic.ymove = misc.lengthdir_y(otherMagic.dir,otherMagic.me.speed)
    tween_move(otherMagic.image,otherMagic.lifetime*100,otherMagic.x+(otherMagic.xmove*otherMagic.lifetime),otherMagic.y+(otherMagic.ymove*otherMagic.lifetime),otherMagic.dir)
end

function deflectorFunctions.onMagic(magic, otherMagic)
    local magicDamage = math.min(magic.magicHealth,otherMagic.magicHealth)
    magic.magicHealth = magic.magicHealth - magicDamage

    if (magic.magicHealth > 0) then
        deflectorFunctions.deflectMagic(magic, otherMagic)
    end

	local alpha = (1/(magic.me.lifetime*magic.me.magicHealth))*(magic.lifetime*magic.magicHealth)
    imagealpha(magic.image,alpha)
    tween_alpha(magic.image,magic.lifetime*100,0)
end

function deflectorFunctions.onUpdate(magic)

end


function deflectorFunctions.onDespawn(magic)

end

mw2.name.WeakDeflector = mw2.addProjectile("Weak Deflector",40,2,-90,0,96,64,"deflector/deflector"
,deflectorFunctions.onSpawn
,deflectorFunctions.onWall
,deflectorFunctions.onPlayer
,deflectorFunctions.onMagic
,deflectorFunctions.onUpdate
,deflectorFunctions.onDespawn)

mw2.name.MediumDeflector = mw2.addProjectile("Medium Deflector",80,2,-90,0,96,128,"deflector/deflector"
,deflectorFunctions.onSpawn
,deflectorFunctions.onWall
,deflectorFunctions.onPlayer
,deflectorFunctions.onMagic
,deflectorFunctions.onUpdate
,deflectorFunctions.onDespawn)

mw2.name.StrongDeflector = mw2.addProjectile("Strong Deflector",160,2,-90,0,96,256,"deflector/deflector"
,deflectorFunctions.onSpawn
,deflectorFunctions.onWall
,deflectorFunctions.onPlayer
,deflectorFunctions.onMagic
,deflectorFunctions.onUpdate
,deflectorFunctions.onDespawn)

mw2.name.SuperiorDeflector = mw2.addProjectile("Superior Deflector",320,2,-90,0,96,512,"deflector/deflector"
,deflectorFunctions.onSpawn
,deflectorFunctions.onWall
,deflectorFunctions.onPlayer
,deflectorFunctions.onMagic
,deflectorFunctions.onUpdate
,deflectorFunctions.onDespawn)

------------
--[SUMMON]--
------------

mw2.name.summonSoldier = mw2.addProjectile("Summon Soldier",10,1,-90,8,16,32,"summon/Tile2304_C75_R10",function(magic)--spawn

end,function(magic) --hit wall
    mw2.magicBack(magic)
    x = misc.pixel_to_tile(magic.x)
    y = misc.pixel_to_tile(magic.y)
    parse("spawnnpc 5 "..x.." "..y)
    magic.dispose = true
end,function(magic,id) --hit player
	mw2.magicBack(magic)
    x = misc.pixel_to_tile(magic.x)
    y = misc.pixel_to_tile(magic.y)
    parse("spawnnpc 5 "..x.." "..y)
    magic.dispose = true
end,function(magic,otherMagic) --hit magic

end,function(magic) --update

end,function(magic) -- despawn
    mw2.magicBack(magic)
    x = misc.pixel_to_tile(magic.x)
    y = misc.pixel_to_tile(magic.y)
    parse("spawnnpc 5 "..x.." "..y)
    magic.dispose = true
end)

mw2.name.summonSoldierArmy = mw2.addProjectile("Summon Soldier Army",15,1,-90,8,16,32,"summon/Tile2304_C75_R10",function(magic)--spawn

end,function(magic) --hit wall
    mw2.magicBack(magic)
    for i = 0, 3 do
        x = misc.pixel_to_tile(magic.x)+math.random(-1,1)
        y = misc.pixel_to_tile(magic.y)+math.random(-1,1)
        parse("spawnnpc 5 "..x.." "..y)
    end
    magic.dispose = true
end,function(magic,id) --hit player
	mw2.magicBack(magic)
    for i = 0, 3 do
        x = misc.pixel_to_tile(magic.x)+math.random(-1,1)
        y = misc.pixel_to_tile(magic.y)+math.random(-1,1)
        parse("spawnnpc 5 "..x.." "..y)
    end
    magic.dispose = true
end,function(magic,otherMagic) --hit magic

end,function(magic) --update

end,function(magic) -- despawn
    mw2.magicBack(magic)
    for i = 0, 3 do
        x = misc.pixel_to_tile(magic.x)+math.random(-1,1)
        y = misc.pixel_to_tile(magic.y)+math.random(-1,1)
        parse("spawnnpc 5 "..x.." "..y)
    end
    magic.dispose = true
end)

mw2.name.summonSoldierArc = mw2.addProjectile("Summon Soldier Arc",20,1,-90,8,16,32,"summon/Tile2304_C75_R10",function(magic)--spawn

end,function(magic) --hit wall
    mw2.magicBack(magic)
    x = misc.pixel_to_tile(magic.x)
    y = misc.pixel_to_tile(magic.y)
    for i = 0, 5 do
        x = x+math.random(-1,1)
        y = y+math.random(-1,1)
        parse("spawnnpc 5 "..x.." "..y)
    end
    magic.dispose = true
end,function(magic,id) --hit player
	mw2.magicBack(magic)
    x = misc.pixel_to_tile(magic.x)
    y = misc.pixel_to_tile(magic.y)
    for i = 0, 5 do
        x = x+math.random(-1,1)
        y = y+math.random(-1,1)
        parse("spawnnpc 5 "..x.." "..y)
    end
    magic.dispose = true
end,function(magic,otherMagic) --hit magic

end,function(magic) --update

end,function(magic) -- despawn
	mw2.magicBack(magic)
    x = misc.pixel_to_tile(magic.x)
    y = misc.pixel_to_tile(magic.y)
    for i = 0, 5 do
        x = x+math.random(-1,1)
        y = y+math.random(-1,1)
        parse("spawnnpc 5 "..x.." "..y)
    end
    magic.dispose = true
end)

mw2.name.summonArrow = mw2.addProjectile("Summoned Arrow",1,1,-90,32,16,32,"arrow/arrow",function(magic)--spawn
    tween_alpha(magic.image,magic.lifetime*100,0)
end,function(magic) --hit wall
    dir = magic.dir
    magic.xmove = 0
    magic.ymove = 0
    xadd = misc.lengthdir_x(dir,3)
    yadd = misc.lengthdir_y(dir,3)
    while (tile(misc.pixel_to_tile(magic.x),misc.pixel_to_tile(magic.y),"wall")) do
        magic.x = magic.x - xadd
        magic.y = magic.y - yadd
    end
    tween_move(magic.image,0,magic.x,magic.y,dir-90)
end,function(magic,id) --hit player
    mw2.damagePlayer(magic.owner,id,15,magic.me.name)
    magic.dispose = true
end,function(magic,otherMagic) --hit magic

end,function(magic) --update

end,function(magic) -- despawn

end)

-------------
--[SPECIAL]--
-------------

mw2.name.linkedPortals = mw2.addProjectile("Linked Portal",50,4,-90,0,32,512,"special/portal",function(magic)--spawn
    tween_rotateconstantly(magic.image,math.random(10,20))
    imagealpha(magic.image,0)
    tween_alpha(magic.image,5000,1)
    magic.charge = 0
end,function(magic) --hit wall

end,function(magic,id) --hit player
    if (magic.charge > 50) then
        --if player(id,"x") < magic.x+magic.size and player(id,"x") > magic.x-magic.size and player(id,"y") < magic.y+magic.size and player(id,"y") > magic.y-magic.size then
        local avaliable_portals = {}
        i = 0
        while (i < #mw2.entity) do
            i = i + 1
            local entity = mw2.entity[i]
            if (magic ~= entity) and (entity.me == magic.me) then
                avaliable_portals[#avaliable_portals+1] = entity
            end
        end
        if #avaliable_portals == 0 then
            magic.charge = 40
            imagealpha(magic.image,0)
            tween_alpha(magic.image,1000,1)
            msg2(id,misc.rgb(255,0,0).."Failed to link portal")
        else
            magic.charge = 0
            imagealpha(magic.image,0)
            tween_alpha(magic.image,5000,1)
            --mw2.spawnProjectile(mw2.name.pushingShockwave,magic.x,magic.y,magic.owner,0)
            msg2(id,misc.rgb(0,255,0).."Portal Linked!")
            mw2.playerSpeed(id,3)
            if player(id,"maxhealth") < 100 then
                parse("setmaxhealth "..id.." 100")
            end
            targetMagic = avaliable_portals[math.random(#avaliable_portals)]
                targetMagic.charge = 0
                imagealpha(targetMagic.image,0)
                tween_alpha(targetMagic.image,5000,1)
                --mw2.spawnProjectile(mw2.name.pushingShockwave,targetMagic.x,targetMagic.y,targetMagic.owner,0)
            cs2d.setpos.call(id,targetMagic.x,targetMagic.y)
        --end
        end
    end
end,function(magic,otherMagic) --hit magic

end,function(magic) --update
    magic.charge = magic.charge + 1
end,function(magic) -- despawn
    mw2.explosion(magic.x,magic.y,64,64,magic.owner)
end)

mw2.name.phaseWall = mw2.addProjectile("phase Wall",25,1,0,5,16,50,"special/Tile2366_C77_R10",function(magic)--spawn
    magic.playerPos = {x=magic.x,y=magic.y}
end,function(magic) --hit wall

end,function(magic,id) --hit player

end,function(magic,otherMagic) --hit magic

end,function(magic) --update
    --mw2.damagePlayer(magic.owner,magic.owner,-1,magic.me.name)
    mw2.playerSpeedSet(magic.owner,-10)
    if (magic.lifetime == 25) then
        if tile(misc.pixel_to_tile(magic.x),misc.pixel_to_tile(magic.y),"walkable") then
            if tile(misc.pixel_to_tile(magic.x),misc.pixel_to_tile(magic.y),"frame") > 0 then
                tween_move(magic.image,2500,magic.playerPos.x,magic.playerPos.y)
                magic.xmove = -magic.xmove
                magic.ymove = -magic.ymove
            else
                magic.dispose = true
                mw2.explosion(magic.x,magic.y,10,10,magic.owner)
            end
        else
            magic.dispose = true
            mw2.explosion(magic.x,magic.y,10,10,magic.owner)
        end
    end
    if (magic.lifetime < 25) then
        magic.playerPos.x = magic.playerPos.x - magic.xmove
        magic.playerPos.y = magic.playerPos.y - magic.ymove
        cs2d.setpos.call(magic.owner,magic.playerPos.x,magic.playerPos.y)
    else
        cs2d.setpos.call(magic.owner,magic.playerPos.x,magic.playerPos.y)
    end
end,function(magic) -- despawn

end)

---------
--[ARC]--
---------

mw2.name.pureArc = mw2.addProjectile("pure Arc",10,1,0,0,0,8,"arc/arc_one",function(magic)--spawn
    tween_alpha(magic.image,magic.lifetime*100,0)
    pl = misc.shuffle_array(player(0,"table"))
    max_dist = math.random(200,250)
    target = nil
    i = 0
    while (i < #pl) do
        i = i + 1
        if (not(pl[i] == magic.owner)) then
            x = player(pl[i],"x")
            y = player(pl[i],"y")
            dist = misc.point_distance(magic.x,magic.y,x,y)
            dist = (dist*(math.random(5,15)/10))
            if dist < max_dist then
                max_dist = dist
                target = {id=pl[i],dist=dist,x=x,y=y}
            end
        end
    end
    if (target == nil) then
        target = {}
        target.id = 0
        target.x = math.random(magic.x-100,magic.x+100)
        target.y = math.random(magic.y-100,magic.y+100)
        target.dist = misc.point_distance(magic.x,magic.y,target.x,target.y)
    end
    dir = misc.point_direction(magic.x,magic.y,target.x,target.y)
    magic.x = magic.x + misc.lengthdir_x(dir,target.dist*0.5)
    magic.y = magic.y + misc.lengthdir_y(dir,target.dist*0.5)
    tween_scale(magic.image,1,1,target.dist/32)
    tween_move(magic.image,1,magic.x,magic.y,dir) -- using tween_move due to cs2d bug
    mw2.explosion(target.x,target.y,50,10,magic.owner)
end,function(magic) --hit wall

end,function(magic,id) --hit player

end,function(magic,otherMagic) --hit magic

end,function(magic) --update

end,function(magic) -- despawn

end)

mw2.name.rangeArc = mw2.addProjectile("range Arc",15,1,0,0,0,8,"arc/arc_one",function(magic)--spawn
    tween_alpha(magic.image,magic.lifetime*100,0)
    pl = misc.shuffle_array(player(0,"table"))
    max_dist = math.random(400,500)
    target = nil
    i = 0
    while (i < #pl) do
        i = i + 1
        if (not(pl[i] == magic.owner)) then
            x = player(pl[i],"x")
            y = player(pl[i],"y")
            dist = misc.point_distance(magic.x,magic.y,x,y)
            dist = (dist*(math.random(5,15)/10))
            if dist < max_dist then
                max_dist = dist
                target = {id=pl[i],dist=dist,x=x,y=y}
            end
        end
    end
    if (target == nil) then
        target = {}
        target.id = 0
        target.x = math.random(magic.x-100,magic.x+100)
        target.y = math.random(magic.y-100,magic.y+100)
        target.dist = misc.point_distance(magic.x,magic.y,target.x,target.y)
    end
    dir = misc.point_direction(magic.x,magic.y,target.x,target.y)
    magic.x = magic.x + misc.lengthdir_x(dir,target.dist*0.5)
    magic.y = magic.y + misc.lengthdir_y(dir,target.dist*0.5)
    tween_scale(magic.image,1,1,target.dist/32)
    tween_move(magic.image,1,magic.x,magic.y,dir) -- using tween_move due to cs2d bug
    mw2.explosion(target.x,target.y,75,25,magic.owner)
end,function(magic) --hit wall

end,function(magic,id) --hit player

end,function(magic,otherMagic) --hit magic

end,function(magic) --update

end,function(magic) -- despawn

end)

mw2.name.powerArc = mw2.addProjectile("Power Arc",20,1,0,0,0,8,"arc/arc_one",function(magic)--spawn
    tween_alpha(magic.image,magic.lifetime*100,0)
    pl = misc.shuffle_array(player(0,"table"))
    max_dist = math.random(250,350)
    target = nil
    i = 0
    while (i < #pl) do
        i = i + 1
        if (not(pl[i] == magic.owner)) then
            x = player(pl[i],"x")
            y = player(pl[i],"y")
            dist = misc.point_distance(magic.x,magic.y,x,y)
            dist = (dist*(math.random(5,15)/10))
            if dist < max_dist then
                max_dist = dist
                target = {id=pl[i],dist=dist,x=x,y=y}
            end
        end
    end
    if (target == nil) then
        target = {}
        target.id = 0
        target.x = math.random(magic.x-100,magic.x+100)
        target.y = math.random(magic.y-100,magic.y+100)
        target.dist = misc.point_distance(magic.x,magic.y,target.x,target.y)
    end
    dir = misc.point_direction(magic.x,magic.y,target.x,target.y)
    magic.x = magic.x + misc.lengthdir_x(dir,target.dist*0.5)
    magic.y = magic.y + misc.lengthdir_y(dir,target.dist*0.5)
    tween_scale(magic.image,1,1,target.dist/32)
    tween_move(magic.image,1,magic.x,magic.y,dir) -- using tween_move due to cs2d bug
    mw2.explosion(target.x,target.y,100,50,magic.owner)
end,function(magic) --hit wall

end,function(magic,id) --hit player

end,function(magic,otherMagic) --hit magic

end,function(magic) --update

end,function(magic) -- despawn

end)

-------------
--[JUMPERS]--
-------------

mw2.name.explosiveCloudJumper = mw2.addProjectile("Explosive Cloud Jumper",40,1,false,8,128,64,"jumpers/firecloud",function(magic)--spawn
    magic.count = math.random(5,10)
end,function(magic) --hit wall

end,function(magic,id) --hit player

end,function(magic,otherMagic) --hit magic

end,function(magic) --update
    magic.count = magic.count - 1
	if magic.count <= 0 then
        magic.count = math.random(5,10)
		mw2.spawnProjectile(mw2.name.ExplosiveJumper,magic.x,magic.y,magic.owner,math.random()*360-180)
	end
end,function(magic) -- despawn

end)

---------------
--[SHOCKWAVE]--
---------------

mw2.name.pureShockwave = mw2.addProjectile("Pure Shockwave",20,1,false,0,0,20,"shockwaves/pureShockwave",function(magic)--spawn
    imagescale(magic.image,0,0)
    tween_scale(magic.image,2000,10,10)
end,function(magic) --hit wall

end,function(magic,id) --hit player
    power = 1
    if (magic.lifetime < 10) then
        power = magic.lifetime*0.1
    end
    if math.random(100) < 50*power then
        mw2.damagePlayer(magic.owner,id,power*7,magic.me.name)
    end
end,function(magic,otherMagic) --hit magic

end,function(magic) --update
    if (magic.lifetime == 10) then
        tween_alpha(magic.image,1000,0)
    end
    hitbox = ((64*10) - ((magic.lifetime*0.5)*64))*0.5
    magic.size = hitbox
end,function(magic) -- despawn

end)

mw2.name.iceShockwave = mw2.addProjectile("Ice Shockwave",30,1,false,0,0,20,"shockwaves/iceShockwave",function(magic)--spawn
    imagescale(magic.image,0,0)
    tween_scale(magic.image,2000,10,10)
end,function(magic) --hit wall

end,function(magic,id) --hit player
    power = 1
    if (magic.lifetime < 10) then
        power = magic.lifetime*0.1
    end
    if math.random(100) < 50*power then
        mw2.damagePlayer(magic.owner,id,power*2,magic.me.name)
    end
    if math.random(100) < 100*power then
        power = power / 2 -- weakening it
        mw2.playerSpeed(id,-(1+(power*2)))
    end
end,function(magic,otherMagic) --hit magic

end,function(magic) --update
    if (magic.lifetime == 10) then
        tween_alpha(magic.image,1000,0)
    end
    hitbox = ((64*10) - ((magic.lifetime*0.5)*64))*0.5
    magic.size = hitbox
end,function(magic) -- despawn

end)

mw2.name.pushingShockwave = mw2.addProjectile("Pushing Shockwave",50,1,false,0,0,20,"shockwaves/pushingShockwave",function(magic)--spawn
    imagescale(magic.image,0,0)
    tween_scale(magic.image,2000,10,10)
end,function(magic) --hit wall

end,function(magic,id) --hit player
    power = 1
    if (magic.lifetime < 10) then
        power = magic.lifetime*0.1
    end

    x = player(id,"x")
    y = player(id,"y")
    dir = misc.point_direction(magic.x,magic.y,x,y)
    if tile(misc.pixel_to_tile(x + misc.lengthdir_x(dir,40*power)),misc.pixel_to_tile(y + misc.lengthdir_y(dir,40*power)),"walkable") then
        x = x + misc.lengthdir_x(dir,20*power)
        y = y + misc.lengthdir_y(dir,20*power)
        cs2d.setpos.call(id,x,y)
    end
end,function(magic,otherMagic) --hit magic

end,function(magic) --update
    if (magic.lifetime == 10) then
        tween_alpha(magic.image,1000,0)
    end
    hitbox = ((64*10) - ((magic.lifetime*0.5)*64))*0.5
    magic.size = hitbox
end,function(magic) -- despawn

end)

mw2.name.explosiveShockwave = mw2.addProjectile("Explosive Shockwave",40,1,false,0,0,20,"shockwaves/explosiveShockwave",function(magic)--spawn
    imagescale(magic.image,0,0)
    tween_scale(magic.image,2000,10,10)
end,function(magic) --hit wall

end,function(magic,id) --hit player
    power = 1
    if (magic.lifetime < 10) then
        power = magic.lifetime*0.1
    end
    if math.random(10) < power*10 then
        mw2.explosion(player(id,"x"),player(id,"y"),25*power,10*power,magic.owner)
    end
end,function(magic,otherMagic) --hit magic

end,function(magic) --update
    if (magic.lifetime == 10) then
        tween_alpha(magic.image,1000,0)
    end
    hitbox = ((64*10) - ((magic.lifetime*0.5)*64))*0.5
    magic.size = hitbox
end,function(magic) -- despawn

end)

---------------
--[ILLUSION]--
---------------

mw2.name.healthPotionIllusion = mw2.addProjectile("Health Potion Illusion",1,1,-90,0,16,512,"build/Tile823_C27_R17",function(magic)--spawn
	freeimage(magic.image)
	magic.image = image("gfx/magic wizard 2/items/Tile1808_C59_R10.png",magic.x,magic.y,0)
	magic.x = (misc.round((magic.x-16)/32)*32)+16
	magic.y = (misc.round((magic.y-16)/32)*32)+16
	tween_move(magic.image,5000,magic.x,magic.y)
end,function(magic) --hit wall

end,function(magic,id) --hit player
	for i=1, 3 do
		parse("spawnnpc "..math.random(1,4).." "..misc.pixel_to_tile(magic.x)+math.random(-1,1).." "..misc.pixel_to_tile(magic.y)+math.random(-1,1).." "..magic.dir)
	end
	msg2(id,"triggered a trap by "..player(magic.owner,"name"))
	magic.dispose = true
end,function(magic,otherMagic) --hit magic

end,function(magic) --update

end,function(magic) -- despawn

end)

---------------
--[FIREWORKS]--
---------------

mw2.name.singleFirework = mw2.addProjectile("Single Firework",2,1,-90,32,16,5,"firework/Tile2700_C88_R3",function(magic)--spawn

end,function(magic) --hit wall

end,function(magic,id) --hit player

end,function(magic,otherMagic) --hit magic

end,function(magic) --update

end,function(magic) -- despawn
    for i=1,math.random(2,3) do
        misc.effect("flare",magic.x+math.random(-32,32),magic.y+math.random(-32,32),math.random(32,64),math.random(32,64),math.random(255),math.random(255),math.random(255))
    end
end)

mw2.name.multiFirework = mw2.addProjectile("Multi Firework",6,1,-90,32,16,5,"firework/Tile2700_C88_R3",function(magic)--spawn

end,function(magic) --hit wall

end,function(magic,id) --hit player

end,function(magic,otherMagic) --hit magic

end,function(magic) --update

end,function(magic) -- despawn
    misc.effect("flare",magic.x+math.random(-32,32),magic.y+math.random(-32,32),math.random(32,64),math.random(32,64),math.random(255),math.random(255),math.random(255))
    for i=1,5 do
        mw2.spawnProjectile(mw2.name.singleFirework,magic.x,magic.y,magic.owner,math.random(360))
    end
end)


------------------------
--[PHASE 2: ADD MAGIC]-- -- mw2.addMagic(name,category,scrollsNeeded,manaUse,file,onUse)
------------------------

-------------
--[CHARGES]--
-------------

mw2.addMagic("Taint Charge","Charge",8,8,"wand_2.png",function(id)
	mw2.spawnProjectile(mw2.name.TaintCharge,player(id,"x"),player(id,"y"),id,player(id,"rot"))
end)

-----------------
--[PROJECTILES]--
-----------------

mw2.addMagic("Taint Ball","Projectile",12,12,"wand_3.png",function(id)
	mw2.spawnProjectile(mw2.name.TaintBall,player(id,"x"),player(id,"y"),id,player(id,"rot"))
end)

-----------
--[BUILD]--
-----------

mw2.addMagic("Anti Build","Build",10,20,"wand_11.png",function(id)
	mw2.spawnProjectile(mw2.name.antiBuild,player(id,"x"),player(id,"y"),id,player(id,"rot"))
end)

--------------------
--[SELF AFFECTORS]--
--------------------

mw2.addMagic("Self Heal","Self Affector",10,25,"wand_9.png",function(id)
	mw2.spawnProjectile(mw2.name.SelfHeal,player(id,"x"),player(id,"y"),id,player(id,"rot"))
end)

----------------
--[DEFLECTORS]--
----------------

mw2.addMagic("Weak Deflector","Deflector",10,20,"wand_16.png",function(id)
	mw2.spawnProjectile(mw2.name.WeakDeflector,player(id,"x"),player(id,"y"),id,player(id,"rot"))
end)

mw2.addMagic("Medium Deflector","Deflector",20,30,"wand_16.png",function(id)
	mw2.spawnProjectile(mw2.name.MediumDeflector,player(id,"x"),player(id,"y"),id,player(id,"rot"))
end)

mw2.addMagic("Strong Deflector","Deflector",30,50,"wand_16.png",function(id)
	mw2.spawnProjectile(mw2.name.StrongDeflector,player(id,"x"),player(id,"y"),id,player(id,"rot"))
end)

mw2.addMagic("Superior Deflector","Deflector",40,60,"wand_16.png",function(id)
	mw2.spawnProjectile(mw2.name.SuperiorDeflector,player(id,"x"),player(id,"y"),id,player(id,"rot"))
end)

------------
--[SUMMON]--
------------

mw2.addMagic("Summon Soldier","Summon",3,25,"wand_12.png",function(id)
	mw2.spawnProjectile(mw2.name.summonSoldier,player(id,"x"),player(id,"y"),id,player(id,"rot"))
end)

mw2.addMagic("Summon Soldier Army","Summon",15,50,"wand_12.png",function(id)
	mw2.spawnProjectile(mw2.name.summonSoldierArmy,player(id,"x"),player(id,"y"),id,player(id,"rot"))
end)

mw2.addMagic("Summon Soldier Arc","Summon",20,75,"wand_12.png",function(id)
	mw2.spawnProjectile(mw2.name.summonSoldierArc,player(id,"x"),player(id,"y"),id,player(id,"rot"))
end)

mw2.addMagic("Summon Arrow","Summon",0,7,"crossbow.png",function(id)
	mw2.spawnProjectile(mw2.name.summonArrow,player(id,"x"),player(id,"y"),id,player(id,"rot"))
end)

mw2.addMagic("Summon multiple Arrows","Summon",7,7*3,"crossbow.png",function(id)
	mw2.spawnProjectile(mw2.name.summonArrow,player(id,"x"),player(id,"y"),id,player(id,"rot")-5)
	mw2.spawnProjectile(mw2.name.summonArrow,player(id,"x"),player(id,"y"),id,player(id,"rot"))
	mw2.spawnProjectile(mw2.name.summonArrow,player(id,"x"),player(id,"y"),id,player(id,"rot")+5)
end)

-------------
--[SPECIAL]--
-------------

mw2.addMagic("Linked Portal","Special",5,50,"wand_13.png",function(id)
	mw2.spawnProjectile(mw2.name.linkedPortals,player(id,"x"),player(id,"y"),id,player(id,"rot"))
end)

mw2.addMagic("Phase Wall","Special",20,30,"wand_13.png",function(id)
	mw2.spawnProjectile(mw2.name.phaseWall,player(id,"x"),player(id,"y"),id,player(id,"rot"))
end)

---------
--[ARC]--
---------

mw2.addMagic("Pure Arc","Arc",10,10,"wand_17.png",function(id)
	mw2.spawnProjectile(mw2.name.pureArc,player(id,"x"),player(id,"y"),id,player(id,"rot"))
end)

mw2.addMagic("Range Arc","Arc",15,15,"wand_17.png",function(id)
	mw2.spawnProjectile(mw2.name.rangeArc,player(id,"x"),player(id,"y"),id,player(id,"rot"))
end)

mw2.addMagic("Power Arc","Arc",25,25,"wand_17.png",function(id)
	mw2.spawnProjectile(mw2.name.powerArc,player(id,"x"),player(id,"y"),id,player(id,"rot"))
end)

-------------
--[JUMPERS]--
-------------

mw2.addMagic("Explosive Cloud Jumper","Jumpers",40,80,"wand_6.png",function(id)
	mw2.spawnProjectile(mw2.name.explosiveCloudJumper,player(id,"x"),player(id,"y"),id,player(id,"rot"))
end)

---------------
--[SHOCKWAVE]--
---------------

mw2.addMagic("Pure Shockwave","Shockwave",10,20,"wand_18.png",function(id)
	mw2.spawnProjectile(mw2.name.pureShockwave,player(id,"x"),player(id,"y"),id,player(id,"rot"))
end)

mw2.addMagic("Ice Shockwave","Shockwave",15,30,"wand_18.png",function(id)
	mw2.spawnProjectile(mw2.name.iceShockwave,player(id,"x"),player(id,"y"),id,player(id,"rot"))
end)

mw2.addMagic("Pushing Shockwave","Shockwave",10,10,"wand_18.png",function(id)
	mw2.spawnProjectile(mw2.name.pushingShockwave,player(id,"x"),player(id,"y"),id,player(id,"rot"))
end)

mw2.addMagic("Explosive Shockwave","Shockwave",20,35,"wand_18.png",function(id)
	mw2.spawnProjectile(mw2.name.explosiveShockwave,player(id,"x"),player(id,"y"),id,player(id,"rot"))
end)

--------------
--[ILLUSION]--
--------------

mw2.addMagic("Health Potion Illusion","Illusion",10,50,"wand_19.png",function(id)
	mw2.spawnProjectile(mw2.name.healthPotionIllusion,player(id,"x"),player(id,"y"),id,player(id,"rot"))
end)

---------------
--[FIREWORKS]--
---------------

mw2.addMagic("Single Firework","Firework",1,5,"wand_19.png",function(id)
	mw2.spawnProjectile(mw2.name.singleFirework,player(id,"x"),player(id,"y"),id,player(id,"rot"))
end)

mw2.addMagic("Multi Firework","Firework",5,15,"wand_19.png",function(id)
	mw2.spawnProjectile(mw2.name.multiFirework,player(id,"x"),player(id,"y"),id,player(id,"rot"))
end)
