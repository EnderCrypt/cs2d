------------------------------
--[PHASE 1: ADD PROJECTILES]--
------------------------------

-----------
--[WINDS]--
-----------

mw2.name.TaintWind = mw2.addProjectileBuilder({
    name = "Transportation whirlwind",
    magicHealth = 17,
    collideWith = 1,
    rotation = false,
    speed = 22,
    size = 64,
    lifetime = 32*5,
    image = "whirlwind/taintwhirlwind",
    onSpawn = function(magic)

    end,
    onHitWall = function(magic)
        mw2.magicBack(magic)
        mw2.explosion(magic.x,magic.y,96,16,magic.owner)
        magic.dispose = true
    end,
    onHitPlayer = function(magic, id)
        local newx = player(id,"x")+(magic.xmove*1.25)
        local newy = player(id,"y")+(magic.ymove*1.25)
        if tile(misc.pixel_to_tile(newx),misc.pixel_to_tile(newy),"walkable") and (tile(misc.pixel_to_tile(newx),misc.pixel_to_tile(newy),"frame") > 0) then
            tp_smoke = false
            cs2d.setpos.call(id,newx,newy)
        end

        mw2.damagePlayer(magic.owner,id,6,magic.me.name)

				local maxhealth = player(id, "maxhealth")
				local newmaxhealth = maxhealth - 3
				parse("setmaxhealth "..id.." "..newmaxhealth)

    end,
    onHitMagic = function(magic, otherMagic)

    end,
    onUpdate = function(magic)

    end,
    onDespawn = function(magic)

    end
    })

mw2.name.VampireWind = mw2.addProjectileBuilder({
    name = "Vampire whirlwind",
    magicHealth = 17,
    collideWith = 1,
    rotation = false,
    speed = 22,
    size = 64,
    lifetime = 32*5,
    image = "whirlwind/blackwhirlwind",
    onSpawn = function(magic)

    end,
    onHitWall = function(magic)
        mw2.magicBack(magic)
        mw2.explosion(magic.x,magic.y,96,16,magic.owner)
        magic.dispose = true
    end,
    onHitPlayer = function(magic, id)
        local newx = player(id,"x")+(magic.xmove*1.25)
        local newy = player(id,"y")+(magic.ymove*1.25)
        local dmg = mw2.damagePlayer(magic.owner,id,5,magic.me.name)
				local healing = -(dmg * 0.75)
        mw2.damagePlayer(id,magic.owner,healing,magic.me.name)
        if tile(misc.pixel_to_tile(newx),misc.pixel_to_tile(newy),"walkable") and (tile(misc.pixel_to_tile(newx),misc.pixel_to_tile(newy),"frame") > 0) then
            tp_smoke = false
            cs2d.setpos.call(id,newx,newy)
        end
    end,
    onHitMagic = function(magic, otherMagic)

    end,
    onUpdate = function(magic)

    end,
    onDespawn = function(magic)

    end
    })

mw2.name.TransportWind = mw2.addProjectileBuilder({
    name = "Transportation whirlwind",
    magicHealth = 20,
    collideWith = 1,
    rotation = false,
    speed = 40,
    size = 32,
    lifetime = 32*5,
    image = "whirlwind/Tile1384_C45_R20",
    onSpawn = function(magic)

    end,
    onHitWall = function(magic)
        magic.dispose = true
    end,
    onHitPlayer = function(magic, id)
        local newx = player(id,"x")+(magic.xmove*1.25)
        local newy = player(id,"y")+(magic.ymove*1.25)
        if tile(misc.pixel_to_tile(newx),misc.pixel_to_tile(newy),"walkable") and (tile(misc.pixel_to_tile(newx),misc.pixel_to_tile(newy),"frame") > 0) then
            tp_smoke = false
            cs2d.setpos.call(id,newx,newy)
        end
    end,
    onHitMagic = function(magic, otherMagic)

    end,
    onUpdate = function(magic)

    end,
    onDespawn = function(magic)

    end
    })

--------------------
--[AREA AFFECTORS]--
--------------------

mw2.name.AreaVampire = mw2.addProjectileBuilder({
    name = "Vampire Affector",
    magicHealth = 20,
    collideWith = 4,
    rotation = false,
    speed = 0.1,
    size = 96,
    lifetime = 128,
    image = "area/Tile1683_C55_R9_vampire",
    onSpawn = function(magic)

    end,
    onHitWall = function(magic)
        magic.dispose = true
    end,
    onHitPlayer = function(magic, id)
        if math.random(100) < 16 then
            local dmg = mw2.damagePlayer(magic.owner,id,10,magic.me.name)
            mw2.damagePlayer(id,magic.owner,-(dmg * 0.75),magic.me.name)
        end
    end,
    onHitMagic = function(magic, otherMagic)

    end,
    onUpdate = function(magic)

    end,
    onDespawn = function(magic)

    end
    })

--------------
--[TELEPORT]--
--------------

mw2.name.Blink = mw2.addProjectileBuilder({
    name = "Blink",
    magicHealth = 1,
    collideWith = 0,
    rotation = -90,
    speed = 50,
    size = 16,
    lifetime = 10,
    image = "teleport/blink",
    onSpawn = function(magic)

    end,
    onHitWall = function(magic)
        mw2.magicBack(magic)
        if (not(mw2flags.hasFlag(magic.owner,mw2flags.library.teleportPrevention))) then
            mw2.damagePlayer(0,magic.owner,5,magic.me.name)
        end
        magic.dispose = true
    end,
    onHitPlayer = function(magic, id)

    end,
    onHitMagic = function(magic, otherMagic)

    end,
    onUpdate = function(magic)

    end,
    onDespawn = function(magic)
        if (not(mw2flags.hasFlag(magic.owner,mw2flags.library.teleportPrevention))) then
            cs2d.setpos.call(magic.owner, magic.x, magic.y)
        end
    end
    })

mw2.name.Displacement = mw2.addProjectileBuilder({
    name = "Displace",
    magicHealth = 3,
    collideWith = 1,
    rotation = -90,
    speed = 50,
    size = 5,
    lifetime = 32,
    image = "teleport/displace",
    onSpawn = function(magic)
        magic.cooldown = 5
        magic.hitId = nil
    end,
    onHitWall = function(magic)
        magic.dispose = true
    end,
    onHitPlayer = function(magic, id)
        if (id ~= magic.hitId) then
            mw2.spawnProjectile(mw2.name.Blink,player(id,"x"),player(id,"y"),id,math.random()*360)
            magic.cooldown = 2
            magic.hitId = id
        end
    end,
    onHitMagic = function(magic, otherMagic)

    end,
    onUpdate = function(magic)
        magic.cooldown = magic.cooldown - 1
        if (magic.cooldown <= 0) then
            local closestId = nil
            local closestDist = 500
            for _,id in ipairs(player(0,"tableliving")) do
                if (id ~= magic.owner) then
                    local x = player(id,"x")
                    local y = player(id,"y")
                    local dist = misc.point_distance(magic.x, magic.y, x, y)
                    if (dist < closestDist) then
                        closestId = id
                        closestDist = dist
                    end
                end
            end
            if (closestId ~= nil) then
                local x = player(closestId,"x")
                local y = player(closestId,"y")
                imagepos(magic.image,magic.x,magic.y,magic.dir)
                mw2.projSetMotion(magic,misc.point_direction(magic.x, magic.y, x, y),magic.me.speed)
                magic.cooldown = 4
                magic.hitId = nil
            end
        end
    end,
    onDespawn = function(magic)

    end
    })

mw2.name.TemporalAbduction = mw2.addProjectileBuilder({
    name = "Temporal Abduction",
    magicHealth = 25,
    collideWith = 1,
    rotation = false,
    speed = 24,
    size = 20,
    lifetime = 50,
    image = "teleport/abduct",
    onSpawn = function(magic)
        tween_rotateconstantly(magic.image,-5)
        magic.target = nil
        magic.abductions = {}
    end,
    onHitWall = function(magic)
        magic.dispose = true
    end,
    onHitPlayer = function(magic, id)
        if (not(mw2flags.hasFlag(id,mw2flags.library.teleportPrevention))) then
            if (magic.target == nil) then
                magic.xmove = 0
                magic.ymove = 0
                local vx = 10
                local vy = 10
                while (tile(vx,vy,"frame") > 0) do
                    vx = misc.round(const.map.xsize*math.random())
                    vy = misc.round(const.map.ysize*math.random())
                end
                tween_move(magic.image,0,magic.x,magic.y)
                imagecolor(magic.image,255,100,100)
                magic.target = {x=misc.tile_to_pixel(vx),y=misc.tile_to_pixel(vy)}
            end
            cs2d.setpos.call(id, magic.target.x, magic.target.y)
            msg2(id,misc.rgb(255,0,0).."You get sucked into the void...")
            magic.abductions[#magic.abductions+1] = id
        end
    end,
    onHitMagic = function(magic, otherMagic)

    end,
    onUpdate = function(magic)

    end,
    onDespawn = function(magic)
        if (magic.target ~= nil) then
            for _,id in ipairs(magic.abductions) do
                msg2(id,misc.rgb(0,0,255).."The void rejects you back into your dimension")
                cs2d.setpos.call(id, magic.x, magic.y)
            end
        end
    end
    })

mw2.name.VoidPortal = mw2.addProjectileBuilder({
    name = "Void Portal",
    magicHealth = 25,
    collideWith = 0,
    rotation = false,
    speed = 0,
    size = 16,
    lifetime = 32,
    image = "teleport/voidPortal",
    onSpawn = function(magic)
        local vx = 10
        local vy = 10
        while (tile(vx,vy,"frame") > 0) do
            vx = misc.round(const.map.xsize*math.random())
            vy = misc.round(const.map.ysize*math.random())
        end
        magic.location = {x=misc.tile_to_pixel(vx),y=misc.tile_to_pixel(vy)}
        cs2d.setpos.call(magic.owner,magic.location.x,magic.location.y)
        misc.effect("flare",magic.x,magic.y,16,16,255,0,255)
        misc.effect("flare",magic.location.x,magic.location.y,16,16,255,0,255)
    end,
    onHitWall = function(magic)

    end,
    onHitPlayer = function(magic, id)

    end,
    onHitMagic = function(magic, otherMagic)

    end,
    onUpdate = function(magic)
        local px = player(magic.owner,"x")
        local py = player(magic.owner,"y")
        local x = magic.location.x
        local y = magic.location.y
        local rx = magic.x + (px - x)
        local ry = magic.y + (py - y)
        misc.effect("flare",rx,ry,5,1,255,0,255)
        local dir = misc.point_direction(x,y,px,py)
        local jumpDist = 32
        local xmove = misc.lengthdir_x(dir,jumpDist)
        local ymove = misc.lengthdir_y(dir,jumpDist)
        local dist = misc.point_distance(x,y,px,py)
        local jumps = math.floor(dist/jumpDist)
        -- draw line
        for i=0,jumps do
            if (math.random() < 0.2) then
                misc.effect("flare",x,y,5,1,255,0,255)
            end
            x = x + xmove
            y = y + ymove
        end
        -- draw nearby players
        for _,pid in ipairs(player(0,"tableliving")) do
            local ppx = player(pid,"x")
            local ppy = player(pid,"y")
            local prx = magic.location.x + (ppx - magic.x)
            local pry = magic.location.y + (ppy - magic.y)
            misc.effect("flare",prx,pry,5,1,255,0,0)
        end
    end,
    onDespawn = function(magic)
        local px = player(magic.owner,"x")
        local py = player(magic.owner,"y")
        misc.effect("flare",px,py,16,16,255,0,255)
        local x = magic.x + (px - magic.location.x)
        local y = magic.y + (py - magic.location.y)
        if ( (x < 0) or (y < 0) or (x > const.map.pxsize) or (y > const.map.pysize) ) then
            msg2(magic.owner,"You voided out of the map and died")
            parse("customkill 0 \"Wall\" "..magic.owner)
            return 0
        end
        cs2d.setpos.call(magic.owner,x,y)
        misc.effect("flare",magic.x,magic.y,16,16,255,0,255)
        misc.effect("flare",x,y,16,16,255,0,255)
        local tx = misc.pixel_to_tile(x)
        local ty = misc.pixel_to_tile(y)
        if (tile(tx,ty,"wall")) then
            parse("customkill 0 \"Wall\" "..magic.owner)
        end
    end
    })

-------------
--[SPECIAL]--
-------------

mw2.name.phaseWall2 = mw2.addProjectileBuilder({
    name = "phase Wall 2",
    magicHealth = 25,
    collideWith = 0,
    rotation = 0,
    speed = 4,
    size = 16,
    lifetime = 50,
    image = "special/phaseWall2",
    onSpawn = function(magic)
        magic.tiles = {}
    end,
    onHitWall = function(magic)
        local tx = misc.pixel_to_tile(magic.x)
        local ty = misc.pixel_to_tile(magic.y)
        local txp = misc.tile_to_pixel(tx)
        local typ = misc.tile_to_pixel(ty)
        local dir = misc.point_direction(magic.x,magic.y,txp,typ)
        dir = misc.round(dir/90)*90
        for i=0,10 do
            magic.tiles[#magic.tiles+1] = {x=tx,y=ty}
            misc.effect("smoke",misc.tile_to_pixel(tx),misc.tile_to_pixel(ty),16,16,0,0,0)
            parse("settile "..tx.." "..ty.." 0")
            tx = tx + misc.lengthdir_x(dir,1)
            ty = ty + misc.lengthdir_y(dir,1)
        end
        magic.dispose = true
    end,
    onHitPlayer = function(magic, id)

    end,
    onHitMagic = function(magic, otherMagic)

    end,
    onUpdate = function(magic)

    end,
    onDespawn = function(magic)
        timer2(3200,{magic},function(magic)
            local worker = LuaWorker.new(function(worker,workspace)
                local magic = workspace.magic
                if (#magic.tiles == 0) then
                    worker:delete()
                    return
                end
                local index = math.ceil(#magic.tiles*math.random())
                local location = magic.tiles[index]
                magic.tiles[index] = magic.tiles[#magic.tiles]
                magic.tiles[#magic.tiles] = nil
                local x = location.x
                local y = location.y
                local frame = tile(x,y,"originalframe")
                misc.effect("smoke",misc.tile_to_pixel(x),misc.tile_to_pixel(y),16,16,0,0,0)
                parse("settile "..x.." "..y.." "..frame)
            end)
            worker.workspace.magic = magic
            worker:setFrequency(LuaWorkerData.ms100)
            worker:start()
        end)
    end
    })

-----------
--[STARS]--
-----------

mw2.name.VampireStar = mw2.addProjectileBuilder({
    name = "Vampire Star",
    magicHealth = 80,
    collideWith = 1,
    rotation = -90,
    speed = 32,
    size = 32,
    lifetime = 128,
    image = "star/Tile3320_C108_R3_v",
    onSpawn = function(magic)

    end,
    onHitWall = function(magic)
        mw2.magicBack(magic)
        mw2.magicBack(magic)
        local dir = 0
        while dir < 360 do
            dir = dir + (360/16)
            mw2.spawnProjectile(mw2.name.VampiricBall,magic.x,magic.y,magic.owner,dir)
        end
        magic.dispose = true
    end,
    onHitPlayer = function(magic, id)
        mw2.magicBack(magic)
        local dir = 0
        while dir < 360 do
            dir = dir + (360/16)
            mw2.spawnProjectile(mw2.name.VampiricBall,magic.x,magic.y,magic.owner,dir)
        end
        local dmg = mw2.damagePlayer(magic.owner,id,100,magic.me.name)
        mw2.damagePlayer(id,magic.owner,-dmg * 0.75,magic.me.name)
        magic.dispose = true
    end,
    onHitMagic = function(magic, otherMagic)

    end,
    onUpdate = function(magic)

    end,
    onDespawn = function(magic)

    end
    })

-----------
--[WAVES]--
-----------

mw2.name.vampireWave = mw2.addProjectileBuilder({
    name = "Vampire Waves",
    magicHealth = 30,
    collideWith = 1,
    rotation = -90,
    speed = 32,
    size = 50,
    lifetime = 3,
    image = "wave/wave_vampire",
    onSpawn = function(magic)

    end,
    onHitWall = function(magic)
        magic.dispose = true
    end,
    onHitPlayer = function(magic, id)
        local dmg = mw2.damagePlayer(magic.owner,id,30,magic.me.name)
        mw2.damagePlayer(id,magic.owner,-(dmg * 0.75),magic.me.name)
        magic.dispose = true
    end,
    onHitMagic = function(magic, otherMagic)

    end,
    onUpdate = function(magic)

    end,
    onDespawn = function(magic)

    end
    })

-------------
--[JUMPERS]--
-------------

mw2.name.IceJumper = mw2.addProjectileBuilder({
    name = "Ice Jumper",
    magicHealth = 20,
    collideWith = 1,
    rotation = false,
    speed = 8,
    size = 128,
    lifetime = 64,
    image = "jumpers/Tile2917_C95_R3",
    onSpawn = function(magic)

    end,
    onHitWall = function(magic)

    end,
    onHitPlayer = function(magic, id)
        if math.random(100) < 25 then
            mw2.damagePlayer(id,magic.owner,1,magic.me.name)
            mw2.playerSpeed(id,-1)
            magic.x = player(id,"x")
            magic.y = player(id,"y")
            magic.xmove = 0
            magic.ymove = 0
            tween_move(magic.image,100,magic.x,magic.y)
        end
    end,
    onHitMagic = function(magic, otherMagic)

    end,
    onUpdate = function(magic)

    end,
    onDespawn = function(magic)

    end
    })

mw2.name.IceCloudJumper = mw2.addProjectileBuilder({
    name = "Ice Cloud Jumper",
    magicHealth = 40,
    collideWith = 1,
    rotation = false,
    speed = 8,
    size = 128,
    lifetime = 64,
    image = "jumpers/icecloud",
    onSpawn = function(magic)
        magic.count = math.random(5,10)
    end,
    onHitWall = function(magic)

    end,
    onHitPlayer = function(magic, id)
        magic.count = magic.count - 1
        if magic.count <= 0 then
            magic.count = math.random(2,6)
            mw2.spawnProjectile(mw2.name.IceJumper,magic.x,magic.y,magic.owner,math.random()*360-180)
        end
    end,
    onHitMagic = function(magic, otherMagic)

    end,
    onUpdate = function(magic)

    end,
    onDespawn = function(magic)

    end
    })

---------------
--[SHOCKWAVE]--
---------------

mw2.name.vampireShockwave = mw2.addProjectileBuilder({
    name = "Vampire Shockwave",
    magicHealth = 30,
    collideWith = 1,
    rotation = false,
    speed = 0,
    size = 0,
    lifetime = 20,
    image = "shockwaves/shockwave_vampire",
    onSpawn = function(magic)
        imagescale(magic.image,0,0)
        tween_scale(magic.image,2000,10,10)
    end,
    onHitWall = function(magic)

    end,
    onHitPlayer = function(magic, id)
        power = 1
        if (magic.lifetime < 10) then
            power = magic.lifetime*0.1
        end
        if math.random(10) < power*10 then
            local dmg = mw2.damagePlayer(magic.owner,id,(power*5),magic.me.name)
            mw2.damagePlayer(id,magic.owner,-(dmg * 0.75),magic.me.name)
        end
    end,
    onHitMagic = function(magic, otherMagic)

    end,
    onUpdate = function(magic)
        if (magic.lifetime == 10) then
            tween_alpha(magic.image,1000,0)
        end
        hitbox = ((64*10) - ((magic.lifetime*0.5)*64))*0.5
        magic.size = hitbox
    end,
    onDespawn = function(magic)

    end
    })

------------
--[SUMMON]--
------------

mw2.name.summonGuidedArrow = mw2.addProjectileBuilder({
    name = "Summoned Guided Arrow",
    magicHealth = 1,
    collideWith = 1,
    rotation = -90,
    speed = 32,
    size = 16,
    lifetime = 64,
    image = "arrow/guidedArrow",
    onSpawn = function(magic)
        tween_alpha(magic.image,magic.lifetime*100,0)
        magic.target = {x=player(magic.owner,"x"),y=player(magic.owner,"y")}
        magic.stuck = false
    end,
    onHitWall = function(magic)
        dir = magic.dir
        magic.xmove = 0
        magic.ymove = 0
        xadd = misc.lengthdir_x(dir,3)
        yadd = misc.lengthdir_y(dir,3)
        while (tile(misc.pixel_to_tile(magic.x),misc.pixel_to_tile(magic.y),"wall")) do
            magic.x = magic.x - xadd
            magic.y = magic.y - yadd
        end
        tween_move(magic.image,0,magic.x,magic.y,dir-90)
        --tween_alpha(magic.image,magic.lifetime*100,0)
        magic.stuck = true
    end,
    onHitPlayer = function(magic, id)
        mw2.damagePlayer(magic.owner,id,15,magic.me.name)
        magic.dispose = true
    end,
    onHitMagic = function(magic, otherMagic)

    end,
    onUpdate = function(magic)
        if ( (magic.stuck == false) and (magic.lifetime % 20) ) then
            -- request new mouse pos
            reqcld2(magic.owner,2,function(id,x,y,magic)
                magic.target = {x=x,y=y}
            end,magic)
            -- change dir towards existing pos
            local dir = misc.point_direction(magic.x,magic.y,magic.target.x,magic.target.y)
            local newDir = misc.turnTowards(magic.dir, dir, 10)
            mw2.projSetMotion(magic,newDir,magic.me.speed)
        end
    end,
    onDespawn = function(magic)

    end
    })

mw2.name.summonSpear = mw2.addProjectileBuilder({
    name = "Summoned Spear",
    magicHealth = 2,
    collideWith = 1,
    rotation = 0,
    speed = 32,
    size = 16,
    lifetime = 64,
    image = "summon/spear",
    onSpawn = function(magic)
        tween_alpha(magic.image,magic.lifetime*100,0)
        magic.target = {x=player(magic.owner,"x"),y=player(magic.owner,"y")}
        magic.stuck = false
    end,
    onHitWall = function(magic)
        dir = magic.dir
        magic.xmove = 0
        magic.ymove = 0
        xadd = misc.lengthdir_x(dir,3)
        yadd = misc.lengthdir_y(dir,3)
        while (tile(misc.pixel_to_tile(magic.x),misc.pixel_to_tile(magic.y),"wall")) do
            magic.x = magic.x - xadd
            magic.y = magic.y - yadd
        end
        tween_move(magic.image,0,magic.x,magic.y,dir)
        magic.stuck = true
    end,
    onHitPlayer = function(magic, id)
        mw2.damagePlayer(magic.owner,id,20,magic.me.name)
        magic.dispose = true
    end,
    onHitMagic = function(magic, otherMagic)

    end,
    onUpdate = function(magic)

    end,
    onDespawn = function(magic)

    end
    })

-----------
--[FLIES]--
-----------

mw2.name.VampireFly = mw2.addProjectileBuilder({
    name = "Vampire Fly",
    magicHealth = 2,
    collideWith = 1,
    rotation = -90,
    speed = 25,
    size = 16,
    lifetime = 32*5,
    image = "flies/vampire",
    onSpawn = function(magic)
        tween_rotateconstantly(magic.image,5)
    end,
    onHitWall = function(magic)
        mw2.magicBack(magic)
        mw2.explosion(magic.x,magic.y,64,30,magic.owner)
        magic.dispose = true
    end,
    onHitPlayer = function(magic, id)
        local dmg = mw2.damagePlayer(magic.owner,id,20,magic.me.name)
        mw2.damagePlayer(id,magic.owner,-(dmg * 0.75),magic.me.name)
        magic.dispose = true
    end,
    onHitMagic = function(magic, otherMagic)

    end,
    onUpdate = function(magic)
        local owner_findable = false
        local best_target = 0
        local best_targt_dir = 180
        local pl = player(0,"tableliving")
        for i=1,#pl do
            local id = pl[i]
            local x = player(id,"x")
            local y = player(id,"y")
            if misc.isInsideScreen(magic.x,magic.y,x,y) then
                if misc.isLineOfSight(magic.x,magic.y,x,y) then
                    if (id == magic.owner) then
                        owner_findable = true
                    else
                        local direction = misc.point_direction(magic.x,magic.y,x,y)
                        local dir_diff = misc.angleDiffrence(direction,magic.dir)
                        if (math.abs(dir_diff) < math.abs(best_targt_dir)) then
                            best_targt_dir = dir_diff
                            best_target  = id
                        end
                    end
                end
            end
        end
        if (best_targt_dir < 180) then
            if (math.abs(best_targt_dir) > 20) then -- othrewise lets not waste network
                local x = player(best_target,"x")
                local y = player(best_target,"y")
                local direction = misc.point_direction(magic.x,magic.y,x,y)
                local distance = misc.point_distance(magic.x,magic.y,x,y)
                magic.dir = magic.dir + misc.maxmin(best_targt_dir,20)
                magic.xmove = misc.lengthdir_x(magic.dir,magic.me.speed)
                magic.ymove = misc.lengthdir_y(magic.dir,magic.me.speed)
                tween_move(magic.image,100*magic.lifetime,magic.x+(magic.xmove*magic.lifetime),magic.y+(magic.ymove*magic.lifetime))
            end
        else
            owner_findable = false
            if (owner_findable) then
                local x = player(magic.owner,"x")
                local y = player(magic.owner,"y")
                --local distance = misc.point_distance(magic.x,magic.y,x,y)
                local dir = misc.point_direction(magic.x,magic.y,x,y)
                if (math.abs(misc.angleDiffrence(magic.dir, dir)) > 40) then
                    magic.dir = misc.turnTowards(magic.dir, dir, 20)
                    magic.xmove = misc.lengthdir_x(magic.dir,magic.me.speed)
                    magic.ymove = misc.lengthdir_y(magic.dir,magic.me.speed)
                    tween_move(magic.image,100*magic.lifetime,magic.x+(magic.xmove*magic.lifetime),magic.y+(magic.ymove*magic.lifetime))
                end
            end
        end
    end,
    onDespawn = function(magic)

    end
    })

-----------
--[SPIKE]--
-----------

mw2.name.FireSpike = mw2.addProjectileBuilder({
    name = "Fire Spike",
    magicHealth = 5,
    collideWith = 1,
    rotation = 0,
    speed = 40,
    size = 16,
    lifetime = 16,
    image = "spike/spike fire",
    onSpawn = function(magic)

    end,
    onHitWall = function(magic)
        magic.dispose = true
    end,
    onHitPlayer = function(magic, id)
        magic.dispose = true
    end,
    onHitMagic = function(magic, otherMagic)

    end,
    onUpdate = function(magic)
        if (magic.lifetime == 10) then
            reqcld2(magic.owner,2,function (id, x, y, magic)
                if (magic.dispose == false) then
                    magic.dir = misc.point_direction(magic.x,magic.y,x,y)
                    magic.xmove = misc.lengthdir_x(magic.dir,magic.me.speed)
                    magic.ymove = misc.lengthdir_y(magic.dir,magic.me.speed)
                    imagepos(magic.image,magic.x,magic.y,magic.dir)
                    tween_move(magic.image,100*magic.lifetime,magic.x+(magic.xmove*magic.lifetime),magic.y+(magic.ymove*magic.lifetime))
                end
            end,magic)
        end
    end,
    onDespawn = function(magic)
        mw2.explosion(magic.x,magic.y,32,15,magic.owner)
    end
    })

mw2.name.IceSpike = mw2.addProjectileBuilder({
    name = "Ice Spike",
    magicHealth = 5,
    collideWith = 1,
    rotation = 0,
    speed = 40,
    size = 16,
    lifetime = 16,
    image = "spike/spike ice",
    onSpawn = function(magic)

    end,
    onHitWall = function(magic)
        magic.dispose = true
    end,
    onHitPlayer = function(magic, id)
        if player(id,"speedmod") > -30 then
            mw2.playerSpeed(id,-2)
        end
        mw2.damagePlayer(magic.owner,id,1,magic.me.name)
        magic.dispose = true
    end,
    onHitMagic = function(magic, otherMagic)

    end,
    onUpdate = function(magic)
        if (magic.lifetime == 10) then
            reqcld2(magic.owner,2,function (id, x, y, magic)
                if (magic.dispose == false) then
                    magic.dir = misc.point_direction(magic.x,magic.y,x,y)
                    magic.xmove = misc.lengthdir_x(magic.dir,magic.me.speed)
                    magic.ymove = misc.lengthdir_y(magic.dir,magic.me.speed)
                    imagepos(magic.image,magic.x,magic.y,magic.dir)
                    tween_move(magic.image,100*magic.lifetime,magic.x+(magic.xmove*magic.lifetime),magic.y+(magic.ymove*magic.lifetime))
                end
            end,magic)
        end
    end,
    onDespawn = function(magic)

    end
    })

mw2.name.ShockSpike = mw2.addProjectileBuilder({
    name = "Shock Spike",
    magicHealth = 5,
    collideWith = 1,
    rotation = 0,
    speed = 40,
    size = 16,
    lifetime = 16,
    image = "spike/spike shock",
    onSpawn = function(magic)

    end,
    onHitWall = function(magic)
        magic.dispose = true
    end,
    onHitPlayer = function(magic, id)
        parse("flashplayer "..id.." 10")
        magic.dispose = true
    end,
    onHitMagic = function(magic, otherMagic)

    end,
    onUpdate = function(magic)
        if (magic.lifetime == 10) then
            reqcld2(magic.owner,2,function (id, x, y, magic)
                if (magic.dispose == false) then
                    magic.dir = misc.point_direction(magic.x,magic.y,x,y)
                    magic.xmove = misc.lengthdir_x(magic.dir,magic.me.speed)
                    magic.ymove = misc.lengthdir_y(magic.dir,magic.me.speed)
                    imagepos(magic.image,magic.x,magic.y,magic.dir)
                    tween_move(magic.image,100*magic.lifetime,magic.x+(magic.xmove*magic.lifetime),magic.y+(magic.ymove*magic.lifetime))
                end
            end,magic)
        end
    end,
    onDespawn = function(magic)
        parse("flashposition "..magic.x.." "..magic.y.." 5")
    end
    })

mw2.name.TaintSpike = mw2.addProjectileBuilder({
    name = "Taint Spike",
    magicHealth = 7,
    collideWith = 1,
    rotation = 0,
    speed = 40,
    size = 16,
    lifetime = 16,
    image = "spike/spike taint",
    onSpawn = function(magic)

    end,
    onHitWall = function(magic)
        magic.dispose = true
    end,
    onHitPlayer = function(magic, id)
        mw2.damageMaxHealth(magic.owner,id,5,magic.me.name)
        magic.dispose = true
    end,
    onHitMagic = function(magic, otherMagic)

    end,
    onUpdate = function(magic)
        if (magic.lifetime == 10) then
            reqcld2(magic.owner,2,function (id, x, y, magic)
                if (magic.dispose == false) then
                    magic.dir = misc.point_direction(magic.x,magic.y,x,y)
                    magic.xmove = misc.lengthdir_x(magic.dir,magic.me.speed)
                    magic.ymove = misc.lengthdir_y(magic.dir,magic.me.speed)
                    imagepos(magic.image,magic.x,magic.y,magic.dir)
                    tween_move(magic.image,100*magic.lifetime,magic.x+(magic.xmove*magic.lifetime),magic.y+(magic.ymove*magic.lifetime))
                end
            end,magic)
        end
    end,
    onDespawn = function(magic)

    end
    })

mw2.name.VampireSpike = mw2.addProjectileBuilder({
    name = "Vampire Spike",
    magicHealth = 7,
    collideWith = 1,
    rotation = 0,
    speed = 40,
    size = 16,
    lifetime = 16,
    image = "spike/spike vampire",
    onSpawn = function(magic)

    end,
    onHitWall = function(magic)
        magic.dispose = true
    end,
    onHitPlayer = function(magic, id)
        local dmg = mw2.damagePlayer(magic.owner,id,5,magic.me.name)
        mw2.damagePlayer(id,magic.owner,-(dmg * 0.75),magic.me.name)
        magic.dispose = true
    end,
    onHitMagic = function(magic, otherMagic)

    end,
    onUpdate = function(magic)
        if (magic.lifetime == 10) then
            reqcld2(magic.owner,2,function (id, x, y, magic)
                if (magic.dispose == false) then
                    magic.dir = misc.point_direction(magic.x,magic.y,x,y)
                    magic.xmove = misc.lengthdir_x(magic.dir,magic.me.speed)
                    magic.ymove = misc.lengthdir_y(magic.dir,magic.me.speed)
                    imagepos(magic.image,magic.x,magic.y,magic.dir)
                    tween_move(magic.image,100*magic.lifetime,magic.x+(magic.xmove*magic.lifetime),magic.y+(magic.ymove*magic.lifetime))
                end
            end,magic)
        end
    end,
    onDespawn = function(magic)

    end
    })

----------
--[TIME]--
----------

mw2.name.WeakTimeInverter = mw2.addProjectileBuilder({
    name = "Weak Time Inverter",
    magicHealth = 100,
    collideWith = 0,
    rotation = false,
    speed = 0,
    size = 10,
    lifetime = 50,
    image = "time/time inverter weak",
    onSpawn = function(magic)
        magic.steps = {}
        magic.stable = true
        local i = 0
        while (i < #mw2.entity) do
            i = i + 1
            local other = mw2.entity[i]
            if ( (other ~= magic) and (string.find(other.me.name, "Time Inverter") ~= nil) and (other.owner == magic.owner) ) then
                other.dispose = true
                other.stable = false
            end
        end
    end,
    onHitWall = function(magic)

    end,
    onHitPlayer = function(magic, id)

    end,
    onHitMagic = function(magic, otherMagic)

    end,
    onUpdate = function(magic)
        if (magic.lifetime % 10 == 0) then
            tween_rotateconstantly(magic.image,magic.lifetime/50)
        end
        if (magic.originalOwner == magic.owner) then
            if (player(magic.owner,"health") > 0) then
                local step = {}
                step.x = player(magic.owner,"x")
                step.y = player(magic.owner,"y")
                magic.steps[#magic.steps+1] = step
                return
            end
        else
            magic.stable = false
        end
        magic.dispose = true
    end,
    onDespawn = function(magic)
        if (magic.stable == true) then
            msg2(magic.owner,misc.rgb(0,255,0).."Your "..magic.me.name.." collapses and time starts rewinding")
            local worker = LuaWorker.new(function(worker, workspace)
                local magic = workspace.magic
                misc.effect("flare",magic.x,magic.y,1,16,255,0,255)
                if (magic.index > 0) then
                    local step = magic.steps[magic.index]
                    if (player(magic.owner,"health") <= 0) then
                        parse("spawnplayer "..magic.owner.." "..step.x.." "..step.y)
                    else
                        cs2d.setpos.call(magic.owner,step.x,step.y)
                    end
                    magic.index = magic.index - 1
                    return
                end
                worker:delete()
            end)
            magic.index = #magic.steps
            worker.workspace.magic = magic
            worker:setFrequency(LuaWorkerData.always)
            worker:start()
        else
            mw2.explosion(magic.x,magic.y,64,64,magic.owner)
        end
    end
    })

mw2.name.MediumTimeInverter = mw2.addProjectileBuilder({
    name = "Medium Time Inverter",
    magicHealth = 100,
    collideWith = 0,
    rotation = false,
    speed = 0,
    size = 10,
    lifetime = 200,
    image = "time/time inverter medium",
    onSpawn = function(magic)
        magic.steps = {}
        magic.stable = true
        local i = 0
        while (i < #mw2.entity) do
            i = i + 1
            local other = mw2.entity[i]
            if ( (other ~= magic) and (string.find(other.me.name, "Time Inverter") ~= nil) and (other.owner == magic.owner) ) then
                other.dispose = true
                other.stable = false
            end
        end
    end,
    onHitWall = function(magic)

    end,
    onHitPlayer = function(magic, id)

    end,
    onHitMagic = function(magic, otherMagic)

    end,
    onUpdate = function(magic)
        if (magic.lifetime % 10 == 0) then
            tween_rotateconstantly(magic.image,magic.lifetime/50)
        end
        if (magic.originalOwner == magic.owner) then
            if (player(magic.owner,"health") > 0) then
                local step = {}
                step.x = player(magic.owner,"x")
                step.y = player(magic.owner,"y")
                magic.steps[#magic.steps+1] = step
                return
            end
        else
            magic.stable = false
        end
        magic.dispose = true
    end,
    onDespawn = function(magic)
        if (magic.stable == true) then
            msg2(magic.owner,misc.rgb(0,255,0).."Your "..magic.me.name.." collapses and time starts rewinding")
            local worker = LuaWorker.new(function(worker, workspace)
                local magic = workspace.magic
                misc.effect("flare",magic.x,magic.y,1,16,255,0,255)
                if (magic.index > 0) then
                    local step = magic.steps[magic.index]
                    if (player(magic.owner,"health") <= 0) then
                        parse("spawnplayer "..magic.owner.." "..step.x.." "..step.y)
                    else
                        cs2d.setpos.call(magic.owner,step.x,step.y)
                    end
                    magic.index = magic.index - 1
                    return
                end
                worker:delete()
            end)
            magic.index = #magic.steps
            worker.workspace.magic = magic
            worker:setFrequency(LuaWorkerData.always)
            worker:start()
        else
            mw2.explosion(magic.x,magic.y,64,64,magic.owner)
        end
    end
    })

mw2.name.StrongTimeInverter = mw2.addProjectileBuilder({
    name = "Strong Time Inverter",
    magicHealth = 100,
    collideWith = 0,
    rotation = false,
    speed = 0,
    size = 10,
    lifetime = 600,
    image = "time/time inverter strong",
    onSpawn = function(magic)
        magic.steps = {}
        magic.stable = true
        local i = 0
        while (i < #mw2.entity) do
            i = i + 1
            local other = mw2.entity[i]
            if ( (other ~= magic) and (string.find(other.me.name, "Time Inverter") ~= nil) and (other.owner == magic.owner) ) then
                other.dispose = true
                other.stable = false
            end
        end
    end,
    onHitWall = function(magic)

    end,
    onHitPlayer = function(magic, id)

    end,
    onHitMagic = function(magic, otherMagic)

    end,
    onUpdate = function(magic)
        if (magic.lifetime % 10 == 0) then
            tween_rotateconstantly(magic.image,magic.lifetime/50)
        end
        if (magic.originalOwner == magic.owner) then
            if (player(magic.owner,"health") > 0) then
                local step = {}
                step.x = player(magic.owner,"x")
                step.y = player(magic.owner,"y")
                magic.steps[#magic.steps+1] = step
                return
            end
        else
            magic.stable = false
        end
        magic.dispose = true
    end,
    onDespawn = function(magic)
        if (magic.stable == true) then
            msg2(magic.owner,misc.rgb(0,255,0).."Your "..magic.me.name.." collapses and time starts rewinding")
            local worker = LuaWorker.new(function(worker, workspace)
                local magic = workspace.magic
                misc.effect("flare",magic.x,magic.y,1,16,255,0,255)
                if (magic.index > 0) then
                    local step = magic.steps[magic.index]
                    if (player(magic.owner,"health") <= 0) then
                        parse("spawnplayer "..magic.owner.." "..step.x.." "..step.y)
                    else
                        cs2d.setpos.call(magic.owner,step.x,step.y)
                    end
                    magic.index = magic.index - 1
                    return
                end
                worker:delete()
            end)
            magic.index = #magic.steps
            worker.workspace.magic = magic
            worker:setFrequency(LuaWorkerData.always)
            worker:start()
        else
            mw2.explosion(magic.x,magic.y,64,64,magic.owner)
        end
    end
    })

----------
--[MIND]--
----------

mw2.name.WeakMindControl = mw2.addProjectileBuilder({
    name = "Weak Mind Control",
    magicHealth = 100,
    collideWith = 1,
    rotation = false,
    speed = 20,
    size = 16,
    lifetime = 50,
    image = "mind/eye1s",
    onSpawn = function(magic)
        --tween_rotateconstantly(magic.image,2+(math.random()))
        magic.target = 0
        magic.location = nil
        local i = 0
        while (i < #mw2.entity) do
            i = i + 1
            local other = mw2.entity[i]
            if ( (other ~= magic) and (string.find(other.me.name, "Mind Control") ~= nil) and (other.owner == magic.owner) ) then
                other.dispose = true
            end
        end
    end,
    onHitWall = function(magic)
        if (magic.target == 0) then
            magic.dispose = true
        end
    end,
    onHitPlayer = function(magic, id)
        if (magic.target == 0) then
            magic.target = id
            msg2(magic.target,misc.rgb(255,0,0).."You sense "..player(magic.owner,"name").."'s mind inside yours...")
            magic.location = {x=player(magic.owner,"x"),y=player(magic.owner,"y")}
        end
    end,
    onHitMagic = function(magic, otherMagic)

    end,
    onUpdate = function(magic)
        if (magic.originalOwner ~= magic.owner) then
            magic.dispose = true
            return
        end
        if (magic.target > 0) then
            if ( (player(magic.owner,"health") <= 0) or (player(magic.target,"exists") == false) or (player(magic.target,"health") <= 0) ) then
                magic.dispose = true
                return
            end
            --
            magic.x = player(magic.target,"x")
            magic.y = player(magic.target,"y")
            local rx = (player(magic.owner,"x") - magic.location.x)
            local ry = (player(magic.owner,"y") - magic.location.y)
            cs2d.setpos.call(magic.owner,magic.location.x,magic.location.y)
            magic.x = magic.x+rx
            magic.y = magic.y+ry
            tween_move(magic.image,100,magic.x,magic.y)
            for _,dir in ipairs(const.dirs.eight) do
                local cx = magic.x + (dir.x * 15)
                local cy = magic.y + (dir.y * 15)
                if (tile(misc.pixel_to_tile(cx),misc.pixel_to_tile(cy),"wall")) then
                    return
                end
            end
            cs2d.setpos.call(magic.target,magic.x,magic.y)
        end
    end,
    onDespawn = function(magic)

    end
    })


mw2.name.MediumMindControl = mw2.addProjectileBuilder({
    name = "Medium Mind Control",
    magicHealth = 150,
    collideWith = 1,
    rotation = false,
    speed = 20,
    size = 16,
    lifetime = 200,
    image = "mind/eye2s",
    onSpawn = function(magic)
        magic.target = 0
        magic.location = nil
        local i = 0
        while (i < #mw2.entity) do
            i = i + 1
            local other = mw2.entity[i]
            if ( (other ~= magic) and (string.find(other.me.name, "Mind Control") ~= nil) and (other.owner == magic.owner) ) then
                other.dispose = true
            end
        end
    end,
    onHitWall = function(magic)
        if (magic.target == 0) then
            magic.dispose = true
        end
    end,
    onHitPlayer = function(magic, id)
        if (magic.target == 0) then
            magic.target = id
            msg2(magic.target,misc.rgb(255,0,0).."You sense "..player(magic.owner,"name").."'s mind inside yours...")
            magic.location = {x=player(magic.owner,"x"),y=player(magic.owner,"y")}
        end
    end,
    onHitMagic = function(magic, otherMagic)

    end,
    onUpdate = function(magic)
        if (magic.originalOwner ~= magic.owner) then
            magic.dispose = true
            return
        end
        if (magic.target > 0) then
            if ( (player(magic.owner,"health") <= 0) or (player(magic.target,"exists") == false) or (player(magic.target,"health") <= 0) ) then
                magic.dispose = true
                return
            end
            --
            magic.x = player(magic.target,"x")
            magic.y = player(magic.target,"y")
            local rx = (player(magic.owner,"x") - magic.location.x)
            local ry = (player(magic.owner,"y") - magic.location.y)
            cs2d.setpos.call(magic.owner,magic.location.x,magic.location.y)
            magic.x = magic.x+rx
            magic.y = magic.y+ry
            tween_move(magic.image,100,magic.x,magic.y)
            for _,dir in ipairs(const.dirs.eight) do
                local cx = magic.x + (dir.x * 15)
                local cy = magic.y + (dir.y * 15)
                if (tile(misc.pixel_to_tile(cx),misc.pixel_to_tile(cy),"wall")) then
                    return
                end
            end
            cs2d.setpos.call(magic.target,magic.x,magic.y)
        end
    end,
    onDespawn = function(magic)

    end
    })

mw2.name.StrongMindControl = mw2.addProjectileBuilder({
    name = "Strong Mind Control",
    magicHealth = 250,
    collideWith = 1,
    rotation = false,
    speed = 20,
    size = 16,
    lifetime = 500,
    image = "mind/eye3s",
    onSpawn = function(magic)
        magic.target = 0
        magic.location = nil
        local i = 0
        while (i < #mw2.entity) do
            i = i + 1
            local other = mw2.entity[i]
            if ( (other ~= magic) and (string.find(other.me.name, "Mind Control") ~= nil) and (other.owner == magic.owner) ) then
                other.dispose = true
            end
        end
    end,
    onHitWall = function(magic)
        if (magic.target == 0) then
            magic.dispose = true
        end
    end,
    onHitPlayer = function(magic, id)
        if (magic.target == 0) then
            magic.target = id
            msg2(magic.target,misc.rgb(255,0,0).."You sense "..player(magic.owner,"name").."'s mind inside yours...")
            magic.location = {x=player(magic.owner,"x"),y=player(magic.owner,"y")}
        end
    end,
    onHitMagic = function(magic, otherMagic)

    end,
    onUpdate = function(magic)
        if (magic.originalOwner ~= magic.owner) then
            magic.dispose = true
            return
        end
        if (magic.target > 0) then
            if ( (player(magic.owner,"health") <= 0) or (player(magic.target,"exists") == false) or (player(magic.target,"health") <= 0) ) then
                magic.dispose = true
                return
            end
            --
            magic.x = player(magic.target,"x")
            magic.y = player(magic.target,"y")
            local rx = (player(magic.owner,"x") - magic.location.x)
            local ry = (player(magic.owner,"y") - magic.location.y)
            cs2d.setpos.call(magic.owner,magic.location.x,magic.location.y)
            magic.x = magic.x+rx
            magic.y = magic.y+ry
            tween_move(magic.image,100,magic.x,magic.y)
            for _,dir in ipairs(const.dirs.eight) do
                local cx = magic.x + (dir.x * 15)
                local cy = magic.y + (dir.y * 15)
                if (tile(misc.pixel_to_tile(cx),misc.pixel_to_tile(cy),"wall")) then
                    return
                end
            end
            cs2d.setpos.call(magic.target,magic.x,magic.y)
        end
    end,
    onDespawn = function(magic)

    end
    })

mw2.name.WeakMindDisruption = mw2.addProjectileBuilder({
    name = "Weak Mind Disruption",
    magicHealth = 10,
    collideWith = 1,
    rotation = -90,
    speed = 32,
    size = 10,
    lifetime = 20,
    image = "mind/disrupt1",
    onSpawn = function(magic)

    end,
    onHitWall = function(magic)
        magic.dispose = true
    end,
    onHitPlayer = function(magic, id)
        mw2.damagePlayer(magic.owner,id,10,magic.me.name)
        parse("shake "..id.." 25")
        magic.dispose = true
    end,
    onHitMagic = function(magic, otherMagic)

    end,
    onUpdate = function(magic)

    end,
    onDespawn = function(magic)

    end
    })

mw2.name.MediumMindDisruption = mw2.addProjectileBuilder({
    name = "Medium Mind Disruption",
    magicHealth = 15,
    collideWith = 1,
    rotation = -90,
    speed = 32,
    size = 10,
    lifetime = 20,
    image = "mind/disrupt2",
    onSpawn = function(magic)

    end,
    onHitWall = function(magic)
        magic.dispose = true
    end,
    onHitPlayer = function(magic, id)
        mw2.damagePlayer(magic.owner,id,15,magic.me.name)
        parse("shake "..id.." 75")
        magic.dispose = true
    end,
    onHitMagic = function(magic, otherMagic)

    end,
    onUpdate = function(magic)

    end,
    onDespawn = function(magic)

    end
    })

mw2.name.StrongMindDisruption = mw2.addProjectileBuilder({
    name = "Strong Mind Disruption",
    magicHealth = 25,
    collideWith = 1,
    rotation = -90,
    speed = 32,
    size = 10,
    lifetime = 20,
    image = "mind/disrupt3",
    onSpawn = function(magic)

    end,
    onHitWall = function(magic)
        magic.dispose = true
    end,
    onHitPlayer = function(magic, id)
        mw2.damagePlayer(magic.owner,id,20,magic.me.name)
        parse("shake "..id.." 150")
        magic.dispose = true
    end,
    onHitMagic = function(magic, otherMagic)

    end,
    onUpdate = function(magic)

    end,
    onDespawn = function(magic)

    end
    })

mw2.name.AngerBot = mw2.addProjectileBuilder({
    name = "Anger Bot",
    magicHealth = 25,
    collideWith = 1,
    rotation = -90,
    speed = 40,
    size = 10,
    lifetime = 20,
    image = "mind/anger",
    onSpawn = function(magic)

    end,
    onHitWall = function(magic)
        magic.dispose = true
    end,
    onHitPlayer = function(magic, id)
        if (player(id,"bot")) then
            local bot = bot.array[id]
            local pl = player(0,"tableliving")
            for i,cid in ipairs(pl) do
                if (cid == id) then
                    pl[i] = pl[#pl]
                    pl[#pl] = nil
                    break
                end
            end
            bot.enemies[magic.owner] = false
            local target = pl[math.ceil(#pl*math.random())]
            bot:target(target)
            msg2(magic.owner,misc.rgb(255,0,0)..player(bot.id,"name").." gets angry at "..player(target,"name"))
        else
            local pl = player(0,"tableliving")
            -- choose random botId
            local botId = nil
            local i = 0
            while ((botId == nil) and (i < 32)) do
                i = i + 1
                local tempId = pl[math.ceil(#pl*math.random())]
                if (player(tempId,"bot")) then
                    botId = tempId
                end
            end
            -- target
            local bot = bot.array[botId]
            bot.enemies[magic.owner] = false
            bot:target(id)
            msg2(magic.owner,misc.rgb(255,0,0)..player(bot.id,"name").." gets angry at "..player(id,"name"))
        end
        magic.dispose = true
    end,
    onHitMagic = function(magic, otherMagic)

    end,
    onUpdate = function(magic)

    end,
    onDespawn = function(magic)

    end
    })

---------------
--[CHAINLINK]--
---------------

mw2.name.WeakChainlink = mw2.addProjectileBuilder({
    name = "Weak Chainlink",
    magicHealth = 25,
    collideWith = 1,
    rotation = false,
    speed = 32,
    size = 32,
    lifetime = 100,
    image = "chain/chain1",
    onSpawn = function(magic)
        magic.target1 = 0
        magic.target2 = 0
    end,
    onHitWall = function(magic)
        magic.dispose = true
    end,
    onHitPlayer = function(magic, id)
        if (magic.target1 == 0) then
            magic.target1 = id
            return
        else
            if (magic.target2 == 0) then
                if (magic.target1 ~= id) then
                    magic.target2 = id
                end
            else
                -- check they are here
                if (misc.isPlayerAlive(magic.target1) and misc.isPlayerAlive(magic.target2)) then
                    -- CHAIN
                    local x1 = player(magic.target1,"x")
                    local y1 = player(magic.target1,"y")
                    local x2 = player(magic.target2,"x")
                    local y2 = player(magic.target2,"y")
                    local dist = misc.point_distance(x1,y1,x2,y2)
                    if (dist > 100) then
                        if (math.random() < 0.5) then
                            cs2d.setpos.call(magic.target1,x2,y2)
                        else
                            cs2d.setpos.call(magic.target2,x1,y1)
                        end
                        return
                    end
                    local dir = misc.point_direction(x1,y1,x2,y2)
                    magic.x = x1 + misc.lengthdir_x(dir,dist/2)
                    magic.y = y1 + misc.lengthdir_y(dir,dist/2)
                    tween_move(magic.image,100,magic.x,magic.y)
                    local xadd = misc.lengthdir_x(dir,16)
                    local yadd = misc.lengthdir_y(dir,16)
                    cs2d.setpos.call(magic.target2,magic.x+xadd,magic.y+yadd)
                    cs2d.setpos.call(magic.target1,magic.x-xadd,magic.y-yadd)
                else
                    magic.dispose = true
                end
            end
        end
    end,
    onHitMagic = function(magic, otherMagic)

    end,
    onUpdate = function(magic)
        if (magic.target1 > 0) then
            magic.x = player(magic.target1,"x")
            magic.y = player(magic.target1,"y")
            tween_move(magic.image,100,magic.x,magic.y)
        end
    end,
    onDespawn = function(magic)

    end
    })

mw2.name.MediumChainlink = mw2.addProjectileBuilder({
    name = "Medium Chainlink",
    magicHealth = 50,
    collideWith = 1,
    rotation = false,
    speed = 32,
    size = 64,
    lifetime = 300,
    image = "chain/chain2",
    onSpawn = function(magic)
        magic.target1 = 0
        magic.target2 = 0
    end,
    onHitWall = function(magic)
        magic.dispose = true
    end,
    onHitPlayer = function(magic, id)
        if (magic.target1 == 0) then
            magic.target1 = id
            return
        else
            if (magic.target2 == 0) then
                if (magic.target1 ~= id) then
                    magic.target2 = id
                end
            else
                -- CHAIN
                local x1 = player(magic.target1,"x")
                local y1 = player(magic.target1,"y")
                local x2 = player(magic.target2,"x")
                local y2 = player(magic.target2,"y")
                local dist = misc.point_distance(x1,y1,x2,y2)
                if (dist > 100) then
                    if (math.random() < 0.5) then
                        cs2d.setpos.call(magic.target1,x2,y2)
                    else
                        cs2d.setpos.call(magic.target2,x1,y1)
                    end
                    return
                end
                local dir = misc.point_direction(x1,y1,x2,y2)
                magic.x = x1 + misc.lengthdir_x(dir,dist/2)
                magic.y = y1 + misc.lengthdir_y(dir,dist/2)
                tween_move(magic.image,100,magic.x,magic.y)
                local xadd = misc.lengthdir_x(dir,16)
                local yadd = misc.lengthdir_y(dir,16)
                cs2d.setpos.call(magic.target2,magic.x+xadd,magic.y+yadd)
                cs2d.setpos.call(magic.target1,magic.x-xadd,magic.y-yadd)
            end
        end
    end,
    onHitMagic = function(magic, otherMagic)

    end,
    onUpdate = function(magic)
        if (magic.target1 > 0) then
            magic.x = player(magic.target1,"x")
            magic.y = player(magic.target1,"y")
            tween_move(magic.image,100,magic.x,magic.y)
        end
    end,
    onDespawn = function(magic)

    end
    })

mw2.name.StrongChainlink = mw2.addProjectileBuilder({
    name = "Strong Chainlink",
    magicHealth = 100,
    collideWith = 1,
    rotation = false,
    speed = 40,
    size = 100,
    lifetime = 600,
    image = "chain/chain3",
    onSpawn = function(magic)
        magic.target1 = 0
        magic.target2 = 0
    end,
    onHitWall = function(magic)
        magic.dispose = true
    end,
    onHitPlayer = function(magic, id)
        if (magic.target1 == 0) then
            magic.target1 = id
            return
        else
            if (magic.target2 == 0) then
                if (magic.target1 ~= id) then
                    magic.target2 = id
                end
            else
                -- CHAIN
                local x1 = player(magic.target1,"x")
                local y1 = player(magic.target1,"y")
                local x2 = player(magic.target2,"x")
                local y2 = player(magic.target2,"y")
                local dist = misc.point_distance(x1,y1,x2,y2)
                if (dist > 100) then
                    if (math.random() < 0.5) then
                        cs2d.setpos.call(magic.target1,x2,y2)
                    else
                        cs2d.setpos.call(magic.target2,x1,y1)
                    end
                    return
                end
                local dir = misc.point_direction(x1,y1,x2,y2)
                magic.x = x1 + misc.lengthdir_x(dir,dist/2)
                magic.y = y1 + misc.lengthdir_y(dir,dist/2)
                tween_move(magic.image,100,magic.x,magic.y)
                local xadd = misc.lengthdir_x(dir,16)
                local yadd = misc.lengthdir_y(dir,16)
                cs2d.setpos.call(magic.target2,magic.x+xadd,magic.y+yadd)
                cs2d.setpos.call(magic.target1,magic.x-xadd,magic.y-yadd)
            end
        end
    end,
    onHitMagic = function(magic, otherMagic)

    end,
    onUpdate = function(magic)
        if (magic.target1 > 0) then
            magic.x = player(magic.target1,"x")
            magic.y = player(magic.target1,"y")
            tween_move(magic.image,100,magic.x,magic.y)
        end
    end,
    onDespawn = function(magic)

    end
    })

------------------------
--[PHASE 2: ADD MAGIC]--
------------------------

-----------
--[WINDS]--
-----------

mw2.addMagic("Taint Whirlwind","Whirlwind",30,40,"wand_4.png",function(id)
    mw2.spawnProjectile(mw2.name.TaintWind,player(id,"x"),player(id,"y"),id,player(id,"rot"))
end)

mw2.addMagic("Vampire Whirlwind","Whirlwind",30,40,"wand_4.png",function(id)
    mw2.spawnProjectile(mw2.name.VampireWind,player(id,"x"),player(id,"y"),id,player(id,"rot"))
end)

mw2.addMagic("Transport Whirlwind","Whirlwind",10,15,"wand_4.png",function(id)
    mw2.spawnProjectile(mw2.name.TransportWind,player(id,"x"),player(id,"y"),id,player(id,"rot"))
end)

--------------------
--[AREA AFFECTORS]--
--------------------

mw2.addMagic("Vampire Affector","Area Affector",25,55,"wand_5.png",function(id)
    mw2.spawnProjectile(mw2.name.AreaVampire,player(id,"x"),player(id,"y"),id,player(id,"rot"))
end)

--------------
--[TELEPORT]--
--------------

mw2.addMagic("Blink","Teleport",20,20,"wand_10.png",function(id)
    mw2.spawnProjectile(mw2.name.Blink,player(id,"x"),player(id,"y"),id,player(id,"rot"))
end)

mw2.addMagic("Displace","Teleport",20,35,"wand_10.png",function(id)
    mw2.spawnProjectile(mw2.name.Displacement,player(id,"x"),player(id,"y"),id,player(id,"rot"))
end)

mw2.addMagic("Temporal Abduction","Teleport",20,40,"wand_10.png",function(id)
    mw2.spawnProjectile(mw2.name.TemporalAbduction,player(id,"x"),player(id,"y"),id,player(id,"rot"))
end)

mw2.addMagic("Void Portal","Teleport",40,75,"wand_10.png",function(id)
    mw2.spawnProjectile(mw2.name.VoidPortal,player(id,"x"),player(id,"y"),id,player(id,"rot"))
end)


-------------
--[SPECIAL]--
-------------

mw2.addMagic("Phase Wall 2","Special",30,40,"wand_13.png",function(id)
    mw2.spawnProjectile(mw2.name.phaseWall2,player(id,"x"),player(id,"y"),id,player(id,"rot"))
end)

----------
--[STAR]--
----------

mw2.addMagic("Vampire Star","Star",25,90,"wand_7.png",function(id)
    mw2.spawnProjectile(mw2.name.VampireStar,player(id,"x"),player(id,"y"),id,player(id,"rot"))
end)

-----------
--[WAVES]--
-----------


mw2.addMagic("Vampire Wave","Waves",20,15,"wand_14.png",function(id)
    mw2.spawnProjectile(mw2.name.vampireWave,player(id,"x"),player(id,"y"),id,player(id,"rot"))
end)

-------------
--[JUMPERS]--
-------------

mw2.addMagic("Ice Jumper","Jumpers",20,30,"wand_6.png",function(id)
    mw2.spawnProjectile(mw2.name.IceJumper,player(id,"x"),player(id,"y"),id,player(id,"rot"))
end)

mw2.addMagic("Ice Cloud Jumper","Jumpers",30,50,"wand_6.png",function(id)
    mw2.spawnProjectile(mw2.name.IceCloudJumper,player(id,"x"),player(id,"y"),id,player(id,"rot"))
end)

---------------
--[SHOCKWAVE]--
---------------

mw2.addMagic("Vampire Shockwave","Shockwave",20,35,"wand_18.png",function(id)
    mw2.spawnProjectile(mw2.name.vampireShockwave,player(id,"x"),player(id,"y"),id,player(id,"rot"))
end)

---------------
--[ATTRACTOR]--
---------------

mw2.addMagic("Weak Chainlink","Attractor",5,20,"wand_21.png",function(id)
    mw2.spawnProjectile(mw2.name.WeakChainlink,player(id,"x"),player(id,"y"),id,player(id,"rot"))
end)

mw2.addMagic("Medium Chainlink","Attractor",14,35,"wand_21.png",function(id)
    mw2.spawnProjectile(mw2.name.MediumChainlink,player(id,"x"),player(id,"y"),id,player(id,"rot"))
end)

mw2.addMagic("Strong Chainlink","Attractor",25,30,"wand_21.png",function(id)
    mw2.spawnProjectile(mw2.name.StrongChainlink,player(id,"x"),player(id,"y"),id,player(id,"rot"))
end)

------------
--[SUMMON]--
------------

mw2.addMagic("Summon Guided Arrow","Summon",5,15,"crossbow.png",function(id)
    mw2.spawnProjectile(mw2.name.summonGuidedArrow,player(id,"x"),player(id,"y"),id,player(id,"rot"))
end)

mw2.addMagic("Summon Spear","Summon",10,12,"crossbow.png",function(id)
    mw2.spawnProjectile(mw2.name.summonSpear,player(id,"x"),player(id,"y"),id,player(id,"rot"))
end)

-----------
--[FLIES]--
-----------

mw2.addMagic("VampireFly","Flies",12,30,"wand_21.png",function(id)
    mw2.spawnProjectile(mw2.name.VampireFly,player(id,"x"),player(id,"y"),id,player(id,"rot"))
end)

-----------
--[SPIKE]--
-----------

mw2.addMagic("Fire Spike","Spike",4,8,"wand_22.png",function(id)
    mw2.spawnProjectile(mw2.name.FireSpike,player(id,"x"),player(id,"y"),id,player(id,"rot")-25)
    mw2.spawnProjectile(mw2.name.FireSpike,player(id,"x"),player(id,"y"),id,player(id,"rot")+25)
end)

mw2.addMagic("Ice Spike","Spike",5,8,"wand_22.png",function(id)
    mw2.spawnProjectile(mw2.name.IceSpike,player(id,"x"),player(id,"y"),id,player(id,"rot")-25)
    mw2.spawnProjectile(mw2.name.IceSpike,player(id,"x"),player(id,"y"),id,player(id,"rot")+25)
end)

mw2.addMagic("Shock Spike","Spike",6,8,"wand_22.png",function(id)
    mw2.spawnProjectile(mw2.name.ShockSpike,player(id,"x"),player(id,"y"),id,player(id,"rot")-25)
    mw2.spawnProjectile(mw2.name.ShockSpike,player(id,"x"),player(id,"y"),id,player(id,"rot")+25)
end)

mw2.addMagic("Taint Spike","Spike",8,12,"wand_22.png",function(id)
    mw2.spawnProjectile(mw2.name.TaintSpike,player(id,"x"),player(id,"y"),id,player(id,"rot")-25)
    mw2.spawnProjectile(mw2.name.TaintSpike,player(id,"x"),player(id,"y"),id,player(id,"rot")+25)
end)

mw2.addMagic("Vampire Spike","Spike",10,15,"wand_22.png",function(id)
    mw2.spawnProjectile(mw2.name.VampireSpike,player(id,"x"),player(id,"y"),id,player(id,"rot")-25)
    mw2.spawnProjectile(mw2.name.VampireSpike,player(id,"x"),player(id,"y"),id,player(id,"rot")+25)
end)

----------
--[TIME]--
----------

mw2.addMagic("Weak Time Inverter","Time",20,25,"wand_23.png",function(id)
    mw2.spawnProjectile(mw2.name.WeakTimeInverter,player(id,"x"),player(id,"y"),id,player(id,"rot"))
end)

mw2.addMagic("Medium Time Inverter","Time",30,50,"wand_23.png",function(id)
    mw2.spawnProjectile(mw2.name.MediumTimeInverter,player(id,"x"),player(id,"y"),id,player(id,"rot"))
end)

mw2.addMagic("Strong Time Inverter","Time",40,100,"wand_23.png",function(id)
    mw2.spawnProjectile(mw2.name.StrongTimeInverter,player(id,"x"),player(id,"y"),id,player(id,"rot"))
end)

----------
--[MIND]--
----------

mw2.addMagic("Weak Mind Control","Mind",10,80,"wand_24.png",function(id)
    mw2.spawnProjectile(mw2.name.WeakMindControl,player(id,"x"),player(id,"y"),id,player(id,"rot"))
end)

mw2.addMagic("Medium Mind Control","Mind",15,90,"wand_24.png",function(id)
    mw2.spawnProjectile(mw2.name.MediumMindControl,player(id,"x"),player(id,"y"),id,player(id,"rot"))
end)

mw2.addMagic("Strong Mind Control","Mind",30,10,"wand_24.png",function(id)
    mw2.spawnProjectile(mw2.name.StrongMindControl,player(id,"x"),player(id,"y"),id,player(id,"rot"))
end)

mw2.addMagic("Weak Mind Disruption","Mind",5,10,"wand_24.png",function(id)
    mw2.spawnProjectile(mw2.name.WeakMindDisruption,player(id,"x"),player(id,"y"),id,player(id,"rot"))
end)

mw2.addMagic("Medium Mind Disruption","Mind",15,15,"wand_24.png",function(id)
    mw2.spawnProjectile(mw2.name.MediumMindDisruption,player(id,"x"),player(id,"y"),id,player(id,"rot"))
end)

mw2.addMagic("Strong Mind Disruption","Mind",25,25,"wand_24.png",function(id)
    mw2.spawnProjectile(mw2.name.StrongMindDisruption,player(id,"x"),player(id,"y"),id,player(id,"rot"))
end)

mw2.addMagic("Anger Bot","Mind",10,40,"wand_24.png",function(id)
    mw2.spawnProjectile(mw2.name.AngerBot,player(id,"x"),player(id,"y"),id,player(id,"rot"))
end)
