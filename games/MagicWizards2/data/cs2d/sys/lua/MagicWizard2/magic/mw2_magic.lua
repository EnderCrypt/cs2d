------------------------------
--[PHASE 1: ADD PROJECTILES]--
------------------------------

-----------
--[BOLTS]--
-----------

mw2.name.PureBolt = mw2.addProjectile("Pure Bolt",1,1,-90,25,16,32,"bolt/Tile189_C7_R3",function(magic)--spawn

end,function(magic) --hit wall
	magic.dispose = true
end,function(magic,id) --hit player
	--mw2.magicForward(magic)
	mw2.damagePlayer(magic.owner,id,10,magic.me.name)
	magic.dispose = true
end,function(magic,otherMagic) --hit magic

end,function(magic) --update

end,function(magic) -- despawn

end)

mw2.name.FireBolt = mw2.addProjectile("Fire Bolt",2,1,-90,25,16,32,"bolt/Tile437_C15_R3",function(magic)--spawn

end,function(magic) --hit wall
	mw2.magicBack(magic)
	mw2.explosion(magic.x,magic.y,40,32,magic.owner)
	magic.dispose = true
end,function(magic,id) --hit player
	--mw2.magicForward(magic)
	mw2.explosion(magic.x,magic.y,40,32,magic.owner)
	magic.dispose = true
end,function(magic,otherMagic) --hit magic

end,function(magic) --update

end,function(magic) -- despawn

end)

mw2.name.FrostBolt = mw2.addProjectile("Frost Bolt",2,1,-90,25,16,32,"bolt/Tile809_C27_R3",function(magic)--spawn

end,function(magic) --hit wall
	magic.dispose = true
end,function(magic,id) --hit player
	--mw2.magicForward(magic)
	mw2.damagePlayer(magic.owner,id,10,magic.me.name)
	mw2.frezze(magic.x,magic.y,10)
	magic.dispose = true
end,function(magic,otherMagic) --hit magic

end,function(magic) --update

end,function(magic) -- despawn

end)

mw2.name.ElectricBolt = mw2.addProjectile("Electric Bolt",2,1,-90,30,16,32,"bolt/Tile1181_C39_R3",function(magic)--spawn

end,function(magic) --hit wall
	mw2.magicBack(magic)
	parse("flashposition "..magic.x.." "..magic.y.." 25")
	magic.dispose = true
end,function(magic,id) --hit player
	--mw2.magicForward(magic)
	mw2.damagePlayer(magic.owner,id,5,magic.me.name)
	parse("flashplayer "..id.." 25")
	magic.dispose = true
end,function(magic,otherMagic) --hit magic

end,function(magic) --update

end,function(magic) -- despawn

end)

mw2.name.HealBolt = mw2.addProjectile("Heal Bolt",3,1,-90,16,16,32,"bolt/Tile561_C19_R3",function(magic)--spawn

end,function(magic) --hit wall
	magic.dispose = true
end,function(magic,id) --hit player
	--mw2.magicForward(magic)
	parse('effect "flare" '..magic.x..' '..magic.y..' 4 16 0 255 0')
	if math.random(1,100) < mw2.honor(id) then
		mw2.damagePlayer(magic.owner,id,-25,magic.me.name)
		if player(id,"health") == player(id,"maxhealth") then
			if mw2.honor(id) > 90 then
                mw2.damageMaxHealth(magic.owner,id,-2,"Heal Bolt")
				--parse("setmaxhealth "..id.." "..player(id,"maxhealth")+2)
			else
                mw2.damageMaxHealth(magic.owner,id,-1,"Heal Bolt")
				parse("setmaxhealth "..id.." "..player(id,"maxhealth")+1)
			end
		end
		-- msg2(magic.owner,player(id,"name").." has ("..player(id,"health").."/"..player(id,"maxhealth")..") hp")
		magic.dispose = true
		if player(id,"health") == 250 then
			return 0
		end
	else
		--mw2.damagePlayer(magic.owner,id,-25,magic.me.name)
		msg2(magic.owner,"you failed to heal "..player(id,"name").." due to honor ("..player(id,"health").." hp)")
		return 0
	end
end,function(magic,otherMagic) --hit magic

end,function(magic) --update

end,function(magic) -- despawn

end)

mw2.name.RainbowBolt = mw2.addProjectile("Rainbow Bolt",5,1,-90,22,16,32,"bolt/Tile1429_C47_R3",function(magic)--spawn

end,function(magic) --hit wall
	mw2.magicBack(magic)
	parse("flashposition "..magic.x.." "..magic.y.." 10")
	mw2.explosion(magic.x,magic.y,40,32,magic.owner)
	magic.dispose = true
end,function(magic,id) --hit player
	mw2.magicForward(magic)
	if player(id,"speedmod") > -25 then
		mw2.playerSpeed(id,-5)
	end
	mw2.explosion(magic.x,magic.y,40,32,magic.owner)
	mw2.damagePlayer(magic.owner,id,10,magic.me.name)
	magic.dispose = true
end,function(magic,otherMagic) --hit magic

end,function(magic) --update

end,function(magic) -- despawn

end)

-------------
--[CHARGES]--
-------------

mw2.name.PureCharge = mw2.addProjectile("Pure Charge",3,1,-90,32,25,32,"charges/Tile1770_C58_R3",function(magic)--spawn

end,function(magic) --hit wall
	magic.dispose = true
end,function(magic,id) --hit player
	mw2.magicForward(magic)
	mw2.damagePlayer(magic.owner,id,32,magic.me.name)
	magic.dispose = true
end,function(magic,otherMagic) --hit magic

end,function(magic) --update

end,function(magic) -- despawn

end)

mw2.name.FireCharge = mw2.addProjectile("Fire Charge",5,1,-90,32,25,32,"charges/Tile1522_C50_R3",function(magic)--spawn

end,function(magic) --hit wall
	mw2.magicBack(magic)
	mw2.explosion(magic.x,magic.y,80,60,magic.owner)
	magic.dispose = true
end,function(magic,id) --hit player
	mw2.magicForward(magic)
	mw2.explosion(magic.x,magic.y,80,60,magic.owner)
	magic.dispose = true
end,function(magic,otherMagic) --hit magic

end,function(magic) --update

end,function(magic) -- despawn

end)

mw2.name.ShockCharge = mw2.addProjectile("Shock Charge",5,1,false,32,25,32,"charges/Tile1708_C56_R3",function(magic)--spawn

end,function(magic) --hit wall
	mw2.magicBack(magic)
	mw2.explosion(magic.x,magic.y,50,32,magic.owner)
	parse("flashposition "..magic.x.." "..magic.y.." 25")
	magic.dispose = true
end,function(magic,id) --hit player
	mw2.damagePlayer(magic.owner,id,5,magic.me.name)
	mw2.magicForward(magic)
	parse("flashplayer "..id.." 25")
	magic.dispose = true
end,function(magic,otherMagic) --hit magic

end,function(magic) --update

end,function(magic) -- despawn

end)

mw2.name.IceCharge = mw2.addProjectile("Ice Charge",5,1,false,32,25,32,"charges/Tile1615_C53_R3",function(magic)--spawn

end,function(magic) --hit wall
	mw2.frezze(magic.x,magic.y,16)
	magic.dispose = true
end,function(magic,id) --hit player
	mw2.magicForward(magic)
	mw2.frezze(magic.x,magic.y,16)
	mw2.damagePlayer(magic.owner,id,20,magic.me.name)
	magic.dispose = true
end,function(magic,otherMagic) --hit magic

end,function(magic) --update

end,function(magic) -- despawn

end)

-----------------
--[PROJECTILES]--
-----------------

mw2.name.FireBall = mw2.addProjectile("Fire Ball",20,1,false,32,32,32,"projectile/Tile3010_C98_R3",function(magic)--spawn

end,function(magic) --hit wall
    mw2.magicBack(magic)
	mw2.explosion(magic.x,magic.y,120,100,magic.owner)
	magic.dispose = true
end,function(magic,id) --hit player
    mw2.magicBack(magic)
	mw2.explosion(magic.x,magic.y,120,100,magic.owner)
	magic.dispose = true
end,function(magic,otherMagic) --hit magic

end,function(magic) --update

end,function(magic) -- despawn

end)

mw2.name.ShockBall = mw2.addProjectile("Shock Ball",20,1,false,32,32,32,"projectile/Tile3103_C101_R3",function(magic)--spawn

end,function(magic) --hit wall
	mw2.magicBack(magic)
	mw2.explosion(magic.x,magic.y,64,32,magic.owner)
	parse("flashposition "..magic.x.." "..magic.y.." 100")
	magic.dispose = true
end,function(magic,id) --hit player
    mw2.magicBack(magic)
	mw2.explosion(magic.x,magic.y,64,32,magic.owner)
	parse("flashplayer "..id.." 100")
	magic.dispose = true
end,function(magic,otherMagic) --hit magic

end,function(magic) --update

end,function(magic) -- despawn

end)

mw2.name.IceBall = mw2.addProjectile("Ice Ball",20,1,false,32,32,32,"projectile/Tile3165_C103_R3",function(magic)--spawn

end,function(magic) --hit wall
	mw2.frezze(magic.x,magic.y,25)
	magic.dispose = true
end,function(magic,id) --hit player
	mw2.frezze(magic.x,magic.y,25)
	mw2.damagePlayer(magic.owner,id,32,magic.me.name)
	magic.dispose = true
end,function(magic,otherMagic) --hit magic

end,function(magic) --update

end,function(magic) -- despawn

end)

-----------
--[WINDS]--
-----------

mw2.name.PureWind = mw2.addProjectile("Pure whirlwind",17,1,false,16,64,32*5,"whirlwind/Tile1353_C44_R20",function(magic)--spawn

end,function(magic) --hit wall
	mw2.magicBack(magic)
	mw2.explosion(magic.x,magic.y,96,16,magic.owner)
	magic.dispose = true
end,function(magic,id) --hit player
	local newx = player(id,"x")+(magic.xmove*1.25)
	local newy = player(id,"y")+(magic.ymove*1.25)
	if tile(misc.pixel_to_tile(newx),misc.pixel_to_tile(newy),"walkable") and (tile(misc.pixel_to_tile(newx),misc.pixel_to_tile(newy),"frame") > 0) then
		tp_smoke = false
		cs2d.setpos.call(id,newx,newy)
	end
end,function(magic,otherMagic) --hit magic

end,function(magic) --update

end,function(magic) -- despawn

end)

mw2.name.StormWind = mw2.addProjectile("Storm whirlwind",27,1,false,32,64,32*5,"whirlwind/yellowhirlwind",function(magic)--spawn

end,function(magic) --hit wall
	mw2.magicBack(magic)
	mw2.explosion(magic.x,magic.y,128,32,magic.owner)
	magic.dispose = true
end,function(magic,id) --hit player
	local newx = player(id,"x")+(magic.xmove*1.25)
	local newy = player(id,"y")+(magic.ymove*1.25)
	if tile(misc.pixel_to_tile(newx),misc.pixel_to_tile(newy),"walkable") and (tile(misc.pixel_to_tile(newx),misc.pixel_to_tile(newy),"frame") > 0) then
		tp_smoke = false
		cs2d.setpos.call(id,newx,newy)
	end
	parse("flashplayer "..id.." 5")
end,function(magic,otherMagic) --hit magic

end,function(magic) --update

end,function(magic) -- despawn

end)

mw2.name.FrozenWind = mw2.addProjectile("Frozen whirlwind",23,1,false,16,64,32*5,"whirlwind/Tile1229_C40_R20",function(magic)--spawn

end,function(magic) --hit wall
	mw2.magicBack(magic)
	--mw2.explosion(magic.x,magic.y,128,16,magic.owner)
	magic.dispose = true
end,function(magic,id) --hit player
	local newx = player(id,"x")+(magic.xmove*1.25)
	local newy = player(id,"y")+(magic.ymove*1.25)
	if tile(misc.pixel_to_tile(newx),misc.pixel_to_tile(newy),"walkable") and (tile(misc.pixel_to_tile(newx),misc.pixel_to_tile(newy),"frame") > 0) then
		tp_smoke = false
		cs2d.setpos.call(id,newx,newy)
	end
	if player(id,"speedmod") > -50 then
		mw2.playerSpeed(id,-1)
	end
end,function(magic,otherMagic) --hit magic

end,function(magic) --update

end,function(magic) -- despawn

end)

mw2.name.ExplosiveWind = mw2.addProjectile("Explosive whirlwind",27,1,false,32,64,32*5,"whirlwind/Tile1322_C43_R20",function(magic)--spawn

end,function(magic) --hit wall
	mw2.explosion(magic.x,magic.y,128,64,magic.owner)
	magic.dispose = true
end,function(magic,id) --hit player
	local newx = player(id,"x")+(magic.xmove*1.25)
	local newy = player(id,"y")+(magic.ymove*1.25)
	if tile(misc.pixel_to_tile(newx),misc.pixel_to_tile(newy),"walkable") and (tile(misc.pixel_to_tile(newx),misc.pixel_to_tile(newy),"frame") > 0) then
		tp_smoke = false
		cs2d.setpos.call(id,newx,newy)
	end
	mw2.explosion(newx,newy,64,8,magic.owner)
end,function(magic,otherMagic) --hit magic

end,function(magic) --update

end,function(magic) -- despawn

end)

--------------------
--[AREA AFFECTORS]--
--------------------

mw2.name.AreaHeal = mw2.addProjectile("Heal Affector",20,4,false,0.1,96,128,"area/Tile1309_C43_R7",function(magic)--spawn

end,function(magic) --hit wall
	magic.dispose = true
end,function(magic,id) --hit player
    if math.random(100) < 10 then
        --if player(id,"x") < magic.x+magic.size and player(id,"x") > magic.x-magic.size and player(id,"y") < magic.y+magic.size and player(id,"y") > magic.y-magic.size then
            mw2.damagePlayer(magic.owner,id,-5,magic.me.name)
            if player(id,"health") == player(id,"maxhealth") then
                mw2.damageMaxHealth(magic.owner,id,-1,"Heal Affector")
                --parse("setmaxhealth "..id.." "..player(id,"maxhealth")+1)
            end
        --end
    end
end,function(magic,otherMagic) --hit magic

end,function(magic) --update

end,function(magic) -- despawn

end)

mw2.name.AreaSpeed = mw2.addProjectile("Speed Affector",20,4,false,0.1,96,128,"area/Tile1340_C44_R7",function(magic)--spawn

end,function(magic) --hit wall
	magic.dispose = true
end,function(magic,id) --hit player
	if math.random(100) < 5 then
        mw2.playerSpeed(id,1)
	end
end,function(magic,otherMagic) --hit magic

end,function(magic) --update

end,function(magic) -- despawn

end)

mw2.name.AreaDamage = mw2.addProjectile("Damage Affector",10,1,false,0.1,96,128,"area/Tile1683_C55_R9",function(magic)--spawn

end,function(magic) --hit wall
	magic.dispose = true
end,function(magic,id) --hit player
    if math.random(100) < 25 then
        mw2.damagePlayer(magic.owner,id,5,magic.me.name)
    end
end,function(magic,otherMagic) --hit magic

end,function(magic) --update

end,function(magic) -- despawn

end)

mw2.name.AreaTaint = mw2.addProjectile("Taint Affector",10,1,false,0.1,96,128,"area/Tile1683_C55_R9_tainted",function(magic)--spawn

end,function(magic) --hit wall
	magic.dispose = true
end,function(magic,id) --hit player
    mw2.damagePlayer(magic.owner,id,10,magic.me.name)
    if math.random(100) < 25 then
        mw2.damageMaxHealth(magic.owner,id,5,"Taint Affector")
        --local maxhealth = player(id,"maxhealth")-5
        --parse("setmaxhealth "..id.." "..maxhealth)
    end
end,function(magic,otherMagic) --hit magic

end,function(magic) --update

end,function(magic) -- despawn

end)

-------------
--[JUMPERS]--
-------------

mw2.name.ElectricJumper = mw2.addProjectile("Electric Jumper",17,1,false,8,128,64,"jumpers/electricjumper",function(magic)--spawn

end,function(magic) --hit wall

end,function(magic,id) --hit player
	if math.random(100) < 25 then
		mw2.damagePlayer(magic.owner,id,3,magic.me.name)
		magic.x = player(id,"x")
		magic.y = player(id,"y")
		magic.xmove = 0
		magic.ymove = 0
		tween_move(magic.image,100,magic.x,magic.y)
	end
end,function(magic,otherMagic) --hit magic

end,function(magic) --update

end,function(magic) -- despawn

end)

mw2.name.ExplosiveJumper = mw2.addProjectile("Explosive Jumper",25,1,false,8,128,64,"jumpers/Tile2979_C97_R3",function(magic)--spawn

end,function(magic) --hit wall

end,function(magic,id) --hit player
	if math.random(100) < 25 then
		mw2.explosion(magic.x,magic.y,64,10,magic.owner)
		magic.x = player(id,"x")
		magic.y = player(id,"y")
		magic.xmove = 0
		magic.ymove = 0
		tween_move(magic.image,100,magic.x,magic.y)
	end
end,function(magic,otherMagic) --hit magic

end,function(magic) --update

end,function(magic) -- despawn

end)

mw2.name.CloudJumper = mw2.addProjectile("Electric Cloud Jumper",40,1,false,4,128,64,"jumpers/Tile1280_C42_R9",function(magic)--spawn
    magic.count = math.random(5,10)
end,function(magic) --hit wall

end,function(magic,id) --hit player

end,function(magic,otherMagic) --hit magic

end,function(magic) --update
    magic.count = magic.count - 1
	if magic.count <= 0 then
        magic.count = math.random(5,10)
		--local dir = misc.point_direction(magic.x,magic.y,player(id,"x"),player(id,"y"))
		mw2.spawnProjectile(mw2.name.ElectricJumper,magic.x,magic.y,magic.owner,math.random()*360-180)
	end
end,function(magic) -- despawn

end)


-----------
--[STARS]--
-----------

mw2.name.ExplosiveStar = mw2.addProjectile("Explosive Star",80,1,-90,32,32,128,"star/Tile3196_C104_R3",function(magic)--spawn

end,function(magic) --hit wall
	mw2.magicBack(magic)
    mw2.magicBack(magic)
	local dir = 0
	while dir < 360 do
		dir = dir + (360/16)
		mw2.spawnProjectile(mw2.name.FireBall,magic.x,magic.y,magic.owner,dir)
	end
	mw2.explosion(magic.x,magic.y,200,100,magic.owner)
	magic.dispose = true
end,function(magic,id) --hit player
	mw2.magicBack(magic)
	local dir = 0
	while dir < 360 do
		dir = dir + (360/16)
		mw2.spawnProjectile(mw2.name.FireBall,magic.x,magic.y,magic.owner,dir)
	end
	mw2.explosion(magic.x,magic.y,200,100,magic.owner)
	magic.dispose = true
end,function(magic,otherMagic) --hit magic

end,function(magic) --update

end,function(magic) -- despawn

end)

mw2.name.IceStar = mw2.addProjectile("Ice Star",80,1,-90,32,32,128,"star/Tile1229_C40_R20",function(magic)--spawn

end,function(magic) --hit wall
	mw2.magicBack(magic)
    mw2.magicBack(magic)
	local dir = 0
	while dir < 360 do
		dir = dir + (360/16)
		mw2.spawnProjectile(mw2.name.IceBall,magic.x,magic.y,magic.owner,dir)
	end
	mw2.frezze(magic.x,magic.y,100)
	magic.dispose = true
end,function(magic,id) --hit player
	mw2.magicBack(magic)
	local dir = 0
	while dir < 360 do
		dir = dir + (360/16)
		mw2.spawnProjectile(mw2.name.IceBall,magic.x,magic.y,magic.owner,dir)
	end
	mw2.frezze(magic.x,magic.y,100)
	magic.dispose = true
end,function(magic,otherMagic) --hit magic

end,function(magic) --update

end,function(magic) -- despawn

end)

mw2.name.ElectricStar = mw2.addProjectile("Electric Star",80,1,-90,32,32,128,"star/Tile3289_C107_R3",function(magic)--spawn

end,function(magic) --hit wall
	mw2.magicBack(magic)
    mw2.magicBack(magic)
	local dir = 0
	while dir < 360 do
		dir = dir + (360/16)
		mw2.spawnProjectile(mw2.name.ShockBall,magic.x,magic.y,magic.owner,dir)
	end
	mw2.explosion(magic.x,magic.y,200,50,magic.owner)
	magic.dispose = true
end,function(magic,id) --hit player
	mw2.magicBack(magic)
	local dir = 0
	while dir < 360 do
		dir = dir + (360/16)
		mw2.spawnProjectile(mw2.name.ShockBall,magic.x,magic.y,magic.owner,dir)
	end
	mw2.explosion(magic.x,magic.y,200,50,magic.owner)
	magic.dispose = true
end,function(magic,otherMagic) --hit magic

end,function(magic) --update

end,function(magic) -- despawn

end)

-----------
--[WARDS]--
-----------

mw2.name.WeakWard = mw2.addProjectile("Weak Ward",50,2,-90,0,96,128,"ward/ward",function(magic)--spawn
	tween_alpha(magic.image,magic.me.lifetime*100,0)
end,function(magic) --hit wall
	magic.dispose = true
end,function(magic,id) --hit player

end,function(magic,otherMagic) --hit magic
    local magicDamage = math.min(magic.magicHealth,otherMagic.magicHealth)
    magic.magicHealth = magic.magicHealth - magicDamage
    if (magic.magicHealth <= 0) then
        magic.dispose = true
    end
    otherMagic.magicHealth = otherMagic.magicHealth - magicDamage
    if (otherMagic.magicHealth <= 0) then
        otherMagic.dispose = true
    end
	local alpha = (1/(magic.me.lifetime*magic.me.magicHealth))*(magic.lifetime*magic.magicHealth)
    imagealpha(magic.image,alpha)
    tween_alpha(magic.image,magic.lifetime*100,0)
end,function(magic) --update

end,function(magic) -- despawn

end)

mw2.name.MediumWard = mw2.addProjectile("Medium Ward",100,2,-90,0,96,256,"ward/ward",function(magic)--spawn
    tween_alpha(magic.image,magic.me.lifetime*100,0)
end,function(magic) --hit wall
	magic.dispose = true
end,function(magic,id) --hit player

end,function(magic,otherMagic) --hit magic
    local magicDamage = math.min(magic.magicHealth,otherMagic.magicHealth)
    magic.magicHealth = magic.magicHealth - magicDamage
    if (magic.magicHealth <= 0) then
        magic.dispose = true
    end
    otherMagic.magicHealth = otherMagic.magicHealth - magicDamage
    if (otherMagic.magicHealth <= 0) then
        otherMagic.dispose = true
    end
	local alpha = (1/(magic.me.lifetime*magic.me.magicHealth))*(magic.lifetime*magic.magicHealth)
    imagealpha(magic.image,alpha)
    tween_alpha(magic.image,magic.lifetime*100,0)
end,function(magic) --update

end,function(magic) -- despawn

end)

mw2.name.StrongWard = mw2.addProjectile("Strong Ward",200,2,-90,0,96,512,"ward/ward",function(magic)--spawn
	tween_alpha(magic.image,magic.me.lifetime*100,0)
end,function(magic) --hit wall
	magic.dispose = true
end,function(magic,id) --hit player

end,function(magic,otherMagic) --hit magic
    local magicDamage = math.min(magic.magicHealth,otherMagic.magicHealth)
    magic.magicHealth = magic.magicHealth - magicDamage
    if (magic.magicHealth <= 0) then
        magic.dispose = true
    end
    otherMagic.magicHealth = otherMagic.magicHealth - magicDamage
    if (otherMagic.magicHealth <= 0) then
        otherMagic.dispose = true
    end
	local alpha = (1/(magic.me.lifetime*magic.me.magicHealth))*(magic.lifetime*magic.magicHealth)
    imagealpha(magic.image,alpha)
    tween_alpha(magic.image,magic.lifetime*100,0)
end,function(magic) --update

end,function(magic) -- despawn

end)

mw2.name.SuperiorWard = mw2.addProjectile("Superior Ward",400,2,-90,0,96,1024,"ward/ward",function(magic)--spawn
	tween_alpha(magic.image,magic.me.lifetime*100,0)
end,function(magic) --hit wall
	magic.dispose = true
end,function(magic,id) --hit player

end,function(magic,otherMagic) --hit magic
    local magicDamage = math.min(magic.magicHealth,otherMagic.magicHealth)
    magic.magicHealth = magic.magicHealth - magicDamage
    if (magic.magicHealth <= 0) then
        magic.dispose = true
    end
    otherMagic.magicHealth = otherMagic.magicHealth - magicDamage
    if (otherMagic.magicHealth <= 0) then
        otherMagic.dispose = true
    end
	local alpha = (1/(magic.me.lifetime*magic.me.magicHealth))*(magic.lifetime*magic.magicHealth)
    imagealpha(magic.image,alpha)
    tween_alpha(magic.image,magic.lifetime*100,0)
end,function(magic) --update

end,function(magic) -- despawn

end)

--------------------
--[SELF AFFECTORS]--
--------------------

mw2.name.SelfSpeed = mw2.addProjectile("Self Speed",2,0,false,0,32,5,"self/Tile1187_C39_R9",function(magic)--spawn
	local speed = 5
	if (player(magic.owner,"bot")) and (player(magic.owner,"speedmod") > 0) then
		speed = speed / 2
	end
	mw2.playerSpeed(magic.owner,speed)
    freeimage(magic.image)

    magic.image = image("gfx/magic wizard 2/projectiles/"..magic.me.image..".png",0,0,200+magic.owner)
    tween_alpha(magic.image,magic.me.lifetime*50,0)
end,function(magic) --hit wall

end,function(magic,id) --hit player

end,function(magic,otherMagic) --hit magic

end,function(magic) --update

end,function(magic) -- despawn

end)

--------------
--[TELEPORT]--
--------------

mw2.name.TeleportTo = mw2.addProjectile("Teleport To Player",25,1,0,32,32,16,"teleport/Tile2397_C78_R10",function(magic)--spawn

end,function(magic) --hit wall

end,function(magic,id) --hit player
    if (not(mw2flags.hasFlag(id,mw2flags.library.teleportPrevention))) then
        cs2d.setpos.call(magic.owner,player(id,"x"),player(id,"y"))
    end
    magic.dispose = true
end,function(magic,otherMagic) --hit magic

end,function(magic) --update

end,function(magic) -- despawn

end)

mw2.name.TeleportFrom = mw2.addProjectile("Teleport Player",25,1,0,32,32,16,"teleport/Tile2335_C76_R10",function(magic)--spawn

end,function(magic) --hit wall

end,function(magic,id) --hit player
	cs2d.setpos.call(id,player(magic.owner,"x"),player(magic.owner,"y"))
	magic.dispose = true
end,function(magic,otherMagic) --hit magic

end,function(magic) --update

end,function(magic) -- despawn

end)

mw2.name.TeleportSwap = mw2.addProjectile("Teleport Swap",30,1,0,52,32,16,"teleport/Tile1167_C38_R20",function(magic)--spawn

end,function(magic) --hit wall

end,function(magic,id) --hit player
	local x = player(magic.owner,"x")
	local y = player(magic.owner,"y")
    if (not(mw2flags.hasFlag(id,mw2flags.library.teleportPrevention))) then
        cs2d.setpos.call(magic.owner,player(id,"x"),player(id,"y"))
    end
	cs2d.setpos.call(id,x,y)
	magic.dispose = true
end,function(magic,otherMagic) --hit magic

end,function(magic) --update

end,function(magic) -- despawn

end)

-------------
--[BUILD]--
-------------

mw2.name.Barricade = mw2.addProjectile("Barricade",2,1,0,16,32,2,"build/Tile823_C27_R17",function(magic)--spawn

end,function(magic) --hit wall
	mw2.magicBack(magic)
	-- spawnobject type x y rot mode team player
	if not(tile(misc.pixel_to_tile(magic.x),misc.pixel_to_tile(magic.y),"entity") == 70) then
		parse("spawnobject 1 "..misc.pixel_to_tile(magic.x).." "..misc.pixel_to_tile(magic.y).." 0 0 "..player(magic.owner,"team").." "..magic.owner)
	end
	magic.dispose = true
end,function(magic,id) --hit player
	if not(tile(misc.pixel_to_tile(magic.x),misc.pixel_to_tile(magic.y),"entity") == 70) then
		parse("spawnobject 1 "..misc.pixel_to_tile(magic.x).." "..misc.pixel_to_tile(magic.y).." 0 0 "..player(magic.owner,"team").." "..magic.owner)
	end
	magic.dispose = true
end,function(magic,otherMagic) --hit magic

end,function(magic) --update

end,function(magic) -- despawn
	if not(tile(misc.pixel_to_tile(magic.x),misc.pixel_to_tile(magic.y),"entity") == 70) then
		parse("spawnobject 1 "..misc.pixel_to_tile(magic.x).." "..misc.pixel_to_tile(magic.y).." 0 0 "..player(magic.owner,"team").." "..magic.owner)
	end
end)

mw2.name.BarbedWire = mw2.addProjectile("Barbed Wire",2,1,0,16,32,2,"build/Tile823_C27_R17",function(magic)--spawn

end,function(magic) --hit wall
	mw2.magicBack(magic)
	-- spawnobject type x y rot mode team player
	if not(tile(misc.pixel_to_tile(magic.x),misc.pixel_to_tile(magic.y),"entity") == 70) then
		parse("spawnobject 2 "..misc.pixel_to_tile(magic.x).." "..misc.pixel_to_tile(magic.y).." 0 0 "..player(magic.owner,"team").." "..magic.owner)
	end
	magic.dispose = true
end,function(magic,id) --hit player
	if not(tile(misc.pixel_to_tile(magic.x),misc.pixel_to_tile(magic.y),"entity") == 70) then
		parse("spawnobject 2 "..misc.pixel_to_tile(magic.x).." "..misc.pixel_to_tile(magic.y).." 0 0 "..player(magic.owner,"team").." "..magic.owner)
	end
	magic.dispose = true
end,function(magic,otherMagic) --hit magic

end,function(magic) --update

end,function(magic) -- despawn
	if not(tile(misc.pixel_to_tile(magic.x),misc.pixel_to_tile(magic.y),"entity") == 70) then
		parse("spawnobject 2 "..misc.pixel_to_tile(magic.x).." "..misc.pixel_to_tile(magic.y).." 0 0 "..player(magic.owner,"team").." "..magic.owner)
	end
end)

mw2.name.Wall1 = mw2.addProjectile("Wall 1",5,1,0,16,32,16,"build/Tile823_C27_R17",function(magic)--spawn

end,function(magic) --hit wall
	mw2.magicBack(magic)
	-- spawnobject type x y rot mode team player
	if not(tile(misc.pixel_to_tile(magic.x),misc.pixel_to_tile(magic.y),"entity") == 70) then
		parse("spawnobject 3 "..misc.pixel_to_tile(magic.x).." "..misc.pixel_to_tile(magic.y).." 0 0 "..player(magic.owner,"team").." "..magic.owner)
	end
	magic.dispose = true
end,function(magic,id) --hit player
	if not(tile(misc.pixel_to_tile(magic.x),misc.pixel_to_tile(magic.y),"entity") == 70) then
		parse("spawnobject 3 "..misc.pixel_to_tile(magic.x).." "..misc.pixel_to_tile(magic.y).." 0 0 "..player(magic.owner,"team").." "..magic.owner)
	end
	magic.dispose = true
end,function(magic,otherMagic) --hit magic

end,function(magic) --update

end,function(magic) -- despawn
	if not(tile(misc.pixel_to_tile(magic.x),misc.pixel_to_tile(magic.y),"entity") == 70) then
		parse("spawnobject 3 "..misc.pixel_to_tile(magic.x).." "..misc.pixel_to_tile(magic.y).." 0 0 "..player(magic.owner,"team").." "..magic.owner)
	end
end)

mw2.name.Wall2 = mw2.addProjectile("Wall 2",7,1,0,16,32,16,"build/Tile823_C27_R17",function(magic)--spawn

end,function(magic) --hit wall
	mw2.magicBack(magic)
	-- spawnobject type x y rot mode team player
	if not(tile(misc.pixel_to_tile(magic.x),misc.pixel_to_tile(magic.y),"entity") == 70) then
		parse("spawnobject 4 "..misc.pixel_to_tile(magic.x).." "..misc.pixel_to_tile(magic.y).." 0 0 "..player(magic.owner,"team").." "..magic.owner)
	end
	magic.dispose = true
end,function(magic,id) --hit player
	if not(tile(misc.pixel_to_tile(magic.x),misc.pixel_to_tile(magic.y),"entity") == 70) then
		parse("spawnobject 4 "..misc.pixel_to_tile(magic.x).." "..misc.pixel_to_tile(magic.y).." 0 0 "..player(magic.owner,"team").." "..magic.owner)
	end
	magic.dispose = true
end,function(magic,otherMagic) --hit magic

end,function(magic) --update

end,function(magic) -- despawn
	if not(tile(misc.pixel_to_tile(magic.x),misc.pixel_to_tile(magic.y),"entity") == 70) then
		parse("spawnobject 4 "..misc.pixel_to_tile(magic.x).." "..misc.pixel_to_tile(magic.y).." 0 0 "..player(magic.owner,"team").." "..magic.owner)
	end
end)

mw2.name.Wall3 = mw2.addProjectile("Wall 3",9,1,0,16,32,16,"build/Tile823_C27_R17",function(magic)--spawn

end,function(magic) --hit wall
	mw2.magicBack(magic)
	-- spawnobject type x y rot mode team player
	if not(tile(misc.pixel_to_tile(magic.x),misc.pixel_to_tile(magic.y),"entity") == 70) then
		parse("spawnobject 5 "..misc.pixel_to_tile(magic.x).." "..misc.pixel_to_tile(magic.y).." 0 0 "..player(magic.owner,"team").." "..magic.owner)
	end
	magic.dispose = true
end,function(magic,id) --hit player
	if not(tile(misc.pixel_to_tile(magic.x),misc.pixel_to_tile(magic.y),"entity") == 70) then
		parse("spawnobject 5 "..misc.pixel_to_tile(magic.x).." "..misc.pixel_to_tile(magic.y).." 0 0 "..player(magic.owner,"team").." "..magic.owner)
	end
	magic.dispose = true
end,function(magic,otherMagic) --hit magic

end,function(magic) --update

end,function(magic) -- despawn
	if not(tile(misc.pixel_to_tile(magic.x),misc.pixel_to_tile(magic.y),"entity") == 70) then
		parse("spawnobject 5 "..misc.pixel_to_tile(magic.x).." "..misc.pixel_to_tile(magic.y).." 0 0 "..player(magic.owner,"team").." "..magic.owner)
	end
end)

mw2.name.GateField = mw2.addProjectile("Gate Field",6,1,0,16,32,16,"build/Tile823_C27_R17",function(magic)--spawn

end,function(magic) --hit wall
	mw2.magicBack(magic)
	-- spawnobject type x y rot mode team player
	if not(tile(misc.pixel_to_tile(magic.x),misc.pixel_to_tile(magic.y),"entity") == 70) then
		parse("spawnobject 6 "..misc.pixel_to_tile(magic.x).." "..misc.pixel_to_tile(magic.y).." 0 0 "..player(magic.owner,"team").." "..magic.owner)
	end
	magic.dispose = true
end,function(magic,id) --hit player
	if not(tile(misc.pixel_to_tile(magic.x),misc.pixel_to_tile(magic.y),"entity") == 70) then
		parse("spawnobject 6 "..misc.pixel_to_tile(magic.x).." "..misc.pixel_to_tile(magic.y).." 0 0 "..player(magic.owner,"team").." "..magic.owner)
	end
	magic.dispose = true
end,function(magic,otherMagic) --hit magic

end,function(magic) --update

end,function(magic) -- despawn
	if not(tile(misc.pixel_to_tile(magic.x),misc.pixel_to_tile(magic.y),"entity") == 70) then
		parse("spawnobject 6 "..misc.pixel_to_tile(magic.x).." "..misc.pixel_to_tile(magic.y).." 0 0 "..player(magic.owner,"team").." "..magic.owner)
	end
end)

mw2.name.Dispenser = mw2.addProjectile("Dispenser",12,1,0,16,32,16,"build/Tile823_C27_R17",function(magic)--spawn

end,function(magic) --hit wall
	mw2.magicBack(magic)
	-- spawnobject type x y rot mode team player
	if not(tile(misc.pixel_to_tile(magic.x),misc.pixel_to_tile(magic.y),"entity") == 70) then
		parse("spawnobject 7 "..misc.pixel_to_tile(magic.x).." "..misc.pixel_to_tile(magic.y).." 0 0 "..player(magic.owner,"team").." "..magic.owner)
	end
	magic.dispose = true
end,function(magic,id) --hit player
	if not(tile(misc.pixel_to_tile(magic.x),misc.pixel_to_tile(magic.y),"entity") == 70) then
		parse("spawnobject 7 "..misc.pixel_to_tile(magic.x).." "..misc.pixel_to_tile(magic.y).." 0 0 "..player(magic.owner,"team").." "..magic.owner)
	end
	magic.dispose = true
end,function(magic,otherMagic) --hit magic

end,function(magic) --update

end,function(magic) -- despawn
	if not(tile(misc.pixel_to_tile(magic.x),misc.pixel_to_tile(magic.y),"entity") == 70) then
		parse("spawnobject 7 "..misc.pixel_to_tile(magic.x).." "..misc.pixel_to_tile(magic.y).." 0 0 "..player(magic.owner,"team").." "..magic.owner)
	end
end)

mw2.name.Turret = mw2.addProjectile("Turret",10,1,0,16,32,16,"build/Tile823_C27_R17",function(magic)--spawn

end,function(magic) --hit wall
	mw2.magicBack(magic)
	-- spawnobject type x y rot mode team player
	if not(tile(misc.pixel_to_tile(magic.x),misc.pixel_to_tile(magic.y),"entity") == 70) then
		parse("spawnobject 8 "..misc.pixel_to_tile(magic.x).." "..misc.pixel_to_tile(magic.y).." 0 0 "..player(magic.owner,"team").." "..magic.owner)
	end
	magic.dispose = true
end,function(magic,id) --hit player
	if not(tile(misc.pixel_to_tile(magic.x),misc.pixel_to_tile(magic.y),"entity") == 70) then
		parse("spawnobject 8 "..misc.pixel_to_tile(magic.x).." "..misc.pixel_to_tile(magic.y).." 0 0 "..player(magic.owner,"team").." "..magic.owner)
	end
	magic.dispose = true
end,function(magic,otherMagic) --hit magic

end,function(magic) --update

end,function(magic) -- despawn
	if not(tile(misc.pixel_to_tile(magic.x),misc.pixel_to_tile(magic.y),"entity") == 70) then
		parse("spawnobject 8 "..misc.pixel_to_tile(magic.x).." "..misc.pixel_to_tile(magic.y).." 0 0 "..player(magic.owner,"team").." "..magic.owner)
	end
end)

mw2.name.Supply = mw2.addProjectile("Supply",7,1,0,16,32,16,"build/Tile823_C27_R17",function(magic)--spawn

end,function(magic) --hit wall
	mw2.magicBack(magic)
	-- spawnobject type x y rot mode team player
	if not(tile(misc.pixel_to_tile(magic.x),misc.pixel_to_tile(magic.y),"entity") == 70) then
		parse("spawnobject 9 "..misc.pixel_to_tile(magic.x).." "..misc.pixel_to_tile(magic.y).." 0 0 "..player(magic.owner,"team").." "..magic.owner)
	end
	magic.dispose = true
end,function(magic,id) --hit player
	if not(tile(misc.pixel_to_tile(magic.x),misc.pixel_to_tile(magic.y),"entity") == 70) then
		parse("spawnobject 9 "..misc.pixel_to_tile(magic.x).." "..misc.pixel_to_tile(magic.y).." 0 0 "..player(magic.owner,"team").." "..magic.owner)
	end
	magic.dispose = true
end,function(magic,otherMagic) --hit magic

end,function(magic) --update

end,function(magic) -- despawn
	if not(tile(misc.pixel_to_tile(magic.x),misc.pixel_to_tile(magic.y),"entity") == 70) then
		parse("spawnobject 9 "..misc.pixel_to_tile(magic.x).." "..misc.pixel_to_tile(magic.y).." 0 0 "..player(magic.owner,"team").." "..magic.owner)
	end
end)

-------------
--[SUMMON]---
-------------

mw2.name.SummonUndead = mw2.addProjectile("Summon Undead Npc",19,1,0,32,32,8,"summon/Tile2304_C75_R10",function(magic)--spawn

end,function(magic) --hit wall
	mw2.magicBack(magic)
	for i=1, 3 do
		parse("spawnnpc "..math.random(1,4).." "..misc.pixel_to_tile(magic.x)+math.random(-1,1).." "..misc.pixel_to_tile(magic.y)+math.random(-1,1).." "..magic.dir)
	end
	magic.dispose = true
end,function(magic,id) --hit player
    mw2flag.new(id,magic.owner,mw2flags.library.deathDoor,60+math.random()*60)
	for i=1, 3 do
		local x = misc.pixel_to_tile(magic.x)+math.random(-1,1)
		local y = misc.pixel_to_tile(magic.y)+math.random(-1,1)
		if tile(x,y,"walkable") then
			parse("spawnnpc "..math.random(1,4).." "..x.." "..y.." "..magic.dir)
		else
			i = i - 1
		end
	end
	magic.dispose = true
end,function(magic,otherMagic) --hit magic

end,function(magic) --update

end,function(magic) -- despawn
	mw2.magicBack(magic)
	for i=1, 5 do
		parse("spawnnpc "..math.random(1,4).." "..misc.pixel_to_tile(magic.x)+math.random(-1,1).." "..misc.pixel_to_tile(magic.y)+math.random(-1,1).." "..magic.dir)
	end
end)

mw2.name.SummonUndeadPlayer = mw2.addProjectile("Summon Undead Player",100,1,0,8,32,16,"summon/Tile1652_C54_R9",function(magic)--spawn
	local rnd = math.random() * 5
	local time = rnd * rnd
	mw2flag.new(magic.owner, magic.owner, mw2flags.library.contaminated, time)
end,function(magic) --hit wall
	mw2.magicBack(magic)
	magic.dispose = true
end,function(magic,id) --hit player
    mw2flag.new(id,magic.owner,mw2flags.library.deathDoor,60+math.random()*60)
	magic.dispose = true
end,function(magic,otherMagic) --hit magic

end,function(magic) --update

end,function(magic) -- despawn
	local pl = player(0,"table")
	-- find target
	local i = 0
	local target = nil
	while ((target == nil) and (i < 32)) do
		i = i + 1
		target = pl[math.ceil(math.random()*#pl)]
		if (player(target,"health") > 0) then
			target = nil
		end
	end
	-- check
	if (target == nil) then
		msg2(magic.owner, misc.rgb(255,0,0).."The spell fizzles out...")
		return false
	end
	-- resurrect
	if player(target,"team") == 0 then
		if (math.random() < 0.5) then
			timer(1,"parse","maket "..target)
		else
			timer(1,"parse","makect "..target)
		end
	end
	timer(2,"parse","spawnplayer "..target.." "..magic.x.." "..magic.y)
	timer2(3,{target},function(id)
		mw2.playerSpeedSet(id,25)
		mw2flag.new(id,magic.owner,mw2flags.library.contaminated,5)
		mw2flag.new(id,magic.owner,mw2flags.library.immortal,10)
		mw2flag.new(id,magic.owner,mw2flags.library.confused,15)
		mw2flag.new(id,magic.owner,mw2flags.library.healing,20)
		mw2flag.new(id,magic.owner,mw2flags.library.teleportPrevention,25)
	end)
end)

-------------
--[SPECIAL]--
-------------

mw2.name.FloatingHole = mw2.addProjectile("Floating Hole",150,1,false,4,64,140,"special/Tile1156_C38_R9",function(magic)--spawn
	local rnd = math.random() * 5
	local time = rnd * rnd
	mw2flag.new(magic.owner, magic.owner, mw2flags.library.contaminated, time)
end,function(magic) --hit wall

end,function(magic,id) --hit player
    --mw2.damagePlayer(magic.owner,id,10,magic.me.name)
end,function(magic,otherMagic) --hit magic
    magic.dispose = true
end,function(magic) --update
    pl = player(0,"tableliving")
    i = 0
    while (i < #pl) do
        i = i + 1
        if (not(pl[i] == magic.owner)) then
            local px = player(pl[i],"x")
            local py = player(pl[i],"y")
            local dist = misc.point_distance(magic.x,magic.y,px,py)
            local dist_max = 750
            local max_expected_damage_second = 120
            local damage_chance = 0.2
            if dist < dist_max then
                local power = dist_max - dist
                local dmg = (power/dist_max)*(max_expected_damage_second*damage_chance)
                dmg = math.ceil(dmg)
                local dist_move = power*0.05
                --negdist = (negdist*0.1)
                --negdist = (negdist*negdist)*0.1
                dir = misc.point_direction(px,py,magic.x,magic.y)
                if (math.random() < damage_chance) then
                    --dmg = misc.round((3/dist_max)*negdist)
                    mw2.damagePlayer(magic.owner,pl[i],dmg,magic.me.name)
                end
                x_add = misc.lengthdir_x(dir,dist_move)
                y_add = misc.lengthdir_y(dir,dist_move)
                if (not(tile(misc.pixel_to_tile(px+x_add),misc.pixel_to_tile(py+y_add),"wall"))) then
                    cs2d.setpos.call(pl[i],px+x_add,py+y_add)
                end
            end
        end
    end
end,function(magic) -- despawn

end)

mw2.name.handOfDoom = mw2.addProjectile("HOD",300,1,false,25,32,256,"special/Tile981_C32_R20",function(magic)--spawn
	local rnd = math.random() * 5
	local time = rnd * rnd
	mw2flag.new(magic.owner, magic.owner, mw2flags.library.contaminated, time)

    magic.userAttacted = nil
end,function(magic) --hit wall
	if not (magic.userAttacted == nil) then
        mw2.damagePlayer(magic.owner,magic.userAttacted,100,magic.me.name)
        mw2.explosion(magic.x,magic.y,100,100,magic.owner)
	end
end,function(magic,id) --hit player
    if (magic.userAttacted == nil) then
        magic.userAttacted = id
        mw2.playerSpeedSet(magic.userAttacted,-100)
    end
end,function(magic,otherMagic) --hit magic

end,function(magic) --update
    if not (magic.userAttacted == nil) then
        if player(magic.userAttacted,"exists") then
            if (player(magic.userAttacted,"health") <= 0) then
                magic.dispose = true
            else
            	if (magic.userAttacted == magic.owner) then
            		magic.dispose = true
            		return
            	end
                parse("strip "..magic.userAttacted.." 0")
                mw2.player.speed[magic.userAttacted] = -100
                local hp = mw2.damagePlayer(magic.owner,magic.userAttacted,5,magic.me.name)
                if (player(magic.userAttacted,"health") <= 0) then
                    misc.explosion(player(magic.userAttacted,"x"),player(magic.userAttacted,"y"),100,100,magic.owner)
                    magic.dispose = true
                    magic.userAttacted = nil
                else
                    magic.xmove = math.random(-64,64)
                    magic.ymove = math.random(-64,64)
                    tp_smoke = false
                    cs2d.setpos.call(magic.userAttacted,magic.x+magic.xmove,magic.y+magic.ymove)
                    tween_move(magic.image,100,magic.x+magic.xmove,magic.y+magic.ymove)
                end
            end
        else
            magic.dispose = true
        end
    end
end,function(magic) -- despawn
    if not (magic.userAttacted == nil) then
        if player(magic.userAttacted,"exists") and (player(magic.userAttacted,"health") > 0) then
            if (tile(player(magic.userAttacted,"tilex"),player(magic.userAttacted,"tiley"),"frame") == 0) then
                parse("customkill "..magic.owner.." \"Void\" "..magic.userAttacted)
            end
        end
    end
end)

mw2.name.Disarm = mw2.addProjectile("Disarm",50,1,false,25,32,32,"special/Tile2273_C74_R10",function(magic)--spawn

end,function(magic) --hit wall

end,function(magic,id) --hit player
    local skip = {50}
    for i=1, #skip do
        if (player(id,"weapontype") == skip[i]) then
            return 0
        end
    end
    msg2(id,misc.rgb(255,0,0).."You feel that your "..itemtype(player(id,"weapontype"),"name").." fades away!")
    msg2(magic.owner,misc.rgb(255,0,0).."You feel that "..player(id,"name").."'s "..itemtype(player(id,"weapontype"),"name").." fades away!")
    --msg2(id,misc.rgb(255,0,0).."You feel that your "..itemtype(player(id,"weapontype"),"name").." starts fading!")
	parse("strip "..id.." "..player(id,"weapontype"))
end,function(magic,otherMagic) --hit magic

end,function(magic) --update

end,function(magic) -- despawn

end)

mw2.name.PowerDisarm = mw2.addProjectile("PowerDisarm",125,1,false,25,32,8,"special/Tile2273_C74_R10",function(magic)--spawn

end,function(magic) --hit wall

end,function(magic,id) --hit player
    mw2.damagePlayer(magic.owner,id,5,magic.me.name)
    local skip = {50} -- weapons to skip
    local weapons = playerweapons(id)
    local i = 0
    while (i < #weapons) do
        i = i + 1
        local do_skip = false
        local wid = weapons[i]
        for i=1, #skip do
            if (wid == skip[i]) then
                do_skip = true
                break
            end
        end
        if (do_skip == false) then
            msg2(id,misc.rgb(255,0,0).."You feel that your "..itemtype(wid,"name").." fades away!")
            msg2(magic.owner,misc.rgb(255,0,0).."You feel that "..player(id,"name").."'s "..itemtype(wid,"name").." fades away!")
            parse("strip "..id.." "..wid)
        end
    end
    -- armor
    local armor = player(id,"armor")
    if (armor > 0) then
    	parse("setarmor "..id.." 0")
    	msg2(id,misc.rgb(255,0,0).."You feel that your armor fades away!")
    	msg2(magic.owner,misc.rgb(255,0,0).."You feel that "..player(id,"name").."'s armor fades away!")
    end
end,function(magic,otherMagic) --hit magic

end,function(magic) --update

end,function(magic) -- despawn

end)

mw2.name.livingLockpicks = mw2.addProjectile("living Lockpicks",30,1,-90,8,32,16,"special/Tile2521_C82_R10",function(magic)--spawn

end,function(magic) --hit wall
	local name = (entity(misc.pixel_to_tile(magic.x),misc.pixel_to_tile(magic.y),"name"))
	if not (name == false) then
		parse("trigger "..name)
	end
	magic.dispose = true
end,function(magic,id) --hit player

end,function(magic,otherMagic) --hit magic

end,function(magic) --update

end,function(magic) -- despawn

end)

-----------
--[WAVES]--
-----------

mw2.name.pureWave = mw2.addProjectile("Pure Waves",30,1,-90,32,50,3,"wave/wave_pure",function(magic)--spawn

end,function(magic) --hit wall
	magic.dispose = true
end,function(magic,id) --hit player
	mw2.damagePlayer(magic.owner,id,30,magic.me.name)
	magic.dispose = true
end,function(magic,otherMagic) --hit magic

end,function(magic) --update

end,function(magic) -- despawn

end)

mw2.name.iceWave = mw2.addProjectile("Ice Waves",40,1,-90,32,50,3,"wave/wave_ice",function(magic)--spawn

end,function(magic) --hit wall
	magic.dispose = true
end,function(magic,id) --hit player
	if player(id,"speedmod") > -30 then
		mw2.playerSpeed(id,-5)
	end
	mw2.damagePlayer(magic.owner,id,25,magic.me.name)
	magic.dispose = true
end,function(magic,otherMagic) --hit magic

end,function(magic) --update

end,function(magic) -- despawn

end)

mw2.name.healWave = mw2.addProjectile("Healing Waves",35,1,-90,20,50,5,"wave/wave_heal",function(magic)--spawn

end,function(magic) --hit wall
	magic.dispose = true
end,function(magic,id) --hit player
	mw2.magicForward(magic)
	parse('effect "flare" '..magic.x..' '..magic.y..' 8 16 0 255 0')
	mw2.damagePlayer(magic.owner,id,-25,magic.me.name)
	if math.random(1,100) < mw2.honor(id) then
		if player(id,"health") == player(id,"maxhealth") then
			if mw2.honor(id) > 90 then
                healMax = 10
			else
                healMax = 5
			end
            mw2.damageMaxHealth(magic.owner,id,-math.random(1,healMax),"Healing Waves")
            --parse("setmaxhealth "..id.." "..player(id,"maxhealth")+math.random(1,healMax))
		end
		-- msg2(magic.owner,player(id,"name").." has ("..player(id,"health").."/"..player(id,"maxhealth")..") hp")
		magic.dispose = true
		if player(id,"health") == 250 then
			return 0
		end
	else
		mw2.damagePlayer(magic.owner,id,-25,magic.me.name)
		msg2(magic.owner,"you failed to heal "..player(id,"name").." due to honor ("..player(id,"health").." hp)")
		return 0
	end
end,function(magic,otherMagic) --hit magic

end,function(magic) --update

end,function(magic) -- despawn

end)

mw2.name.taintWave = mw2.addProjectile("Tainted Waves",50,1,-90,32,50,3,"wave/wave_taint",function(magic)--spawn

end,function(magic) --hit wall
	magic.dispose = true
end,function(magic,id) --hit player
	mw2.damagePlayer(magic.owner,id,25,magic.me.name)
	parse("setmaxhealth "..id.." "..player(id,"maxhealth")-10)
    magic.dispose = true
end,function(magic,otherMagic) --hit magic

end,function(magic) --update

end,function(magic) -- despawn

end)

--------------
--[ILLUSION]--
--------------

mw2.name.illusionSilver = mw2.addProjectile("Silver Coin Illusion",1,1,-90,0,16,512,"build/Tile823_C27_R17",function(magic)--spawn
	freeimage(magic.image)
	magic.image = image("gfx/magic wizard 2/items/Tile288_C10_R9.png",magic.x,magic.y,0)
	magic.x = (misc.round((magic.x-16)/32)*32)+16
	magic.y = (misc.round((magic.y-16)/32)*32)+16
	tween_move(magic.image,5000,magic.x,magic.y)
end,function(magic) --hit wall

end,function(magic,id) --hit player
	mw2.explosion(magic.x,magic.y,64,100,magic.owner)
	msg2(id,"triggered a trap by "..player(magic.owner,"name"))
	magic.dispose = true
end,function(magic,otherMagic) --hit magic

end,function(magic) --update

end,function(magic) -- despawn

end)

mw2.name.illusionScroll = mw2.addProjectile("Scroll Illusion",1,1,-90,0,16,512,"build/Tile823_C27_R17",function(magic)--spawn
	freeimage(magic.image)
	magic.image = image("gfx/magic wizard 2/items/Tile2303_C75_R9.png",magic.x,magic.y,0)
	magic.x = (misc.round((magic.x-16)/32)*32)+16
	magic.y = (misc.round((magic.y-16)/32)*32)+16
	tween_move(magic.image,5000,magic.x,magic.y)
end,function(magic) --hit wall

end,function(magic,id) --hit player
	mw2.explosion(magic.x,magic.y,64,100,magic.owner)
	msg2(id,"triggered a trap by "..player(magic.owner,"name"))
	magic.dispose = true
end,function(magic,otherMagic) --hit magic

end,function(magic) --update

end,function(magic) -- despawn

end)

------------------------
--[PHASE 2: ADD MAGIC]--
------------------------

-----------
--[BOLTS]--
-----------

mw2.addMagic("Pure Bolt","Bolts",0,1,"wand_1.png",function(id)
	mw2.spawnProjectile(mw2.name.PureBolt,player(id,"x"),player(id,"y"),id,player(id,"rot"))
end)

mw2.addMagic("Fire Bolt","Bolts",2,2,"wand_1.png",function(id)
	mw2.spawnProjectile(mw2.name.FireBolt,player(id,"x"),player(id,"y"),id,player(id,"rot"))
end)

mw2.addMagic("Frost Bolt","Bolts",3,2,"wand_1.png",function(id)
	mw2.spawnProjectile(mw2.name.FrostBolt,player(id,"x"),player(id,"y"),id,player(id,"rot"))
end)

mw2.addMagic("Electric Bolt","Bolts",3,2,"wand_1.png",function(id)
	mw2.spawnProjectile(mw2.name.ElectricBolt,player(id,"x"),player(id,"y"),id,player(id,"rot"))
end)

mw2.addMagic("Heal Bolt","Bolts",2,3,"wand_1.png",function(id)
	mw2.spawnProjectile(mw2.name.HealBolt,player(id,"x"),player(id,"y"),id,player(id,"rot"))
end)

mw2.addMagic("Rainbow Bolt","Bolts",6,5,"wand_1.png",function(id)
	mw2.spawnProjectile(mw2.name.RainbowBolt,player(id,"x"),player(id,"y"),id,player(id,"rot"))
end)

-------------
--[CHARGES]--
-------------

mw2.addMagic("Pure Charge","Charge",5,5,"wand_2.png",function(id)
	mw2.spawnProjectile(mw2.name.PureCharge,player(id,"x"),player(id,"y"),id,player(id,"rot"))
end)

mw2.addMagic("Fire Charge","Charge",7,5,"wand_2.png",function(id)
	mw2.spawnProjectile(mw2.name.FireCharge,player(id,"x"),player(id,"y"),id,player(id,"rot"))
end)

mw2.addMagic("Shock Charge","Charge",7,5,"wand_2.png",function(id)
	mw2.spawnProjectile(mw2.name.ShockCharge,player(id,"x"),player(id,"y"),id,player(id,"rot"))
end)

mw2.addMagic("Ice Charge","Charge",7,5,"wand_2.png",function(id)
	mw2.spawnProjectile(mw2.name.IceCharge,player(id,"x"),player(id,"y"),id,player(id,"rot"))
end)

-----------------
--[PROJECTILES]--
-----------------

mw2.addMagic("Fire Ball","Projectile",10,10,"wand_3.png",function(id)
	mw2.spawnProjectile(mw2.name.FireBall,player(id,"x"),player(id,"y"),id,player(id,"rot"))
end)

mw2.addMagic("Shock Ball","Projectile",10,10,"wand_3.png",function(id)
	mw2.spawnProjectile(mw2.name.ShockBall,player(id,"x"),player(id,"y"),id,player(id,"rot"))
end)

mw2.addMagic("Ice Ball","Projectile",10,10,"wand_3.png",function(id)
	mw2.spawnProjectile(mw2.name.IceBall,player(id,"x"),player(id,"y"),id,player(id,"rot"))
end)

-----------
--[WINDS]--
-----------

mw2.addMagic("Pure Whirlwind","Whirlwind",15,20,"wand_4.png",function(id)
	mw2.spawnProjectile(mw2.name.PureWind,player(id,"x"),player(id,"y"),id,player(id,"rot"))
end)

mw2.addMagic("Storm Whirlwind","Whirlwind",20,30,"wand_4.png",function(id)
	mw2.spawnProjectile(mw2.name.StormWind,player(id,"x"),player(id,"y"),id,player(id,"rot"))
end)

mw2.addMagic("Frozen Whirlwind","Whirlwind",25,25,"wand_4.png",function(id)
	mw2.spawnProjectile(mw2.name.FrozenWind,player(id,"x"),player(id,"y"),id,player(id,"rot"))
end)

mw2.addMagic("Explosive Whirlwind","Whirlwind",30,35,"wand_4.png",function(id)
	mw2.spawnProjectile(mw2.name.ExplosiveWind,player(id,"x"),player(id,"y"),id,player(id,"rot"))
end)

--------------------
--[AREA AFFECTORS]--
--------------------

mw2.addMagic("Heal Affector","Area Affector",15,20,"wand_5.png",function(id)
	mw2.spawnProjectile(mw2.name.AreaHeal,player(id,"x"),player(id,"y"),id,player(id,"rot"))
end)

mw2.addMagic("Speed Affector","Area Affector",10,25,"wand_5.png",function(id)
	mw2.spawnProjectile(mw2.name.AreaSpeed,player(id,"x"),player(id,"y"),id,player(id,"rot"))
end)

mw2.addMagic("Damage Affector","Area Affector",20,40,"wand_5.png",function(id)
	mw2.spawnProjectile(mw2.name.AreaDamage,player(id,"x"),player(id,"y"),id,player(id,"rot"))
end)

mw2.addMagic("Taint Affector","Area Affector",25,50,"wand_5.png",function(id)
	mw2.spawnProjectile(mw2.name.AreaTaint,player(id,"x"),player(id,"y"),id,player(id,"rot"))
end)

-------------
--[JUMPERS]--
-------------

mw2.addMagic("Electric Jumper","Jumpers",15,25,"wand_6.png",function(id)
	mw2.spawnProjectile(mw2.name.ElectricJumper,player(id,"x"),player(id,"y"),id,player(id,"rot"))
end)

mw2.addMagic("Explosive Jumper","Jumpers",20,30,"wand_6.png",function(id)
	mw2.spawnProjectile(mw2.name.ExplosiveJumper,player(id,"x"),player(id,"y"),id,player(id,"rot"))
end)

mw2.addMagic("Electric Cloud Jumper","Jumpers",30,60,"wand_6.png",function(id)
	mw2.spawnProjectile(mw2.name.CloudJumper,player(id,"x"),player(id,"y"),id,player(id,"rot"))
end)

----------
--[STAR]--
----------

mw2.addMagic("Explosive Star","Star",25,80,"wand_7.png",function(id)
	mw2.spawnProjectile(mw2.name.ExplosiveStar,player(id,"x"),player(id,"y"),id,player(id,"rot"))
end)

mw2.addMagic("Ice Star","Star",25,85,"wand_7.png",function(id)
	mw2.spawnProjectile(mw2.name.IceStar,player(id,"x"),player(id,"y"),id,player(id,"rot"))
end)

mw2.addMagic("Shock Star","Star",20,75,"wand_7.png",function(id)
	mw2.spawnProjectile(mw2.name.ElectricStar,player(id,"x"),player(id,"y"),id,player(id,"rot"))
end)

-----------
--[WARDS]--
-----------

mw2.addMagic("Weak Ward","Ward",5,20,"wand_8.png",function(id)
	mw2.spawnProjectile(mw2.name.WeakWard,player(id,"x"),player(id,"y"),id,player(id,"rot"))
end)

mw2.addMagic("Medium Ward","Ward",10,30,"wand_8.png",function(id)
	mw2.spawnProjectile(mw2.name.MediumWard,player(id,"x"),player(id,"y"),id,player(id,"rot"))
end)

mw2.addMagic("Strong Ward","Ward",20,50,"wand_8.png",function(id)
	mw2.spawnProjectile(mw2.name.StrongWard,player(id,"x"),player(id,"y"),id,player(id,"rot"))
end)

mw2.addMagic("Superior Ward","Ward",40,75,"wand_8.png",function(id)
	mw2.spawnProjectile(mw2.name.SuperiorWard,player(id,"x"),player(id,"y"),id,player(id,"rot"))
end)

--------------------
--[SELF AFFECTORS]--
--------------------

mw2.addMagic("Self Speed","Self Affector",20,40,"wand_9.png",function(id)
	mw2.spawnProjectile(mw2.name.SelfSpeed,player(id,"x"),player(id,"y"),id,player(id,"rot"))
end)

--------------
--[TELEPORT]--
--------------

mw2.addMagic("Teleport To","Teleport",10,25,"wand_10.png",function(id)
	mw2.spawnProjectile(mw2.name.TeleportTo,player(id,"x"),player(id,"y"),id,player(id,"rot"))
end)

mw2.addMagic("Teleport player","Teleport",10,25,"wand_10.png",function(id)
	mw2.spawnProjectile(mw2.name.TeleportFrom,player(id,"x"),player(id,"y"),id,player(id,"rot"))
end)

mw2.addMagic("Teleport Swapper","Teleport",15,25,"wand_10.png",function(id)
	mw2.spawnProjectile(mw2.name.TeleportSwap,player(id,"x"),player(id,"y"),id,player(id,"rot"))
end)

-----------
--[BUILD]--
-----------

mw2.addMagic("Barricade","Build",1,10,"wand_11.png",function(id)
	mw2.spawnProjectile(mw2.name.Barricade,player(id,"x"),player(id,"y"),id,player(id,"rot"))
end)

mw2.addMagic("Barbed Wire","Build",2,10,"wand_11.png",function(id)
	mw2.spawnProjectile(mw2.name.BarbedWire,player(id,"x"),player(id,"y"),id,player(id,"rot"))
end)

mw2.addMagic("Wall 1","Build",2,15,"wand_11.png",function(id)
	mw2.spawnProjectile(mw2.name.Wall1,player(id,"x"),player(id,"y"),id,player(id,"rot"))
end)

mw2.addMagic("Wall 2","Build",3,20,"wand_11.png",function(id)
	mw2.spawnProjectile(mw2.name.Wall2,player(id,"x"),player(id,"y"),id,player(id,"rot"))
end)

mw2.addMagic("Wall 3","Build",4,25,"wand_11.png",function(id)
	mw2.spawnProjectile(mw2.name.Wall3,player(id,"x"),player(id,"y"),id,player(id,"rot"))
end)

mw2.addMagic("Gate Field","Build",4,25,"wand_11.png",function(id)
	mw2.spawnProjectile(mw2.name.GateField,player(id,"x"),player(id,"y"),id,player(id,"rot"))
end)

mw2.addMagic("Dispenser","Build",6,50,"wand_11.png",function(id)
	mw2.spawnProjectile(mw2.name.Dispenser,player(id,"x"),player(id,"y"),id,player(id,"rot"))
end)

mw2.addMagic("Turret","Build",10,75,"wand_11.png",function(id)
	mw2.spawnProjectile(mw2.name.Turret,player(id,"x"),player(id,"y"),id,player(id,"rot"))
    mw2.player.weaponUse[id] = mw2.player.weaponUse[id] + 100
end)

mw2.addMagic("Supply","Build",7,40,"wand_11.png",function(id)
	mw2.spawnProjectile(mw2.name.Supply,player(id,"x"),player(id,"y"),id,player(id,"rot"))
end)

-------------
--[SUMMON]--
-------------

mw2.addMagic("Summon Armor","Summon",12,30,"wand_12.png",function(id)
	local num = 78+math.ceil(math.random(4))
		msg2(id,"You summon a "..itemtype(num,"name"))
		parse("equip "..id.." "..num)
end)

mw2.addMagic("Summon Undead npc","Summon",10,35,"wand_12.png",function(id)
	mw2.spawnProjectile(mw2.name.SummonUndead,player(id,"x"),player(id,"y"),id,player(id,"rot"))
end)

mw2.addMagic("Summon Undead Player","Summon",10,100,"wand_12.png",function(id)
	if player(id,"health") <= 50 then
		mw2.damagePlayer(id,id,10000,"Sacrificed himself")
        --parse("customkill "..id.." \"Sacrificed himself\" "..id)
	else
        mw2.damageMaxHealth(id,id,50,"Sacrificed himself")
		--parse("setmaxhealth "..id.." "..player(id,"maxhealth")-50)
	end
	mw2.spawnProjectile(mw2.name.SummonUndeadPlayer,player(id,"x"),player(id,"y"),id,player(id,"rot"))
end)

-------------
--[SPECIAL]--
-------------

mw2.addMagic("Floating Hole","Special",50,100,"wand_13.png",function(id)
	msg(misc.rgb(255,0,0)..player(id,"name").." Launched the Floating Hole!")
    mw2.spawnProjectile(mw2.name.FloatingHole,player(id,"x"),player(id,"y"),id,player(id,"rot"))
end)

mw2.addMagic("Hand of Doom","Special",50,100,"wand_13.png",function(id)
	msg(misc.rgb(255,0,0)..player(id,"name").." Launched the Hand Of Doom!")
    mw2.spawnProjectile(mw2.name.handOfDoom,player(id,"x"),player(id,"y"),id,player(id,"rot"))
end)

mw2.addMagic("Disarm","Special",15,5,"wand_13.png",function(id)
	mw2.spawnProjectile(mw2.name.Disarm,player(id,"x"),player(id,"y"),id,player(id,"rot"))
end)

mw2.addMagic("Power Disarm","Special",25,20,"wand_13.png",function(id)
	mw2.spawnProjectile(mw2.name.PowerDisarm,player(id,"x"),player(id,"y"),id,player(id,"rot"))
end)

mw2.addMagic("living Lockpicks","Special",15,25,"wand_13.png",function(id)
	mw2.spawnProjectile(mw2.name.livingLockpicks,player(id,"x"),player(id,"y"),id,player(id,"rot"))
end)

-----------
--[WAVES]--
-----------

mw2.addMagic("Pure Wave","Waves",10,10,"wand_14.png",function(id)
	mw2.spawnProjectile(mw2.name.pureWave,player(id,"x"),player(id,"y"),id,player(id,"rot"))
end)

mw2.addMagic("Ice Wave","Waves",15,10,"wand_14.png",function(id)
	mw2.spawnProjectile(mw2.name.iceWave,player(id,"x"),player(id,"y"),id,player(id,"rot"))
end)

mw2.addMagic("Heal Wave","Waves",10,10,"wand_14.png",function(id)
	mw2.spawnProjectile(mw2.name.healWave,player(id,"x"),player(id,"y"),id,player(id,"rot"))
end)

mw2.addMagic("Taint Wave","Waves",20,10,"wand_14.png",function(id)
	mw2.spawnProjectile(mw2.name.taintWave,player(id,"x"),player(id,"y"),id,player(id,"rot"))
end)

--------------
--[ILLUSION]--
--------------

mw2.addMagic("Silver Coin illusion","Illusion",5,50,"wand_15.png",function(id)
	mw2.spawnProjectile(mw2.name.illusionSilver,player(id,"x"),player(id,"y"),id,player(id,"rot"))
end)

mw2.addMagic("Scroll illusion","Illusion",5,50,"wand_15.png",function(id)
	mw2.spawnProjectile(mw2.name.illusionScroll,player(id,"x"),player(id,"y"),id,player(id,"rot"))
end)
