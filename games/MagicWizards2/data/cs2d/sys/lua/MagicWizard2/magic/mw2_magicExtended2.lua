------------------------------
--[PHASE 1: ADD PROJECTILES]--
------------------------------

-----------
--[BOLTS]--
-----------

mw2.name.VampiricBolt = mw2.addProjectile("Vampiric Bolt",1,1,-90,25,16,32,"bolt/Tile809_C27_R3_v",function(magic)--spawn

end,function(magic) --hit wall
	magic.dispose = true
end,function(magic,id) --hit player
	--mw2.magicForward(magic)
    local dmg = mw2.damagePlayer(magic.owner,id,10,magic.me.name)
    mw2.damagePlayer(id,magic.owner,-dmg * 0.75,magic.me.name)
	magic.dispose = true
end,function(magic,otherMagic) --hit magic

end,function(magic) --update

end,function(magic) -- despawn

end)

-------------
--[CHARGES]--
-------------

mw2.name.VampiricCharge = mw2.addProjectile("Vampiric Charge",3,1,-90,32,25,32,"charges/Tile1615_C53_R3_v",function(magic)--spawn

end,function(magic) --hit wall
	magic.dispose = true
end,function(magic,id) --hit player
    local dmg = mw2.damagePlayer(magic.owner,id,20,magic.me.name)
    mw2.damagePlayer(id,magic.owner,-dmg * 0.75,magic.me.name)
	magic.dispose = true
end,function(magic,otherMagic) --hit magic

end,function(magic) --update

end,function(magic) -- despawn

end)

-----------------
--[PROJECTILES]--
-----------------

mw2.name.VampiricBall = mw2.addProjectile("Vampiric Ball",20,1,false,32,32,32,"projectile/Tile3165_C103_R3_v",function(magic)--spawn

end,function(magic) --hit wall
    mw2.magicBack(magic)
	mw2.explosion(magic.x,magic.y,64,64,magic.owner)
	magic.dispose = true
end,function(magic,id) --hit player
    local dmg = mw2.damagePlayer(magic.owner,id,50,magic.me.name)
    mw2.damagePlayer(id,magic.owner,-dmg * 0.75,magic.me.name)
	magic.dispose = true
end,function(magic,otherMagic) --hit magic

end,function(magic) --update

end,function(magic) -- despawn

end)

-----------
--[STARS]--
-----------

mw2.name.TaintStar = mw2.addProjectile("Taint Star",80,1,-90,32,32,128,"star/Tile3320_C108_R3",function(magic)--spawn

end,function(magic) --hit wall
	mw2.magicBack(magic)
    mw2.magicBack(magic)
	local dir = 0
	while dir < 360 do
		dir = dir + (360/16)
		mw2.spawnProjectile(mw2.name.TaintBall,magic.x,magic.y,magic.owner,dir)
	end
	mw2.explosion(magic.x,magic.y,200,100,magic.owner)
	magic.dispose = true
end,function(magic,id) --hit player
	mw2.magicBack(magic)
	local dir = 0
	while dir < 360 do
		dir = dir + (360/16)
		mw2.spawnProjectile(mw2.name.TaintBall,magic.x,magic.y,magic.owner,dir)
	end
	mw2.explosion(magic.x,magic.y,200,100,magic.owner)
	magic.dispose = true
end,function(magic,otherMagic) --hit magic

end,function(magic) --update

end,function(magic) -- despawn

end)

--------------
--[TELEPORT]--
--------------

mw2.name.Telekinesis = mw2.addProjectile("Telekinesis",10,0,0,32,16,16,"teleport/telek",function(magic)--spawn
    tween_alpha(magic.image,100*magic.lifetime,0)
	magic.isSlowed = false
end,function(magic) --hit wall
    if (magic.isSlowed == false) then
        magic.isSlowed = true
        mw2.projSetMotion(magic,magic.dir,8)
    end
end,function(magic,id) --hit player

end,function(magic,otherMagic) --hit magic

end,function(magic) --update
    x = misc.pixel_to_tile(magic.x)
    y = misc.pixel_to_tile(magic.y)
    if not (mw2.items.grid[x.."|"..y] == nil) then
        if (not(mw2flags.hasFlag(magic.owner,mw2flags.library.teleportPrevention))) then
            local index = mw2.items.grid[x.."|"..y].item.id
            action = mw2.items.grid[x.."|"..y].item.onTake(magic.owner,mw2.items.grid[x.."|"..y])
            if not (action == false) then
                parse("sv_sound2 "..magic.owner.." items/pickup.wav")
                mw2.items.count[index] = mw2.items.count[index] - 1
                msg2(magic.owner,"Picked up "..mw2.items.grid[x.."|"..y].item.name)
                freeimage(mw2.items.grid[x.."|"..y].image)
                mw2.items.grid[x.."|"..y] = nil
                -- remove reference
                local i = 0
                while (i < #mw2.items.grid_list) do
                    i = i + 1
                    local item_ref = mw2.items.grid_list[i]
                    if ((item_ref.x == x) and (item_ref.y == y)) then
                        mw2.items.grid_list[i] =  mw2.items.grid_list[#mw2.items.grid_list]
                        mw2.items.grid_list[#mw2.items.grid_list] = nil
                        --print("there are "..#mw2.items.grid_list.." items")
                    end
                end
            end
        end
         magic.dispose = true
     end
end,function(magic) -- despawn

end)

-------------
--[SPECIAL]--
-------------

mw2.name.Follower = mw2.addProjectile("Follower",10,1,false,0,32,250,"special/Tile2645_C86_R10",function(magic)--spawn
    magic.userAttacted = 0
    magic.ownerX = player(magic.owner,"x")
    magic.ownerY = player(magic.owner,"y")
end,function(magic) --hit wall
    
end,function(magic,id) --hit player
    if (magic.userAttacted == 0) and (not(magic.owner == id)) then
        magic.userAttacted = id
        msg2(magic.owner,misc.rgb(0,255,0).."Following "..player(magic.userAttacted,"name"))
    end
end,function(magic,otherMagic) --hit magic
    
end,function(magic) --update
    if (player(magic.owner,"health") > 0) then
        -- delete if moved
        local pX = player(magic.owner,"x")
        local pY = player(magic.owner,"y")
        local owner_movement = misc.primitive_point_distance(pX,pY,magic.ownerX,magic.ownerY)
        --msg(misc.round(owner_movement,1))
        local owner_interrupt = (owner_movement > 5)
        if (owner_interrupt) then
            msg2(magic.owner,misc.rgb(255,0,0).."Follower interrupted by your movement")
            magic.dispose = true
        end
        -- follow target
        if (magic.userAttacted > 0) then
            if (player(magic.userAttacted,"exists") and (player(magic.userAttacted,"health") > 0)) then
                local x = player(magic.userAttacted,"x")
                local y = player(magic.userAttacted,"y")
                local dist = misc.point_distance(x,y,magic.x,magic.y)
                if (dist > 100+(mw2.player.speed[magic.userAttacted]*2)) then
                    msg2(magic.owner,misc.rgb(255,0,0).."Follower lost connection with "..player(magic.userAttacted,"name"))
                    magic.dispose = true
                else
                    if (dist > 32) then
                        local dir = misc.point_direction(x,y,magic.x,magic.y)
                        magic.x = x+misc.lengthdir_x(dir,32)
                        magic.y = y+misc.lengthdir_y(dir,32)
                        tween_move(magic.image,100,magic.x,magic.y)
                        cs2d.setpos.call(magic.owner,magic.x,magic.y)
                    end
                end
            else
                msg2(magic.owner,misc.rgb(255,0,0).."Follower lost connection with "..player(magic.userAttacted,"name"))
                magic.dispose = true
            end
        end
        magic.ownerX = magic.x
        magic.ownerY = magic.y
    else
        magic.dispose = true
    end
end,function(magic) -- despawn
    
end)

mw2.name.strongFollower = mw2.addProjectile("Strong Follower",25,1,false,0,32,1000,"special/Tile2645_C86_R10",function(magic)--spawn
    magic.userAttacted = 0
    magic.ownerX = player(magic.owner,"x")
    magic.ownerY = player(magic.owner,"y")
end,function(magic) --hit wall
    
end,function(magic,id) --hit player
    if (magic.userAttacted == 0) and (not(magic.owner == id)) then
        magic.userAttacted = id
        msg2(magic.owner,misc.rgb(0,255,0).."Following "..player(magic.userAttacted,"name"))
    end
end,function(magic,otherMagic) --hit magic
    
end,function(magic) --update
    if (player(magic.owner,"health") > 0) then
        -- delete if moved
        local pX = player(magic.owner,"x")
        local pY = player(magic.owner,"y")
        local owner_movement = misc.primitive_point_distance(pX,pY,magic.ownerX,magic.ownerY)
        --msg(misc.round(owner_movement,1))
        local owner_interrupt = (owner_movement > 5)
        if (owner_interrupt) then
            msg2(magic.owner,misc.rgb(255,0,0).."Follower interrupted by your movement")
            magic.dispose = true
        end
        -- follow target
        if (magic.userAttacted > 0) then
            if (player(magic.userAttacted,"exists") and (player(magic.userAttacted,"health") > 0)) then
                local x = player(magic.userAttacted,"x")
                local y = player(magic.userAttacted,"y")
                local dist = misc.point_distance(x,y,magic.x,magic.y)
                if (dist > 200+(mw2.player.speed[magic.userAttacted]*2)) then
                    msg2(magic.owner,misc.rgb(255,0,0).."Follower lost connection with "..player(magic.userAttacted,"name"))
                    magic.dispose = true
                else
                    if (dist > 32) then
                        local dir = misc.point_direction(x,y,magic.x,magic.y)
                        magic.x = x+misc.lengthdir_x(dir,32)
                        magic.y = y+misc.lengthdir_y(dir,32)
                        tween_move(magic.image,100,magic.x,magic.y)
                        cs2d.setpos.call(magic.owner,magic.x,magic.y)
                    end
                end
            else
                msg2(magic.owner,misc.rgb(255,0,0).."Follower lost connection with "..player(magic.userAttacted,"name"))
                magic.dispose = true
            end
        end
        magic.ownerX = magic.x
        magic.ownerY = magic.y
    else
        magic.dispose = true
    end
end,function(magic) -- despawn
    
end)

---------------
--[SHOCKWAVE]--
---------------

mw2.name.healShockwave = mw2.addProjectile("Heal Shockwave",30,1,false,0,0,20,"shockwaves/healShockwave",function(magic)--spawn
    imagescale(magic.image,0,0)
    tween_scale(magic.image,2000,10,10)
end,function(magic) --hit wall
	
end,function(magic,id) --hit player
    power = 1
    if (magic.lifetime < 10) then
        power = magic.lifetime*0.1
    end
    if math.random(10) < power*10 then
        mw2.damagePlayer(magic.owner,id,-(power*2),magic.me.name)
    end
end,function(magic,otherMagic) --hit magic
    
end,function(magic) --update
    if (magic.lifetime == 10) then
        tween_alpha(magic.image,1000,0)
    end
    hitbox = ((64*10) - ((magic.lifetime*0.5)*64))*0.5
    magic.size = hitbox
end,function(magic) -- despawn

end)

-------------
--[SUMMON]---
-------------

mw2.name.summonMoney = mw2.addProjectile("Summon Money",10,0,-90,8,16,32,"summon/Tile2304_C75_R10",function(magic)--spawn
    magic.lastX = player(magic.owner,"tilex")
    magic.lastY = player(magic.owner,"tilex")
end,function(magic) --hit wall
    magic.dispose = true
end,function(magic,id) --hit player
    
end,function(magic,otherMagic) --hit magic
    
end,function(magic) --update
    local x = misc.pixel_to_tile(magic.x)
    local y = misc.pixel_to_tile(magic.y)
    if ((not(x == magic.lastX)) or (not(y == magic.lastY))) then
        local dont_spawn = false
        local items = item(0,"table")
        for i=1,#items do
            local it = items[i]
            local typ = item(it,"type")
            local ix = item(it,"x")
            local iy = item(it,"y")
            if ((typ >= 65) and (typ <= 68) and (ix == x) and (iy == y)) then
                dont_spawn = true
                break
            end
        end
        if (not(dont_spawn)) then
            magic.lastX = x
            magic.lastY = y
            local item = 65+(math.floor(math.random(3)))
            parse("spawnitem "..item.." "..x.." "..y)
        end
    end
end,function(magic) -- despawn
    
end)

-----------
--[EYE]--
-----------

mw2.name.pureBoltEye = mw2.addProjectile("Pure Bolt Eye",10,4,90,0,320,250,"eye/pure",function(magic)--spawn
    magic.targets = {}
    magic.reload = 0
end,function(magic) --hit wall
	magic.dispose = true
end,function(magic,id) --hit player
    if (not(magic.owner == id)) then
        local px = player(id,"x")
        local py = player(id,"y")
        if (misc.isInsideScreen(magic.x,magic.y,px,py)) then
            if (misc.isLineOfSight(magic.x,magic.y,px,py)) then
                magic.targets[#magic.targets+1] = {id,misc.primitive_point_distance(px,py,magic.x,magic.y)}
            end
        end
    end
end,function(magic,otherMagic) --hit magic

end,function(magic) --update
    magic.reload = magic.reload - 1
    local closest_dist = 640
    local closest_id = 0
    for i=1,#magic.targets do
        local pl = magic.targets[i]
        local id = pl[1]
        local dist = pl[2]
        if (player(id,"exists")) then
            if (dist < closest_dist) then
                closest_dist = dist
                closest_id = id
            end
        end
    end
    --
    if (closest_id > 0) then
        local dir = misc.point_direction(magic.x,magic.y,player(closest_id,"x"),player(closest_id,"y"))
        tween_rotate(magic.image,100,dir+90)
        -- shoot
        if (magic.reload <= 0) then
            magic.reload = 5
            mw2.spawnProjectile(mw2.name.PureBolt,magic.x,magic.y,magic.owner,dir)
        end
    end
    --
    magic.targets = {}
end,function(magic) -- despawn

end)

mw2.name.fireBoltEye = mw2.addProjectile("Fire Bolt Eye",10,4,90,0,320,250,"eye/fire",function(magic)--spawn
    magic.targets = {}
    magic.reload = 0
end,function(magic) --hit wall
	magic.dispose = true
end,function(magic,id) --hit player
    if (not(magic.owner == id)) then
        local px = player(id,"x")
        local py = player(id,"y")
        if (misc.isInsideScreen(magic.x,magic.y,px,py)) then
            if (misc.isLineOfSight(magic.x,magic.y,px,py)) then
                magic.targets[#magic.targets+1] = {id,misc.primitive_point_distance(px,py,magic.x,magic.y)}
            end
        end
    end
end,function(magic,otherMagic) --hit magic

end,function(magic) --update
    magic.reload = magic.reload - 1
    local closest_dist = 640
    local closest_id = 0
    for i=1,#magic.targets do
        local pl = magic.targets[i]
        local id = pl[1]
        local dist = pl[2]
        if (player(id,"exists")) then
            if (dist < closest_dist) then
                closest_dist = dist
                closest_id = id
            end
        end
    end
    --
    if (closest_id > 0) then
        local dir = misc.point_direction(magic.x,magic.y,player(closest_id,"x"),player(closest_id,"y"))
        tween_rotate(magic.image,100,dir+90)
        -- shoot
        if (magic.reload <= 0) then
            magic.reload = 5
            mw2.spawnProjectile(mw2.name.FireBolt,magic.x,magic.y,magic.owner,dir)
        end
    end
    --
    magic.targets = {}
end,function(magic) -- despawn

end)

mw2.name.frostBoltEye = mw2.addProjectile("Ice Bolt Eye",10,4,90,0,320,250,"eye/frost",function(magic)--spawn
    magic.targets = {}
    magic.reload = 0
end,function(magic) --hit wall
	magic.dispose = true
end,function(magic,id) --hit player
    if (not(magic.owner == id)) then
        local px = player(id,"x")
        local py = player(id,"y")
        if (misc.isInsideScreen(magic.x,magic.y,px,py)) then
            if (misc.isLineOfSight(magic.x,magic.y,px,py)) then
                magic.targets[#magic.targets+1] = {id,misc.primitive_point_distance(px,py,magic.x,magic.y)}
            end
        end
    end
end,function(magic,otherMagic) --hit magic

end,function(magic) --update
    magic.reload = magic.reload - 1
    local closest_dist = 640
    local closest_id = 0
    for i=1,#magic.targets do
        local pl = magic.targets[i]
        local id = pl[1]
        local dist = pl[2]
        if (player(id,"exists")) then
            if (dist < closest_dist) then
                closest_dist = dist
                closest_id = id
            end
        end
    end
    --
    if (closest_id > 0) then
        local dir = misc.point_direction(magic.x,magic.y,player(closest_id,"x"),player(closest_id,"y"))
        tween_rotate(magic.image,100,dir+90)
        -- shoot
        if (magic.reload <= 0) then
            magic.reload = 5
            mw2.spawnProjectile(mw2.name.FrostBolt,magic.x,magic.y,magic.owner,dir)
        end
    end
    --
    magic.targets = {}
end,function(magic) -- despawn

end)

mw2.name.electricBoltEye = mw2.addProjectile("Electric Bolt Eye",10,4,90,0,320,250,"eye/electric",function(magic)--spawn
    magic.targets = {}
    magic.reload = 0
end,function(magic) --hit wall
	magic.dispose = true
end,function(magic,id) --hit player
    if (not(magic.owner == id)) then
        local px = player(id,"x")
        local py = player(id,"y")
        if (misc.isInsideScreen(magic.x,magic.y,px,py)) then
            if (misc.isLineOfSight(magic.x,magic.y,px,py)) then
                magic.targets[#magic.targets+1] = {id,misc.primitive_point_distance(px,py,magic.x,magic.y)}
            end
        end
    end
end,function(magic,otherMagic) --hit magic

end,function(magic) --update
    magic.reload = magic.reload - 1
    local closest_dist = 640
    local closest_id = 0
    for i=1,#magic.targets do
        local pl = magic.targets[i]
        local id = pl[1]
        local dist = pl[2]
        if (player(id,"exists")) then
            if (dist < closest_dist) then
                closest_dist = dist
                closest_id = id
            end
        end
    end
    --
    if (closest_id > 0) then
        local dir = misc.point_direction(magic.x,magic.y,player(closest_id,"x"),player(closest_id,"y"))
        tween_rotate(magic.image,100,dir+90)
        -- shoot
        if (magic.reload <= 0) then
            magic.reload = 5
            mw2.spawnProjectile(mw2.name.ElectricBolt,magic.x,magic.y,magic.owner,dir)
        end
    end
    --
    magic.targets = {}
end,function(magic) -- despawn

end)

mw2.name.healBoltEye = mw2.addProjectile("Heal Bolt Eye",10,4,90,0,320,250,"eye/heal",function(magic)--spawn
    magic.targets = {}
    magic.reload = 0
end,function(magic) --hit wall
	magic.dispose = true
end,function(magic,id) --hit player
    if (not(magic.owner == id)) then
        local px = player(id,"x")
        local py = player(id,"y")
        if (misc.isInsideScreen(magic.x,magic.y,px,py)) then
            if (misc.isLineOfSight(magic.x,magic.y,px,py)) then
                magic.targets[#magic.targets+1] = {id,misc.primitive_point_distance(px,py,magic.x,magic.y)}
            end
        end
    end
end,function(magic,otherMagic) --hit magic

end,function(magic) --update
    magic.reload = magic.reload - 1
    local closest_dist = 640
    local closest_id = 0
    for i=1,#magic.targets do
        local pl = magic.targets[i]
        local id = pl[1]
        local dist = pl[2]
        if (player(id,"exists")) then
            if (dist < closest_dist) then
                closest_dist = dist
                closest_id = id
            end
        end
    end
    --
    if (closest_id > 0) then
        local dir = misc.point_direction(magic.x,magic.y,player(closest_id,"x"),player(closest_id,"y"))
        tween_rotate(magic.image,100,dir+90)
        -- shoot
        if (magic.reload <= 0) then
            magic.reload = 5
            mw2.spawnProjectile(mw2.name.HealBolt,magic.x,magic.y,magic.owner,dir)
        end
    end
    --
    magic.targets = {}
end,function(magic) -- despawn

end)

mw2.name.pureChargeEye = mw2.addProjectile("Pure Charge Eye",10,4,90,0,320,250,"eye/pure",function(magic)--spawn
    magic.targets = {}
    magic.reload = 0
end,function(magic) --hit wall
	magic.dispose = true
end,function(magic,id) --hit player
    if (not(magic.owner == id)) then
        local px = player(id,"x")
        local py = player(id,"y")
        if (misc.isInsideScreen(magic.x,magic.y,px,py)) then
            if (misc.isLineOfSight(magic.x,magic.y,px,py)) then
                magic.targets[#magic.targets+1] = {id,misc.primitive_point_distance(px,py,magic.x,magic.y)}
            end
        end
    end
end,function(magic,otherMagic) --hit magic

end,function(magic) --update
    magic.reload = magic.reload - 1
    local closest_dist = 640
    local closest_id = 0
    for i=1,#magic.targets do
        local pl = magic.targets[i]
        local id = pl[1]
        local dist = pl[2]
        if (player(id,"exists")) then
            if (dist < closest_dist) then
                closest_dist = dist
                closest_id = id
            end
        end
    end
    --
    if (closest_id > 0) then
        local dir = misc.point_direction(magic.x,magic.y,player(closest_id,"x"),player(closest_id,"y"))
        tween_rotate(magic.image,100,dir+90)
        -- shoot
        if (magic.reload <= 0) then
            magic.reload = 10
            mw2.spawnProjectile(mw2.name.PureCharge,magic.x,magic.y,magic.owner,dir)
        end
    end
    --
    magic.targets = {}
end,function(magic) -- despawn

end)

mw2.name.fireChargeEye = mw2.addProjectile("Fire Charge Eye",10,4,90,0,320,250,"eye/fire",function(magic)--spawn
    magic.targets = {}
    magic.reload = 0
end,function(magic) --hit wall
	magic.dispose = true
end,function(magic,id) --hit player
    if (not(magic.owner == id)) then
        local px = player(id,"x")
        local py = player(id,"y")
        if (misc.isInsideScreen(magic.x,magic.y,px,py)) then
            if (misc.isLineOfSight(magic.x,magic.y,px,py)) then
                magic.targets[#magic.targets+1] = {id,misc.primitive_point_distance(px,py,magic.x,magic.y)}
            end
        end
    end
end,function(magic,otherMagic) --hit magic

end,function(magic) --update
    magic.reload = magic.reload - 1
    local closest_dist = 640
    local closest_id = 0
    for i=1,#magic.targets do
        local pl = magic.targets[i]
        local id = pl[1]
        local dist = pl[2]
        if (player(id,"exists")) then
            if (dist < closest_dist) then
                closest_dist = dist
                closest_id = id
            end
        end
    end
    --
    if (closest_id > 0) then
        local dir = misc.point_direction(magic.x,magic.y,player(closest_id,"x"),player(closest_id,"y"))
        tween_rotate(magic.image,100,dir+90)
        -- shoot
        if (magic.reload <= 0) then
            magic.reload = 10
            mw2.spawnProjectile(mw2.name.FireCharge,magic.x,magic.y,magic.owner,dir)
        end
    end
    --
    magic.targets = {}
end,function(magic) -- despawn

end)

mw2.name.shockChargeEye = mw2.addProjectile("Shock Charge Eye",10,4,90,0,320,250,"eye/electric",function(magic)--spawn
    magic.targets = {}
    magic.reload = 0
end,function(magic) --hit wall
	magic.dispose = true
end,function(magic,id) --hit player
    if (not(magic.owner == id)) then
        local px = player(id,"x")
        local py = player(id,"y")
        if (misc.isInsideScreen(magic.x,magic.y,px,py)) then
            if (misc.isLineOfSight(magic.x,magic.y,px,py)) then
                magic.targets[#magic.targets+1] = {id,misc.primitive_point_distance(px,py,magic.x,magic.y)}
            end
        end
    end
end,function(magic,otherMagic) --hit magic

end,function(magic) --update
    magic.reload = magic.reload - 1
    local closest_dist = 640
    local closest_id = 0
    for i=1,#magic.targets do
        local pl = magic.targets[i]
        local id = pl[1]
        local dist = pl[2]
        if (player(id,"exists")) then
            if (dist < closest_dist) then
                closest_dist = dist
                closest_id = id
            end
        end
    end
    --
    if (closest_id > 0) then
        local dir = misc.point_direction(magic.x,magic.y,player(closest_id,"x"),player(closest_id,"y"))
        tween_rotate(magic.image,100,dir+90)
        -- shoot
        if (magic.reload <= 0) then
            magic.reload = 10
            mw2.spawnProjectile(mw2.name.ShockCharge,magic.x,magic.y,magic.owner,dir)
        end
    end
    --
    magic.targets = {}
end,function(magic) -- despawn

end)

mw2.name.iceChargeEye = mw2.addProjectile("Ice Charge Eye",10,4,90,0,320,250,"eye/frost",function(magic)--spawn
    magic.targets = {}
    magic.reload = 0
end,function(magic) --hit wall
	magic.dispose = true
end,function(magic,id) --hit player
    if (not(magic.owner == id)) then
        local px = player(id,"x")
        local py = player(id,"y")
        if (misc.isInsideScreen(magic.x,magic.y,px,py)) then
            if (misc.isLineOfSight(magic.x,magic.y,px,py)) then
                magic.targets[#magic.targets+1] = {id,misc.primitive_point_distance(px,py,magic.x,magic.y)}
            end
        end
    end
end,function(magic,otherMagic) --hit magic

end,function(magic) --update
    magic.reload = magic.reload - 1
    local closest_dist = 640
    local closest_id = 0
    for i=1,#magic.targets do
        local pl = magic.targets[i]
        local id = pl[1]
        local dist = pl[2]
        if (player(id,"exists")) then
            if (dist < closest_dist) then
                closest_dist = dist
                closest_id = id
            end
        end
    end
    --
    if (closest_id > 0) then
        local dir = misc.point_direction(magic.x,magic.y,player(closest_id,"x"),player(closest_id,"y"))
        tween_rotate(magic.image,100,dir+90)
        -- shoot
        if (magic.reload <= 0) then
            magic.reload = 10
            mw2.spawnProjectile(mw2.name.IceCharge,magic.x,magic.y,magic.owner,dir)
        end
    end
    --
    magic.targets = {}
end,function(magic) -- despawn

end)

mw2.name.taintChargeEye = mw2.addProjectile("Taint Charge Eye",10,4,90,0,320,250,"eye/taint",function(magic)--spawn
    magic.targets = {}
    magic.reload = 0
end,function(magic) --hit wall
	magic.dispose = true
end,function(magic,id) --hit player
    if (not(magic.owner == id)) then
        local px = player(id,"x")
        local py = player(id,"y")
        if (misc.isInsideScreen(magic.x,magic.y,px,py)) then
            if (misc.isLineOfSight(magic.x,magic.y,px,py)) then
                magic.targets[#magic.targets+1] = {id,misc.primitive_point_distance(px,py,magic.x,magic.y)}
            end
        end
    end
end,function(magic,otherMagic) --hit magic

end,function(magic) --update
    magic.reload = magic.reload - 1
    local closest_dist = 640
    local closest_id = 0
    for i=1,#magic.targets do
        local pl = magic.targets[i]
        local id = pl[1]
        local dist = pl[2]
        if (player(id,"exists")) then
            if (dist < closest_dist) then
                closest_dist = dist
                closest_id = id
            end
        end
    end
    --
    if (closest_id > 0) then
        local dir = misc.point_direction(magic.x,magic.y,player(closest_id,"x"),player(closest_id,"y"))
        tween_rotate(magic.image,100,dir+90)
        -- shoot
        if (magic.reload <= 0) then
            magic.reload = 10
            mw2.spawnProjectile(mw2.name.TaintCharge,magic.x,magic.y,magic.owner,dir)
        end
    end
    --
    magic.targets = {}
end,function(magic) -- despawn

end)

mw2.name.fireBallEye = mw2.addProjectile("Fire Ball Eye",10,4,90,0,320,250,"eye/fire",function(magic)--spawn
    magic.targets = {}
    magic.reload = 0
end,function(magic) --hit wall
	magic.dispose = true
end,function(magic,id) --hit player
    if (not(magic.owner == id)) then
        local px = player(id,"x")
        local py = player(id,"y")
        --if (misc.isInsideScreen(magic.x,magic.y,px,py)) then
            if (misc.isLineOfSight(magic.x,magic.y,px,py)) then
                magic.targets[#magic.targets+1] = {id,misc.primitive_point_distance(px,py,magic.x,magic.y)}
            end
        --end
    end
end,function(magic,otherMagic) --hit magic

end,function(magic) --update
    magic.reload = magic.reload - 1
    local closest_dist = 640
    local closest_id = 0
    for i=1,#magic.targets do
        local pl = magic.targets[i]
        local id = pl[1]
        local dist = pl[2]
        if (player(id,"exists")) then
            if (dist < closest_dist) then
                closest_dist = dist
                closest_id = id
            end
        end
    end
    --
    if (closest_id > 0) then
        local dir = misc.point_direction(magic.x,magic.y,player(closest_id,"x"),player(closest_id,"y"))
        tween_rotate(magic.image,100,dir+90)
        -- shoot
        if (magic.reload <= 0) then
            magic.reload = 15
            mw2.spawnProjectile(mw2.name.FireBall,magic.x,magic.y,magic.owner,dir)
        end
    end
    --
    magic.targets = {}
end,function(magic) -- despawn

end)

mw2.name.shockBallEye = mw2.addProjectile("Shock Ball Eye",10,4,90,0,320,250,"eye/electric",function(magic)--spawn
    magic.targets = {}
    magic.reload = 0
end,function(magic) --hit wall
	magic.dispose = true
end,function(magic,id) --hit player
    if (not(magic.owner == id)) then
        local px = player(id,"x")
        local py = player(id,"y")
        --if (misc.isInsideScreen(magic.x,magic.y,px,py)) then
            if (misc.isLineOfSight(magic.x,magic.y,px,py)) then
                magic.targets[#magic.targets+1] = {id,misc.primitive_point_distance(px,py,magic.x,magic.y)}
            end
        --end
    end
end,function(magic,otherMagic) --hit magic

end,function(magic) --update
    magic.reload = magic.reload - 1
    local closest_dist = 640
    local closest_id = 0
    for i=1,#magic.targets do
        local pl = magic.targets[i]
        local id = pl[1]
        local dist = pl[2]
        if (player(id,"exists")) then
            if (dist < closest_dist) then
                closest_dist = dist
                closest_id = id
            end
        end
    end
    --
    if (closest_id > 0) then
        local dir = misc.point_direction(magic.x,magic.y,player(closest_id,"x"),player(closest_id,"y"))
        tween_rotate(magic.image,100,dir+90)
        -- shoot
        if (magic.reload <= 0) then
            magic.reload = 15
            mw2.spawnProjectile(mw2.name.ShockBall,magic.x,magic.y,magic.owner,dir)
        end
    end
    --
    magic.targets = {}
end,function(magic) -- despawn

end)

mw2.name.iceBallEye = mw2.addProjectile("Ice Ball Eye",10,4,90,0,320,250,"eye/frost",function(magic)--spawn
    magic.targets = {}
    magic.reload = 0
end,function(magic) --hit wall
	magic.dispose = true
end,function(magic,id) --hit player
    if (not(magic.owner == id)) then
        local px = player(id,"x")
        local py = player(id,"y")
        --if (misc.isInsideScreen(magic.x,magic.y,px,py)) then
            if (misc.isLineOfSight(magic.x,magic.y,px,py)) then
                magic.targets[#magic.targets+1] = {id,misc.primitive_point_distance(px,py,magic.x,magic.y)}
            end
        --end
    end
end,function(magic,otherMagic) --hit magic

end,function(magic) --update
    magic.reload = magic.reload - 1
    local closest_dist = 640
    local closest_id = 0
    for i=1,#magic.targets do
        local pl = magic.targets[i]
        local id = pl[1]
        local dist = pl[2]
        if (player(id,"exists")) then
            if (dist < closest_dist) then
                closest_dist = dist
                closest_id = id
            end
        end
    end
    --
    if (closest_id > 0) then
        local dir = misc.point_direction(magic.x,magic.y,player(closest_id,"x"),player(closest_id,"y"))
        tween_rotate(magic.image,100,dir+90)
        -- shoot
        if (magic.reload <= 0) then
            magic.reload = 15
            mw2.spawnProjectile(mw2.name.IceBall,magic.x,magic.y,magic.owner,dir)
        end
    end
    --
    magic.targets = {}
end,function(magic) -- despawn

end)

mw2.name.taintBallEye = mw2.addProjectile("Taint Ball Eye",10,4,90,0,320,250,"eye/taint",function(magic)--spawn
    magic.targets = {}
    magic.reload = 0
end,function(magic) --hit wall
	magic.dispose = true
end,function(magic,id) --hit player
    if (not(magic.owner == id)) then
        local px = player(id,"x")
        local py = player(id,"y")
        --if (misc.isInsideScreen(magic.x,magic.y,px,py)) then
            if (misc.isLineOfSight(magic.x,magic.y,px,py)) then
                magic.targets[#magic.targets+1] = {id,misc.primitive_point_distance(px,py,magic.x,magic.y)}
            end
        --end
    end
end,function(magic,otherMagic) --hit magic

end,function(magic) --update
    magic.reload = magic.reload - 1
    local closest_dist = 640
    local closest_id = 0
    for i=1,#magic.targets do
        local pl = magic.targets[i]
        local id = pl[1]
        local dist = pl[2]
        if (player(id,"exists")) then
            if (dist < closest_dist) then
                closest_dist = dist
                closest_id = id
            end
        end
    end
    --
    if (closest_id > 0) then
        local dir = misc.point_direction(magic.x,magic.y,player(closest_id,"x"),player(closest_id,"y"))
        tween_rotate(magic.image,100,dir+90)
        -- shoot
        if (magic.reload <= 0) then
            magic.reload = 15
            mw2.spawnProjectile(mw2.name.TaintBall,magic.x,magic.y,magic.owner,dir)
        end
    end
    --
    magic.targets = {}
end,function(magic) -- despawn

end)

---------------
--[ATTRACTOR]--
---------------

mw2.name.weakAttractorShield = mw2.addProjectile("Weak Attracor Shield",20,1,false,0,2.5*64,100,"attractor/weak",function(magic)--spawn
    imagescale(magic.image,0,0)
    tween_scale(magic.image,500,5,5)
    tween_alpha(magic.image,magic.me.lifetime*100,0)
end,function(magic) --hit wall
	
end,function(magic,id) --hit player
    local px = player(id,"x")
    local py = player(id,"y")
    local size = (2.5*64)
    local dist = misc.point_distance(magic.x,magic.y,px,py)
    if (dist < size) then
        local percent = (100/size)*dist
        local cut_percent = math.max(0,(100-((100-percent)*2)))
        local power = ((dist/100)*cut_percent)*0.5
        --power = (power/100)*cut_percent
        power = (power/magic.me.magicHealth)*magic.magicHealth
        power = (power/magic.me.lifetime)*magic.lifetime
        --msg("power: "..power)
        local dir = misc.point_direction(px,py,magic.x,magic.y)
        local x = misc.lengthdir_x(dir,power)
        local y = misc.lengthdir_y(dir,power)
        cs2d.setpos.call(id,px+x,py+y)
    end
end,function(magic,otherMagic) --hit magic
    
end,function(magic) --update
    
end,function(magic) -- despawn

end)

mw2.name.mediumAttractorShield = mw2.addProjectile("Medium Attracor Shield",40,1,false,0,2.5*64,200,"attractor/weak",function(magic)--spawn
    imagescale(magic.image,0,0)
    tween_scale(magic.image,500,5,5)
    tween_alpha(magic.image,magic.me.lifetime*100,0)
end,function(magic) --hit wall
	
end,function(magic,id) --hit player
    local px = player(id,"x")
    local py = player(id,"y")
    local size = (2.5*64)
    local dist = misc.point_distance(magic.x,magic.y,px,py)
    if (dist < size) then
        local percent = (100/size)*dist
        local cut_percent = math.max(0,(100-((100-percent)*2)))
        local power = ((dist/100)*cut_percent)*0.5
        --power = (power/100)*cut_percent
        power = (power/magic.me.magicHealth)*magic.magicHealth
        power = (power/magic.me.lifetime)*magic.lifetime
        --msg("power: "..power)
        local dir = misc.point_direction(px,py,magic.x,magic.y)
        local x = misc.lengthdir_x(dir,power)
        local y = misc.lengthdir_y(dir,power)
        cs2d.setpos.call(id,px+x,py+y)
    end
end,function(magic,otherMagic) --hit magic
    
end,function(magic) --update
    
end,function(magic) -- despawn

end)

mw2.name.strongAttractorShield = mw2.addProjectile("Strong Attracor Shield",60,1,false,0,2.5*64,400,"attractor/weak",function(magic)--spawn
    imagescale(magic.image,0,0)
    tween_scale(magic.image,500,5,5)
    tween_alpha(magic.image,magic.me.lifetime*100,0)
end,function(magic) --hit wall
	
end,function(magic,id) --hit player
    local px = player(id,"x")
    local py = player(id,"y")
    local size = (2.5*64)
    local dist = misc.point_distance(magic.x,magic.y,px,py)
    if (dist < size) then
        local percent = (100/size)*dist
        local cut_percent = math.max(0,(100-((100-percent)*2)))
        local power = ((dist/100)*cut_percent)*0.5
        --power = (power/100)*cut_percent
        power = (power/magic.me.magicHealth)*magic.magicHealth
        power = (power/magic.me.lifetime)*magic.lifetime
        --msg("power: "..power)
        local dir = misc.point_direction(px,py,magic.x,magic.y)
        local x = misc.lengthdir_x(dir,power)
        local y = misc.lengthdir_y(dir,power)
        cs2d.setpos.call(id,px+x,py+y)
    end
end,function(magic,otherMagic) --hit magic
    
end,function(magic) --update
    
end,function(magic) -- despawn

end)

---------------
--[PATH WARD]--
---------------

mw2.name.WeakPathWard = mw2.addProjectile("Weak Path Ward",32,2,-90,0,64,128,"ward/ward",function(magic)--spawn
    imagescale(magic.image,1/3*2,1/3*2)
	tween_alpha(magic.image,magic.me.lifetime*100,0)
end,function(magic) --hit wall
	magic.dispose = true
end,function(magic,id) --hit player
	
end,function(magic,otherMagic) --hit magic
    local magicDamage = math.min(magic.magicHealth,otherMagic.magicHealth)
    magic.magicHealth = magic.magicHealth - magicDamage
    if (magic.magicHealth <= 0) then
        magic.dispose = true
    end
    otherMagic.magicHealth = otherMagic.magicHealth - magicDamage
    if (otherMagic.magicHealth <= 0) then
        otherMagic.dispose = true
    end
	local alpha = (1/(magic.me.lifetime*magic.me.magicHealth))*(magic.lifetime*magic.magicHealth)
    imagealpha(magic.image,alpha)
    tween_alpha(magic.image,magic.lifetime*100,0)
end,function(magic) --update
    local x = player(magic.owner,"x")
    local y = player(magic.owner,"y")
    local dir = player(magic.owner,"rot")
    magic.x = x+misc.lengthdir_x(dir,64)
    magic.y = y+misc.lengthdir_y(dir,64)
    tween_move(magic.image,100,magic.x,magic.y,dir-90)
end,function(magic) -- despawn

end)

mw2.name.MediumPathWard = mw2.addProjectile("Medium Path Ward",64,2,-90,0,64,256,"ward/ward",function(magic)--spawn
    imagescale(magic.image,1/3*2,1/3*2)
    tween_alpha(magic.image,magic.me.lifetime*100,0)
end,function(magic) --hit wall
	magic.dispose = true
end,function(magic,id) --hit player
	
end,function(magic,otherMagic) --hit magic
    local magicDamage = math.min(magic.magicHealth,otherMagic.magicHealth)
    magic.magicHealth = magic.magicHealth - magicDamage
    if (magic.magicHealth <= 0) then
        magic.dispose = true
    end
    otherMagic.magicHealth = otherMagic.magicHealth - magicDamage
    if (otherMagic.magicHealth <= 0) then
        otherMagic.dispose = true
    end
	local alpha = (1/(magic.me.lifetime*magic.me.magicHealth))*(magic.lifetime*magic.magicHealth)
    imagealpha(magic.image,alpha)
    tween_alpha(magic.image,magic.lifetime*100,0)
end,function(magic) --update
    local x = player(magic.owner,"x")
    local y = player(magic.owner,"y")
    local dir = player(magic.owner,"rot")
    magic.x = x+misc.lengthdir_x(dir,64)
    magic.y = y+misc.lengthdir_y(dir,64)
    tween_move(magic.image,100,magic.x,magic.y,dir-90)
end,function(magic) -- despawn

end)

mw2.name.StrongPathWard = mw2.addProjectile("Strong Path Ward",128,2,-90,0,64,512,"ward/ward",function(magic)--spawn
	imagescale(magic.image,1/3*2,1/3*2)
    tween_alpha(magic.image,magic.me.lifetime*100,0)
end,function(magic) --hit wall
	magic.dispose = true
end,function(magic,id) --hit player
	
end,function(magic,otherMagic) --hit magic
    local magicDamage = math.min(magic.magicHealth,otherMagic.magicHealth)
    magic.magicHealth = magic.magicHealth - magicDamage
    if (magic.magicHealth <= 0) then
        magic.dispose = true
    end
    otherMagic.magicHealth = otherMagic.magicHealth - magicDamage
    if (otherMagic.magicHealth <= 0) then
        otherMagic.dispose = true
    end
	local alpha = (1/(magic.me.lifetime*magic.me.magicHealth))*(magic.lifetime*magic.magicHealth)
    imagealpha(magic.image,alpha)
    tween_alpha(magic.image,magic.lifetime*100,0)
end,function(magic) --update
    local x = player(magic.owner,"x")
    local y = player(magic.owner,"y")
    local dir = player(magic.owner,"rot")
    magic.x = x+misc.lengthdir_x(dir,64)
    magic.y = y+misc.lengthdir_y(dir,64)
    tween_move(magic.image,100,magic.x,magic.y,dir-90)
end,function(magic) -- despawn

end)

-----------
--[FLIES]--
-----------

mw2.name.FireFly = mw2.addProjectile("Fire Fly",2,1,-90,25,16 ,32*5,"flies/fire",function(magic)--spawn
    tween_rotateconstantly(magic.image,5)
end,function(magic) --hit wall
	mw2.magicBack(magic)
	mw2.explosion(magic.x,magic.y,64,50,magic.owner)
	magic.dispose = true
end,function(magic,id) --hit player
	mw2.magicForward(magic)
	mw2.explosion(magic.x,magic.y,64,50,magic.owner)
	magic.dispose = true
end,function(magic,otherMagic) --hit magic

end,function(magic) --update
    local owner_findable = false
    local best_target = 0
    local best_targt_dir = 180
    local pl = player(0,"tableliving")
    for i=1,#pl do
        local id = pl[i]
        local x = player(id,"x")
        local y = player(id,"y")
        if misc.isInsideScreen(magic.x,magic.y,x,y) then
            if misc.isLineOfSight(magic.x,magic.y,x,y) then
                if (id == magic.owner) then
                    owner_findable = true
                else
                    local direction = misc.point_direction(magic.x,magic.y,x,y)
                    local dir_diff = misc.angleDiffrence(direction,magic.dir)
                    if (math.abs(dir_diff) < math.abs(best_targt_dir)) then
                        best_targt_dir = dir_diff
                        best_target  = id
                    end
                end
            end
        end
    end
    if (best_targt_dir < 180) then
        if (math.abs(best_targt_dir) > 20) then -- othrewise lets not waste network
            local x = player(best_target,"x")
            local y = player(best_target,"y")
            local direction = misc.point_direction(magic.x,magic.y,x,y)
            local distance = misc.point_distance(magic.x,magic.y,x,y)
            magic.dir = magic.dir + misc.maxmin(best_targt_dir,20)
            magic.xmove = misc.lengthdir_x(magic.dir,magic.me.speed)
            magic.ymove = misc.lengthdir_y(magic.dir,magic.me.speed)
            tween_move(magic.image,100*magic.lifetime,magic.x+(magic.xmove*magic.lifetime),magic.y+(magic.ymove*magic.lifetime))
        end
    else
        owner_findable = false
        if (owner_findable) then
            local x = player(magic.owner,"x")
            local y = player(magic.owner,"y")
            --local distance = misc.point_distance(magic.x,magic.y,x,y)
            local dir = misc.point_direction(magic.x,magic.y,x,y)
            if (math.abs(misc.angleDiffrence(magic.dir, dir)) > 40) then
                magic.dir = misc.turnTowards(magic.dir, dir, 20)
                magic.xmove = misc.lengthdir_x(magic.dir,magic.me.speed)
                magic.ymove = misc.lengthdir_y(magic.dir,magic.me.speed)
                tween_move(magic.image,100*magic.lifetime,magic.x+(magic.xmove*magic.lifetime),magic.y+(magic.ymove*magic.lifetime))
            end
        end
    end
end,function(magic) -- despawn

end)

mw2.name.IceFly = mw2.addProjectile("Ice Fly",2,1,-90,25,16 ,32*5,"flies/ice",function(magic)--spawn
    tween_rotateconstantly(magic.image,5)
end,function(magic) --hit wall
	mw2.frezze(magic.x,magic.y,16)
	magic.dispose = true
end,function(magic,id) --hit player
	mw2.magicForward(magic)
	mw2.frezze(magic.x,magic.y,16)
	mw2.damagePlayer(magic.owner,id,20,magic.me.name)
	magic.dispose = true
end,function(magic,otherMagic) --hit magic

end,function(magic) --update
    local owner_findable = false
    local best_target = 0
    local best_targt_dir = 180
    local pl = player(0,"tableliving")
    for i=1,#pl do
        local id = pl[i]
        local x = player(id,"x")
        local y = player(id,"y")
        if misc.isInsideScreen(magic.x,magic.y,x,y) then
            if misc.isLineOfSight(magic.x,magic.y,x,y) then
                if (id == magic.owner) then
                    owner_findable = true
                else
                    local direction = misc.point_direction(magic.x,magic.y,x,y)
                    local dir_diff = misc.angleDiffrence(direction,magic.dir)
                    if (math.abs(dir_diff) < math.abs(best_targt_dir)) then
                        best_targt_dir = dir_diff
                        best_target  = id
                    end
                end
            end
        end
    end
    if (best_targt_dir < 180) then
        if (math.abs(best_targt_dir) > 20) then -- othrewise lets not waste network
            local x = player(best_target,"x")
            local y = player(best_target,"y")
            local direction = misc.point_direction(magic.x,magic.y,x,y)
            local distance = misc.point_distance(magic.x,magic.y,x,y)
            magic.dir = magic.dir + misc.maxmin(best_targt_dir,20)
            magic.xmove = misc.lengthdir_x(magic.dir,magic.me.speed)
            magic.ymove = misc.lengthdir_y(magic.dir,magic.me.speed)
            tween_move(magic.image,100*magic.lifetime,magic.x+(magic.xmove*magic.lifetime),magic.y+(magic.ymove*magic.lifetime))
        end
    else
        owner_findable = false
        if (owner_findable) then
            local x = player(magic.owner,"x")
            local y = player(magic.owner,"y")
            --local distance = misc.point_distance(magic.x,magic.y,x,y)
            local dir = misc.point_direction(magic.x,magic.y,x,y)
            if (math.abs(misc.angleDiffrence(magic.dir, dir)) > 40) then
                magic.dir = misc.turnTowards(magic.dir, dir, 20)
                magic.xmove = misc.lengthdir_x(magic.dir,magic.me.speed)
                magic.ymove = misc.lengthdir_y(magic.dir,magic.me.speed)
                tween_move(magic.image,100*magic.lifetime,magic.x+(magic.xmove*magic.lifetime),magic.y+(magic.ymove*magic.lifetime))
            end
        end
    end
end,function(magic) -- despawn

end)

mw2.name.ElectricFly = mw2.addProjectile("Electric Fly",2,1,-90,25,16 ,32*5,"flies/shock",function(magic)--spawn
    tween_rotateconstantly(magic.image,5)
end,function(magic) --hit wall
	mw2.magicBack(magic)
	mw2.explosion(magic.x,magic.y,50,32,magic.owner)
	parse("flashposition "..magic.x.." "..magic.y.." 25")
	magic.dispose = true
end,function(magic,id) --hit player
	mw2.magicForward(magic)
    mw2.explosion(magic.x,magic.y,50,32,magic.owner)
	parse("flashplayer "..id.." 25")
	magic.dispose = true
end,function(magic,otherMagic) --hit magic

end,function(magic) --update
    local owner_findable = false
    local best_target = 0
    local best_targt_dir = 180
    local pl = player(0,"tableliving")
    for i=1,#pl do
        local id = pl[i]
        local x = player(id,"x")
        local y = player(id,"y")
        if misc.isInsideScreen(magic.x,magic.y,x,y) then
            if misc.isLineOfSight(magic.x,magic.y,x,y) then
                if (id == magic.owner) then
                    owner_findable = true
                else
                    local direction = misc.point_direction(magic.x,magic.y,x,y)
                    local dir_diff = misc.angleDiffrence(direction,magic.dir)
                    if (math.abs(dir_diff) < math.abs(best_targt_dir)) then
                        best_targt_dir = dir_diff
                        best_target  = id
                    end
                end
            end
        end
    end
    if (best_targt_dir < 180) then
        if (math.abs(best_targt_dir) > 20) then -- othrewise lets not waste network
            local x = player(best_target,"x")
            local y = player(best_target,"y")
            local direction = misc.point_direction(magic.x,magic.y,x,y)
            local distance = misc.point_distance(magic.x,magic.y,x,y)
            magic.dir = magic.dir + misc.maxmin(best_targt_dir,20)
            magic.xmove = misc.lengthdir_x(magic.dir,magic.me.speed)
            magic.ymove = misc.lengthdir_y(magic.dir,magic.me.speed)
            tween_move(magic.image,100*magic.lifetime,magic.x+(magic.xmove*magic.lifetime),magic.y+(magic.ymove*magic.lifetime))
        end
    else
        owner_findable = false
        if (owner_findable) then
            local x = player(magic.owner,"x")
            local y = player(magic.owner,"y")
            --local distance = misc.point_distance(magic.x,magic.y,x,y)
            local dir = misc.point_direction(magic.x,magic.y,x,y)
            if (math.abs(misc.angleDiffrence(magic.dir, dir)) > 40) then
                magic.dir = misc.turnTowards(magic.dir, dir, 20)
                magic.xmove = misc.lengthdir_x(magic.dir,magic.me.speed)
                magic.ymove = misc.lengthdir_y(magic.dir,magic.me.speed)
                tween_move(magic.image,100*magic.lifetime,magic.x+(magic.xmove*magic.lifetime),magic.y+(magic.ymove*magic.lifetime))
            end
        end
    end
end,function(magic) -- despawn

end)

mw2.name.TaintFly = mw2.addProjectile("Tainted Fly",3,1,-90,25,16 ,32*5,"flies/taint",function(magic)--spawn
    tween_rotateconstantly(magic.image,5)
end,function(magic) --hit wall
	magic.dispose = true
end,function(magic,id) --hit player
    mw2.damagePlayer(magic.owner,id,25,magic.me.name)
    mw2.damageMaxHealth(magic.owner,id,20,"TaintFly")
	magic.dispose = true
end,function(magic,otherMagic) --hit magic

end,function(magic) --update
    local owner_findable = false
    local best_target = 0
    local best_targt_dir = 180
    local pl = player(0,"tableliving")
    for i=1,#pl do
        local id = pl[i]
        local x = player(id,"x")
        local y = player(id,"y")
        if misc.isInsideScreen(magic.x,magic.y,x,y) then
            if misc.isLineOfSight(magic.x,magic.y,x,y) then
                if (id == magic.owner) then
                    owner_findable = true
                else
                    local direction = misc.point_direction(magic.x,magic.y,x,y)
                    local dir_diff = misc.angleDiffrence(direction,magic.dir)
                    if (math.abs(dir_diff) < math.abs(best_targt_dir)) then
                        best_targt_dir = dir_diff
                        best_target  = id
                    end
                end
            end
        end
    end
    if (best_targt_dir < 180) then
        if (math.abs(best_targt_dir) > 20) then -- othrewise lets not waste network
            local x = player(best_target,"x")
            local y = player(best_target,"y")
            local direction = misc.point_direction(magic.x,magic.y,x,y)
            local distance = misc.point_distance(magic.x,magic.y,x,y)
            magic.dir = magic.dir + misc.maxmin(best_targt_dir,20)
            magic.xmove = misc.lengthdir_x(magic.dir,magic.me.speed)
            magic.ymove = misc.lengthdir_y(magic.dir,magic.me.speed)
            tween_move(magic.image,100*magic.lifetime,magic.x+(magic.xmove*magic.lifetime),magic.y+(magic.ymove*magic.lifetime))
        end
    else
        owner_findable = false
        if (owner_findable) then
            local x = player(magic.owner,"x")
            local y = player(magic.owner,"y")
            --local distance = misc.point_distance(magic.x,magic.y,x,y)
            local dir = misc.point_direction(magic.x,magic.y,x,y)
            if (math.abs(misc.angleDiffrence(magic.dir, dir)) > 40) then
                magic.dir = misc.turnTowards(magic.dir, dir, 20)
                magic.xmove = misc.lengthdir_x(magic.dir,magic.me.speed)
                magic.ymove = misc.lengthdir_y(magic.dir,magic.me.speed)
                tween_move(magic.image,100*magic.lifetime,magic.x+(magic.xmove*magic.lifetime),magic.y+(magic.ymove*magic.lifetime))
            end
        end
    end
end,function(magic) -- despawn

end)

--------------------
--[SELF AFFECTORS]--
--------------------

mw2.name.WeaponImmunity = mw2.addProjectile("Weapon Immunity",32,0,false,0,32,10*60*5,"self/weapon",function(magic)--spawn
    magic.flag = mw2flag.new(magic.owner,magic.owner,mw2flags.library.weaponImmunity,magic.lifetime/10)

    --if (mw2flags.hasFlag(magic.owner,mw2flags.library.weaponImmunity) == false) then
        freeimage(magic.image)
        magic.image = image("gfx/magic wizard 2/projectiles/"..magic.me.image..".png",0,0,200+magic.owner)
        imagealpha(magic.image,0.5)
        tween_scale(magic.image,100*magic.lifetime,0,0)
        tween_alpha(magic.image,100*magic.lifetime,0)   
    --end
end,function(magic) --hit wall

end,function(magic,id) --hit player
	
end,function(magic,otherMagic) --hit magic

end,function(magic) --update
    if (player(magic.owner,"health") <= 0) then
        magic.dispose = true
    end
end,function(magic) -- despawn
    if (not(magic.flag == nil)) then
        magic.flag:stop()
    end
end)

------------------------
--[PHASE 2: ADD MAGIC]-- -- mw2.addMagic(name,category,scrollsNeeded,manaUse,file,onUse)
------------------------

-----------
--[BOLTS]--
-----------

mw2.addMagic("Vampiric Bolt","Bolts",6,10,"wand_1.png",function(id)
	mw2.spawnProjectile(mw2.name.VampiricBolt,player(id,"x"),player(id,"y"),id,player(id,"rot"))
end)

-------------
--[CHARGES]--
-------------

mw2.addMagic("Vampiric Charge","Charge",11,15,"wand_2.png",function(id)
	mw2.spawnProjectile(mw2.name.VampiricCharge,player(id,"x"),player(id,"y"),id,player(id,"rot"))
end)

-----------------
--[PROJECTILES]--
-----------------

mw2.addMagic("Vampiric Ball","Projectile",15,20,"wand_3.png",function(id)
	mw2.spawnProjectile(mw2.name.VampiricBall,player(id,"x"),player(id,"y"),id,player(id,"rot"))
end)

-----------
--[STARS]--
-----------

mw2.addMagic("Taint Star","Star",30,90,"wand_7.png",function(id)
	mw2.spawnProjectile(mw2.name.TaintStar,player(id,"x"),player(id,"y"),id,player(id,"rot"))
end)

-----------
--[BUILD]--
-----------

mw2.addMagic("Wall 1 Barrier","Build",3,25,"wand_11.png",function(id)
    local x = misc.tile_to_pixel(player(id,"tilex"))
    local y = misc.tile_to_pixel(player(id,"tiley"))
	cs2d.setpos.call(id,x,y)
    -- spawn barrier
    local buildingType = 3
    parse("spawnobject "..buildingType.." "..(misc.pixel_to_tile(x)+1).." "..(misc.pixel_to_tile(y)+1).." 0 0 "..player(id,"team").." "..id)
    parse("spawnobject "..buildingType.." "..(misc.pixel_to_tile(x)+1).." "..misc.pixel_to_tile(y).." 0 0 "..player(id,"team").." "..id)
    parse("spawnobject "..buildingType.." "..(misc.pixel_to_tile(x)+1).." "..(misc.pixel_to_tile(y)-1).." 0 0 "..player(id,"team").." "..id)
    parse("spawnobject "..buildingType.." "..(misc.pixel_to_tile(x)-1).." "..(misc.pixel_to_tile(y)+1).." 0 0 "..player(id,"team").." "..id)
    parse("spawnobject "..buildingType.." "..(misc.pixel_to_tile(x)-1).." "..misc.pixel_to_tile(y).." 0 0 "..player(id,"team").." "..id)
    parse("spawnobject "..buildingType.." "..(misc.pixel_to_tile(x)-1).." "..(misc.pixel_to_tile(y)-1).." 0 0 "..player(id,"team").." "..id)
    parse("spawnobject "..buildingType.." "..misc.pixel_to_tile(x).." "..(misc.pixel_to_tile(y)+1).." 0 0 "..player(id,"team").." "..id)
    parse("spawnobject "..buildingType.." "..misc.pixel_to_tile(x).." "..(misc.pixel_to_tile(y)-1).." 0 0 "..player(id,"team").." "..id)
end)

mw2.addMagic("Wall 2 Barrier","Build",6,30,"wand_11.png",function(id)
    local x = misc.tile_to_pixel(player(id,"tilex"))
    local y = misc.tile_to_pixel(player(id,"tiley"))
	cs2d.setpos.call(id,x,y)
    -- spawn barrier
    local buildingType = 4
    parse("spawnobject "..buildingType.." "..(misc.pixel_to_tile(x)+1).." "..(misc.pixel_to_tile(y)+1).." 0 0 "..player(id,"team").." "..id)
    parse("spawnobject "..buildingType.." "..(misc.pixel_to_tile(x)+1).." "..misc.pixel_to_tile(y).." 0 0 "..player(id,"team").." "..id)
    parse("spawnobject "..buildingType.." "..(misc.pixel_to_tile(x)+1).." "..(misc.pixel_to_tile(y)-1).." 0 0 "..player(id,"team").." "..id)
    parse("spawnobject "..buildingType.." "..(misc.pixel_to_tile(x)-1).." "..(misc.pixel_to_tile(y)+1).." 0 0 "..player(id,"team").." "..id)
    parse("spawnobject "..buildingType.." "..(misc.pixel_to_tile(x)-1).." "..misc.pixel_to_tile(y).." 0 0 "..player(id,"team").." "..id)
    parse("spawnobject "..buildingType.." "..(misc.pixel_to_tile(x)-1).." "..(misc.pixel_to_tile(y)-1).." 0 0 "..player(id,"team").." "..id)
    parse("spawnobject "..buildingType.." "..misc.pixel_to_tile(x).." "..(misc.pixel_to_tile(y)+1).." 0 0 "..player(id,"team").." "..id)
    parse("spawnobject "..buildingType.." "..misc.pixel_to_tile(x).." "..(misc.pixel_to_tile(y)-1).." 0 0 "..player(id,"team").." "..id)
end)

mw2.addMagic("Wall 3 Barrier","Build",9,35,"wand_11.png",function(id)
    local x = misc.tile_to_pixel(player(id,"tilex"))
    local y = misc.tile_to_pixel(player(id,"tiley"))
	cs2d.setpos.call(id,x,y)
    -- spawn barrier
    local buildingType = 5
    parse("spawnobject "..buildingType.." "..(misc.pixel_to_tile(x)+1).." "..(misc.pixel_to_tile(y)+1).." 0 0 "..player(id,"team").." "..id)
    parse("spawnobject "..buildingType.." "..(misc.pixel_to_tile(x)+1).." "..misc.pixel_to_tile(y).." 0 0 "..player(id,"team").." "..id)
    parse("spawnobject "..buildingType.." "..(misc.pixel_to_tile(x)+1).." "..(misc.pixel_to_tile(y)-1).." 0 0 "..player(id,"team").." "..id)
    parse("spawnobject "..buildingType.." "..(misc.pixel_to_tile(x)-1).." "..(misc.pixel_to_tile(y)+1).." 0 0 "..player(id,"team").." "..id)
    parse("spawnobject "..buildingType.." "..(misc.pixel_to_tile(x)-1).." "..misc.pixel_to_tile(y).." 0 0 "..player(id,"team").." "..id)
    parse("spawnobject "..buildingType.." "..(misc.pixel_to_tile(x)-1).." "..(misc.pixel_to_tile(y)-1).." 0 0 "..player(id,"team").." "..id)
    parse("spawnobject "..buildingType.." "..misc.pixel_to_tile(x).." "..(misc.pixel_to_tile(y)+1).." 0 0 "..player(id,"team").." "..id)
    parse("spawnobject "..buildingType.." "..misc.pixel_to_tile(x).." "..(misc.pixel_to_tile(y)-1).." 0 0 "..player(id,"team").." "..id)
end)

mw2.addMagic("Dispenser Barrier","Build",12,40,"wand_11.png",function(id)
    local x = misc.tile_to_pixel(player(id,"tilex"))
    local y = misc.tile_to_pixel(player(id,"tiley"))
	cs2d.setpos.call(id,x,y)
    -- spawn barrier
    local buildingType = 7
    parse("spawnobject "..buildingType.." "..(misc.pixel_to_tile(x)+1).." "..(misc.pixel_to_tile(y)+1).." 0 0 "..player(id,"team").." "..id)
    parse("spawnobject "..buildingType.." "..(misc.pixel_to_tile(x)+1).." "..misc.pixel_to_tile(y).." 0 0 "..player(id,"team").." "..id)
    parse("spawnobject "..buildingType.." "..(misc.pixel_to_tile(x)+1).." "..(misc.pixel_to_tile(y)-1).." 0 0 "..player(id,"team").." "..id)
    parse("spawnobject "..buildingType.." "..(misc.pixel_to_tile(x)-1).." "..(misc.pixel_to_tile(y)+1).." 0 0 "..player(id,"team").." "..id)
    parse("spawnobject "..buildingType.." "..(misc.pixel_to_tile(x)-1).." "..misc.pixel_to_tile(y).." 0 0 "..player(id,"team").." "..id)
    parse("spawnobject "..buildingType.." "..(misc.pixel_to_tile(x)-1).." "..(misc.pixel_to_tile(y)-1).." 0 0 "..player(id,"team").." "..id)
    parse("spawnobject "..buildingType.." "..misc.pixel_to_tile(x).." "..(misc.pixel_to_tile(y)+1).." 0 0 "..player(id,"team").." "..id)
    parse("spawnobject "..buildingType.." "..misc.pixel_to_tile(x).." "..(misc.pixel_to_tile(y)-1).." 0 0 "..player(id,"team").." "..id)
end)

--------------
--[TELEPORT]--
--------------

mw2.addMagic("Telekinesis","Teleport",10,10,"wand_10.png",function(id)
	mw2.spawnProjectile(mw2.name.Telekinesis,player(id,"x"),player(id,"y"),id,player(id,"rot"))
end)

-------------
--[SPECIAL]--
-------------

mw2.addMagic("Follower","Special",0,10,"wand_13.png",function(id)
    mw2.spawnProjectile(mw2.name.Follower,player(id,"x"),player(id,"y"),id,player(id,"rot"))
end)


mw2.addMagic("Strong Follower","Special",5,20,"wand_13.png",function(id)
    mw2.spawnProjectile(mw2.name.strongFollower,player(id,"x"),player(id,"y"),id,player(id,"rot"))
end)


---------------
--[SHOCKWAVE]--
---------------

mw2.addMagic("Heal Shockwave","Shockwave",12,20,"wand_18.png",function(id)
	mw2.spawnProjectile(mw2.name.healShockwave,player(id,"x"),player(id,"y"),id,player(id,"rot"))
end)

------------
--[SUMMON]--
------------

mw2.addMagic("Summon Money","Summon",3,25,"wand_12.png",function(id)
    mw2.spawnProjectile(mw2.name.summonMoney,player(id,"x"),player(id,"y"),id,player(id,"rot"))
end)

-----------
--[EYE]--
-----------

function mw2.countProjectilesByName(name_string)
    local encounters = 0
    for _,entity in ipairs(mw2.entity) do
        if (string.find(entity.me.name,name_string) ~= nil) then
            encounters = encounters + 1
        end
    end
    return encounters
end

-- bolt
mw2.addMagic("Pure Bolt Eye","Eye",5,20,"wand_20.png",function(id)
    if (mw2.countProjectilesByName("Eye") < 5) then
        mw2.spawnProjectile(mw2.name.pureBoltEye,player(id,"x"),player(id,"y"),id,player(id,"rot"))
    end
end)

mw2.addMagic("Fire Bolt Eye","Eye",7,25,"wand_20.png",function(id)
    if (mw2.countProjectilesByName("Eye") < 5) then
    	mw2.spawnProjectile(mw2.name.fireBoltEye,player(id,"x"),player(id,"y"),id,player(id,"rot"))
    end
end)

mw2.addMagic("Frost Bolt Eye","Eye",8,25,"wand_20.png",function(id)
    if (mw2.countProjectilesByName("Eye") < 5) then
    	mw2.spawnProjectile(mw2.name.frostBoltEye,player(id,"x"),player(id,"y"),id,player(id,"rot"))
    end
end)

mw2.addMagic("Electric Bolt Eye","Eye",8,25,"wand_20.png",function(id)
    if (mw2.countProjectilesByName("Eye") < 5) then
    	mw2.spawnProjectile(mw2.name.electricBoltEye,player(id,"x"),player(id,"y"),id,player(id,"rot"))
    end
end)

mw2.addMagic("Heal Bolt Eye","Eye",3,20,"wand_20.png",function(id)
    if (mw2.countProjectilesByName("Eye") < 5) then
    	mw2.spawnProjectile(mw2.name.healBoltEye,player(id,"x"),player(id,"y"),id,player(id,"rot"))
    end
end)

-- charge
mw2.addMagic("Pure Charge Eye","Eye",8,35,"wand_20.png",function(id)
    if (mw2.countProjectilesByName("Eye") < 5) then
        mw2.spawnProjectile(mw2.name.pureChargeEye,player(id,"x"),player(id,"y"),id,player(id,"rot"))
    end
end)

mw2.addMagic("Fire Charge Eye","Eye",10,40,"wand_20.png",function(id)
    if (mw2.countProjectilesByName("Eye") < 5) then
    	mw2.spawnProjectile(mw2.name.fireChargeEye,player(id,"x"),player(id,"y"),id,player(id,"rot"))
    end
end)

mw2.addMagic("Shock Charge Eye","Eye",10,40,"wand_20.png",function(id)
    if (mw2.countProjectilesByName("Eye") < 5) then
    	mw2.spawnProjectile(mw2.name.shockChargeEye,player(id,"x"),player(id,"y"),id,player(id,"rot"))
    end
end)

mw2.addMagic("Ice Charge Eye","Eye",10,40,"wand_20.png",function(id)
    if (mw2.countProjectilesByName("Eye") < 5) then
    	mw2.spawnProjectile(mw2.name.iceChargeEye,player(id,"x"),player(id,"y"),id,player(id,"rot"))
    end
end)

mw2.addMagic("Taint Charge Eye","Eye",15,50,"wand_20.png",function(id)
    if (mw2.countProjectilesByName("Eye") < 5) then
    	mw2.spawnProjectile(mw2.name.taintChargeEye,player(id,"x"),player(id,"y"),id,player(id,"rot"))
    end
end)

-- projectiles
mw2.addMagic("Fire Ball Eye","Eye",20,50,"wand_20.png",function(id)
    if (mw2.countProjectilesByName("Eye") < 5) then
    	mw2.spawnProjectile(mw2.name.fireBallEye,player(id,"x"),player(id,"y"),id,player(id,"rot"))
    end
end)

mw2.addMagic("Shock Ball Eye","Eye",20,50,"wand_20.png",function(id)
    if (mw2.countProjectilesByName("Eye") < 5) then
    	mw2.spawnProjectile(mw2.name.shockBallEye,player(id,"x"),player(id,"y"),id,player(id,"rot"))
    end
end)

mw2.addMagic("Ice Ball Eye","Eye",20,60,"wand_20.png",function(id)
    if (mw2.countProjectilesByName("Eye") < 5) then
    	mw2.spawnProjectile(mw2.name.iceBallEye,player(id,"x"),player(id,"y"),id,player(id,"rot"))
    end
end)

mw2.addMagic("Taint Ball Eye","Eye",25,70,"wand_20.png",function(id)
    if (mw2.countProjectilesByName("Eye") < 5) then
    	mw2.spawnProjectile(mw2.name.taintBallEye,player(id,"x"),player(id,"y"),id,player(id,"rot"))
    end
end)

---------------
--[ATTRACTOR]--
---------------

mw2.addMagic("Weak Attractor Shield","Attractor",10,20,"wand_21.png",function(id)
	mw2.spawnProjectile(mw2.name.weakAttractorShield,player(id,"x"),player(id,"y"),id,0)
end)

mw2.addMagic("Medium Attractor Shield","Attractor",15,40,"wand_21.png",function(id)
	mw2.spawnProjectile(mw2.name.mediumAttractorShield,player(id,"x"),player(id,"y"),id,0)
end)

mw2.addMagic("Strong Attractor Shield","Attractor",20,80,"wand_21.png",function(id)
	mw2.spawnProjectile(mw2.name.strongAttractorShield,player(id,"x"),player(id,"y"),id,0)
end)

---------------
--[PATH WARD]--
---------------

mw2.addMagic("Weak Path Ward","Ward",10,20,"wand_8.png",function(id)
	mw2.spawnProjectile(mw2.name.WeakPathWard,player(id,"x"),player(id,"y"),id,player(id,"rot"))
end)

mw2.addMagic("Medium Path Ward","Ward",15,40,"wand_8.png",function(id)
	mw2.spawnProjectile(mw2.name.MediumPathWard,player(id,"x"),player(id,"y"),id,player(id,"rot"))
end)

mw2.addMagic("Strong Path Ward","Ward",25,60,"wand_8.png",function(id)
	mw2.spawnProjectile(mw2.name.StrongPathWard,player(id,"x"),player(id,"y"),id,player(id,"rot"))
end)

---------
--[FLY]--
---------

mw2.addMagic("Fire Fly","Flies",7,20,"wand_21.png",function(id)
	mw2.spawnProjectile(mw2.name.FireFly,player(id,"x"),player(id,"y"),id,player(id,"rot"))
end)

mw2.addMagic("Ice Fly","Flies",7,20,"wand_21.png",function(id)
	mw2.spawnProjectile(mw2.name.IceFly,player(id,"x"),player(id,"y"),id,player(id,"rot"))
end)

mw2.addMagic("Electric Fly","Flies",7,20,"wand_21.png",function(id)
	mw2.spawnProjectile(mw2.name.ElectricFly,player(id,"x"),player(id,"y"),id,player(id,"rot"))
end)

mw2.addMagic("Taint Fly","Flies",10,30,"wand_21.png",function(id)
	mw2.spawnProjectile(mw2.name.TaintFly,player(id,"x"),player(id,"y"),id,player(id,"rot"))
end)

--------------------
--[SELF AFFECTORS]--
--------------------

mw2.addMagic("Weapon Immunity","Self Affector",2,20,"wand_9.png",function(id)
	mw2.spawnProjectile(mw2.name.WeaponImmunity,player(id,"x"),player(id,"y"),id,player(id,"rot"))
end)