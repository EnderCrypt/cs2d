local classIdCounter = 0
function Class(namespace,constructor)
    namespace.__index = namespace
    namespace.new = function(...)
        local outerSelf = self -- used to allow constructors inside constructors
        -- aliases
        local this = {}
        self = this
        -- id counter
        classIdCounter = classIdCounter + 1
        this.classId = classIdCounter
        -- finish
        setmetatable(this,namespace)
        constructor(unpack(arg))
        self = outerSelf -- used to allow constructors inside constructors
        return this
    end
end

-- how to call

--[[
classname = {}
Class(classname,function(my_arguments)
    self.something = my_arguments
end) -- do not return

function classname:method()
    print(this)
end
]]--
