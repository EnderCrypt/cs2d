Point = {}
Class(Point,function(x,y)
    self.x = x
    self.y = y
    return self
end)

--[[
functions return themself by default to allow things like

variable = Point.new(10,10):reverse()
                           ^
]]--

-- add
function Point:add(x,y)
    if (y == nil) then
        y = x
    end
    self.x = self.x + x
    self.y = self.y + y
    return self
end

function Point:addP(point)
    self:add(point.x,point.y)
    return self
end

-- subtract
function Point:subtract(x,y)
    if (y == nil) then
        y = x
    end
    self:add(-x,-y)
    return self
end

function Point:subtractP(point)
    self:subtract(point.x,point.y)
    return self
end

-- multiply
function Point:multiply(x,y)
    if (y == nil) then
        y = x
    end
    self.x = self.x * x
    self.y = self.y * y
    return self
end

function Point:multiplyP(point)
    self:multiply(point.x,point.y)
    return self
end

-- divide
function Point:divide(x,y)
    if (y == nil) then
        y = x
    end
    self.x = self.x / x
    self.y = self.y / y
    return self
end

function Point:divideP(point)
    self:divide(point.x,point.y)
    return self
end

-- floor/round/ceil
function Point:floor()
    self.x = math.floor(self.x)
    self.y = math.floor(self.y)
    return self
end

function Point:round(decimals)
    self.x = misc.round(self.x,decimals)
    self.y = misc.round(self.y,decimals)
    return self
end

function Point:ceil()
    self.x = math.ceil(self.x)
    self.y = math.ceil(self.y)
    return self
end

function Point:direction(x,y)
    if (x == nil) then
        return misc.point_direction(0,0,self.x,self.y)
    else
        return misc.point_direction(self.x,self.y,x,y)
    end
end

function Point:directionP(p)
    return self:direction(p.x,p.y)
end

function Point:distance()
    if (x == nil) then
        return misc.misc.point_distance(0,0,self.x,self.y)
    else
        return misc.point_distance(self.x,self.y,x,y)
    end
end

function Point:distanceP(p)
    return self:distance(p.x,p.y)
end

-- vectors
function Point:addVector(direction,distance)
    self.x = self.x + misc.lengthdir_x(direction,distance)
    self.y = self.y + misc.lengthdir_y(direction,distance)
    return self
end

function Point:subtractVector(direction,distance)
    self:addDirDist(-direction,distance)
    return self
end

function Point:getVector()
    return self:distance(), self:direction()
end


-- extra methods
function Point:string(comment)
    if (comment == nil) then
        comment = ""
    else
        comment = " {"..comment.."}"
    end
    return "Class.Point [X="..self.x..", Y="..self.y.."]"..comment
end

function Point:reverse() -- makes negative positive, and positive negative
    self.x = -point.x
    self.y = -point.y
    return self
end

function Point:copy() -- creates a copy of the point and returns it
    return Point.new(self.x,self.y)
end

--[[
CS2D ONLY METHODS
]]--

function Point:toPixel()
    self.x = (self.x*32)+16
    self.y = (self.y*32)+16
    return self
end

function Point:toTile()
    self.x = (self.x-16)/32 -- needs rounding
    self.y = (self.y-16)/32 -- needs rounding
    return self
end