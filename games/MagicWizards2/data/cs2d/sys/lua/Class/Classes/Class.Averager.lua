Averager = {}
Class(Averager,function(size)
	self.size = size
	self:reset()
end)

function Averager:reset()
	self.full = false
	self.index = 0
	self.array = {}
end

function Averager:isFilled()
	return self.full
end

function Averager:push(value)
	self.index = self.index + 1
	if (self.index > self.size) then
		self.index = 1
		self.full = true
	end
	self.array[self.index] = value
end

function Averager:average()
	local limit = self.index
	if (self.full) then
		limit = self.size
	end
	-- calculate average
	local total = 0
	for i=1,limit do
    	total = total + self.array[i]
    end
    return total/limit
end