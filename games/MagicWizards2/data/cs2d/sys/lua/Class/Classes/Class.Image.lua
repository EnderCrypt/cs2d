Image = {}
Image.originalImageFunction = image

Image.refresh_frequency = 1

Image.Class = {}

Image.array = {}
Image.queue = {}
Image.auto_refresh_id = 1
Image.auto_refresh_timer = 0

addhook("ms100", "Image.ms100")
function Image.ms100()
	Image.auto_refresh_timer = Image.auto_refresh_timer + 0.1
	if (Image.auto_refresh_timer < Image.refresh_frequency) then
		return
	end
	Image.refresh_frequency = Image.refresh_frequency - Image.refresh_frequency
	local image = Image.queue[Image.auto_refresh_id]
	if (image == nil) then
		Image.auto_refresh_id = 1
		return
	end
	if (image.valid and image.auto_refresh) then
		image:refresh()
	end
	Image.auto_refresh_id = Image.auto_refresh_id + 1
end

--[[
Mode 0 - floor image (covered by players etc)

Mode 1 - top image (covering players)

Mode 2 - HUD image (covering everything, part of the interface, affected by mp_hudscale)

Mode 3 - super top image (covering everything on the map)

Mode 4 - background image (covering only the background)

Mode 101-132 - draw at player, covered by player (player id+100)

Mode 201-232 - draw at player, covering player (player id+200)

Mode 133-164 - draw at player, covering player and entity images (player id+132)
]]--

Class(Image.Class, function(path, x, y, mode, id)
	self.path = path
	self.position = {x = x, y = y}
	self.scale = {x = 1, y = 1}
	self.blend = {red = 0, green = 0, blue = 0, alpha = 1}
	self.mode = mode
	self.rot = nil
	self.id = id

	self.auto_refresh = false
	self.valid = true

	if (misc.fileExists(self.path) == false) then
		error("Image file "..self.path.." does not exists")
	end
	if (self.id == nil) then
		self.id = 0
	end
	self.image = Image.originalImageFunction(self.path, self.position.x, self.position.y, self.mode, self.id)

	Image.array[self.image] = self
	self.queue_index = #Image.queue + 1;
	Image.queue[self.queue_index] = self
end)

-- custom

function Image.Class:setAutoRefresh(auto_refresh)
	self.auto_refresh = auto_refresh
end

function Image.Class:isValid()
    return self.valid
end

function Image.Class:refresh()
	if (self.valid) then
		freeimage(self.image)
		self.image = Image.originalImageFunction(self.path, self.position.x, self.position.y, self.mode, self.id)
		if (self.rot ~= nil) then
			self:imagepos(self.position.x, self.position.y, self.rot)
		end
		if (self.blend.red ~= 0 or self.blend.green ~= 0 or self.blend.blue ~= 0) then
			self:imagecolor(self.blend.red, self.blend.green, self.blend.blue)
		end
		if (self.blend.alpha ~= 1) then
			self:imagealpha(self.blend.alpha)
		end
		if (self.scale.x ~= 1 or self.scale.y ~= 1) then
			self:imagescale(self.scale.x, self.scale.y)
		end
	end
end

-- default

function Image.Class:imagecolor(red, green, blue)
	if (self.valid) then
		imagecolor(self.image, red, green, blue)
		self.blend.red = red
		self.blend.green = green
		self.blend.blue = blue
	end
end

function Image.Class:imagealpha(alpha)
	if (self.valid) then
		imagealpha(self.image, alpha)
		self.blend.alpha = alpha
	end
end

function Image.Class:imageblend(mode)
	if (self.valid) then
		imageblend(self.image,mode)
	end
end

function Image.Class:imagescale(x, y)
	if (self.valid) then
		imagescale(self.image,x,y)
		self.scale.x = x
		self.scale.y = y
	end
end

function Image.Class:imagepos(x, y, rot)
	if (self.valid) then
		imagepos(self.image,x,y,rot)
		self.position.x = x
		self.position.y = y
		self.rot = rot
	end
end

function Image.Class:imageframe(frame)
	if (self.valid) then
		imageframe(self.image,frame)
	end
end

function Image.Class:imagehitzone(mode,xoffset,yoffset,width,height)
	if (self.valid) then
		imagehitzone(self.image,mode,xoffset,yoffset,width,height)
	end
end

function Image.Class:disableimagehitzone()
	if (self.valid) then
		self:imagehitzone(0,nil,nil,nil,nil)
	end
end

function Image.Class:tween_move(time, x, y, rot)
	if (self.valid) then
		tween_move(self.image,time,x,y,rot)
		self.position.x = x
		self.position.y = y
		self.rot = rot
	end
end

function Image.Class:tween_rotate(time, rot)
	if (self.valid) then
		tween_rotate(self.image,time,rot)
		self.rot = rot
	end
end

function Image.Class:tween_rotateconstantly(speed)
	if (self.valid) then
		tween_rotateconstantly(self.image,speed)
	end
end

function Image.Class:tween_alpha(time, alpha)
	if (self.valid) then
		tween_alpha(self.image,time,alpha)
		self.blend.alpha = alpha
	end
end

function Image.Class:tween_color(time, red, green, blue)
	if (self.valid) then
		tween_color(self.image, time, red, green, blue)
	end
end

function Image.Class:tween_scale(time, x, y)
	if (self.valid) then
		tween_scale(self.image, time, x, y)
		self.scale.x = x
		self.scale.y = y
	end
end

function Image.Class:freeimage()
	if (self.valid) then
		self.valid = false

		Image.array[self.image] = nil

		local last = Image.queue[#Image.queue]
		last.queue_index = self.queue_index
		Image.queue[self.queue_index] = last
		Image.queue[#Image.queue] = nil

		freeimage(self.image)
	end
end
