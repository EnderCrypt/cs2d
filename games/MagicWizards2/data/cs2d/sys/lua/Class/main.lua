dofile("sys/lua/Class/Class.lua")

function installClass(name)
    dofile("sys/lua/Class/Classes/Class."..name..".lua")
end


installClass("Image")
installClass("Point")
installClass("Averager")
