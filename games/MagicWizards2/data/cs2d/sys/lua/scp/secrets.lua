-- EXAMPLE LUA: scp.spawn(173,57,50)

-- SCP SPAWNING CODE
-- 96 = destroy a building then place a portal where it was whitin 2 seconds
-- 106 = kill a building with portals around it (left and right OR top and down)
-- 173 = kill a building while wielding a portal gun (the  building must be destroyed WHILE holding the portal gun, use delayed explosion?)

addhook("objectkill","server_objectkill")
function server_objectkill(id,player_id)
    local pl_wep = player(player_id,"weapon")
    local building_type = object(id,"type")
    local x = object(id,"tilex")
    local y = object(id,"tiley")
    if (pl_wep == 88) then -- kill a building while wielding a portal gun
        if (const.buildings[building_type].blockingAir == true) then
            scp.spawn(173,x,y)
            --return 0
        end
    end
    if not(const.buildings[building_type] == nil) then
        if (const.buildings[building_type].blockingMovement) then
            timer2(2000,{x,y,player_id},function(x,y,player_id)
                local objs = object(0,"table")
                local around_x = nil
                local around_y = nil
                local i = 0
                while (i < #objs) do
                    i = i + 1
                    local type_id = object(objs[i],"type")
                    if ((type_id == 22) or (type_id == 23)) then
                        local ox = object(objs[i],"tilex")
                        local oy = object(objs[i],"tiley")
                        -- check for portal on (previous) building position
                        if ((x == ox) and (y == oy)) then
                            scp.spawn(96,x,y)
                            --return 0
                        end
                        -- check on both sides
                        if ((around_x == nil) or (around_x == nil)) then
                            if ((math.abs(x-ox) == 1) or (math.abs(y-oy) == 1)) then
                                around_x = x+(x-ox)
                                around_y = y+(y-oy)
                            end
                        else
                            if ((ox == around_x) and (oy == around_y)) then
                                scp.spawn(106,x,y)
                                --return 0
                            end
                        end
                    end
                end
            end)
        end
    end
end
