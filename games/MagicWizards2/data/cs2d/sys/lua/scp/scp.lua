scp = {}
scp.hook = {}       -- cs2d hooks
scp.entities = {}   -- this contains instances
scp.types = {}      -- this contains types
scp.scps = {}      -- this contains each scp in order
scp.imageDir = "gfx/scp/"

function scp.create(scpID,name,onCreate,onUpdate,size,onTouch,onDestroy)
    local self = {}
    misc.addServerTransfer(scp.imageDir..scpID..".png")
    self.scpID = scpID -- SCP-ID
    self.name = name -- SCP name
    self.onCreate = onCreate -- function that gets called on create of instance
    self.onUpdate = onUpdate -- function that gets called every ms100
    self.size = size -- the creature size, set to 0 to disable collision check
    self.onTouch = onTouch -- function that gets called when a player bumbs into the scp
    self.onDestroy = onDestroy -- function that gets called when the scp instane is destroyed
    scp.types[scpID] = self
    scp.scps[#scp.scps+1] = scpID
end

function scp.spawn(scpID,x,y)
    if (scp.types[scpID] == nil) then
        error("SCP "..scpID.." does not exist!")
    end
    local self = {}
    self.scpID = scpID
    --self.onCreate = scp.types[scpID].onCreate
    --self.onUpdate = scp.types[scpID].onUpdate
    --self.onDestroy = scp.types[scpID].onDestroy
    self.x = misc.tile_to_pixel(x)
    self.y = misc.tile_to_pixel(y)
    self.image = image(scp.imageDir..scpID..".png",0,0,1)
    imagepos(self.image,self.x,self.y,(math.random()*360)-180)
    self.data = {} -- used for storing custom data
    self.destroy = false
    scp.entities[#scp.entities+1] = self
    -- create event
    scp.types[scpID].onCreate(self)
end

addhook("ms100","scp.hook.ms100")
function scp.hook.ms100()
    -- update players
    local pl = player(0,"tableliving")
    local i = 0
    while (i < #pl) do
        i = i + 1
        local id = pl[i]
        local x = player(id,"x")
        local y = player(id,"y")
        pl[i] = {}
        pl[i].id = id
        pl[i].x = x
        pl[i].y = y
        -- entity collisions
        local ii = 0
        while (ii < #scp.entities) do
            ii = ii + 1
            local entity = scp.entities[ii]
            local scpID = entity.scpID
            if (scp.types[scpID].size > 0) then
                local size_check = 16+(scp.types[scpID].size/2)
                if ((pl[i].x < entity.x+size_check) and (pl[i].y < entity.y+size_check) and (pl[i].x > entity.x-size_check) and (pl[i].y > entity.y-size_check)) then --simple square check
                    local dist = misc.point_distance(entity.x,entity.y,pl[i].x,pl[i].y)
                    if (dist <= size_check) then
                        local dir = misc.point_direction(entity.x,entity.y,pl[i].x,pl[i].y)
                        local newx = entity.x+misc.lengthdir_x(dir,size_check)
                        local newy = entity.y+misc.lengthdir_y(dir,size_check)
                        cs2d.setpos.call(id,newx,newy)
                        --parse("setpos "..id.." "..newx.." "..newy)
                        scp.types[scpID].onTouch(entity,pl,pl[i])
                    end
                end
            end
        end
    end
    -- update entities
    local i = 0
    while (i < #scp.entities) do
        i = i + 1
        local entity = scp.entities[i]
        local scpID = entity.scpID
        scp.types[scpID].onUpdate(entity,pl)
        if (entity.destroy) then
            scp.types[scpID].onDestroy(entity,pl)
            if not(scp.entities[i].image == nil) then
                freeimage(scp.entities[i].image)
            end
            scp.entities[i] = scp.entities[#scp.entities]
            scp.entities[#scp.entities] = nil
            i = i - 1
        end
    end
end

addhook("endround","scp.hook.endround")
function scp.hook.endround(mode)
    print("endround")
    local i = #scp.entities
    while (i > 0) do
        freeimage(scp.entities[i].image)
        scp.entities[i] = nil
        i = i - 1
    end
end

function scp.clear()
    local total = #scp.entities
    local i = total
    while (i > 0) do
        freeimage(scp.entities[i].image)
        scp.entities[i] = nil
        i = i - 1
    end
    print("Removed all ("..total..") SCP's")
end

function scp.kill(me,id)
    if ((mw2flags == nil) or (mw2flags.hasFlag(id,mw2flags.library.immortal) == false)) then -- MW2 FLAG integration
        if (me.scpID > 0) then
            parse("customkill 0 \"SCP-"..me.scpID.."\" "..id)
        else
            parse("customkill 0 \""..scp.types[me.scpID].name.."\" "..id)
        end
    end
end

function scp.rcon(cmd)
    scp.hook.rcon(cmd,1,nil,nil)
end

addhook("rcon","scp.hook.rcon")
function scp.hook.rcon(cmds,id,ip,port)
    local cmd = misc.commandSplit(cmds)
    if (cmd[1] == "scp") then
        if (cmd[2] == "clear") then
            scp.clear()
            return 1
        end
        if (cmd[2] == "types") then
            print2(id,"Lists of SCP types ("..#scp.scps..")")
            local i = 0
            while (i < #scp.scps) do
                i = i + 1
                local scp_data = scp.types[scp.scps[i]]
                print2(id,"SCP-"..scp_data.scpID.." "..scp_data.name.."'")
            end
            return 1
        end
        if (cmd[2] == "list") then
            print2(id,"Lists of active SCP's ("..#scp.entities..")")
            local i = 0
            while (i < #scp.entities) do
                i = i + 1
                local entity = scp.entities[i]
                local scp_data = scp.types[entity.scpID]
                print2(id,"SCP-"..entity.scpID.." '"..scp_data.name.."' ID: "..i)
                print2(id,"TILES: X = "..misc.pixel_to_tile(entity.x)..", Y = "..misc.pixel_to_tile(entity.y).." | PIXELS: X = "..entity.x..", Y = "..entity.y.."")
            end
            return 1
        end
        if (cmd[2] == "remove") then
            local scpID = tonumber(cmd[3])
            if (scpID == "all") then
                local total = #scp.entities
                local i = total
                while (i > 0) do
                    freeimage(scp.entities[i].image)
                    scp.entities[i] = nil
                    i = i - 1
                end
                print2(id,"Removed all ("..total..") SCP's")
            else
                if (scp.entities[scpID] == nil) then
                    print2(id,"Unexistant SCP: "..scpID)
                else
                    scp.entities[scpID].destroy = true
                end
            end
            return 1
        end
        if (cmd[2] == "terminate") then
            local scpID = tonumber(cmd[3])
            if (scpID == "all") then
                local total = #scp.entities
                local i = total
                while (i > 0) do
                    freeimage(scp.entities[i].image)
                    scp.entities[i] = nil
                    i = i - 1
                end
                print2(id,"Removed all ("..total..") SCP's")
            else
                if (scp.entities[scpID] == nil) then
                    print2(id,"Unexistant SCP: "..scpID)
                else
                    freeimage(scp.entities[scpID].image)
                    scp.entities[scpID] = scp.entities[#scp.entities]
                    scp.entities[#scp.entities] = nil
                end
            end
            return 1
        end
        if (cmd[2] == "create") then
            local scpID = tonumber(cmd[3])
            if (scp.types[scpID] == nil) then
                print2(id,"Unexistant SCP: "..scpID)
            else
                local x = tonumber(cmd[4])
                local y = tonumber(cmd[5])
                scp.spawn(scpID,x,y)
            end
            return 1
        end
        print2(id,"scp clear (removes all active SCP's)")
        print2(id,"scp list (shows all active SCP's)")
        print2(id,"scp types (shows all SCP types)")
        print2(id,"scp remove <scp-id> (removes a SCP, includes onDestroy event)")
        print2(id,"scp terminate <scp-id> (instantly deletes an active SCP, W/O onDestroy event)")
        print2(id,"scp create <scp-id> <x-tile> <y-tile> (creates an active scp)")
        print2(id,"scp (lists all types of scp's available)")
        return 1
    end
    return 0
end

addhook("projectile","scp.hook.projectile")
function scp.hook.projectile(id,weapon,x,y)
    local weapons = item(0,"table")
    local i = 0
    while (i < #scp.entities) do
        i = i + 1
        local entity = scp.entities[i]
        if (misc.primitive_point_distance(x,y,entity.x,entity.y) <= 100) then
            local ii = 0
            while (ii < #weapons) do
                ii = ii + 1
                local weaponID = weapons[ii]
                if (item(weaponID,"type") == 88) then -- portal gun
                    if (item(weaponID,"x") == misc.pixel_to_tile(entity.x)) and (item(weaponID,"y") == misc.pixel_to_tile(entity.y)) then
                        local scpID = entity.scpID
                        msg(misc.rgb(255,0,255)..player(id,"name").." banished a SCP-"..scpID.." from this earth")
                        parse("spawnitem 68 "..item(weaponID,"x").." "..item(weaponID,"y"))
                        scp.types[scpID].onDestroy(entity,player(0,"tableliving"))
                        if not(scp.entities[i].image == nil) then
                            freeimage(scp.entities[i].image)
                        end
                        scp.entities[i] = scp.entities[#scp.entities]
                        scp.entities[#scp.entities] = nil
                        i = i - 1
                        break
                    end
                end
            end
        end
    end
end


dofile("sys/lua/scp/scp_database.lua")
