--[[
    REQUIRES:
        MISC LIBRARY
        TIMER 2
]]--

chatbot3 = {} -- DO NOT MODIFY

chatbot3.color = {}
chatbot3.color.spect = misc.rgb(254,219,0)
chatbot3.color.t = misc.rgb(252,25,0)
chatbot3.color.ct = misc.rgb(50,146,242)
chatbot3.color.pvp = misc.rgb(0,240,0)

chatbot3.lastMessage = nil -- DO NOT MODIFY
chatbot3.data = {} -- DO NOT MODIFY
chatbot3.links = 0 -- DO NOT MODIFY
chatbot3.sentances = 0 -- DO NOT MODIFY
chatbot3.lastRespondedTo = 0 -- DO NOT MODIFY
chatbot3.responses = 0 -- DO NOT MODIFY (used to determine intelligence)
chatbot3.unknownSentances = 0 -- DO NOT MODIFY (used to determine intelligence)
chatbot3.cbColor = chatbot3.color.spect -- name of chatbot's name in the chat
chatbot3.cbName = "ChatBot" -- name of chatbot to be shown in chat
chatbot3.enabled = true -- should chatbot run in silent mode (chatbot will LEARN better, but wont respond!)
chatbot3.selfStrength = false -- should chatbot learn itself, what it says (causes words that are commonly said, to be more common)
chatbot3.conversationTimeout = 15000 -- when chatbot should declare a conversation stopped (1000 = 1 second)
chatbot3.exceptions = {"rank"} -- words that chatbot will IGNORE
chatbot3.ignoreCommandSign = "!" -- the sign commands start with (making chatbot ignore them)
chatbot3.spectChat = true -- allows spectators to chat with chatbot (be sure to make the spectators messages visible for everyone, otherwise it will just look like chatbot chats to itself)
chatbot3.myAlias = {"chatbot","chat bot","Chatbot","CHATBOT","chatbox","bot", "Admin", "admin", "IRC Admin", "irc admin", "ADMIN"}
chatbot3.trollName = true
chatbot3.trollRisk = 0.1
chatbot3.alertUSGN = {5783}
chatbot3.saveFile = "sys/lua/chatbot3/save/memory.data"

addhook("say","chatbot3.say")
function chatbot3.say(id,text)
    if (player(id,"bot")) then
        return 0
    end
	if (player(id,"team") > 0) or (chatbot3.spectChat == true) then
		if string.lower(text) == "!chatbot" then
			msg("ChatBot3 has "..chatbot3.links.." links")
			msg("and "..chatbot3.sentances.." linked sentances")
			msg(chatbot3.links+chatbot3.sentances.." lines of data in memory!")
            if ((chatbot3.responses+chatbot3.unknownSentances) > 0) then
                msg("Response rate: "..math.floor(((100/(chatbot3.responses+chatbot3.unknownSentances)*chatbot3.responses)*math.pow(10,2))+0.5)/math.pow(10,2).."% ("..chatbot3.responses.."/"..(chatbot3.responses+chatbot3.unknownSentances)..")")
			end
            msg(misc.rgb(0,255,100).."Chatbot was made by EnderCrypt (USGN: 5783)")
            
            if (chatbot3.usingTrollName) then
                msg(misc.rgb(255,0,0).."Chatbot: EXTENSION: troll name")
            end
		end
        for i=1, #chatbot3.exceptions do
            if (chatbot3.exceptions[i] == text) then
                return 0
            end
        end
        if (not(string.sub(text,1,1) == chatbot3.ignoreCommandSign)) then
            if chatbot3.enabled then
                chatbot3.sayLinkedWord(id,text)
            end
            chatbot3.indexLinkedWord(text)
        end
	end
end

addhook("minute","chatbot3.minute")
function chatbot3.minute()
	chatbot3.save()
    if ((chatbot3.responses+chatbot3.unknownSentances) > 0) then
        print("Response rate: "..math.floor(((100/(chatbot3.responses+chatbot3.unknownSentances)*chatbot3.responses)*math.pow(10,2))+0.5)/math.pow(10,2).."% ("..chatbot3.responses.."/"..(chatbot3.responses+chatbot3.unknownSentances)..")")
    end
end

function chatbot3.sayLinkedWord(id,text)
	local data = chatbot3.data[chatbot3.filterBeforeIndex(text)]
	if (data == nil) then
        chatbot3.unknownSentances = chatbot3.unknownSentances + 1
    else
        chatbot3.responses = chatbot3.responses + 1
        if (#data == 0) then
            print(misc.rgb(255,0,0).."Error: Chatbot link empty!")
            return;
        end
		local index = math.random(#data)
		delay = string.len(data[index])*200
		if chatbot3.selfStrength then
			chatbot3.indexLinkedWord(chatbot3.filterBeforeSay(id,data[index]))
		end
        chatbot3.lastRespondedTo = id
        cb_msg = chatbot3.filterBeforeSay(id,data[index])
		--timer(delay,"chatbot3.cb_say",cb_msg)
		timer2(delay,{id,cb_msg},function(id,cb_msg)
            local plid = chatbot3.getName(id)
            if (plid > 0) then
                name = player(plid,"name")
                print(misc.rgb(255,0,255).."-----{ NOTICE: Chatbot disguised as "..name)
                if (not (server_chat_add == nil)) then
                    server_chat_add("Chatbot ("..name..")",cb_msg)
                end
                if (not (cb3r == nil)) then
                    cb3r.addMessage("Chatbot ("..name..")",cb_msg)
                end
                
                --[[ EnderCrypt message]]--
                local pl = player(0,"table")
                for i=1,#pl do
                    local iid = pl[i]
                    local usgn = player(iid,"usgn")
                    for ii=1,#chatbot3.alertUSGN do
                        local usgn_check = chatbot3.alertUSGN[ii]
                        if (usgn == usgn_check) then
                            msg2(iid,misc.rgb(255,0,255).."Chatbot ("..name.."): "..cb_msg)
                        end
                    end
                end
                msg3(plid,chatbot3.getChatColor(plid)..name..chatbot3.getStatus(plid)..":"..misc.rgb(255,220,0).." "..cb_msg)
            else
                if (not (server_chat_add == nil)) then
                    server_chat_add("Chatbot",cb_msg)
                end
                if (not (cb3r == nil)) then
                    cb3r.addMessage("Chatbot",cb_msg)
                end
                msg(chatbot3.color.pvp..chatbot3.cbName..":"..misc.rgb(255,220,0).." "..cb_msg)
            end
        end)
	end
end

function chatbot3.getChatColor(id)
    if ((id == nil) or (id == 0)) then
        return chatbot3.color.pvp--"�000255000"
    else
        local team = player(id,"team")
        if (team == 0) then
            return chatbot3.color.spect
        else
            if (game("sv_gamemode") == "1") then
                return chatbot3.color.pvp
            else
                if (team == 1) then
                    return chatbot3.color.t
                else
                    return chatbot3.color.ct
                end
            end
        end
    end
end

function chatbot3.getStatus(id)
    if (id == 0) then
        return ""
    else
        if (player(id,"team") == 0) then
            return " *SPECT*"
        else
            if (player(id,"health") <= 0) then
                return " *DEAD*"
            else
                return ""
            end
        end
    end
end

function chatbot3.getName(ignore)
    if (chatbot3.trollName) then
        if (math.random() >= chatbot3.trollRisk) then
            return 0
        end
        local pl = player(0,"table")
        local pl_real = {}
        for i=1,#pl do
            local id = pl[i]
            if (not(player(id,"bot"))) then
                if (not(id == ignore)) then
                    pl_real[#pl_real+1] = id
                end
            end
        end
        if (#pl_real > 0) then
            local id = pl_real[math.random(#pl_real)]
            --return chatbot3.getChatColor(id)..player(id,"name")..chatbot3.getStatus(id)
            return id
        end
    end
    return 0
    --return chatbot3.color.pvp.."Chatbot"
end

function chatbot3.indexLinkedWord(text)
	if not (chatbot3.lastMessage == nil) then
		local index = chatbot3.filterBeforeIndex(chatbot3.lastMessage)
		if chatbot3.data[index] == nil then
			chatbot3.data[index] = {}
			chatbot3.links = chatbot3.links + 1
		end
		chatbot3.data[index][#chatbot3.data[index]+1] = text
		chatbot3.sentances = chatbot3.sentances + 1
	end
	chatbot3.lastMessage = text
	timer(chatbot3.conversationTimeout,"chatbot3.conversationEnded",chatbot3.lastMessage)
end

function chatbot3.conversationEnded(text)
	if chatbot3.lastMessage == text then
		print("ChatBot3 conversation ended!")
		chatbot3.lastMessage = nil
	end
end

function chatbot3.filterBeforeIndex(text) -- whats filtered here will be compared with links to find a match (and indexing)
	local text = string.lower(text)
	return text
end

function chatbot3.filterBeforeSay(id,text) -- whats filtered here will be said by chatbot
    local name = player(id,"name")
    text = string.gsub(text,"!","")
    text = string.gsub(text,"?","")
    text = string.gsub(text,"'","")
    local i = 0
    while (i < #chatbot3.myAlias) do
        i = i + 1
        text = string.gsub(text,chatbot3.myAlias[i],name)
    end
	return text
end

function chatbot3.save()
	local file = io.open(chatbot3.saveFile,"w")
	for index in pairs(chatbot3.data) do
		file:write("#"..index.."\n")
		for i=1,#chatbot3.data[index] do
			file:write("-"..chatbot3.data[index][i].."\n")
		end
	end
	file:close()
	print(misc.rgb(0,255,0).."Saved ChatBot3 memory!")
end

function chatbot3.load()
	local file = io.open(chatbot3.saveFile,"r")
	if not (file == nil) then
		local setTextIndex = nil
		local indata = file:read()
		while not (indata == nil) do
			text = string.sub(indata,2,string.len(indata))
			if string.sub(indata,1,1) == "#" then
				setTextIndex = chatbot3.filterBeforeIndex(text)
				chatbot3.data[setTextIndex] = {}
				chatbot3.links = chatbot3.links + 1
			else
				chatbot3.data[setTextIndex][#chatbot3.data[setTextIndex]+1] = text
				chatbot3.sentances = chatbot3.sentances + 1
			end
			indata = file:read()
		end
		file:close()
	end
	print(misc.rgb(0,255,0).."Loaded ChatBot3 memory!")
end

addhook("join","chatbot3.join")
function chatbot3.join(id)
    if (player(id,"exists")) then
        if (not (string.find(string.lower(player(id,"name")),"chatbot") == nil)) then
            parse("setname "..id.." "..math.random(0,9001))
        end
    end
end

addhook("name","chatbot3.name")
function chatbot3.name(id, oldname, newname)
    if string.find(string.lower(newname),"chatbot") then
        return 1
    end
end

chatbot3.load()
