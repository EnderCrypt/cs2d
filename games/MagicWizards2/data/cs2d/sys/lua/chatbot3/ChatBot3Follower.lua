chatbot3F = {}
chatbot3F.enabled = true
chatbot3F.imageGFX = "gfx/CB3.png"
misc.addServerTransfer(chatbot3F.imageGFX)
chatbot3F.random_offset = {x=0,y=0}
chatbot3F.targetPlayer = 0
chatbot3F.image = image(chatbot3F.imageGFX,0,0,3)
chatbot3F.tempLocation = {x=0,y=0}

function chatbot3F.moveToPlayer()
    if ((chatbot3F.targetPlayer > 0) and (player(chatbot3F.targetPlayer,"exists")) and (player(chatbot3F.targetPlayer,"health") > 0)) then
        x = player(chatbot3F.targetPlayer,"x") + chatbot3F.random_offset.x
        y = player(chatbot3F.targetPlayer,"y") + chatbot3F.random_offset.y
        chatbot3F.tempLocation = {x=x,y=y}
        if (not(chatbot3F.image == nil)) then
            tween_move(chatbot3F.image,1000,x,y)
        end
    else
        chatbot3F.targetPlayer = 0
        chatbot3F.tempLocation.x = chatbot3F.tempLocation.x + chatbot3F.random_offset.x
        chatbot3F.tempLocation.y = chatbot3F.tempLocation.y + chatbot3F.random_offset.y
        --msg("x: "..chatbot3F.tempLocation.x..", y: "..chatbot3F.tempLocation.y)
        if (not(chatbot3F.image == nil)) then
            tween_move(chatbot3F.image,1000,chatbot3F.tempLocation.x,chatbot3F.tempLocation.y)
        end
    end
    --misc.debugImage(chatbot3F.tempLocation.x,chatbot3F.tempLocation.y)
end

addhook("endround","chatbot3F.endround")
function chatbot3F.endround()
    freeimage(chatbot3F.image)
end

addhook("startround","chatbot3F.startround")
function chatbot3F.startround()
    chatbot3F.image = image(chatbot3F.imageGFX,0,0,3)
end

function chatbot3F.enable(enabled)
    chatbot3F.enabled = enabled
	if (enabled) then
		tween_alpha(chatbot3F.image,1000,1)
	else
		tween_alpha(chatbot3F.image,1000,0)
	end
end

addhook("second","chatbot3F.second")
function chatbot3F.second()
    if (chatbot3F.enabled) then
        chatbot3F.random_offset = {x=math.random(-64,64),y=math.random(-64,64)}
        chatbot3F.moveToPlayer()
    end
end

addhook("ms100","chatbot3F.ms100")
function chatbot3F.ms100()
    if (chatbot3F.enabled) then
        if (chatbot3F.targetPlayer ~= chatbot3.lastRespondedTo) then
            local temp = chatbot3.lastRespondedTo
            if ((player(temp,"exists")) and (player(temp,"health") > 0)) then
                chatbot3F.targetPlayer = temp
                chatbot3F.moveToPlayer()
            else
                chatbot3F.targetPlayer = 0
            end
        end
    end
end
