#!/usr/bin/env bash

set -e

DOCKER="docker"
if [[ -z "$(id -nG | grep docker)" ]]; then
		DOCKER="sudo docker"
fi

set -x
cd "$(dirname $0)"
for GAME in */ ; do
	cd "$GAME"
	$DOCKER build \
		-t "endercrypt/cs2d_$(cat data/docker-name.txt):1.0.1.3" \
		--build-arg "SOURCE_DIRECTORY=$GAME" \
		--build-arg "DEFAULT_SERVER_MAP=$(cat data/server-map.txt)" \
		--build-arg "DEFAULT_SERVER_NAME=$(cat data/server-name.txt)" \
		..

	cd ..
done
