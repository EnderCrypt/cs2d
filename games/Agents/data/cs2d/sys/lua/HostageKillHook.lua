HostageKill = {}
HostageKill.listeners = {}
HostageKill.playerHostageKills = {}

function HostageKill.addListener(callback) -- function(id,kills) since multiple hostages can be killed at once
	HostageKill.listeners[#HostageKill.listeners+1] = callback
end

addhook("join","HostageKill.join")
function HostageKill.join(id)
	HostageKill.playerHostageKills[id] = 0
end

addhook("attack","HostageKill.attack")
function HostageKill.attack(id)
	HostageKill.checkPlayer(id)
end

addhook("attack2","HostageKill.attack2")
function HostageKill.attack2(id,mode)
	HostageKill.checkPlayer(id)
end

addhook("projectile","HostageKill.projectile")
function HostageKill.projectile(id,weapon,x,y)
	HostageKill.checkPlayer(id)
end

function HostageKill.checkPlayer(id)
	local hostageKills = player(id,"hostagekills")
	local kills = hostageKills-HostageKill.playerHostageKills[id]
	if (kills > 0) then
		HostageKill.playerHostageKills[id] = hostageKills
		for i,callback in ipairs(HostageKill.listeners) do
			callback(id, kills)
		end
	end
end

