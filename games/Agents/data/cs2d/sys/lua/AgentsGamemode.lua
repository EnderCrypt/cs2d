AgentGamemode = {}
AgentGamemode.respawnEnemies = true
AgentGamemode.respawnImage = "gfx/botAlert.png"
AgentGamemode.respawnData = {}
AgentGamemode.damageSpeedmod = -15
AgentGamemode.botRandomPrimaryWeapons = {20,21,22,23,24,30,31,32,33,34,35,36,37,38,39,40,45,48,49,47,91,46,90}
AgentGamemode.botRandomExtra = {52,58,72,51,73,76,84,79,80}
AgentGamemode.allowedItems = {1,2,3,4,5,6,57,58,62}
AgentGamemode.saveDir = "sys/lua/AgentsSave/"
AgentGamemode.timePassed = 0
AgentGamemode.solo = false
AgentGamemode.autoRestartCounter = 0

if (fragGrenade ~= nil) then
	fragGrenade.splitInto = 0
end

serverTransfer.add(AgentGamemode.respawnImage)

parse("sv_gamemode 0")
parse("mp_hud 127")
parse("mp_damagefactor 5")
local buyString = ""
for i,v in ipairs(AgentGamemode.allowedItems) do
	buyString = buyString..v..","
end
buyString = string.sub(buyString,1,#buyString-1)
parse("mp_buymenu "..buyString)
parse("sv_maxplayers 32")
parse("sv_fow 0")
parse("sv_friendlyfire 1")
parse("mp_roundtime 3")
parse("mp_autoteambalance 0")
parse("mp_startmoney 500")
parse("mp_buytime 60")
parse("mp_weaponfadeout 60")
parse("bot_prefix \"\"")
tileFire.damageFactor = 5

--
timer2(1000,{},function()
	for i=0,27 do
		parse("bot_add_t")
	end
end)
--

addhook("join", "AgentGamemode.join")
function AgentGamemode.join(id)
	local usgn = player(id,"usgn")
	if (usgn > 0) then
		local filename = AgentGamemode.saveDir..usgn..".txt"
		SaveEngine.open(filename)
		-- initalize variables
		-- generic stats
		SaveEngine.read(filename,"GeneralStats","BestTime",math.huge)
		SaveEngine.read(filename,"GeneralStats","SoloWins",0)
		SaveEngine.read(filename,"GeneralStats","Wins",0)
		SaveEngine.read(filename,"GeneralStats","Loses",0)
		SaveEngine.read(filename,"GeneralStats","HostagesRescued",0)
		SaveEngine.read(filename,"GeneralStats","HostagesKilled",0)
		SaveEngine.read(filename,"GeneralStats","Shots",0)
		SaveEngine.read(filename,"GeneralStats","ShotsHit",0)
		-- team stats (caused by/to team)
		SaveEngine.read(filename,"TeamStats","Kills",0)
		SaveEngine.read(filename,"TeamStats","Deaths",0)
		SaveEngine.read(filename,"TeamStats","DamageCaused",0)
		SaveEngine.read(filename,"TeamStats","DamageTaken",0)
		-- enemy stats (cause by/to enemy)
		SaveEngine.read(filename,"EnemyStats","Kills",0)
		SaveEngine.read(filename,"EnemyStats","Deaths",0)
		SaveEngine.read(filename,"EnemyStats","DamageCaused",0)
		SaveEngine.read(filename,"EnemyStats","DamageTaken",0)
	end
end

function AgentGamemode.setStat(id, label, variable, value)
	if (player(id,"exists") == false) then
		error("player id "..id.." does not exist!")
	end
	local usgn = player(id,"usgn")
	if (usgn > 0) then
		local filename = AgentGamemode.saveDir..usgn..".txt"
		local sCache = SaveEngine.cache[filename]
		if (sCache == nil) then
			error("Savefile not open: "..filename)
		end
		local sLabel = sCache[label]
		if (sLabel == nil) then
			error("Unexistant label: "..label)
		end
		local sVariable = sLabel[variable]
		if (sVariable == nil) then
			error("Unexistant variable: "..variable)
		end
		sLabel[variable] = value
		SaveEngine.save(filename)
	end
end

function AgentGamemode.getStat(id, label, variable)
	if (player(id,"exists") == false) then
		error("player id "..id.." does not exist!")
	end
	local usgn = player(id,"usgn")
	if (usgn > 0) then
		local filename = AgentGamemode.saveDir..usgn..".txt"
		local sCache = SaveEngine.cache[filename]
		if (sCache == nil) then
			error("Savefile not open: "..filename)
		end
		local sLabel = sCache[label]
		if (sLabel == nil) then
			error("Unexistant label: "..label)
		end
		local sVariable = sLabel[variable]
		if (sVariable == nil) then
			error("Unexistant variable: "..variable)
		end
		return sLabel[variable]
	end
	error("cant get stat for user whitout USGN")
end

function AgentGamemode.addStat(id, label, variable, amount)
	if (amount == nil) then
		amount = 1
	end
	if (player(id,"usgn") > 0) then
		local current = AgentGamemode.getStat(id, label, variable)
		local value = current + amount
		AgentGamemode.setStat(id, label, variable, value)
	end
end

addhook("say", "AgentGamemode.say")
function AgentGamemode.say(id, text)
	local types = {}
	types["general"] = AgentGamemode.displayGeneralStatus
	types["team"] = AgentGamemode.displayTeamStatus
	types["enemy"] = AgentGamemode.displayEnemyStatus
	local command = misc.split(text," ")
	if ( (command[1] == "!status") or (command[1] == "!stats") ) then
		local statListFunction = types[command[2]]
		local target_id = nil
		if (#command >= 3) then
			target_id = tonumber(command[3])
		else
			target_id = id
		end
		if ( (target_id == nil) or (statListFunction == nil) ) then
			msg2(id, misc.rgb(255,0,0)..command[1].." (general,team,enemy) <id> ")
			return 1
		end
		local usgn = player(target_id,"usgn")
		if (usgn == 0) then
			msg2(id, misc.rgb(255,0,0).."not tracking stats for players whitout usgn ("..player(target_id,"name")..")")
			return 1
		end
		statListFunction(target_id)
		return 1
	end
end

function AgentGamemode.displayGeneralStatus(id)
	local color = misc.rgb(0,255,255)
	local filename = AgentGamemode.saveDir..player(id,"usgn")..".txt"

	msg(color.."General stats for "..player(id,"name"))

	local shotsHit = AgentGamemode.getStat(id, "GeneralStats", "ShotsHit")
	local totalShots = AgentGamemode.getStat(id, "GeneralStats", "Shots")
	msg(color.."Hit "..shotsHit.." targets with "..totalShots.." bullets ("..misc.round(100/totalShots*shotsHit,1).."%)!")

	local wins = AgentGamemode.getStat(id, "GeneralStats", "Wins")
	local loses = AgentGamemode.getStat(id, "GeneralStats", "Loses")
	local total = (wins+loses)
	local percentage = (total > 0) and (100/total*wins) or 0
	msg(color.."Played "..total.." games and won "..wins.." out of them ("..misc.round(percentage,2).."%)")

	local soloWins = AgentGamemode.getStat(id, "GeneralStats", "SoloWins")
	local percentage = (total > 0) and (100/total*soloWins) or 0
	msg(color.."And "..soloWins.." of those wins where solo wins ("..misc.round(percentage,3).."%)")

	local hostagesRescued = AgentGamemode.getStat(id, "GeneralStats", "HostagesRescued")
	local hostagesKilled = AgentGamemode.getStat(id, "GeneralStats", "HostagesKilled")
	msg(color.."Rescued "..hostagesRescued.." hostages and killed "..hostagesKilled)

	local bestTime = AgentGamemode.getStat(id, "GeneralStats", "BestTime")
	msg(color.."Fastest completion time: "..misc.round(bestTime,2).." seconds")
end

function AgentGamemode.displayTeamStatus(id)
	local color = misc.rgb(0,255,255)
	local filename = AgentGamemode.saveDir..player(id,"usgn")..".txt"

	msg(color.."Team stat for "..player(id,"name"))

	local kills = AgentGamemode.getStat(id, "TeamStats", "Kills")
	local deaths = AgentGamemode.getStat(id, "TeamStats", "Deaths")
	local percentage = (deaths > 0) and (1/deaths*kills) or 0
	msg(color.."Teamkilled "..kills.." team-members and got killed "..deaths.." times by them ("..misc.round(percentage,2).." K/D)")

	local damageCaused = AgentGamemode.getStat(id, "TeamStats", "DamageCaused")
	local DamageTaken = AgentGamemode.getStat(id, "TeamStats", "DamageTaken")
	msg(color.."Damage delivered "..misc.round((damageCaused/1000),2).."K, Damage taken "..misc.round((DamageTaken/1000),2).."K")
end

function AgentGamemode.displayEnemyStatus(id)
	local color = misc.rgb(0,255,255)
	local filename = AgentGamemode.saveDir..player(id,"usgn")..".txt"

	msg(color.."Enemy action stat for "..player(id,"name"))

	local kills = AgentGamemode.getStat(id, "EnemyStats", "Kills")
	local deaths = AgentGamemode.getStat(id, "EnemyStats", "Deaths")
	local percentage = (deaths > 0) and (1/deaths*kills) or 0
	msg(color.."Killed "..kills.." enemies and got killed "..deaths.." times by them ("..misc.round(percentage,2).." K/D)")

	local damageCaused = AgentGamemode.getStat(id, "EnemyStats", "DamageCaused")
	local DamageTaken = AgentGamemode.getStat(id, "EnemyStats", "DamageTaken")
	msg(color.."Damage delivered "..misc.round((damageCaused/1000),2).."K, Damage taken "..misc.round((DamageTaken/1000),2).."K")
end

addhook("spawn", "AgentGamemode.spawn")
function AgentGamemode.spawn(id)
	cs2d.speedmod.call(id,0)
	AgentGamemode.addStat(id, "GeneralStats", "Loses", 1) -- this will be negated if players win this round
	if (string.find(string.lower(player(id,"name")), "agent") ~= nil) then
		msg2(id,misc.rgb(0,255,0).."You are recognized as an agent!")
		cs2d.speedmod.call(id,3)
	end
	if (player(id,"bot") == true) then
		AgentGamemode.giveStuff(id)
	end
	msg2(id,misc.rgb(0,255,0).."Goal: Rescue stleast 3 captured hostages!")
	msg2(id,misc.rgb(0,255,0).."Remember: Guns, Knife, Armour only! (press B then click Handgun)")
	msg2(id,misc.rgb(255,0,0).."!!! Important: Friendly Fire is ON, you have been warned!")
end

function AgentGamemode.giveStuff(id)
	timer2(100,{id},function()
		parse("strip "..id.." 0")
		-- primary
		local index = math.ceil(math.random()*#AgentGamemode.botRandomPrimaryWeapons)
		local primary = AgentGamemode.botRandomPrimaryWeapons[index]
		parse("equip "..id.." "..primary)
		timer2(1000,{id,primary},function(id, primary)
			parse("setweapon "..id.." "..primary)
		end)
		-- extra
		local chance = 0.25
		for i,item in ipairs(AgentGamemode.botRandomExtra) do
			chance = chance * 0.5
			if (math.random() < chance) then
				parse("equip "..id.." "..item)
			end
		end
	end)
end

addhook("always", "AgentGamemode.always")
function AgentGamemode.always()
	AgentGamemode.timePassed = AgentGamemode.timePassed + (1/50)
end

addhook("second", "AgentGamemode.second")
function AgentGamemode.second()
	local ctAliveCount = #player(0,"team2living")
	local ctCount = #player(0,"team2")
	-- check for solo attempt interrupts
	if (AgentGamemode.solo) then
		if (ctAliveCount > 1) then
			msg(misc.rgb(255,0,255).."Solo attempt interrupted!")
			AgentGamemode.solo = false
		end
	end
	-- check if CT is empty and force restart should activate
	if (ctAliveCount == 0) and (ctCount > 0) then
		AgentGamemode.autoRestartCounter = AgentGamemode.autoRestartCounter + 1
		if (AgentGamemode.autoRestartCounter >= 10) then
			msg(misc.rgb(255,0,255).."Manually restarting the round")
			parse("restartround")
		end
	else
		AgentGamemode.autoRestartCounter = 0
	end
end

addhook("startround", "AgentGamemode.startround")
function AgentGamemode.startround(mode)
	local cts = player(0,"team2living")
	AgentGamemode.solo = (#cts == 1)
	if (AgentGamemode.solo) then
		msg(misc.rgb(255,0,255).."Solo attempt in progress by "..player(cts[1],"name").."!")
	end
	AgentGamemode.timePassed = 0
end

addhook("endround", "AgentGamemode.endround")
function AgentGamemode.endround(mode)
	-- change score
	local won = (mode == 31) or (mode == 2)

	print("EndRound mode: "..mode.." (win: "..tostring(won)..")")

	if (won == false) then
		msg(misc.rgb(255,0,255).."Mission failed!")
	end

	if (won) then
		for i,id in ipairs(player(0,"team2living")) do
			-- mission
			msg2(id,misc.rgb(0,255,0).."-- { MISSION COMPLETED} --")
			AgentGamemode.addStat(id, "GeneralStats", "Wins", 1)
			AgentGamemode.addStat(id, "GeneralStats", "Loses", -1)
			if (AgentGamemode.solo) then
				msg(misc.rgb(255,0,255)..player(id,"name").." Successfully completed a solo run!")
				AgentGamemode.addStat(id, "GeneralStats", "SoloWins")
			end
			-- time passed score
			if (player(id,"usgn") > 0) then
				local previousbestTime = AgentGamemode.getStat(id, "GeneralStats", "BestTime")
				if (AgentGamemode.timePassed < previousbestTime) then
					local previousTimeMessage = "never"
					if (previousbestTime ~= math.huge) then
						previousTimeMessage = misc.round(previousbestTime,2).." seconds"
					end
					msg(misc.rgb(0,255,255)..player(id,"name").." beat their previous best time ("..previousTimeMessage..") with a new record: "..misc.round(AgentGamemode.timePassed,2).." seconds")
					AgentGamemode.setStat(id, "GeneralStats", "BestTime", AgentGamemode.timePassed)
				end
			end
		end
	end
	-- remove respawn timers
	for i,data in ipairs(AgentGamemode.respawnData) do
		data.active = false
	end
	AgentGamemode.respawnData = {}
end

addhook("die", "AgentGamemode.die")
function AgentGamemode.die(victim, killer, weapon, x, y, objid)
	--respawn
	if ( (AgentGamemode.respawnEnemies == true) and (player(victim,"team") == 1) ) then
		AgentGamemode.respawnTerrorist(victim)
	end
	-- kill score
	if (weapon ~= 50) then
		if (killer == 0) then
				if (player(victim, "team") == 1) then
					--> server killed t
				else
					--> server killed ct
					AgentGamemode.addStat(victim, "EnemyStats", "Deaths")
				end
		else
			if (player(killer, "team") == 1) then
				if (player(victim, "team") == 1) then
					--> t killed t
				else
					--> t killed ct
					AgentGamemode.addStat(victim, "EnemyStats", "Deaths")
				end
			else
				if (player(victim, "team") == 1) then
					--> ct killed t
					AgentGamemode.addStat(killer, "EnemyStats", "Kills")
				else
					--> ct killed ct
					AgentGamemode.addStat(victim, "TeamStats", "Deaths")
					AgentGamemode.addStat(killer, "TeamStats", "Kills")
				end
			end
		end
	end
	-- message about ct dying
	if (player(victim,"bot") == false) then
		local total = #player(0,"team2")
		local pl = player(0,"team2living")
		local living = #pl
		if (living > 0) then
			local peopleIcon = " |"
			local peopleIndicator = const.color.green..string.rep(peopleIcon,living)..const.color.red..string.rep(peopleIcon,total-living)
			msg(misc.rgb(255,0,255)..player(victim, "name").." died! ("..living.."/"..total.." left) "..peopleIndicator)
			if (living == 1) then
				local last = pl[1]
				msg(misc.rgb(255,0,255).."Its all up to "..player(last,"name").." now")
				msg2(last,misc.rgb(255,0,0).."Your the last one alive! make this count!@C")
			end
		end
	end
end

function AgentGamemode.respawnTerrorist(id)
	local data = {}
	table.insert(AgentGamemode.respawnData, data)
	-- prepare
	data.active = true
	data.id = id
	data.time = misc.round(16+(math.random()*16))
	while (true) do
		data.x = misc.round(math.random()*const.map.xsize)
		data.y = misc.round(math.random()*const.map.ysize)
		if ( (tile(data.x,data.y,"walkable")) and (tile(data.x,data.y,"frame") > 0) ) then
			break
		end
	end
	data.px = misc.tile_to_pixel(data.x)
	data.py = misc.tile_to_pixel(data.y)
	-- 10 seconds before
	timer2((data.time-10)*1000,{data},function(data)
		if (data.active) then
			data.image = image(AgentGamemode.respawnImage,data.px,data.py,0)
			imagealpha(data.image,0.25)
			tween_alpha(data.image,(1000*10),1)
			imagecolor(data.image,255,0,0)
		end
	end)
	-- 1 second before
	timer2((data.time-1)*1000,{data},function(data)
		if (data.active) then
			data.image2 = image(AgentGamemode.respawnImage,data.px,data.py,3)
			imagealpha(data.image2,0)
			imagescale(data.image2,5,5)
			imagecolor(data.image2,255,0,0)

			tween_alpha(data.image2,250,1)
			tween_scale(data.image2,1000,0,0)
		end
	end)
	-- on respawn
	timer2((data.time-0)*1000,{data},function(data)
		if (data.active) then
			freeimage(data.image)
			freeimage(data.image2)
			parse("spawnplayer "..data.id.." "..data.px.." "..data.py)
			AgentGamemode.giveStuff(data.id)
		end
	end)
end

addhook("attack", "AgentGamemode.attack")
function AgentGamemode.attack(id)
	local weapon = player(id,"weapontype")
	if (weapon ~= 50) then
		AgentGamemode.addStat(id, "GeneralStats", "Shots")
	end
end

HostageKill.addListener(function(id,kills)
	if (player(id,"bot") == false) then
		local message = "a hostage"
		if (kills > 1) then
			message = kills.." hostages"
		end
		msg(misc.rgb(255,0,0)..player(id,"name").." killed "..message.." (attacker-id: "..id..")!")
		AgentGamemode.addStat(id, "GeneralStats", "HostagesKilled", kills)
	end
end)

addhook("hit", "AgentGamemode.hit")
function AgentGamemode.hit(id, source, weapon, hpdmg, apdmg, rawdmg, objid)
	if (player(source, "team") == 1) then
		if (player(id, "team") == 1) then
			--> t hit t
			return 1
		else
			--> t hit ct
			AgentGamemode.addStat(id, "EnemyStats", "DamageTaken", hpdmg)
		end
		--> t hit someone
	else
		if (player(id, "team") == 1) then
			--> ct hit t
			if (misc.isInsideScreen(player(id,"x"),player(id,"y"),player(source,"x"),player(source,"y")) == false) then
				return 1
			end
			if (weapon ~= 50) then
				AgentGamemode.addStat(source, "EnemyStats", "DamageCaused", hpdmg)
			end
		else
			--> ct hit ct
			local got = nil
			if (weapon == 50) then
				got = "knifed"
			else
				got = "shot"
				AgentGamemode.addStat(source, "TeamStats", "DamageCaused", hpdmg)
				AgentGamemode.addStat(id, "TeamStats", "DamageTaken", hpdmg)
			end
			msg(misc.rgb(255,0,0)..player(id,"name").." got "..got.." by "..player(source,"name").." (attacker-id: "..source..")!")
		end
		--> ct hit someone
		if (weapon ~= 50) then
			AgentGamemode.addStat(source, "GeneralStats", "ShotsHit")
		end
	end
	--> anyone got damaged
	-- speedmod damage
	local hp = player(id,"health")-hpdmg
	local maxhp = player(id,"maxhealth")
	local percentage = (1/maxhp*hp)
	local speed = (1-percentage)*AgentGamemode.damageSpeedmod
	cs2d.speedmod.call(id, speed)
end

addhook("hostagerescue", "AgentGamemode.hostagerescue")
function AgentGamemode.hostagerescue(id, x, y)
	AgentGamemode.addStat(id, "GeneralStats", "HostagesRescued")
end


addhook("team", "AgentGamemode.team")
function AgentGamemode.team(id, team, look)
	if ( (player(id,"bot") == false) and (team == 1) ) then
		msg2(id, "you are not allowed to join Terrorists")
		parse("makect "..id)
		return 1
	end
end

addhook("walkover", "AgentGamemode.walkover")
function AgentGamemode.walkover(id, iid, type, ain, a, mode)
	-- if human
	if (player(id,"bot") == false) then
		-- and item is allowed for pickup
		for i,v in ipairs(AgentGamemode.allowedItems) do
			if (v == type) then
				return 0
			end
		end
		return 1
	end
end
