-- library lua
dofile("sys/lua/MiscLibrary.lua")
dofile("sys/lua/FunctionHook.lua")
dofile("sys/lua/servertransfer.lua")
dofile("sys/lua/selectedWeapon.lua")
dofile("sys/lua/HostageKillHook.lua")
dofile("sys/lua/SaveEngineLinux.lua")
dofile("sys/lua/timer2.lua")

-- secondary lua
dofile("sys/lua/ShieldCover.lua")
dofile("sys/lua/TileFire.lua")
dofile("sys/lua/fragGrenade.lua")

-- primary lua
dofile("sys/lua/AgentsGamemode.lua")

-- finalize
serverTransfer.finalize()
misc.mp_hud(true,true,true,true,true,true,true)
