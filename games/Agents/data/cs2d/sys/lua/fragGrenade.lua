fragGrenade = {}
fragGrenade.player = {}
fragGrenade.weaponID = 51
fragGrenade.splitInto = 5
fragGrenade.splitDistance = 100

addhook("attack","fragGrenade.attack")
function fragGrenade.attack(id)
	local weapon = selectedWeapon.player[id]
	if (weapon == fragGrenade.weaponID) then
		fragGrenade.player[id] = true
	end
end

addhook("projectile","fragGrenade.projectile")
function fragGrenade.projectile(id, weapon, x, y)
	if ((weapon == fragGrenade.weaponID) and (fragGrenade.player[id] == true)) then
		fragGrenade.player[id] = false
		local anglePart = 360 / fragGrenade.splitInto
		for i=0, fragGrenade.splitInto do
			local angle = anglePart * i
			parse("spawnprojectile "..id.." "..weapon.." "..x.." "..y.." "..fragGrenade.splitDistance.." "..angle)
		end
	end
end