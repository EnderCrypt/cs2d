selectedWeapon = {}
selectedWeapon.player = {}

addhook("spawn", "selectedWeapon.spawn")
function selectedWeapon.spawn(id)
	selectedWeapon.player[id] = 0
	timer2(1,{id},function(id)
		selectedWeapon.player[id] = player(id,"weapontype")
	end)
end

addhook("die", "selectedWeapon.stop")
addhook("leave", "selectedWeapon.stop")
function selectedWeapon.stop(id)
	selectedWeapon.player[id] = nil
end

addhook("select", "selectedWeapon.select")
function selectedWeapon.select(id, weapon)
	selectedWeapon.player[id] = weapon
end