local class = {}
Class(class, function(id)
	self.id = id
	parse("setmoney "..self.id.." 8000")
	parse("equip "..self.id.." 54") -- flare
	parse("setmaxhealth "..self.id.." 100")
	parse("speedmod "..self.id.." 0")
	msg2(self.id,misc.rgb(0,255,0).."To get weapons, press [B], to use flashlight press [F]")
end)

function class:postSpawn()
	for _,id in pairs(player(0, "team1living")) do
		if (Alien.standardCheck(id)) then
			local data = Alien.players[id]
			local marker = Image.Class.new(Alien.gfx.targetMark, 0, 1, 200+self.id, id)
			marker:imagecolor(0,255,0)
			data.enemyMarkers[self.id] = marker
		end
	end
end

function class:ms100()
	
end

function class:second()
	
end

function class:kill(victim,weapon,x,y)

end

function class:attack()

end

function class:attack2()

end

function class:dispose()
	for _,id in pairs(player(0, "team1living")) do
		if (Alien.standardCheck(id)) then
			local data = Alien.players[id]
			local img = data.enemyMarkers[self.id]
			if (img ~= nil) then
				Alien.fadeDeleteImage(img)
			end
		end
	end
end

function class:serveraction(action)

end

function class:walkover(iid, itemType, ain, a, mode)
	if (itemType == 59) or (itemType == 75) then -- nightvision or snowball
		return 1
	end
	if (itemType == 78) then -- claw
		return 1
	end
	if (itemType >= 79) and (itemType <= 84) then -- any armor
		return 1
	end
	if (itemType == 65) then -- bandages
		local maxHealth = player(self.id,"maxhealth")
		local health = player(self.id,"health")
		if (maxHealth == health) then
			parse("setmaxhealth "..self.id.." "..(maxHealth+10))
			parse("removeitem "..iid)
		end
	end
end

function class:buy(weapon)
	if (weapon == 59) then
		msg2(self.id,misc.rgb(255,0,0).."You are not allowed to buy nightvision, use your flashlight (press F)")
		return 1
	end
end

function class:drop(id, iid, itemType, ain, a, mode, x, y)

end

function class:hit(source, weapon, hpdmg, apdmg, rawdmg, objId)
	if (weapon == 86) and (hpdmg > 0) then -- gut bomb
		local health = player(source,"health")+hpdmg
		parse("sethealth "..source.." "..health)
		msg2(source, misc.rgb(0,255,0).."+"..hpdmg.." HP")
	end
	-- enemy markers on alien side
	local maxhealth = player(self.id, "maxhealth")
	local health = player(self.id, "health")
	local percentage = (100 / maxhealth) * health
	local g = 255 / 100 * percentage
	local r = 255 - g
	local b = 0
	for _,id in pairs(player(0, "team1living")) do
		if (Alien.standardCheck(id)) then
			local data = Alien.players[id]
			local marker = data.enemyMarkers[self.id]
			if (marker ~= nil) then
				marker:imagecolor(r,g,b)
			end
		end
	end
end

return class