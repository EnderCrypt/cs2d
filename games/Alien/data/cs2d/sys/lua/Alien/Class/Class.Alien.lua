local class = {}
Class(class, function(id)
	self.id = id

	self.hud = {}
	self.enemyMarkers = {}

	self.minimap = nil

	parse("strip "..self.id.." 0") -- remove all
	parse("equip "..self.id.." 78") -- claw
	parse("setweapon "..self.id.." 78") -- claw
	parse("setarmor "..self.id.." 203") -- heavy armor
	parse("equip "..self.id.." 86") -- gutbomb
	parse("spawnitem 59 "..player(self.id,"tilex").." "..player(self.id,"tiley")) -- nightvision
	local x = player(self.id,"x")
	local y = player(self.id,"y")
	parse("setpos "..self.id.." 0 0")
	parse("setpos "..self.id.." "..x.." "..y) -- move back to nightvision
	parse("strip "..self.id.." 50") -- remove knife

	msg2(self.id,misc.rgb(0,255,0).."You got nightvision by pressing [N]")
	msg2(self.id,misc.rgb(0,255,0).."Gut bombs are infinite and do 16 damage and heals you when they hit survivors")
	msg2(self.id,misc.rgb(0,255,0).."Right click with claw to teleport")
	msg2(self.id,misc.rgb(0,255,0).."[F] for minimap")
end)

function class:postSpawn()
	local ctCount = #player(0,"team2living")
	local hp = math.max(1, ctCount * 25)
	parse("setmaxhealth "..self.id.." 255")
	parse("sethealth "..self.id.." "..hp)
end

function class:ms100()
	-- minimap
	if (self.minimap ~= nil) then
		self.minimap:update(100)
	end
	-- distance
	local x = player(self.id,"x")
	local y = player(self.id,"y")
	local closest = 1000 -- math.huge
	for _,id in ipairs(player(0,"team2living")) do
		local ctx = player(id,"x")
		local cty = player(id,"y")
		local dist = misc.point_distance(x,y,ctx,cty)
		if (dist < closest) then
			closest = dist
		end
	end
	-- speed
	local speed = (closest/60)*(closest/60)
	parse("speedmod "..self.id.." "..speed)
end

function class:second()

end

function class:kill(victim,weapon,x,y)

end

function class:attack()
	local hasGutBomb = false
	for _,wId in ipairs(playerweapons(self.id)) do
		if (wId == 86) then
			hasGutBomb = true
		end
	end
	if (hasGutBomb == false) then
		parse("equip "..self.id.." 86") -- gutbomb
		parse("setweapon "..self.id.." 86")
	end
end

function class:attack2()
	if (player(self.id,"weapontype") ~= 78) then -- claw
		return
	end
	local x = player(self.id, "mousemapx")
	local y = player(self.id, "mousemapy")
	local tx = misc.pixel_to_tile(x)
	local ty = misc.pixel_to_tile(y)
	local walkable = tile(tx,ty,"walkable") and tile(tx,ty,"frame") ~= 0
	if (walkable == false) then
		msg2(id,misc.rgb(255,0,0).."Cant teleport to that location!")
		return
	end
	for _, pid in ipairs(player(0,"team2living")) do
		local player_distance = misc.point_distance(x, y, player(pid, "x"), player(pid, "y"))
		if (player_distance < 32*3) then
			msg2(id,misc.rgb(255,0,0).."Thats a bit too close to "..player(pid, "name"))
			return
		end
	end
	self:teleport(x, y)
end

function class:teleport(x, y)
	local worker = LuaWorker.new(function(worker,workspace)
		local id = workspace.id
		local target = workspace.target

		local x = player(id,"x")
		local y = player(id,"y")
		local dist = misc.point_distance(x,y,target.x,target.y)
		if (dist < 1) then
			worker:delete()
			return
		end
		local dir = misc.point_direction(x,y,target.x,target.y)
		local travel = math.min(dist,32)
		x = x + misc.lengthdir_x(dir,travel)
		y = y + misc.lengthdir_y(dir,travel)
		if (cs2d == nil) then
			parse("setpos "..id.." "..x.." "..y)
		else
			cs2d.setpos.call(id,x,y)
		end
	end)
	worker.workspace.id = self.id
	worker.workspace.target = {x=x, y=y}
	worker:setFrequency(LuaWorkerData.always)
	worker:start()
end

function class:dispose()
	-- hud
	local freeimageTable = function(imageTable)
		for index,img in pairs(imageTable) do
			img:freeimage()
		end
	end

	freeimageTable(self.hud)

	freeimageTable(self.enemyMarkers)

	-- minimap
	if (self.minimap ~= nil) then
		self.minimap:dispose()
		self.minimap = nil
	end
end

function class:serveraction(action)
	if (action == 1) then
		if (self.minimap == nil) then
			self.minimap = Alien.Class.Minimap.new(self.id)
			self.minimap:setTransparancy(0.75)
		else
			self.minimap:dispose()
			self.minimap = nil
		end
	end
end

function class:walkover(iid, itemType, ain, a, mode)
	if (itemType ~= 59) and (itemType ~= 75) then -- nightvision or snowball
		return 1
	end
end

function class:buy(weapon)
	msg2(self.id,misc.rgb(255,0,0).."You are not allowed to buy items as an alien")
	return 1
end

function class:drop(id, iid, itemType, ain, a, mode, x, y)
	return 1
end

function class:hit(source, weapon, hpdmg, apdmg, rawdmg, objId)

end

return class
