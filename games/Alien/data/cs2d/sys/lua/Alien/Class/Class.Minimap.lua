local class = {}
Class(class, function(id)
	self.id = id
	self.valid = true

	self.team = player(id,"team")
	self.enemyTeam = (self.id == 1) and 2 or 1

	self.screenCenter = {x=player(self.id,"screenw")/2, y=player(self.id,"screenh")/2}
	self.map = Image.Class.new(Alien.gfx.minimap, self.screenCenter.x, self.screenCenter.y, const.image.hud, self.id)
	self.self = Image.Class.new(Alien.gfx.minimapSelfMark, self.screenCenter.x, self.screenCenter.y, const.image.hud, self.id)
	self.players = {}
	for _,id in ipairs(player(0,"team"..self.enemyTeam.."living")) do
		local data = {}
		data.image = Image.Class.new(Alien.gfx.minimapTargetMark,self.screenCenter.x,self.screenCenter.y,const.image.hud,self.id)
		
		self.players[id] = data
	end
	-- align everything
	self:update(0)
end)

function class:setTransparancy(transparancy)
	self.map:imagealpha(transparancy)
	self.self:imagealpha(transparancy)
	for id,data in pairs(self.players) do
		data.image:imagealpha(transparancy)
	end
end

function class:verifyValid()
	if (self.valid == false) then
		error("Minimap not valid!")
	end
end

function class:update(time)
	self:verifyValid()
	local selfx = player(self.id,"x")
	local selfy = player(self.id,"y")
	local x = self.screenCenter.x + ((map("xsize")+1) * (32/Alien.mapScale) / 2) - (selfx / Alien.mapScale)
	local y = self.screenCenter.y + ((map("ysize")+1) * (32/Alien.mapScale) / 2) - (selfy / Alien.mapScale)
	
	if (time <= 0) then
		self.map:imagepos(x, y, 0)
	else
		self.map:tween_move(time, x, y)
	end
	
	for id,data in pairs(self.players) do
		-- health
		local maxhealth = player(id, "maxhealth")
		local health = player(id, "health")
		if (health <= 0) then
			Alien.fadeDeleteImage(data.image)
			self.players[id] = nil
		else
			local percentage = (100 / maxhealth) * health
			local g = 255 / 100 * percentage
			local r = 255 - g
			local b = 0
			data.image:tween_color(time,r,g,b)
			-- position
			local x = player(id,"x")
			local y = player(id,"y")
			x = self.screenCenter.x + ((x-selfx) / Alien.mapScale)
			y = self.screenCenter.y + ((y-selfy) / Alien.mapScale)

			if (time <= 0) then
				data.image:imagepos(x, y, 0)
			else
				data.image:tween_move(time, x, y)
			end
		end
	end
end

function class:dispose()
	self:verifyValid()
	self.valid = false
	self.map:freeimage()
	self.self:freeimage()
	for id,data in pairs(self.players) do
		data.image:freeimage()
	end
end

return class
