Alien.teamBalance = {}
Alien.teamBalance.current = nil

addhook("spawn", "Alien.teamBalance.spawn")
function Alien.teamBalance.spawn(id)
	if (player(id,"team") == 1) then
		-- if is alien
		if (Alien.teamBalance.current == id) then
			msg(misc.rgb(255,0,0)..player(id,"name").." is the Alien")
			return
		end
		-- wrong team
		parse("makect "..id)
	end
end

function Alien.teamBalance.randomAlien(exception)
	local players = player(0,"team2")
	if (exception ~= nil) then
		for index,id in ipairs(players) do
			if (id == exception) then
				table.remove(players, index)
				break
			end
		end
	end
	Alien.teamBalance.current = nil
	if (#players > 0) then
		local index = math.ceil(#players * math.random())
		local id = players[index]
		parse("customkill 0 \"Became Alien\" "..id)
		Alien.teamBalance.current = id
		parse("maket "..id)
		msg(misc.rgb(255,0,0).."A random player became the alien! ("..player(id,"name")..")")
	end
end


addhook("leave", "Alien.teamBalance.leave")
function Alien.teamBalance.leave(id)
	if (id == Alien.teamBalance.current) then
		msg(misc.rgb(255,0,0).."The alien ("..player(victim,"name")..") left the game!")
		Alien.teamBalance.randomAlien()
	end
end

addhook("die", "Alien.teamBalance.die")
function Alien.teamBalance.die(victim,killer,weapon,x,y)
	if (victim == Alien.teamBalance.current) then
		msg(misc.rgb(255,0,0).."The alien ("..player(victim,"name")..") died!")
		parse("makect "..victim)
		--msg("killer: "..killer)
		if (killer > 0) and (killer <= 32) then
			msg(misc.rgb(255,0,0).."The alien killer ("..player(killer,"name")..") became the new alien!")
			Alien.teamBalance.current = nil

		parse("customkill 0 \"Became Alien\" "..killer)
		Alien.teamBalance.current = killer
		parse("maket "..killer)
		else
			Alien.teamBalance.randomAlien(Alien.teamBalance.current)
		end
	end
end

addhook("team", "Alien.teamBalance.team")
function Alien.teamBalance.team(id,team,look)
	--msg(player(id,"name").." old: "..player(id,"team").." new: "..team)
	if (team == 1) then
		if (Alien.teamBalance.current == nil) then
			parse("customkill 0 \"Became Alien\" "..id)
			Alien.teamBalance.current = id
			msg(misc.rgb(255,0,0)..player(id,"name").." switched to Alien")
			return 0
		end
		if (id == Alien.teamBalance.current) then
			return 0
		end
		msg2(id,misc.rgb(255,0,0).."An alien already exists, you cannot switch to be one")
		msg2(id,misc.rgb(255,0,0).."To become an alien, you have to kill the current one")
		return 1
	end
end

addhook("endround", "Alien.teamBalance.endround")
function Alien.teamBalance.endround(mode)
	if (mode == 3) then
		if (Alien.teamBalance.current ~= nil) then
			local previous = Alien.teamBalance.current
			Alien.teamBalance.current = nil
			parse("makect "..previous)
			msg(misc.rgb(255,0,0).."The alien failed to kill all survivors!")
			Alien.teamBalance.randomAlien(previous)
		end
	end
end

--[[
addhook("second", "Alien.teamBalance.second")
function Alien.teamBalance.second()
	if (#player(0,"table1") == 0) and (#player(0,"team2") > 0) then -- only ct exists
		local players = player(player(0,"table2"))
		local index = math.ceil(#player * math.random())
		parse("")
	end
end
]]--
