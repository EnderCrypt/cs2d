Alien = {}

-- directories
Alien.gfxDirectory = "gfx/Alien/"
Alien.directory = "sys/lua/Alien/"
Alien.mapDirectory = Alien.directory.."Map/"
Alien.classDirectory = Alien.directory.."Class/"

-- classes
Alien.Class = {}
Alien.Class.Alien = dofile(Alien.classDirectory.."Class.Alien.lua")
Alien.Class.Survivor = dofile(Alien.classDirectory.."Class.Survivor.lua")
Alien.Class.Minimap = dofile(Alien.classDirectory.."Class.Minimap.lua")

-- settings
parse("mp_wpndmg claw 90")
parse("mp_wpndmg \"gut bomb\" 16")
parse("mp_wpndmg \"snowball\" 0")
parse("sv_daylighttime 0")
parse("sv_gamemode 0")
parse("mp_buytime 5")
parse("mp_roundtime 10")
parse("sv_forcelight 1")
parse("sv_fow 2")
parse("mp_radar 0")
parse("sv_friendlyfire 0")
parse("mp_idleaction 2")

Alien.spawnItems = {
57, -- kevlar
65, -- bandages
61, -- primary ammo
62, -- secondary ammo
54 -- flare
}

-- gfx
Alien.gfx = {}
Alien.gfx.targetMark = "targetMark.png"
Alien.gfx.minimapSelfMark = "minimapSelfMark.png"
Alien.gfx.minimapTargetMark = "minimapTargetMark.png"

for index,file in pairs(Alien.gfx) do
	Alien.gfx[index] = Alien.gfxDirectory..file
end

-- map
Alien.mapScale = 8
Alien.gfx.minimap = "gfx/Alien/maps/"..map("name").."(X"..Alien.mapScale..").png"
if (misc.fileExists(Alien.gfx.minimap) == false) then
	error("Missing minimap image: "..Alien.gfx.minimap)
end

-- server tansfer
for name,gfx in pairs(Alien.gfx) do
	serverTransfer.add(gfx)
end

-- data
Alien.players = {}

-- functions

function Alien.fadeDeleteImage(img)
	img:tween_alpha(1000,0)
	img:tween_scale(1000,5,5)
	timer2(1000,{img}, function(img)
		img:freeimage()
	end)
end

-- hooks
addhook("spawn", "Alien.spawn")
function Alien.spawn(id)
	local team = player(id,"team")
	if (team == 1) then
		Alien.players[id] = Alien.Class.Alien.new(id)
	end
	if (team == 2) then
		Alien.players[id] = Alien.Class.Survivor.new(id)
	end
	timer2(1,{id, Alien.players[id]},Alien.postSpawn)
end

function Alien.postSpawn(id, verifyPlayer)
	if (Alien.standardCheck(id) and (Alien.players[id] == verifyPlayer)) then
		Alien.players[id]:postSpawn()
	end
end

addhook("ms100", "Alien.ms100")
function Alien.ms100()
	for _,id in ipairs(player(0,"tableliving")) do
		if (Alien.standardCheck(id)) then
			Alien.players[id]:ms100()
		end
	end
end

addhook("second", "Alien.second")
function Alien.second()
	if (math.random() < 0.5) then
		local x = math.random()*const.map.xsize
		local y = math.random()*const.map.ysize
		if (tile(x,y,"frame") ~= 0) and tile(x,y,"walkable") then
			local index = math.ceil(math.random() * #Alien.spawnItems)
			local item = Alien.spawnItems[index]
			parse("spawnitem "..item.." "..x.." "..y)
		end
	end
end

addhook("kill", "Alien.kill")
function Alien.kill(killer,victim,weapon,x,y)
	--msg("kill killer: "..killer.." victim: "..victim)
	if (Alien.standardCheck(killer)) then
		return Alien.players[killer]:kill(victim,weapon,x,y)
	end
end

addhook("die", "Alien.die")
function Alien.die(id, killer, weapon, x, y, objID)
	--msg("die killer: "..killer.." victim: "..id)
	Alien.dispose(id)
end

addhook("leave", "Alien.leave")
function Alien.leave(id, reason)
	Alien.dispose(id)
end

function Alien.dispose(id)
	local data = Alien.players[id]
	if (data ~= nil) then
		data:dispose()
		Alien.players[id] = nil
	end
end

function Alien.standardCheck(id)
	return Alien.players[id] ~= nil and player(id,"exists") and (player(id,"health") > 0)
end

addhook("attack", "Alien.attack")
function Alien.attack(id)
	if (Alien.standardCheck(id)) then
		return Alien.players[id]:attack()
	end
end

addhook("attack2", "Alien.attack2")
function Alien.attack2(id)
	if (Alien.standardCheck(id)) then
		return Alien.players[id]:attack2()
	end
end

addhook("serveraction", "Alien.serveraction")
function Alien.serveraction(id, action)
	if (Alien.standardCheck(id)) then
		return Alien.players[id]:serveraction(action)
	end
end

addhook("walkover", "Alien.walkover")
function Alien.walkover(id, iid, itemType, ain, a, mode)
	if (Alien.standardCheck(id)) then
		return Alien.players[id]:walkover(iid, itemType, ain, a, mode)
	end
end

addhook("buy", "Alien.buy")
function Alien.buy(id, weapon)
	if (Alien.standardCheck(id)) then
		return Alien.players[id]:buy(weapon)
	end
end

addhook("drop", "Alien.drop")
function Alien.drop(id, iid, itemType, ain, a, mode, x, y)
	if (Alien.standardCheck(id)) then
		return Alien.players[id]:drop(iid, itemType, ain, a, mode, x, y)
	end
end

addhook("endround", "Alien.endround")
function Alien.endround(mode)
	for _,id in ipairs(player(0,"tableliving")) do
		if (Alien.standardCheck(id)) then
			Alien.dispose(id)
		end
	end
end

addhook("hit", "Alien.hit")
function Alien.hit(id, source, weapon, hpdmg, apdmg, rawdmg, objId)
	if (Alien.standardCheck(id)) then
		return Alien.players[id]:hit(source, weapon, hpdmg, apdmg, rawdmg, objId)
	end
end
