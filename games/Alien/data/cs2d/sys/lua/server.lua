-- library lua
dofile("sys/lua/MiscLibrary.lua")
dofile("sys/lua/Class/Class.lua")
dofile("sys/lua/LuaWorker.lua")
dofile("sys/lua/timer2.lua")
dofile("sys/lua/servertransfer.lua")

-- secondary lua

-- primary lua
dofile("sys/lua/Alien/Alien.lua")
dofile("sys/lua/Alien/AlienTeambalance.lua")

-- finalize
serverTransfer.finalize()
misc.mp_hud(true,true,true,true,true,true,true)
