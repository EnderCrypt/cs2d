#!/usr/bin/env bash

set -e

cd "$(dirname $0)"

# cs2d
./cs2d/docker-build.sh

# games
./games/docker-build.sh
