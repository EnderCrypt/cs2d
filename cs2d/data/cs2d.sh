#!/usr/bin/env bash

set -e

# SETTINGS
function setting {
	KEY="$1"
	VALUE="$2"
	sed -i -E "s/^$KEY .*/$KEY $VALUE/g" ./sys/server.cfg
	if [[ -z "$(grep -P "^$KEY " ./sys/server.cfg)" ]]; then
		echo "$KEY $VALUE" >> ./sys/server.cfg
	fi
	echo "[setting] $KEY = $VALUE"
}

setting sv_name "$SERVER_NAME"
setting sv_map "$SERVER_MAP"
setting sv_hostport "$SERVER_PORT"
setting sv_maxplayers "$MAX_PLAYERS"
if [[ "$RCON_PASSWORD" == "" ]]; then
	export RCON_PASSWORD="$(cat /dev/urandom | tr -dc '[:alpha:]' | fold -w ${1:-20} | head -n 1)"
fi
setting sv_rcon "$RCON_PASSWORD"
setting mp_mapvoteratio 0

# GAME
cd /app/cs2d
./cs2d_dedicated $@
