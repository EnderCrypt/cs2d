evaluate = {}
evaluate.filename = "evaluate"
evaluate.outputFilename = "output."..evaluate.filename
evaluate.inputFilename  = "input."..evaluate.filename
evaluate.hooks = {}
evaluate.storage = nil

function fileExists(path)
   local file = io.open(path, "r")
   if file ~= nil then
		file:close()
		return true
   end
   return false
end

function trim(text)
   return text:match "^%s*(.-)%s*$"
end

function clean(text)
	return clean -- string.gsub(text, "%g%d%d%d%d%d%d%d%d%d", "")
end

addhook("ms100", "evaluate.hooks.ms100")
function evaluate.hooks.ms100()
	if fileExists(evaluate.outputFilename) then
		return
	end
	local inputFile = io.open(evaluate.inputFilename, "r")
	if (inputFile ~= nil) then
		local command = trim(inputFile:read("*all"))
		inputFile:close()

		local result = evaluate.execute(command)
		local outputFile = io.open(evaluate.outputFilename, "w")
		outputFile:write(result)
		outputFile:close()
	end
end

function evaluate.execute(command)
	print("EVALUATE: "..command)
	evaluate.storage = {}
	parse(command)
	local result = clean(table.concat(evaluate.storage, "\n"))
	evaluate.storage = nil
	return result
end

if (ah2 == nil) then
	addhook("log", "evaluate.hooks.log")
else
	ah2.original_addhook("log", "evaluate.hooks.log")
end
function evaluate.hooks.log(message)
	if evaluate.storage ~= nil then
		evaluate.storage[#evaluate.storage + 1] = message
		return 1
	end
end
