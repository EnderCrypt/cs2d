#!/usr/bin/env bash

CS2D_DIR="/app/cs2d"
CS2D_INPUT="$CS2D_DIR/input.evaluate"
CS2D_OUTPUT="$CS2D_DIR/output.evaluate"
SOURCE_DIR="$(mktemp -d)"
SOURCE_FILE="$SOURCE_DIR/input.evaluate"
echo "$@" > "$SOURCE_FILE"

mv "$SOURCE_FILE" "$CS2D_INPUT"

while [ ! -f "$CS2D_OUTPUT" ];
do
	sleep 0.1
done
cat "$CS2D_OUTPUT"
rm "$CS2D_OUTPUT"
rm "$CS2D_INPUT"
