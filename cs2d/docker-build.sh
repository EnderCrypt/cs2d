#!/usr/bin/env bash

set -e

cd "$(dirname $0)"

DOCKER="docker"
if [[ -z "$(id -nG | grep docker)" ]]; then
		DOCKER="sudo docker"
fi

set -x
$DOCKER build -t "endercrypt/cs2d:1.0.1.3" .
